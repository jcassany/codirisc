createcluster:
	k3d create cluster default -w 3

deletecluster:
	k3d delete cluster default

nodes:
	KUBECONFIG=$$(k3d get kubeconfig default) kubectl get nodes

init:
	KUBECONFIG=$$(k3d get kubeconfig default) kubectl apply -f kubernetes -n codirisc

get:
	KUBECONFIG=$$(k3d get kubeconfig default) kubectl get all -n codirisc

run:
	KUBECONFIG=$$(k3d get kubeconfig default) kubectl port-forward service/apache -n codirisc 8080:80
