<meta charset="UTF-8">
<meta content="width=device-width, initial-scale=1.0, minimum-scale=1" name="viewport">

<?php
if ($_SERVER['REQUEST_URI'] == '/') {
    $_SERVER['REQUEST_URI'] = '/index.html';
}

$actual_host = $base_href = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://" . $_SERVER['HTTP_HOST'] . '' . dirname($_SERVER['SCRIPT_NAME']);

if (substr($base_href, -1) != '/') {
    $base_href .= '/';
}

$actual_link = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://" . $_SERVER['HTTP_HOST'] . "" . $_SERVER['REQUEST_URI'] . "";

$home_canonical = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://".$_SERVER['HTTP_HOST'];
/* === Tratamiento para url's dinamicas === */
$url_db_file = $_SERVER['REQUEST_URI']; // <- Old System
if ($_GET['id'] == "") {
    $et = db_query($link,"SELECT * FROM textos t LEFT JOIN textos_content tc ON (t.id=tc.id AND tc.idioma='".IDIOMA."') WHERE t.text_nodisabled='".substr($_SERVER['PHP_SELF'], 1)."'");
    $elementt = mysqli_fetch_array($et);
    $url_db_file = "/".$elementt['textarea_titulo'];
}
elseif ($_SERVER['PHP_SELF'] == "/ficha_noticias.php") {
	$element = mysqli_fetch_array(db_query($link,"SELECT * FROM ".$table." t LEFT JOIN ".$table_content." tc ON (t.id=tc.id AND tc.idioma='".IDIOMA."') WHERE t.id='".$id."'"));
	$title = $element["text_titulo"];
	$url_db_file = "/" . url_element($element, $title, 'n', IDIOMA);
}
elseif ($_SERVER['PHP_SELF'] == "/ficha_productos.php") {
	$element = mysqli_fetch_array(db_query($link,"SELECT * FROM ".$table." t LEFT JOIN ".$table_content." tc ON (t.id=tc.id AND tc.idioma='".IDIOMA."') WHERE t.id='".$id."'"));
	$title = $element["text_titulo"];
	$url_db_file = "/" . url_element($element, $title, 'p', IDIOMA);
}
elseif ($_SERVER['PHP_SELF'] == "/listado_productos.php") {
	$element = mysqli_fetch_array(db_query($link,"SELECT * FROM categorias_publicaciones t LEFT JOIN categorias_publicaciones_content tc ON (t.id=tc.id AND tc.idioma='".IDIOMA."') WHERE t.id='".$_GET['id']."'"));
	$title = $element["text_titulo"];
	$url_db_file = "/" . url_element($element, $title, 't', IDIOMA);
}

/* === End Tratamiento para url's dinamicas === */
$home_canonical .= ($_SERVER['REQUEST_URI'] == "" || $_SERVER['REQUEST_URI'] == "/" || $_SERVER['REQUEST_URI'] == "/index.php" || $_SERVER['REQUEST_URI'] == "/index.html") ? "" : $url_db_file; //$_SERVER['PHP_SELF']
/* Home Canonical Idiomas */
/*
$home_canonical .= ($_SERVER['REQUEST_URI'] == "/home") ? "/home" : $_SERVER['REQUEST_URI'];
$home_canonical .= ($_SERVER['REQUEST_URI'] == "/index-fr") ? "/index-fr" : $_SERVER['REQUEST_URI'];
*/

if ($og_image == '') {
    $og_image = 'share.jpg';
}
?>
									
<link rel="canonical" href="<?php echo $home_canonical; //$actual_link ?>" />
<base href="<?php echo $base_href; ?>">
<link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
<link rel="manifest" href="/site.webmanifest">
<meta name="msapplication-TileColor" content="#da532c">
<meta name="theme-color" content="#ffffff">
<title><?php echo $ht_title; ?></title>
<meta name="keywords" content="<?php echo CONFIG_METAS_KW; ?>" />
<meta name="description" content="<?php echo htmlspecialchars($ht_description, ENT_QUOTES, 'UTF-8'); ?>" />

<!-- Metas Open Graph -->
<link rel="image_src" href="<?php echo $actual_host; ?>/<?php echo $og_image; ?>" />
<meta property="og:image" content="<?php echo $actual_host; ?>/<?php echo $og_image; ?>"/>
<meta property="og:image:type" content="image/jpeg" />
<meta property="og:image:width" content="200" />
<meta property="og:image:height" content="200" />
<meta property="og:url" content="<?php echo $actual_link; ?>" />
<meta property="og:title" content="<?php echo $ht_title; ?>" />
<meta property="og:description" content="<?php echo $ht_description; ?>" />

<!-- Metas Facebook -->
<?php if (SOCIAL_FACEBOOK_LINK != '') {?>
<meta property="article:publisher" content="<?php echo SOCIAL_FACEBOOK_LINK; ?>"/>
<?php }?>

<!-- Twitter Card data -->
<meta name="twitter:title" content="<?php echo $ht_title; ?>">
<meta name="twitter:description" content="<?php echo $ht_description; ?>">
<meta name="twitter:image" content="<?php echo $actual_host; ?>/<?php echo $og_image; ?>">
<?php if (SOCIAL_TWITTER_USER != '') {?>
<meta name="twitter:site" content="<?php echo SOCIAL_TWITTER_USER; ?>">
<meta name="twitter:creator" content="<?php echo SOCIAL_TWITTER_USER; ?>">
<?php }?>

<link href="css/app.css" rel="stylesheet" type="text/css" media="screen" />

<script src="js/jquery-3.1.1.min.js"></script>
<script src="js/modernizr.min.js"></script>
<script src="js/functions.js"></script> <!-- Cargamos nuestros scripts generales -->

<!--[if lt IE 9]>
<script src="js/html5.js"></script>
<![endif]-->

<?php $detect = new Mobile_Detect;?>

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','<?php echo TAG_MANAGER_CODE; ?>');</script>
<!-- End Google Tag Manager -->

<?php if ($google_recaptcha) { ?>
<script src="https://www.google.com/recaptcha/api.js" async defer></script>
<?php } ?>