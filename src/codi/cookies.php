<?php if($_COOKIE["accept_cookies"] != "yes"){ ?>
	<div class="cookies">
		<div class="cookies-msg">
			<div class="cookies-content text-center">
				<p><?php echo COMMON_POLITICA_COOKIES_TEXTE; ?>
				<a href="<?php echo $links['politica-cookies.php'];?>" title="política de cookies"><?php echo COMMON_POLITICA_COOKIES_INFO; ?></a></p>
				<span id="cookies" class="btn btn-ghost btn-sm btn-cookies" ><?php echo COMMON_POLITICA_COOKIES_ACEPTAR; ?></span>
			</div>
		</div>
	</div>
<?php } ?>
