<?php
include("head.php");
include("auto_functions.php");

//Configurar
$table = "noticias";
$table_content = $table."_content";
$sufijo_plural = "Noticias";
$sufijo_singular = "Noticia";
$campo_listar = "t.text_fecha"; //t.campo si es de la tabla 'normal' y tc.campo si es de la tabla 'content'
$orden_listar = "DESC";
$pinta_listar = "text_titulo";
$pint2_listar = "textarea_intro";
$seo_url 		 = "text_ht_url";
$seo_title 		 = "text_ht_title";
$seo_description = "notiny_ht_description";
$cols = "SHOW FULL COLUMNS FROM ".$DATABASE.".".$table;
$cols_content = "SHOW FULL COLUMNS FROM ".$DATABASE.".".$table_content;
//End configurar

$id = $_GET['id'];
$_SESSION['id'] = $id;
$e = db_query($link,"SELECT * FROM ".$table." t LEFT JOIN ".$table_content." tc ON (t.id=tc.id AND tc.idioma='".IDIOMA."') WHERE t.id='".$id."'");
$element = mysqli_fetch_array($e);

if (false) { // Show $array data
	showVPHP($element);}

/* === Redireccion en caso de que no exista en la base de datos === */
if (is_null($element)) {
	header("Location: 404.php", true, 404);
	require_once('./404.php');
} else {

	$ht_title = ($element[$seo_title] != "") ? $element[$seo_title]: html_entity_decode(strip_tags($element[$pinta_listar]), ENT_COMPAT, 'UTF-8');
	$ht_description = ($element[$seo_description] != "") ? $element[$seo_description]: html_entity_decode(strip_tags($element[$pint2_listar]), ENT_COMPAT, 'UTF-8');
	$listado_imagenes = mysqli_query($link,"SELECT * FROM fotosnoticias WHERE id_elem = '".$id."' ORDER BY orden ASC");

	if(mysqli_num_rows($listado_imagenes)>0){
		$row_image = mysqli_fetch_array($listado_imagenes);
		$og_image = 'pics_fotos'.$table.'/'.$id.'/'.$row_image['foto'].'';
	}

	?>
	<!DOCTYPE html>
	<html lang="<?php echo IDIOMA;?>">
	<head>
		<?php if ($element['checkbox_visible'] == 0) { ?>
		<meta name="robots" content="noindex">
		<?php }
		include("header.php");?>
	</head>
	<body>
		<?php include("body.php");?>

		<main>
			<div class="container">
				<div class="row justify-content-center">
					<div class="col-md-7">

						<span onClick="javascript:history.back();" class="btn btn-primary btn-sm"><?php echo COMMON_VOLVER;?></span>

						<article class="newssheet">

							<h1 class="newssheet-title text-center"><?php pinta('text_titulo');?></h1>

							<div class="newssheet-date text-center mb-25"><?php pinta('text_fecha');?></div>

							<div class="newssheet-image">
							
								<div class="attached_siglephoto w-sm-75 mx-auto"><?php pinta('file_foto');?></div>

								<div class="slick-slider" id="slider">
									<?php
									$tablafotos = 'fotosnoticias';
									$ff = db_query($link,"SELECT * FROM ".$tablafotos." WHERE id_elem='".$element['id']."' ORDER BY orden ASC");
									while ($ffoto = mysqli_fetch_array($ff)) {
										if ($ffoto['foto'] != "") {
											echo '<img src="pics_fotosnoticias/' . $element['id'] . '/big_crop_' . $ffoto['foto'] . '" alt="' . $element['text_titulo'] . '" />';
										}
									}
									?>
								</div>

								<?php if(mysqli_num_rows(db_query($link,"SELECT * FROM ".$tablafotos." WHERE id_elem='".$element['id']."' ORDER BY orden ASC"))>1){ ?>
								<div class="slick-slider mb-50" id="slider-nav">
									<?php
									$tablafotos = 'fotosnoticias';
									$ff = db_query($link,"SELECT * FROM ".$tablafotos." WHERE id_elem='".$element['id']."' ORDER BY orden ASC");
									while ($ffoto = mysqli_fetch_array($ff)) {
										if ($ffoto['foto'] != "") {
											echo '<img src="pics_fotosnoticias/' . $element['id'] . '/th_crop_' . $ffoto['foto'] . '" alt="' . $element['text_titulo'] . '" class="item-nav" />';
										}
									}
									?>
								</div>
								<?php } ?>
							</div>

							<div class="newssheet-content my-30">
								<div class="fileblock attached_file"><?php pinta('file_fichero');?></div>

								<div class="bodysheet"><?php pinta('textarea_texto');?></div>

								<?php
									$fotos = db_query($link,"SELECT * FROM ficherosnoticias WHERE id_elem=".$_GET['id']." AND idioma='".IDIOMA."' ORDER BY orden ASC");
									if(mysqli_num_rows($fotos) > 0){ ?>
										<div class="fileblock fileuploader"><?php pinta('fileuploader_ficherosnoticias', IDIOMA);?></div>
									<?php }
								?>

							</div>

						</article>
					</div>
				</div>
				<div class="row justify-content-center">
					<div class="col-md-7">
						<?php if(isset($element['notiny_video']) && $element['notiny_video'] != ''){ ?>
							<div class="videoblock attached_video">
								<div class="embed-responsive embed-responsive-16by9">
									<?php pinta('notiny_video'); ?>
								</div>
							</div>
						<?php } ?>
					</div>
				</div>
			</div>
		</main>


		<?php
			$limite = " LIMIT ".CONFIG_LATERAL."";
			$listar = "SELECT * FROM ".$table." t LEFT JOIN ".$table_content." tc ON (t.id=tc.id AND tc.idioma='".IDIOMA."') WHERE t.id != '".$id."' AND t.checkbox_visible='1' ORDER BY ".$campo_listar." ".$orden_listar." ".$limite;

			$l = db_query($link,$listar);
			if (mysqli_num_rows($l) > 0) {
			?>

				<aside class="block3">
					<div class="container">
						<h2 class="text-center bottomLine mt-0"><?php echo NOTICIAS_RELACIONADAS_H2; ?></h2>

							<div class="newsList row pt-50 justify-content-center">
							<?php
							while($list = mysqli_fetch_array($l)){
								$title = $list[$pinta_listar];
								$url = url_element($list, $title, 'n', IDIOMA);
								?>

								<article class="newsList__item col-12 col-md-4">
									<div class="inner-item">
										<div class="thumb-wrapper">
											<div class="thumb img-frame">
												<a href="<?php echo $url; ?>" title="<?php echo $title; ?>">
												<?php
												//Foto
												$foto_default = true;
												$r = db_query($link,"SELECT * FROM ".$table." WHERE id=".$list['id']);
												$registro = mysqli_fetch_array($r);
												if($registro['file_foto'] != ""){
													echo '<img src="images/'.$table.'/th_crop_'.$registro['file_foto'].'" alt="'.$title.'" class="img-responsive center-block" />';
													$foto_default = false;
												} else{
													//O foto de uploader, averiguo si hay campo de fileuploader
													$list_cols = db_query($link,$cols);
													while($lista = mysqli_fetch_array($list_cols)){
														$field = $lista[0];
														if(substr($field,0,12) == "fotouploader"){
															$tablafotos = substr($field,13,strlen($field)-13);
															$ff = db_query($link,"SELECT * FROM ".$tablafotos." WHERE id_elem=".$list['id']." AND orden=0 LIMIT 1");
															$ffoto = mysqli_fetch_array($ff);
															if($ffoto['foto'] != ""){
																echo '<img src="pics_fotos'.$table.'/'.$list['id'].'/th_crop_'.$ffoto['foto'].'" alt="'.$title.'" class="img-responsive center-block" />';
																$foto_default = false;
															}
														}
													}
												}
												if ($foto_default) {
													echo '<img src="images/no-foto.jpg" alt="'.$title.'" />';
												}
												?>
												</a>
											</div>
										</div>

										<div class="content">
											<div class="text-wrapper">
												<h3 class="title"><a href="<?php echo $url; ?>" title="<?php echo $title; ?>"><?php echo $title; ?></a></h3>
												<div class="text"><?php echo $list[$pint2_listar]; ?></div>
											</div>
											<div class="date"><?php echo fechaEspExt($list['text_fecha']); ?></div>
										</div>
									</div>
								</article>
							<?php }	?>
							</div>
						</div>
				</aside>
			<?php } ?>

		<?php include("footer.php");?>

		<script src="js/slick.min.js"></script>
		<script>
			$(function(){
				$('#slider').slick({
					slidesToShow: 1,
					slidesToScroll: 1,
					arrows: false,
					dots: false
				});
				$('#slider-nav').slick({
					slide: '.item-nav',
					asNavFor: '#slider',
					slidesToShow: 6,
					slidesToScroll: 1,
					arrows: false,
					dots: false,
					focusOnSelect: true
				});
			});
		</script>
	</body>
	</html>
	<?php include("bottom.php");
}
?>