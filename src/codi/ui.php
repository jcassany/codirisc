<?php
include("head.php");

$ht_title = "UI";
$ht_description = "UI";

?>
<!DOCTYPE html>
<html lang="<?php echo IDIOMA;?>">

<head><?php include("header.php");?></head>

<body>

	<!-- HEADER / CABECERA -->
    <!-- ---------------------------------------------- -->
    <header>
        <div class="container">
            <div id="logo"><a href="/" title=""><img src="images/logo.svg" alt=""></a></div>
        </div>
    
        <div class="mainMenu">
            <div class="container">
                <div class="langMenu langMenu--select">
                    <span href="#" class="current">ES</span>
                    <a href="#">CA</a>
                    <a href="#">EN</a>
                </div>
            
                <nav class="mainMenu__nav">
                    <ul class="first-level">
                        <li class="active"><a href="#" title="">Home</a></li>
                        <li><a href="#" title="">Empresa</a></li>
                        <li><a href="#" title="">Servicios</a></li>
                        <li><span class="toggle">Productos</span>
                            <ul class="second-level">
                                <li><a href="#" title="">Productos 1</a></li>
                                <li><a href="#" title="">Productos 2</a>
                                    <i class="toggle-icon"></i>
                                    <ul class="second-level">
                                        <li><a href="#" title="">Productos 1</a></li>
                                        <li><a href="#" title="">Productos 2</a></li>
                                        <li><a href="#" title="">Productos 3</a></li>
                                    </ul>
                                </li>
                                <li><a href="#" title="">Productos 3</a></li>
                            </ul>
                        </li>
                        <li><a href="#" title="">Noticias</a></li>
                        <li><a href="#" title="">Contacto</a></li>
                    </ul>
                </nav>
    
                <div class="socialMenu">
                    <a href="#" class="social_facebook"></a>
                    <a href="#" class="social_twitter"></a>
                    <a href="#" class="social_instagram"></a>
                </div>
            </div>
        </div>
    
        <div class="menuBurger"><span class="menuBurger__icon"></span></div>
    </header>

    <div class="generalContent">
        <!-- SLIDER / CAROUSEL / SLICKSLIDER -->
        <!-- ---------------------------------------------- -->
        <div class="slickSlider slickSlider--home js-home-slider">
            <div class="slickSlider__item">
                <div class="img-frame"><img src="https://dummyimage.com/2000x500" alt="" /></div>
                <a href="">
                    <div class="claim-container container">
                        <div class="claim w-md-50">
                            <h1 class="title h-display">Claim title</h1>
                            <p>Claim text. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sit cum modi quia laboriosam eveniet nostrum ad harum aperiam.</p>
                            <span class="btn btn-ghost">Claim btn</span>
                        </div>
                    </div>
                </a>
            </div>
            <div class="slickSlider__item">
                <div class="img-frame"><img src="https://dummyimage.com/2000x500" alt="" /></div>
                <div class="claim-container container">
                    <div class="claim w-md-50">
                        <h1 class="title h-display">Claim title</h1>
                        <p>Claim text. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sit cum modi quia laboriosam eveniet nostrum ad harum aperiam.</p>
                        <a href="#" class="btn btn-primary">Claim btn</a>
                    </div>
                </div>
            </div>
            <div class="slickSlider__item">
                <div class="img-frame"><img src="https://dummyimage.com/2000x500" alt="" /></div>
                <div class="claim-container container">
                    <div class="claim w-md-50">
                        <h1 class="title h-display">Claim title</h1>
                        <p>Claim text. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sit cum modi quia laboriosam eveniet nostrum ad harum aperiam.</p>
                        <a href="#" class="btn btn-primary">Claim btn</a>
                    </div>
                </div>
            </div>
        </div>


        <!-- TYPE / FONTS / TIPOS / TIPOGRAFIA -->
        <!-- ---------------------------------------------- -->
        <section class="">
            <div class="container">

                <h2 class="mt-0 text-center">TEXT styles</h2>

                <hr>

                <div class="row">
                    <div class="col-md-6">
                        <small>class="h-display"</small>
                        <h1 class="mt-0 h-display">The spectacle before us was indeed sublime.</h1>

                        <small>h1 or class="h1"</small>
                        <h1 class="mt-0">The spectacle before us was indeed sublime.</h1>

                        <small class="mt-15 mt-md-50 d-block">h2 or class="h2"</small>
                        <h2 class="mt-0">The spectacle before us was indeed sublime.</h2>

                        <small class="mt-15 mt-md-50 d-block">h3 or class="h3"</small>
                        <h3 class="mt-0">The spectacle before us was indeed sublime.  us was indeed sublime.</h3>

                        <small class="mt-15 mt-md-50 d-block">h4 or class="h4"</small>
                        <h4 class="mt-0">The spectacle before us was indeed sublime.</h4>

                        <div class="row mb-50">
                            <div class="col"><small class="d-block">class="btn btn-primary"</small><a href="#" class="btn btn-primary">Primary</a></div>
                            <div class="col"><small class="d-block">class="btn btn-secondary"</small><a href="#" class="btn btn-secondary">Secondary</a></div>
                        </div>

                        <div class="row mb-50">
                            <div class="col"><small class="d-block">class="btn btn-ghost"</small><a href="#" class="btn btn-ghost">Ghost</a></div>
                            <div class="col"><small class="d-block">class="btn btn-link"</small><a href="#" class="btn btn-link">Link</a></div>
                        </div>

                        <div class="row mb-50">
                            <div class="col"><small class="d-block">class="btn-lg"</small><a href="#" class="btn btn-primary btn-lg">Primary</a></div>
                            <div class="col"><small class="d-block">class="btn-sm"</small><a href="#" class="btn btn-primary btn-sm">Primary</a></div>
                        </div>
                        <div class="row mb-50">
                            <div class="col-md-6"><small class="d-block">class="btn-block"</small><a href="#" class="btn btn-primary btn-block">Primary</a></div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <small class="d-block">paragraph</small>

                        <h1 class="mt-0">The spectacle before us was indeed sublime.</h1>

                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam dui enim, blandit in volutpat in, scelerisque eleifend nibh. Sed in lacus magna. Nullam placerat ultrices aliquet. Aenean ornare arcu non tortor tincidunt, vel placerat velit dignissim. Mauris dignissim nulla nec lacus pellentesque tincidunt.</p>

                        <h2>The spectacle before us was indeed sublime.</h2>

                        <p>Integer ut ipsum non enim malesuada porta ut sit amet magna. <a href="">Praesent tincidunt gravida nisl</a> a tincidunt. Vestibulum massa diam, eleifend quis lacus vel, ullamcorper laoreet ex. Etiam rutrum orci eu porta egestas. Cras feugiat rhoncus justo non feugiat.</p>

                        <ul>
                            <li>List 1
                                <ul>
                                    <li>List 1</li>
                                    <li>List 1</li>
                                    <li>List 1</li>
                                </ul>
                            </li>
                            <li>List 1</li>
                            <li>List 1</li>
                            <li>List 1</li>
                        </ul>

                        <h3>The spectacle before us was indeed sublime.  us was indeed sublime.</h3>

                        <p>Fusce vitae tincidunt ipsum, at placerat lectus. Phasellus et nulla vitae diam dapibus mollis bibendum non nunc. <strong>Vivamus posuere vulputate commodo.</strong> Maecenas accumsan justo in varius tempus. Nullam sit amet luctus odio. Cras placerat quam quis libero bibendum, <em>eget fermentum risus auctor. Curabitur ut aliquam ex, a consequat mauris.</em></p>

                        <h4>The spectacle before us was indeed sublime.</h4>

                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam dui enim, blandit in volutpat in, scelerisque eleifend nibh. Sed in lacus magna.</p>
                        
                        <ol>
                            <li>List 1</li>
                            <li>List 1
                                <ol>
                                    <li>List 1</li>
                                    <li>List 1</li>
                                </ol>
                            </li>
                            <li>List 1</li>
                            <li>List 1</li>
                        </ol>

                        <p>Nullam placerat ultrices aliquet. Aenean ornare arcu non tortor tincidunt, vel placerat velit dignissim. Mauris dignissim nulla nec lacus pellentesque tincidunt.</p>
    
                        <small class="d-block">blockquote class="blockquote"</small>

                        <blockquote class="blockquote">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante. Nam dui enim.</blockquote>

                        <small class="d-block">label</small>

                        <label>Lorem ipsum dolor sit</label>
                    </div>
                </div>
            </div>
        </section>


        <!-- BLOCK1 -->
        <!-- ---------------------------------------------- -->
        <section class="block1">
            <div class="container">

                <h2 class="mt-0 text-center">TEXT styles</h2>

                <hr>

                <div class="row">
                    <div class="col-md-6">
                        <small>class="h-display"</small>
                        <h1 class="mt-0 h-display">The spectacle before us was indeed sublime.</h1>

                        <small>h1 or class="h1"</small>
                        <h1 class="mt-0">The spectacle before us was indeed sublime.</h1>

                        <small class="mt-15 mt-md-50 d-block">h2 or class="h2"</small>
                        <h2 class="mt-0">The spectacle before us was indeed sublime.</h2>

                        <small class="mt-15 mt-md-50 d-block">h3 or class="h3"</small>
                        <h3 class="mt-0">The spectacle before us was indeed sublime.  us was indeed sublime.</h3>

                        <small class="mt-15 mt-md-50 d-block">h4 or class="h4"</small>
                        <h4 class="mt-0">The spectacle before us was indeed sublime.</h4>

                        <div class="row mb-50">
                            <div class="col"><small class="d-block">class="btn btn-primary"</small><a href="#" class="btn btn-primary">Primary</a></div>
                            <div class="col"><small class="d-block">class="btn btn-secondary"</small><a href="#" class="btn btn-secondary">Secondary</a></div>
                        </div>

                        <div class="row mb-50">
                            <div class="col"><small class="d-block">class="btn btn-ghost"</small><a href="#" class="btn btn-ghost">Ghost</a></div>
                            <div class="col"><small class="d-block">class="btn btn-link"</small><a href="#" class="btn btn-link">Link</a></div>
                        </div>

                        <div class="row mb-50">
                            <div class="col"><small class="d-block">class="btn-lg"</small><a href="#" class="btn btn-primary btn-lg">Primary</a></div>
                            <div class="col"><small class="d-block">class="btn-sm"</small><a href="#" class="btn btn-primary btn-sm">Primary</a></div>
                        </div>
                        <div class="row mb-50">
                            <div class="col-md-6"><small class="d-block">class="btn-block"</small><a href="#" class="btn btn-primary btn-block">Primary</a></div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <small class="d-block">paragraph</small>

                        <h1 class="mt-0">The spectacle before us was indeed sublime.</h1>

                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam dui enim, blandit in volutpat in, scelerisque eleifend nibh. Sed in lacus magna. Nullam placerat ultrices aliquet. Aenean ornare arcu non tortor tincidunt, vel placerat velit dignissim. Mauris dignissim nulla nec lacus pellentesque tincidunt.</p>

                        <h2>The spectacle before us was indeed sublime.</h2>

                        <p>Integer ut ipsum non enim malesuada porta ut sit amet magna. <a href="">Praesent tincidunt gravida nisl</a> a tincidunt. Vestibulum massa diam, eleifend quis lacus vel, ullamcorper laoreet ex. Etiam rutrum orci eu porta egestas. Cras feugiat rhoncus justo non feugiat.</p>

                        <ul>
                            <li>List 1
                                <ul>
                                    <li>List 1</li>
                                    <li>List 1</li>
                                    <li>List 1</li>
                                </ul>
                            </li>
                            <li>List 1</li>
                            <li>List 1</li>
                            <li>List 1</li>
                        </ul>

                        <h3>The spectacle before us was indeed sublime.  us was indeed sublime.</h3>

                        <p>Fusce vitae tincidunt ipsum, at placerat lectus. Phasellus et nulla vitae diam dapibus mollis bibendum non nunc. <strong>Vivamus posuere vulputate commodo.</strong> Maecenas accumsan justo in varius tempus. Nullam sit amet luctus odio. Cras placerat quam quis libero bibendum, <em>eget fermentum risus auctor. Curabitur ut aliquam ex, a consequat mauris.</em></p>

                        <h4>The spectacle before us was indeed sublime.</h4>

                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam dui enim, blandit in volutpat in, scelerisque eleifend nibh. Sed in lacus magna.</p>
                        
                        <ol>
                            <li>List 1</li>
                            <li>List 1
                                <ol>
                                    <li>List 1</li>
                                    <li>List 1</li>
                                </ol>
                            </li>
                            <li>List 1</li>
                            <li>List 1</li>
                        </ol>

                        <p>Nullam placerat ultrices aliquet. Aenean ornare arcu non tortor tincidunt, vel placerat velit dignissim. Mauris dignissim nulla nec lacus pellentesque tincidunt.</p>
    
                        <small class="d-block">blockquote class="blockquote"</small>

                        <blockquote class="blockquote">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante. Nam dui enim.</blockquote>

                        <small class="d-block">label</small>

                        <label>Lorem ipsum dolor sit</label>
                    </div>
                </div>
            </div>
        </section>

        <!-- BLOCK2 -->
        <!-- ---------------------------------------------- -->
        <section class="block2">
            <div class="container">

                <h2 class="mt-0 text-center">TEXT styles</h2>

                <hr>

                <div class="row">
                    <div class="col-md-6">
                        <small>class="h-display"</small>
                        <h1 class="mt-0 h-display">The spectacle before us was indeed sublime.</h1>

                        <small>h1 or class="h1"</small>
                        <h1 class="mt-0">The spectacle before us was indeed sublime.</h1>

                        <small class="mt-15 mt-md-50 d-block">h2 or class="h2"</small>
                        <h2 class="mt-0">The spectacle before us was indeed sublime.</h2>

                        <small class="mt-15 mt-md-50 d-block">h3 or class="h3"</small>
                        <h3 class="mt-0">The spectacle before us was indeed sublime.  us was indeed sublime.</h3>

                        <small class="mt-15 mt-md-50 d-block">h4 or class="h4"</small>
                        <h4 class="mt-0">The spectacle before us was indeed sublime.</h4>

                        <div class="row mb-50">
                            <div class="col"><small class="d-block">class="btn btn-primary"</small><a href="#" class="btn btn-primary">Primary</a></div>
                            <div class="col"><small class="d-block">class="btn btn-secondary"</small><a href="#" class="btn btn-secondary">Secondary</a></div>
                        </div>

                        <div class="row mb-50">
                            <div class="col"><small class="d-block">class="btn btn-ghost"</small><a href="#" class="btn btn-ghost">Ghost</a></div>
                            <div class="col"><small class="d-block">class="btn btn-link"</small><a href="#" class="btn btn-link">Link</a></div>
                        </div>

                        <div class="row mb-50">
                            <div class="col"><small class="d-block">class="btn-lg"</small><a href="#" class="btn btn-primary btn-lg">Primary</a></div>
                            <div class="col"><small class="d-block">class="btn-sm"</small><a href="#" class="btn btn-primary btn-sm">Primary</a></div>
                        </div>
                        <div class="row mb-50">
                            <div class="col-md-6"><small class="d-block">class="btn-block"</small><a href="#" class="btn btn-primary btn-block">Primary</a></div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <small class="d-block">paragraph</small>

                        <h1 class="mt-0">The spectacle before us was indeed sublime.</h1>

                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam dui enim, blandit in volutpat in, scelerisque eleifend nibh. Sed in lacus magna. Nullam placerat ultrices aliquet. Aenean ornare arcu non tortor tincidunt, vel placerat velit dignissim. Mauris dignissim nulla nec lacus pellentesque tincidunt.</p>

                        <h2>The spectacle before us was indeed sublime.</h2>

                        <p>Integer ut ipsum non enim malesuada porta ut sit amet magna. <a href="">Praesent tincidunt gravida nisl</a> a tincidunt. Vestibulum massa diam, eleifend quis lacus vel, ullamcorper laoreet ex. Etiam rutrum orci eu porta egestas. Cras feugiat rhoncus justo non feugiat.</p>

                        <ul>
                            <li>List 1
                                <ul>
                                    <li>List 1</li>
                                    <li>List 1</li>
                                    <li>List 1</li>
                                </ul>
                            </li>
                            <li>List 1</li>
                            <li>List 1</li>
                            <li>List 1</li>
                        </ul>

                        <h3>The spectacle before us was indeed sublime.  us was indeed sublime.</h3>

                        <p>Fusce vitae tincidunt ipsum, at placerat lectus. Phasellus et nulla vitae diam dapibus mollis bibendum non nunc. <strong>Vivamus posuere vulputate commodo.</strong> Maecenas accumsan justo in varius tempus. Nullam sit amet luctus odio. Cras placerat quam quis libero bibendum, <em>eget fermentum risus auctor. Curabitur ut aliquam ex, a consequat mauris.</em></p>

                        <h4>The spectacle before us was indeed sublime.</h4>

                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam dui enim, blandit in volutpat in, scelerisque eleifend nibh. Sed in lacus magna.</p>
                        
                        <ol>
                            <li>List 1</li>
                            <li>List 1
                                <ol>
                                    <li>List 1</li>
                                    <li>List 1</li>
                                </ol>
                            </li>
                            <li>List 1</li>
                            <li>List 1</li>
                        </ol>

                        <p>Nullam placerat ultrices aliquet. Aenean ornare arcu non tortor tincidunt, vel placerat velit dignissim. Mauris dignissim nulla nec lacus pellentesque tincidunt.</p>
    
                        <small class="d-block">blockquote class="blockquote"</small>

                        <blockquote class="blockquote">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante. Nam dui enim.</blockquote>

                        <small class="d-block">label</small>

                        <label>Lorem ipsum dolor sit</label>
                    </div>
                </div>
            </div>
        </section>

        <!-- BLOCK3 -->
        <!-- ---------------------------------------------- -->
        <section class="block3">
            <div class="container">

                <h2 class="mt-0 text-center">TEXT styles</h2>

                <hr>

                <div class="row">
                    <div class="col-md-6">
                        <small>class="h-display"</small>
                        <h1 class="mt-0 h-display">The spectacle before us was indeed sublime.</h1>

                        <small>h1 or class="h1"</small>
                        <h1 class="mt-0">The spectacle before us was indeed sublime.</h1>

                        <small class="mt-15 mt-md-50 d-block">h2 or class="h2"</small>
                        <h2 class="mt-0">The spectacle before us was indeed sublime.</h2>

                        <small class="mt-15 mt-md-50 d-block">h3 or class="h3"</small>
                        <h3 class="mt-0">The spectacle before us was indeed sublime.  us was indeed sublime.</h3>

                        <small class="mt-15 mt-md-50 d-block">h4 or class="h4"</small>
                        <h4 class="mt-0">The spectacle before us was indeed sublime.</h4>

                        <div class="row mb-50">
                            <div class="col"><small class="d-block">class="btn btn-primary"</small><a href="#" class="btn btn-primary">Primary</a></div>
                            <div class="col"><small class="d-block">class="btn btn-secondary"</small><a href="#" class="btn btn-secondary">Secondary</a></div>
                        </div>

                        <div class="row mb-50">
                            <div class="col"><small class="d-block">class="btn btn-ghost"</small><a href="#" class="btn btn-ghost">Ghost</a></div>
                            <div class="col"><small class="d-block">class="btn btn-link"</small><a href="#" class="btn btn-link">Link</a></div>
                        </div>

                        <div class="row mb-50">
                            <div class="col"><small class="d-block">class="btn-lg"</small><a href="#" class="btn btn-primary btn-lg">Primary</a></div>
                            <div class="col"><small class="d-block">class="btn-sm"</small><a href="#" class="btn btn-primary btn-sm">Primary</a></div>
                        </div>
                        <div class="row mb-50">
                            <div class="col-md-6"><small class="d-block">class="btn-block"</small><a href="#" class="btn btn-primary btn-block">Primary</a></div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <small class="d-block">paragraph</small>

                        <h1 class="mt-0">The spectacle before us was indeed sublime.</h1>

                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam dui enim, blandit in volutpat in, scelerisque eleifend nibh. Sed in lacus magna. Nullam placerat ultrices aliquet. Aenean ornare arcu non tortor tincidunt, vel placerat velit dignissim. Mauris dignissim nulla nec lacus pellentesque tincidunt.</p>

                        <h2>The spectacle before us was indeed sublime.</h2>

                        <p>Integer ut ipsum non enim malesuada porta ut sit amet magna. <a href="">Praesent tincidunt gravida nisl</a> a tincidunt. Vestibulum massa diam, eleifend quis lacus vel, ullamcorper laoreet ex. Etiam rutrum orci eu porta egestas. Cras feugiat rhoncus justo non feugiat.</p>

                        <ul>
                            <li>List 1
                                <ul>
                                    <li>List 1</li>
                                    <li>List 1</li>
                                    <li>List 1</li>
                                </ul>
                            </li>
                            <li>List 1</li>
                            <li>List 1</li>
                            <li>List 1</li>
                        </ul>

                        <h3>The spectacle before us was indeed sublime.  us was indeed sublime.</h3>

                        <p>Fusce vitae tincidunt ipsum, at placerat lectus. Phasellus et nulla vitae diam dapibus mollis bibendum non nunc. <strong>Vivamus posuere vulputate commodo.</strong> Maecenas accumsan justo in varius tempus. Nullam sit amet luctus odio. Cras placerat quam quis libero bibendum, <em>eget fermentum risus auctor. Curabitur ut aliquam ex, a consequat mauris.</em></p>

                        <h4>The spectacle before us was indeed sublime.</h4>

                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam dui enim, blandit in volutpat in, scelerisque eleifend nibh. Sed in lacus magna.</p>
                        
                        <ol>
                            <li>List 1</li>
                            <li>List 1
                                <ol>
                                    <li>List 1</li>
                                    <li>List 1</li>
                                </ol>
                            </li>
                            <li>List 1</li>
                            <li>List 1</li>
                        </ol>

                        <p>Nullam placerat ultrices aliquet. Aenean ornare arcu non tortor tincidunt, vel placerat velit dignissim. Mauris dignissim nulla nec lacus pellentesque tincidunt.</p>
    
                        <small class="d-block">blockquote class="blockquote"</small>

                        <blockquote class="blockquote">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante. Nam dui enim.</blockquote>

                        <small class="d-block">label</small>

                        <label>Lorem ipsum dolor sit</label>
                    </div>
                </div>
            </div>
        </section>


        <!-- FORMS / FORMULARIOS / CONTACTO -->
        <!-- ---------------------------------------------- -->
        <section>
            <div class="container">
        
                <h2 class="mt-0 text-center">FORM Styles</h2>
                
                <hr>
        
                <form>
                    <div class="form-group">
                        <label for="Name">Name</label>
                        <input type="text" class="form-control" id="Name" placeholder="Enter Name">
                    </div>
                    <div class="form-row">
                        <div class="form-group col-12 col-md-6">
                            <label for="Phone">Phone</label>
                            <input type="text" class="form-control" id="phone" placeholder="Enter phone">
                        </div>
                        <div class="form-group col-12 col-md-6">
                            <label for="Email">Email</label>
                            <input type="email" class="form-control" id="Email" aria-describedby="emailHelp" placeholder="Enter email">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="comments">Comments</label>
                        <textarea class="form-control" id="comments" placeholder="Enter comments" rows="5"></textarea>
                    </div>
                    <label class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input">
                        <span class="custom-control-indicator"></span>
                        <span class="custom-control-description">Check this custom checkbox</span>
                    </label>
                    <button type="submit" class="btn btn-primary float-right">Submit</button>
                </form>
            </div>
        </section>


        <!-- ICONS / ICONOS-->
        <!-- ---------------------------------------------- -->
        <section>
            <div class="container">
        
                <h2 class="mt-0 text-center">ICONS Styles</h2>

                <hr>

                <small class="mb-15">color icons</small>
                <div class="d-flex flex-wrap padding-box">
                    <span class="icon_search icon-left w-100 w-sm-50 w-md-25 mb-15">icon_search</span>
                    <span class="icon_close icon-left w-100 w-sm-50 w-md-25 mb-15">icon_close</span>
                    <span class="icon_menu icon-left w-100 w-sm-50 w-md-25 mb-15">icon_menu</span>
                    <span class="icon_minus icon-left w-100 w-sm-50 w-md-25 mb-15">icon_minus</span>
                    <span class="icon_plus icon-left w-100 w-sm-50 w-md-25 mb-15">icon_plus</span>
                    <span class="icon_up icon-left w-100 w-sm-50 w-md-25 mb-15">icon_up</span>
                    <span class="icon_right icon-left w-100 w-sm-50 w-md-25 mb-15">icon_right</span>
                    <span class="icon_down icon-left w-100 w-sm-50 w-md-25 mb-15">icon_down</span>
                    <span class="icon_left icon-left w-100 w-sm-50 w-md-25 mb-15">icon_left</span>
                    <span class="icon_ok icon-left w-100 w-sm-50 w-md-25 mb-15">icon_ok</span>
                    <span class="icon_arrow icon-left w-100 w-sm-50 w-md-25 mb-15">icon_arrow</span>
                    <span class="icon_download icon-left w-100 w-sm-50 w-md-25 mb-15">icon_download</span>
                    <span class="icon_download2 icon-left w-100 w-sm-50 w-md-25 mb-15">icon_download2</span>
                    <span class="icon_upload icon-left w-100 w-sm-50 w-md-25 mb-15">icon_upload</span>
                    <span class="icon_exit icon-left w-100 w-sm-50 w-md-25 mb-15">icon_exit</span>
                    <span class="icon_copy icon-left w-100 w-sm-50 w-md-25 mb-15">icon_copy</span>
                    <span class="icon_attach icon-left w-100 w-sm-50 w-md-25 mb-15">icon_attach</span>
                    <span class="icon_bag icon-left w-100 w-sm-50 w-md-25 mb-15">icon_bag</span>
                    <span class="icon_cart icon-left w-100 w-sm-50 w-md-25 mb-15">icon_cart</span>
                    <span class="icon_user icon-left w-100 w-sm-50 w-md-25 mb-15">icon_user</span>
                    <span class="icon_list icon-left w-100 w-sm-50 w-md-25 mb-15">icon_list</span>
                    <span class="icon_phone icon-left w-100 w-sm-50 w-md-25 mb-15">icon_phone</span>
                    <span class="icon_mail icon-left w-100 w-sm-50 w-md-25 mb-15">icon_mail</span>
                    <span class="icon_placeholder icon-left w-100 w-sm-50 w-md-25 mb-15">icon_placeholder</span>
                    <span class="icon_send icon-left w-100 w-sm-50 w-md-25 mb-15">icon_send</span>
                    <span class="icon_print icon-left w-100 w-sm-50 w-md-25 mb-15">icon_print</span>
                    <span class="icon_adjust icon-left w-100 w-sm-50 w-md-25 mb-15">icon_adjust</span>
                    <span class="icon_trash icon-left w-100 w-sm-50 w-md-25 mb-15">icon_trash</span>
                    <span class="icon_success icon-left w-100 w-sm-50 w-md-25 mb-15">icon_success</span>
                    <span class="icon_error icon-left w-100 w-sm-50 w-md-25 mb-15">icon_error</span>
                    <span class="icon_warning icon-left w-100 w-sm-50 w-md-25 mb-15">icon_warning</span>
                    <span class="icon_info icon-left w-100 w-sm-50 w-md-25 mb-15">icon_info</span>
                    <span class="icon_radiobtn icon-left w-100 w-sm-50 w-md-25 mb-15">icon_radiobtn</span>
                    <span class="icon_radiobtnactive icon-left w-100 w-sm-50 w-md-25 mb-15">icon_radiobtnactive</span>
                    <span class="icon_check icon-left w-100 w-sm-50 w-md-25 mb-15">icon_check</span>
                    <span class="icon_checkactive icon-left w-100 w-sm-50 w-md-25 mb-15">icon_checkactive</span>
                    <span class="icon_edit icon-left w-100 w-sm-50 w-md-25 mb-15">icon_edit</span>
                    <span class="icon_wishlist icon-left w-100 w-sm-50 w-md-25 mb-15">icon_wishlist</span>
                    <span class="icon_review icon-left w-100 w-sm-50 w-md-25 mb-15">icon_review</span>
                    <span class="icon_star icon-left w-100 w-sm-50 w-md-25 mb-15">icon_star</span>
                    <span class="icon_hand_ok icon-left w-100 w-sm-50 w-md-25 mb-15">icon_hand_ok</span>
                    <span class="icon_hand_ko icon-left w-100 w-sm-50 w-md-25 mb-15">icon_hand_ko</span>
                    <span class="icon_arrows_sm icon-left w-100 w-sm-50 w-md-25 mb-15">icon_arrows_sm</span>
                    <span class="icon_arrows_disabled_down_sm icon-left w-100 w-sm-50 w-md-25 mb-15">icon_arrows_disabled_down_sm</span>
                    <span class="icon_arrows_disabled_up_sm icon-left w-100 w-sm-50 w-md-25 mb-15">icon_arrows_disabled_up_sm</span>
                    <span class="icon_arrow_up_sm icon-left w-100 w-sm-50 w-md-25 mb-15">icon_arrow_up_sm</span>
                    <span class="icon_arrow_down_sm icon-left w-100 w-sm-50 w-md-25 mb-15">icon_arrow_down_sm</span>
                    <span class="icon_arrow_select_sm icon-left w-100 w-sm-50 w-md-25 mb-15">icon_arrow_select_sm</span>
                    <span class="icon_phone_sm icon-left w-100 w-sm-50 w-md-25 mb-15">icon_phone_sm</span>
                    <span class="icon_mail_sm icon-left w-100 w-sm-50 w-md-25 mb-15">icon_mail_sm</span>
                    <span class="icon_placeholder_sm icon-left w-100 w-sm-50 w-md-25 mb-15">icon_placeholder_sm</span>
                    <span class="icon_fax_sm icon-left w-100 w-sm-50 w-md-25 mb-15">icon_fax_sm</span>
                </div>

                <small class="mb-15 mt-50">black icons</small>
                <div class="d-flex flex-wrap padding-box">
                    <span class="icon_search_b icon-left w-100 w-sm-50 w-md-25 mb-15">icon_search_b</span>
                    <span class="icon_close_b icon-left w-100 w-sm-50 w-md-25 mb-15">icon_close_b</span>
                    <span class="icon_menu_b icon-left w-100 w-sm-50 w-md-25 mb-15">icon_menu_b</span>
                    <span class="icon_minus_b icon-left w-100 w-sm-50 w-md-25 mb-15">icon_minus_b</span>
                    <span class="icon_plus_b icon-left w-100 w-sm-50 w-md-25 mb-15">icon_plus_b</span>
                    <span class="icon_up_b icon-left w-100 w-sm-50 w-md-25 mb-15">icon_up_b</span>
                    <span class="icon_right_b icon-left w-100 w-sm-50 w-md-25 mb-15">icon_right_b</span>
                    <span class="icon_down_b icon-left w-100 w-sm-50 w-md-25 mb-15">icon_down_b</span>
                    <span class="icon_left_b icon-left w-100 w-sm-50 w-md-25 mb-15">icon_left_b</span>
                    <span class="icon_ok_b icon-left w-100 w-sm-50 w-md-25 mb-15">icon_ok_b</span>
                    <span class="icon_arrow_b icon-left w-100 w-sm-50 w-md-25 mb-15">icon_arrow_b</span>
                    <span class="icon_download_b icon-left w-100 w-sm-50 w-md-25 mb-15">icon_download_b</span>
                    <span class="icon_download2_b icon-left w-100 w-sm-50 w-md-25 mb-15">icon_download2_b</span>
                    <span class="icon_upload_b icon-left w-100 w-sm-50 w-md-25 mb-15">icon_upload_b</span>
                    <span class="icon_exit_b icon-left w-100 w-sm-50 w-md-25 mb-15">icon_exit_b</span>
                    <span class="icon_copy_b icon-left w-100 w-sm-50 w-md-25 mb-15">icon_copy_b</span>
                    <span class="icon_attach_b icon-left w-100 w-sm-50 w-md-25 mb-15">icon_attach_b</span>
                    <span class="icon_bag_b icon-left w-100 w-sm-50 w-md-25 mb-15">icon_bag_b</span>
                    <span class="icon_cart_b icon-left w-100 w-sm-50 w-md-25 mb-15">icon_cart_b</span>
                    <span class="icon_user_b icon-left w-100 w-sm-50 w-md-25 mb-15">icon_user_b</span>
                    <span class="icon_list_b icon-left w-100 w-sm-50 w-md-25 mb-15">icon_list_b</span>
                    <span class="icon_phone_b icon-left w-100 w-sm-50 w-md-25 mb-15">icon_phone_b</span>
                    <span class="icon_mail_b icon-left w-100 w-sm-50 w-md-25 mb-15">icon_mail_b</span>
                    <span class="icon_placeholder_b icon-left w-100 w-sm-50 w-md-25 mb-15">icon_placeholder_b</span>
                    <span class="icon_send_b icon-left w-100 w-sm-50 w-md-25 mb-15">icon_send_b</span>
                    <span class="icon_print_b icon-left w-100 w-sm-50 w-md-25 mb-15">icon_print_b</span>
                    <span class="icon_adjust_b icon-left w-100 w-sm-50 w-md-25 mb-15">icon_adjust_b</span>
                    <span class="icon_trash_b icon-left w-100 w-sm-50 w-md-25 mb-15">icon_trash_b</span>
                    <span class="icon_success_b icon-left w-100 w-sm-50 w-md-25 mb-15">icon_success_b</span>
                    <span class="icon_error_b icon-left w-100 w-sm-50 w-md-25 mb-15">icon_error_b</span>
                    <span class="icon_warning_b icon-left w-100 w-sm-50 w-md-25 mb-15">icon_warning_b</span>
                    <span class="icon_info_b icon-left w-100 w-sm-50 w-md-25 mb-15">icon_info_b</span>
                    <span class="icon_radiobtn_b icon-left w-100 w-sm-50 w-md-25 mb-15">icon_radiobtn_b</span>
                    <span class="icon_radiobtnactive_b icon-left w-100 w-sm-50 w-md-25 mb-15">icon_radiobtnactive_b</span>
                    <span class="icon_check_b icon-left w-100 w-sm-50 w-md-25 mb-15">icon_check_b</span>
                    <span class="icon_checkactive_b icon-left w-100 w-sm-50 w-md-25 mb-15">icon_checkactive_b</span>
                    <span class="icon_edit_b icon-left w-100 w-sm-50 w-md-25 mb-15">icon_edit_b</span>
                    <span class="icon_wishlist_b icon-left w-100 w-sm-50 w-md-25 mb-15">icon_wishlist_b</span>
                    <span class="icon_review_b icon-left w-100 w-sm-50 w-md-25 mb-15">icon_review_b</span>
                    <span class="icon_star_b icon-left w-100 w-sm-50 w-md-25 mb-15">icon_star_b</span>
                    <span class="icon_hand_ok_b icon-left w-100 w-sm-50 w-md-25 mb-15">icon_hand_ok_b</span>
                    <span class="icon_hand_ko_b icon-left w-100 w-sm-50 w-md-25 mb-15">icon_hand_ko_b</span>
                    <span class="icon_arrows_sm_b icon-left w-100 w-sm-50 w-md-25 mb-15">icon_arrows_sm_b</span>
                    <span class="icon_arrows_disabled_down_sm_b icon-left w-100 w-sm-50 w-md-25 mb-15">icon_arrows_disabled_down_sm_b</span>
                    <span class="icon_arrows_disabled_up_sm_b icon-left w-100 w-sm-50 w-md-25 mb-15">icon_arrows_disabled_up_sm_b</span>
                    <span class="icon_arrow_up_sm_b icon-left w-100 w-sm-50 w-md-25 mb-15">icon_arrow_up_sm_b</span>
                    <span class="icon_arrow_down_sm_b icon-left w-100 w-sm-50 w-md-25 mb-15">icon_arrow_down_sm_b</span>
                    <span class="icon_arrow_select_sm_b icon-left w-100 w-sm-50 w-md-25 mb-15">icon_arrow_select_sm_b</span>
                    <span class="icon_phone_sm_b icon-left w-100 w-sm-50 w-md-25 mb-15">icon_phone_sm_b</span>
                    <span class="icon_mail_sm_b icon-left w-100 w-sm-50 w-md-25 mb-15">icon_mail_sm_b</span>
                    <span class="icon_placeholder_sm_b icon-left w-100 w-sm-50 w-md-25 mb-15">icon_placeholder_sm_b</span>
                    <span class="icon_fax_sm_b icon-left w-100 w-sm-50 w-md-25 mb-15">icon_fax_sm_b</span>
                </div>

                <small class="mb-15 mt-50">white icons</small>
                <div class="block1 padding-box">
                    <div class="d-flex flex-wrap">
                        <span class="icon_search_w icon-left w-100 w-sm-50 w-md-25 mb-15">icon_search_w</span>
                        <span class="icon_close_w icon-left w-100 w-sm-50 w-md-25 mb-15">icon_close_w</span>
                        <span class="icon_menu_w icon-left w-100 w-sm-50 w-md-25 mb-15">icon_menu_w</span>
                        <span class="icon_minus_w icon-left w-100 w-sm-50 w-md-25 mb-15">icon_minus_w</span>
                        <span class="icon_plus_w icon-left w-100 w-sm-50 w-md-25 mb-15">icon_plus_w</span>
                        <span class="icon_up_w icon-left w-100 w-sm-50 w-md-25 mb-15">icon_up_w</span>
                        <span class="icon_right_w icon-left w-100 w-sm-50 w-md-25 mb-15">icon_right_w</span>
                        <span class="icon_down_w icon-left w-100 w-sm-50 w-md-25 mb-15">icon_down_w</span>
                        <span class="icon_left_w icon-left w-100 w-sm-50 w-md-25 mb-15">icon_left_w</span>
                        <span class="icon_ok_w icon-left w-100 w-sm-50 w-md-25 mb-15">icon_ok_w</span>
                        <span class="icon_arrow_w icon-left w-100 w-sm-50 w-md-25 mb-15">icon_arrow_w</span>
                        <span class="icon_download_w icon-left w-100 w-sm-50 w-md-25 mb-15">icon_download_w</span>
                        <span class="icon_download2_w icon-left w-100 w-sm-50 w-md-25 mb-15">icon_download2_w</span>
                        <span class="icon_upload_w icon-left w-100 w-sm-50 w-md-25 mb-15">icon_upload_w</span>
                        <span class="icon_exit_w icon-left w-100 w-sm-50 w-md-25 mb-15">icon_exit_w</span>
                        <span class="icon_copy_w icon-left w-100 w-sm-50 w-md-25 mb-15">icon_copy_w</span>
                        <span class="icon_attach_w icon-left w-100 w-sm-50 w-md-25 mb-15">icon_attach_w</span>
                        <span class="icon_wag_w icon-left w-100 w-sm-50 w-md-25 mb-15">icon_wag_w</span>
                        <span class="icon_cart_w icon-left w-100 w-sm-50 w-md-25 mb-15">icon_cart_w</span>
                        <span class="icon_user_w icon-left w-100 w-sm-50 w-md-25 mb-15">icon_user_w</span>
                        <span class="icon_list_w icon-left w-100 w-sm-50 w-md-25 mb-15">icon_list_w</span>
                        <span class="icon_phone_w icon-left w-100 w-sm-50 w-md-25 mb-15">icon_phone_w</span>
                        <span class="icon_mail_w icon-left w-100 w-sm-50 w-md-25 mb-15">icon_mail_w</span>
                        <span class="icon_placeholder_w icon-left w-100 w-sm-50 w-md-25 mb-15">icon_placeholder_w</span>
                        <span class="icon_send_w icon-left w-100 w-sm-50 w-md-25 mb-15">icon_send_w</span>
                        <span class="icon_print_w icon-left w-100 w-sm-50 w-md-25 mb-15">icon_print_w</span>
                        <span class="icon_adjust_w icon-left w-100 w-sm-50 w-md-25 mb-15">icon_adjust_w</span>
                        <span class="icon_trash_w icon-left w-100 w-sm-50 w-md-25 mb-15">icon_trash_w</span>
                        <span class="icon_success_w icon-left w-100 w-sm-50 w-md-25 mb-15">icon_success_w</span>
                        <span class="icon_error_w icon-left w-100 w-sm-50 w-md-25 mb-15">icon_error_w</span>
                        <span class="icon_warning_w icon-left w-100 w-sm-50 w-md-25 mb-15">icon_warning_w</span>
                        <span class="icon_info_w icon-left w-100 w-sm-50 w-md-25 mb-15">icon_info_w</span>
                        <span class="icon_radiobtn_w icon-left w-100 w-sm-50 w-md-25 mb-15">icon_radiobtn_w</span>
                        <span class="icon_radiobtnactive_w icon-left w-100 w-sm-50 w-md-25 mb-15">icon_radiobtnactive_w</span>
                        <span class="icon_check_w icon-left w-100 w-sm-50 w-md-25 mb-15">icon_check_w</span>
                        <span class="icon_checkactive_w icon-left w-100 w-sm-50 w-md-25 mb-15">icon_checkactive_w</span>
                        <span class="icon_edit_w icon-left w-100 w-sm-50 w-md-25 mb-15">icon_edit_w</span>
                        <span class="icon_wishlist_w icon-left w-100 w-sm-50 w-md-25 mb-15">icon_wishlist_w</span>
                        <span class="icon_review_w icon-left w-100 w-sm-50 w-md-25 mb-15">icon_review_w</span>
                        <span class="icon_star_w icon-left w-100 w-sm-50 w-md-25 mb-15">icon_star_w</span>
                        <span class="icon_hand_ok_w icon-left w-100 w-sm-50 w-md-25 mb-15">icon_hand_ok_w</span>
                        <span class="icon_hand_ko_w icon-left w-100 w-sm-50 w-md-25 mb-15">icon_hand_ko_w</span>
                        <span class="icon_arrows_sm_w icon-left w-100 w-sm-50 w-md-25 mb-15">icon_arrows_sm_w</span>
                        <span class="icon_arrows_disabled_down_sm_w icon-left w-100 w-sm-50 w-md-25 mb-15">icon_arrows_disabled_down_sm_w</span>
                        <span class="icon_arrows_disabled_up_sm_w icon-left w-100 w-sm-50 w-md-25 mb-15">icon_arrows_disabled_up_sm_w</span>
                        <span class="icon_arrow_up_sm_w icon-left w-100 w-sm-50 w-md-25 mb-15">icon_arrow_up_sm_w</span>
                        <span class="icon_arrow_down_sm_w icon-left w-100 w-sm-50 w-md-25 mb-15">icon_arrow_down_sm_w</span>
                        <span class="icon_arrow_select_sm_w icon-left w-100 w-sm-50 w-md-25 mb-15">icon_arrow_select_sm_w</span>
                        <span class="icon_phone_sm_w icon-left w-100 w-sm-50 w-md-25 mb-15">icon_phone_sm_w</span>
                        <span class="icon_mail_sm_w icon-left w-100 w-sm-50 w-md-25 mb-15">icon_mail_sm_w</span>
                        <span class="icon_placeholder_sm_w icon-left w-100 w-sm-50 w-md-25 mb-15">icon_placeholder_sm_w</span>
                        <span class="icon_fax_sm_w icon-left w-100 w-sm-50 w-md-25 mb-15">icon_fax_sm_w</span>
                    </div>
                </div>

                <small class="mb-15 mt-50">social icons</small>
                <div class="d-flex flex-wrap padding-box">
                    <span class="social_facebook icon-left w-100 w-sm-50 w-md-25 mb-15">social_facebook</span>
                    <span class="social_twitter icon-left w-100 w-sm-50 w-md-25 mb-15">social_twitter</span>
                    <span class="social_youtube icon-left w-100 w-sm-50 w-md-25 mb-15">social_youtube</span>
                    <span class="social_instagram icon-left w-100 w-sm-50 w-md-25 mb-15">social_instagram</span>
                    <span class="social_pinterest icon-left w-100 w-sm-50 w-md-25 mb-15">social_pinterest</span>
                    <span class="social_linkedin icon-left w-100 w-sm-50 w-md-25 mb-15">social_linkedin</span>
                    <span class="social_whatsapp icon-left w-100 w-sm-50 w-md-25 mb-15">social_whatsapp</span>
                </div>

                <small class="mb-15 mt-50">social black icons</small>
                <div class="d-flex flex-wrap padding-box">
                    <span class="social_facebook_b icon-left w-100 w-sm-50 w-md-25 mb-15">social_facebook_b</span>
                    <span class="social_twitter_b icon-left w-100 w-sm-50 w-md-25 mb-15">social_twitter_b</span>
                    <span class="social_youtube_b icon-left w-100 w-sm-50 w-md-25 mb-15">social_youtube_b</span>
                    <span class="social_instagram_b icon-left w-100 w-sm-50 w-md-25 mb-15">social_instagram_b</span>
                    <span class="social_pinterest_b icon-left w-100 w-sm-50 w-md-25 mb-15">social_pinterest_b</span>
                    <span class="social_linkedin_b icon-left w-100 w-sm-50 w-md-25 mb-15">social_linkedin_b</span>
                    <span class="social_whatsapp_b icon-left w-100 w-sm-50 w-md-25 mb-15">social_whatsapp_b</span>
                </div>

                <small class="mb-15 mt-50">social icons</small>
                <div class="block1 padding-box">
                    <div class="d-flex flex-wrap">
                        <span class="social_facebook_w icon-left w-100 w-sm-50 w-md-25 mb-15">social_facebook_w</span>
                        <span class="social_twitter_w icon-left w-100 w-sm-50 w-md-25 mb-15">social_twitter_w</span>
                        <span class="social_youtube_w icon-left w-100 w-sm-50 w-md-25 mb-15">social_youtube_w</span>
                        <span class="social_instagram_w icon-left w-100 w-sm-50 w-md-25 mb-15">social_instagram_w</span>
                        <span class="social_pinterest_w icon-left w-100 w-sm-50 w-md-25 mb-15">social_pinterest_w</span>
                        <span class="social_linkedin_w icon-left w-100 w-sm-50 w-md-25 mb-15">social_linkedin_w</span>
                        <span class="social_whatsapp_w icon-left w-100 w-sm-50 w-md-25 mb-15">social_whatsapp_w</span>
                    </div>
                </div>
            </div>
        </section>

        <!-- FOOTER / PIE DE PÁGINA -->
        <!-- ---------------------------------------------- -->
        <footer>
            <div class="container">
                <div class="row">
                    <div class="col-md-3">
                        <ul class="list-unstyled">
                            <li><a href="#" title="">Home</a></li>
                            <li><a href="#" title="">Empresa</a></li>
                            <li><a href="#" title="">Servicios</a></li>
                            <li><a href="#" title="">Productos</a></li>
                            <li><a href="#" title="">Contacto</a></li>
                        </ul>
                    </div>

                    <div class="col-md-3">
                        <span class="icon_placeholder_sm_b icon-left">[DIRECCION EMPRESA]</span>
                        <span class="icon_phone_sm_b icon-left">Tel. [TELEFONO]</span>
                    </div>

                    <div class="col">
                        <ul class="socialMenu">
                            <a href="#" class="social_facebook"></a>
                            <a href="#" class="social_twitter"></a>
                            <a href="#" class="social_instagram"></a>
                        </ul>
                    </div>
                </div>
            </div>
        </footer>
    </div>

	<script src="js/slick.min.js"></script>
	<script>
		$(document).ready(function(){
			$('.js-home-slider').slick({
				slidesToShow: 1,
				slidesToScroll: 1,
				dots: true,
				arrows: true,
				nextArrow: '<button class="slick-next slick-arrow" aria-label="Next" type="button" style=""></button>',
				prevArrow: '<button class="slick-prev slick-arrow" aria-label="Prev" type="button" style=""></button>',
				infinite: true, 
				speed: 300,
	            autoplay: true,
	            autoplaySpeed: 3500
			});
		});
	</script>
</body>
</html>
