<?php
include("head.php");
$ht_title = CONTACTO_METAS_TIT;
$ht_description = htmlspecialchars(CONTACTO_METAS_DESC, ENT_QUOTES, 'UTF-8');
?>
<!DOCTYPE html>
<html lang="<?php echo IDIOMA;?>">
<head>
	<meta name="robots" content="noindex">
	<?php include("header.php");?>
</head>
<body>
	<?php include("body.php");?>

	<section class="marmol-header pt-0 pb-md-0">
		<div class="container-fluid px-md-0">						
			<div class="row violetBg">
				<div class="col-lg-8 col-md-6 whiteB z-2 align-self-end">
					<h1 class="text-lg-right text-center lightblueBg pr-lg-30 py-25 mb-0"><?php echo CONTACTO_H1 ;?></h1>
				</div>
				<div class="col-lg-6 col-md-8 z-1 pr-md-0 pr-lg-20 ml-negative mb-lg-0">
					<div class="img-frame h-md-100">
						<img src="/images/slider-contacto.jpg" alt="<?php echo _ALT ;?>" class="img-fluid d-block mx-auto">
					</div>
				</div>
			</div>
		</div>
	</section>

	<section>
		<div class="container">
			<div class="row">
				<div class="col-10 offset-1 col-md-8 offset-md-2">
					<?php
					if((int)$_GET['error']=='1'){
						echo '<div class="col-12 alert alert-danger" role="alert">' . COMMON_ENVIOERROR . '</div>';
					}else{
						echo '<div class="col-12 alert alert-success" role="alert">' .  COMMON_ENVIOOK . '</div>';
					}
					?>
				</div>
			</div>
		</div>
	</section>

	<?php include("footer.php");?>
</body>
</html>
<?php include("bottom.php");?>
