
<footer id="footer" class="pb-0">
	<div class="container">
		<div class="row">
			<div class="col-md-3">
				<div id="logo">
					<a href="<?php echo $links['index.php'];?>" title=""><img src="images/logo_w.svg" alt="" class="img-fluid"></a>
					<div class="mt-25 areaInv"><a href="<?php echo FOOTER_AREA_INVESTIGADORES_LINK;?>" title="<?php echo FOOTER_AREA_INVESTIGADORES;?>" class=""><?php echo FOOTER_AREA_INVESTIGADORES;?></a></div>

				</div>
			</div>
			<div class="col-md-4">
				<ul class="list-unstyled">
					<li><strong class="text-decoration-underline text-uppercase"><?php echo FOOTER_COORDINACION;?></strong></li>
					<li><?php echo FOOTER_COORDINACION_TEXT;?></li>
				</ul>
			</div>
			<div class="col-md-4">
				<ul class="list-unstyled">
					<li><strong class="text-decoration-underline text-uppercase"><?php echo FOOTER_UBICACION;?></strong></li>
					<li><?php echo CONFIG_DIRECCION;?></li>
				</ul>
				<ul class="list-unstyled">
					<li><strong class="text-decoration-underline text-uppercase"><?php echo FOOTER_CONTACTO;?></strong></li>
					<li><?php echo CONFIG_MAILTO;?></li>
				</ul>
			</div>
		</div>
		<div class="d-flex flex-wrap justify-content-center d-inline-flex logos-footer mt-sm-25 w-100">
			<img src="/images/LOGO-01-ISCIII-GRANDE.png" alt="<?php echo _ALT ;?>" class="img-fluid d-block">
			<img src="/images/inst-carlos.png" alt="<?php echo _ALT ;?>" class="img-fluid d-block">
			<img src="/images/ue.png" alt="<?php echo _ALT ;?>" class="img-fluid d-block">
		</div>
		<div class="row justify-content-center mt-50">			
			<div class="col-md-6 text-center">
				<ul class="list-inline">
					<li class="list-inline-items mr-15"><a href="<?php echo $links['politica-legal.php']; ?>" title="<?php echo COMMON_NOTALEGAL;?>"><?php echo COMMON_NOTALEGAL;?></a></li>
					<li class="list-inline-items mr-15"><a href="<?php echo $links['politica-privacidad.php']; ?>" title="<?php echo COMMON_PRIVACIDAD;?>"><?php echo COMMON_PRIVACIDAD;?></a></li>
					<li class="list-inline-items mr-15"><a href="<?php echo $links['politica-cookies.php']; ?>" title="<?php echo COMMON_COOKIES;?>"><?php echo COMMON_COOKIES;?></a></li>
					<?php if($print_ltw_link){ ?><li class="list-inline-items"><a href="https://www.latevaweb.com/" title="<?php echo COMMON_MARKETINGONLINE;?>" target="_blank"><?php echo COMMON_MARKETINGONLINE;?></a></li><?php } ?>
				</ul>
			</div>				
		</div>
		<div class="row justify-content-center mt-15 pb-25">			
			<div class="col text-center">
				<small><?php echo FOOTER_BOTTOM_END;?>		</small>	
			</div>
		</div>
	</div>
	<!-- <?php if($detect->isMobile() && !$detect->isTablet()){ ?>
		<a href="tel:<?php echo CONFIG_PHONE;?>" class="phoneButton"><span class="social_whatsapp_w"></span></a>
	<?php } ?> -->
</footer>

</div>


<script src="js/slick.min.js"></script>
	<script>         
		$(document).ready(function(){
   
			$('.carousel').slick({
				slidesToShow: 1,
				slidesToScroll: 1,
				mobileFirst: true,
				arrows: false,
				dots: false,
				autoplay: true,
				responsive: [
					{
						breakpoint: 990,
						settings: 'unslick'
					}
				]
			});

			$(window).on('resize', function() {
				$('.carousel').slick('resize');
			});
		});
	</script>