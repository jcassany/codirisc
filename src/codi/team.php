<?php
include("head.php");

//Configurar
$table = "equipo";
$table_content = $table."_content";
$hay_locationmap = 1; // 1 si hay mapa de localizacion
$sufijo_plural = "Publicaciones";
$sufijo_singular = "Publicacion";
$campo_listar = "t.orden"; //t.campo si es de la tabla 'normal' y tc.campo si es de la tabla 'content'
$orden_listar = "ASC";
$pinta_listar = "text_titulo";
$pint2_listar = "textarea_intro";
$seo_url 		 = "text_ht_url";
$seo_title 		 = "text_ht_title";
$seo_description = "notiny_ht_description";
$cols = "SHOW FULL COLUMNS FROM ".$DATABASE.".".$table;
$cols_content = "SHOW FULL COLUMNS FROM ".$DATABASE.".".$table_content;

// Paginacion
$este_fichero = $_SERVER['PHP_SELF'];
$porpagina = CONFIG_PORPAGINA;
if ((isset($_GET["pag"])) && (!isset($_POST["pagina"]))) $pagina= $_GET["pag"];
if ($pagina<=0) $pagina=1;
$limite = " LIMIT ".(($pagina-1)*$porpagina).",".$porpagina;

// Consulta per saber el total de projectes que compleixen
if(isset($_GET['id'])){
	$sql = "SELECT COUNT(*) FROM ".$table." t LEFT JOIN ".$table_content." tc ON (t.id=tc.id AND tc.idioma='".IDIOMA."') WHERE tc.".$pinta_listar." != '' AND t.checkbox_visible='1' AND t.select_categorias_publicaciones = '".(int)$_GET['id']."'";
}else{
	$sql = "SELECT COUNT(*) FROM ".$table." t LEFT JOIN ".$table_content." tc ON (t.id=tc.id AND tc.idioma='".IDIOMA."') WHERE tc.".$pinta_listar." != '' AND t.checkbox_visible='1'";
}
$resultpagina = db_query($link,$sql);
$linepagina = mysqli_fetch_array($resultpagina);
$total_buscats_paginas = $linepagina["COUNT(*)"];

//Busco nom categoria
if(isset($_GET['id'])){
	$current_category = (int)$_GET['id'];
	$cate = db_query($link,"SELECT * FROM categorias_publicaciones_content WHERE idioma='".IDIOMA."' AND id=".$current_category);
	$category = mysqli_fetch_array($cate);
	$catNombre = ": ".$category['text_nombre'];

	$ht_title = ($category[$seo_title]) ? $category[$seo_title]: html_entity_decode(strip_tags($category['text_nombre']), ENT_COMPAT, 'UTF-8');
	$ht_description = ($category[$seo_description]) ? $category[$seo_description]: html_entity_decode(strip_tags($category['textarea_description']), ENT_COMPAT, 'UTF-8');
} else{
	$catNombre = "";
	$ht_title = PRODUCTOS_METAS_TIT;
	$ht_description = PRODUCTOS_METAS_DESC;
}

/* === Redireccion en caso de que no exista en la base de datos === */
if (isset($_GET['id']) && $category == null) {
	header("Location: 404.php", true, 404);
	require_once('./404.php');
} else {

	include("auto_functions.php");
	?>
	<!DOCTYPE html>
	<html lang="<?php echo IDIOMA;?>">
	<head><?php include("header.php");?></head>
	<body>
		<?php include("body.php");?>


		<?php
			if(isset($_GET['id'])){
				$listar = "SELECT * FROM ".$table." t LEFT JOIN ".$table_content." tc ON (t.id=tc.id AND tc.idioma='".IDIOMA."') WHERE tc.".$pinta_listar." != '' AND t.checkbox_visible='1' AND t.select_categorias_publicaciones = '".(int)$_GET['id']."' ORDER BY ".$campo_listar." ".$orden_listar." ".$limite;
			} else{
				$listar = "SELECT * FROM ".$table." t LEFT JOIN ".$table_content." tc ON (t.id=tc.id AND tc.idioma='".IDIOMA."') WHERE tc.".$pinta_listar." != '' AND t.checkbox_visible='1' ORDER BY ".$campo_listar." ".$orden_listar." ".$limite;
			}

			$l = db_query($link,$listar);
			if (mysqli_num_rows($l) > 0) {
			?>


			<section class="team-header pt-0">
					<div class="container-fluid px-md-0">						
						<div class="row align-items-center violetBg pr">
							<div class="col-lg-8 offset-lg-2 z-1 pr-md-0 pr-lg-20">
								<div class="img-frame">
									<img src="/images/slider-equipo.jpg" alt="<?php echo _ALT ;?>" class="img-fluid d-block mx-auto">
								</div>
							</div>
							<div class="col-lg-7 whiteB z-2 researchTitle">
								<h1 class="text-lg-left text-center lightblueBg pl-lg-30 py-25 mt-0"><?php echo TEAM_TITULO ;?></h1>
							</div>
						</div>

					</div>
				</section>
				
				<section>
					<div class="container">
						
						<div class="row">

							<div class="col">
								
								<div class="teamsList teamsList--column row">
								<?php
								while($list = mysqli_fetch_array($l)){
									$title = $list[$pinta_listar];
									$url = $url = url_element($list, $title, 'p', IDIOMA);
									?>

									<article class="teamsList__item col-md-4">
										<div class="inner-item p-md-30">
											

											<?php $categoria = mysqli_fetch_array(db_query_default($link, 'categorias_publicaciones', $where = "t.id = '".$list['select_categorias_publicaciones']."'")); ?>
													<?php
													$r = db_query($link,"SELECT * FROM ".$table." WHERE id=".$list['id']);
													$registro = mysqli_fetch_array($r);
													if($registro['file_foto'] != ""){ ?>
														<div class="thumb-wrapper <?php if($categoria['text_nombre'] == $categoria['text_nombre']) { echo $categoria['text_nombre'];}?>">
														<div class="thumb">
														
														<img src="images/<?php echo $table; ?>/<?php echo $registro['file_foto']; ?>" alt="<?php echo $title; ?>" class="img-fluid mr-auto d-block"/>
													<?php } ?>
														
												</div>
											</div>

											<div class="content">
												<div class="text-wrapper">
													
													<div class="date">
													<?php
														$categoria = mysqli_fetch_array(db_query_default($link, 'categorias_publicaciones', $where = "t.id = '".$list['select_categorias_publicaciones']."'")); ?>
														<span class="text-uppercase"> <?php echo $categoria['text_nombre'];?></span>
													<?php	if($categoria['text_nombre']){
															echo '<span class="mx-5">|</span>';
														}
													?>
													<?php echo fechaEspExtAno($list['text_fecha']); ?>
													</div>
													<h3 class="title"><span href="<?php echo $list['text_link']; ?>" title="<?php echo $title; ?>"><?php echo $title; ?></span></h3>
													<div class="text_investigadores my-15">
														<?php echo $list['textarea_investigadores']; ?>
													</div>

													<?php if ($list['textarea_investigadores_extra']){ ?>
														<div class="text_investigadores mb-15">
															<?php echo $list['textarea_investigadores_extra']; ?>
														</div>
													<?php } ?>

													<div class="text">
														<?php echo $list['textarea_intro']; ?>
													</div>
												</div>
											</div>
										</div>
									</article>
								<?php } ?>
							</div>

							<?php $total_paginas = ceil($total_buscats_paginas/$porpagina);
							if($total_paginas > 1){
								$url = parse_url($_SERVER["REQUEST_URI"], PHP_URL_PATH)."?";
								paginacion($total_paginas,$pagina,$url);
							} ?>
							</div>
						</div>


						
					</div>
				</section>
			<?php } ?>

		<?php include("footer.php");?>
	</body>
	</html>
	<?php include("bottom.php");
}
?>