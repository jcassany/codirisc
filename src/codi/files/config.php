<?php

//Debug Mode
define('DEBUG_MODE', true);

//Time Zone
date_default_timezone_set('Europe/Madrid');

if (DEBUG_MODE) {
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    ini_set('error_reporting', E_ALL ^ E_NOTICE);
    error_reporting(E_ALL ^ E_NOTICE);
} else {
    ini_set('display_errors', 0);
    ini_set('error_reporting', E_ALL ^ E_NOTICE ^ E_WARNING);
}

//BBDD
$HOSTNAME = "mysql"; //SERVIDOR
$USERNAME = "mycodiriscuser"; //USUARIO
$PASSWORD = "o6ejbbmGbeXhdSemSEhb"; //CONTRASEÑA
$DATABASE = "mycodiriscdatabase"; //BASE DE DATOS

define("IDIOMADEFAULT", "es");

//Google recaptcha
$google_recaptcha            = false;
$google_recaptcha_web_key    = "";
$google_recaptcha_secret_key = "";

//Inicio la sesión
session_start();

//Directiva magic quotes activada ->
if (get_magic_quotes_gpc() === 1) {

    function stripslashes_array(&$arr)
    {
        foreach ($arr as $k => &$v) {
            $nk = stripslashes($k);
            if ($nk != $k) {
                $arr[$nk] = &$v;
                unset($arr[$k]);
            }
            if (is_array($v)) {
                stripslashes_array($v);
            } else {
                $arr[$nk] = stripslashes($v);
            }
        }
    }

    stripslashes_array($_POST);
    stripslashes_array($_GET);
    stripslashes_array($_REQUEST);
    stripslashes_array($_COOKIE);
}
