<?php

// Llamadas a otros php de configuracion, inicio de sesiones, etc
include "head.php";

if ($google_recaptcha) {

    $url_ok = $_POST['url_ok'];
    $url_ko = $_POST['url_ko'];

} else {

    $url_ok = $_POST['url'];
    $url_ko = $_POST['url'];

}

if (isset($_POST['form']) && !empty($_POST['form'])) {
    ;
} else {
    header("Location: " . $url_ko . "?error=1#contact_form");
    exit;
}

if (isset($_POST['hAfgFNBBt9yUtxYzWWQ1']) && ($_POST['hAfgFNBBt9yUtxYzWWQ1'] != '')) {
    header("Location: " . $url_ko . "?error=1#contact_form");
    exit;
}

$_SESSION['send_form_variables'] = $_POST;

unset($_POST['form']);
unset($_POST['button']);
unset($_POST['iso_idioma']);
unset($_POST['url']);
unset($_POST['url_ok']);
unset($_POST['url_ko']);
unset($_POST['termsConditions']);
unset($_POST['hAfgFNBBt9yUtxYzWWQ1']);

if ($google_recaptcha) {

    if (isset($_POST['g-recaptcha-response']) && !empty($_POST['g-recaptcha-response'])) {

        // Google reCAPTCHA API secret key
        $secretKey = $google_recaptcha_secret_key;

        // Verify the reCAPTCHA response
        $verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret=' . $secretKey . '&response=' . $_POST['g-recaptcha-response']);

        // Decode json data
        $responseData = json_decode($verifyResponse);

        // If reCAPTCHA response is valid
        if ($responseData->success) {

            unset($_POST['g-recaptcha-response']);

        } else {

            header("Location: " . $url_ko . "?error=3#contact_form");
            exit;

        }

    } else {

        header("Location: " . $url_ko . "?error=2#contact_form");
        exit;

    }

}

$search  = array('Telefono', 'comercialInfo');
$replace = array('Teléfono', 'Acepto recibir publicidad');

$autorespuesta = COMMON_MAILAUTORESPTEXTO;
$autorespuesta = str_replace("{CONFIG_CLIENTE}", CONFIG_CLIENTE, $autorespuesta);
$autorespuesta = str_replace("{CONFIG_MAILTO}", CONFIG_MAILTO, $autorespuesta);
$autorespuesta = str_replace("{CONFIG_PHONE}", CONFIG_PHONE, $autorespuesta);

$subject = stripslashes($_POST['subject']);

if (base64_encode($subject)) {

    $subject = "=?UTF-8?B?" . base64_encode($subject) . "?=";

}

unset($_POST['subject']);

$body = "";

foreach ($_POST as $key => $value) {

    if ($value != '') {

        $body .= "<strong>" . str_replace($search, $replace, $key) . ":</strong> " . stripslashes($value) . "<br />";

    }

}

if (!empty($_FILES)) {

    foreach ($_FILES as $key => $value) {

        $allowedExts = array("pdf", "doc", "docx");
        $extension   = end(explode(".", $value["name"]));

        //Validación ->
        if (($value["type"] == "application/pdf") || ($value["type"] == "application/msword") || ($value["type"] == "application/vnd.openxmlformats-officedocument.wordprocessingml.document") && ($value["size"] < 20000000) && in_array($extension, $allowedExts)) {

            $token = md5(uniqid(rand(), true));

            mkdir("./UQgz7R2HybwT/" . $token, 0777, true);

            $target_dir = "./UQgz7R2HybwT/" . $token . "/";

            $file_name   = basename($value["name"]);
            $target_file = $target_dir . $file_name;

            if (move_uploaded_file($value["tmp_name"], $target_file)) {

                $body .= '<strong> ' . str_replace($search, $replace, $key) . ':</strong> <a href="http://' . $_SERVER['HTTP_HOST'] . '/UQgz7R2HybwT/' . $token . '/' . $file_name . '" target="_blank">' . $file_name . '</a>';

            }

        }

    }

}

$mailto                                 = CONFIG_MAILTO;
$mail_replay                            = CONFIG_MAIL_REPLAY;
if (empty($mail_replay)) $mail_replay   = $mailto;
$mail_name                              = CONFIG_MAIL_NAME;
$mail_subject                           = CONFIG_MAIL_SUBJECT;

$error    = 0;
$location = '';

if (mail($mailto, $subject, $body, "From: " . $mailto . "\r\n" . "MIME-Version: 1.0" . "\r\n" . "Reply-To: " . $_POST['Email'] . "\r\n" . "Content-type: text/html; charset=UTF-8" . "\r\n")) {

    $body_to_customer = "<strong>" . stripslashes($_POST['Nombre']) . "</strong><br /><br />" . $autorespuesta;
    mail($_POST['Email'], $mail_subject, $body_to_customer, "From: ".$mail_name." <" . $mailto . ">\r\n" . "Reply-To: " . $mail_replay . "\r\n" . "MIME-Version: 1.0" . "\r\n" . "Content-type: text/html; charset=UTF-8" . "\r\n");

    $location = $url_ok;

} else {

    $error    = 1;
    $location = $url_ko . "?error=4#contact_form";

}

/* Register DB */
db_query($link, "INSERT INTO mails (id, fecha, text_email, text_body, error) VALUES (null, '" . date('Y-m-d H:i:s') . "', '" . $_POST['Email'] . "', '" . addslashes($body) . "', '" . $error . "')");

header("Location: " . $location);
