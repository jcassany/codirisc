<?php
include("head.php");

//Configurar
$table = "noticias";
$table_content = $table."_content";
$hay_locationmap = 0; // 1 si hay mapa de localizacion
$sufijo_plural = "Noticias";
$sufijo_singular = "Noticia";
$campo_listar = "t.text_fecha"; //t.campo si es de la tabla 'normal' y tc.campo si es de la tabla 'content'
$orden_listar = "DESC";
$pinta_listar = "text_titulo";
$pint2_listar = "textarea_intro";
$seo_url 		 = "text_ht_url";
$seo_title 		 = "text_ht_title";
$seo_description = "notiny_ht_description";
$cols = "SHOW FULL COLUMNS FROM ".$DATABASE.".".$table;
$cols_content = "SHOW FULL COLUMNS FROM ".$DATABASE.".".$table_content;

// Paginacion
$este_fichero = $_SERVER['PHP_SELF'];
$porpagina = CONFIG_PORPAGINA;
if ((isset($_GET["pag"])) && (!isset($_POST["pagina"]))) $pagina= $_GET["pag"];
if ($pagina<=0) $pagina=1;
$limite = " LIMIT ".(($pagina-1)*$porpagina).",".$porpagina;

// Consulta per saber el total de projectes que compleixen
if(isset($_GET['id'])){
	$sql = "SELECT COUNT(*) FROM ".$table." t LEFT JOIN ".$table_content." tc ON (t.id=tc.id AND tc.idioma='".IDIOMA."') WHERE tc.".$pinta_listar." != '' AND t.text_fecha <= CURDATE() AND t.checkbox_visible='1' AND t.select_categorias_noticias = '".(int)$_GET['id']."'";
}else{
	$sql = "SELECT COUNT(*) FROM ".$table." t LEFT JOIN ".$table_content." tc ON (t.id=tc.id AND tc.idioma='".IDIOMA."') WHERE tc.".$pinta_listar." != '' AND t.text_fecha <= CURDATE() AND t.checkbox_visible='1'";
}
$resultpagina = db_query($link,$sql);
$linepagina = mysqli_fetch_array($resultpagina);
$total_buscats_paginas = $linepagina["COUNT(*)"];

//Busco nom categoria
if(isset($_GET['id'])){
	$current_category = (int)$_GET['id'];
	$cate = db_query($link,"SELECT * FROM categorias_noticias_content WHERE idioma='".IDIOMA."' AND id=".$current_category);
	$category = mysqli_fetch_array($cate);
	$catNombre = ": ".$category['text_nombre'];

	$ht_title = html_entity_decode(strip_tags($category['text_ht_title']), ENT_COMPAT, 'UTF-8');
	$ht_description = html_entity_decode(strip_tags($category['text_ht_description']), ENT_COMPAT, 'UTF-8');

} else{
	$catNombre = "";
	$ht_title = NOTICIAS_METAS_TIT;
	$ht_description = NOTICIAS_METAS_DESC;
}

/* === Redireccion en caso de que no exista en la base de datos === */
if (isset($_GET['id']) && $category == null) {
	header("Location: 404.php", true, 404);
	require_once('./404.php');
} else {

	include("auto_functions.php");
	?>
	<!DOCTYPE html>
	<html lang="<?php echo IDIOMA;?>">
	<head><?php include("header.php");?></head>
	<body>
		<?php include("body.php");?>


		<?php
			if(isset($_GET['id'])){
				$listar = "SELECT * FROM ".$table." t LEFT JOIN ".$table_content." tc ON (t.id=tc.id AND tc.idioma='".IDIOMA."') WHERE tc.".$pinta_listar." != '' AND t.checkbox_visible='1' AND t.select_categorias_noticias = '".(int)$_GET['id']."' ORDER BY ".$campo_listar." ".$orden_listar." ".$limite;
			} else {
				$listar = "SELECT * FROM ".$table." t LEFT JOIN ".$table_content." tc ON (t.id=tc.id AND tc.idioma='".IDIOMA."') WHERE tc.".$pinta_listar." != '' AND t.checkbox_visible='1' ORDER BY ".$campo_listar." ".$orden_listar." ".$limite;
			}
			$l = db_query($link,$listar);
			if (mysqli_num_rows($l) > 0) {
				?>

				<section class="marmol-header pt-0">
					<div class="container-fluid px-md-0">						
						<div class="row violetBg">
							<div class="col-lg-8 col-md-6 whiteB z-2 align-self-center">
								<h1 class="text-lg-right text-center lightblueBg pr-lg-30 py-25"><?php echo NOTICIAS_H1 ;?></h1>
							</div>
							<div class="col-lg-6 col-md-8 z-1 pr-md-0 ml-negative align-self-xs-center align-self-md-start">
								<div class="bottomBLL violetBg"></div>
							</div>
						</div>

					</div>
				</section>

				<section class="">
					<div class="container">

						<div class="newsList newsList--column row justify-content-center">
							<?php
							while($list = mysqli_fetch_array($l)){
								$title = $list[$pinta_listar];
								$url = url_element($list, $title, 'n', IDIOMA);
								?>
								<article class="newsList__item col-md-4">
									<div class="inner-item">
										

										<div class="thumb-wrapper">
											<div class="thumb img-frame">
												<a href="<?php echo $url; ?>" title="<?php echo $title; ?>">
												<?php
												//Foto
												$foto_default = true;
												$r = db_query($link,"SELECT * FROM ".$table." WHERE id=".$list['id']);
												$registro = mysqli_fetch_array($r);
												if($registro['file_foto'] != ""){
													echo '<img src="images/'.$table.'/th_crop_'.$registro['file_foto'].'" alt="'.$title.'" />';
													$foto_default = false;
												} else {
													$list_cols = db_query($link,$cols);
													while($lista = mysqli_fetch_array($list_cols)){
														$field = $lista[0];
														if(substr($field,0,12) == "fotouploader"){
															$tablafotos = substr($field,13,strlen($field)-13);
															$ff = db_query($link,"SELECT * FROM ".$tablafotos." WHERE id_elem='".$list['id']."' ORDER BY orden ASC LIMIT 0,1");
															$ffoto = mysqli_fetch_array($ff);
															if($ffoto['foto'] != ""){
																echo '<img src="pics_fotos'.$table.'/'.$list['id'].'/th_crop_'.$ffoto['foto'].'" alt="'.$title.'" />';
																$foto_default = false;
															}
														}
													}
												}
												if ($foto_default) {
													echo '<img src="images/no-foto.jpg" alt="'.$title.'" />';
												}
												?>
												</a>
											</div>
										</div>

										<div class="content">
											<div class="text-wrapper">
												<h3 class="title"><a href="<?php echo $url; ?>" title="<?php echo $title; ?>"><?php echo $title; ?></a></h3>
												<div class="text"><?php echo $list[$pint2_listar]; ?></div>
											</div>
											<div class="date"><?php echo fechaEspExtAno($list['text_fecha']); ?></div>
										</div>
									</div>
								</article>
							<?php }	?>
						</div>

						<?php $total_paginas = ceil($total_buscats_paginas/$porpagina);
						if($total_paginas > 1){
							$url = parse_url($_SERVER["REQUEST_URI"], PHP_URL_PATH)."?";
							paginacion($total_paginas,$pagina,$url);
						} ?>

					</div>
				</section>
			<?php } ?>

		<?php include("footer.php");?>
	</body>
	</html>
	<?php include("bottom.php");
}
?>