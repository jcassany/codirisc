<?php

if (isset($logout) || isset($_GET["logout"]) || isset($_POST["logout"])) {

    include __DIR__ . "/logout.php";

} else {

    include __DIR__ . "/checkLogin.php";

}
