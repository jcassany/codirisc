-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Servidor: mysql-5603.dinaserver.com:3306
-- Tiempo de generación: 27-02-2020 a las 16:56:41
-- Versión del servidor: 5.6.47-log
-- Versión de PHP: 7.2.22

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `codir`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `asociaciones`
--

CREATE TABLE `asociaciones` (
  `id` int(11) NOT NULL,
  `orden` int(11) NOT NULL,
  `checkbox_visible` int(11) NOT NULL COMMENT 'Visible'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `asociaciones`
--

INSERT INTO `asociaciones` (`id`, `orden`, `checkbox_visible`) VALUES
(1, 0, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `asociaciones_content`
--

CREATE TABLE `asociaciones_content` (
  `id` int(11) NOT NULL,
  `idioma` varchar(2) NOT NULL,
  `text_titulo` varchar(250) NOT NULL COMMENT 'Título',
  `textarea_texto` text NOT NULL COMMENT 'Texto',
  `text_link` varchar(255) NOT NULL COMMENT 'Link'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `asociaciones_content`
--

INSERT INTO `asociaciones_content` (`id`, `idioma`, `text_titulo`, `textarea_texto`, `text_link`) VALUES
(1, 'ca', '', '', ''),
(1, 'en', '', '', ''),
(1, 'es', 'American Foundation for Suicide Prevention', 'Asociacion de supervivientes y entidades colaboradoras', 'https://afsp.org/');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categorias_publicaciones`
--

CREATE TABLE `categorias_publicaciones` (
  `id` int(11) NOT NULL,
  `text_titulo` varchar(250) NOT NULL COMMENT 'Nombre (interno)',
  `select_categorias_publicaciones` int(11) NOT NULL COMMENT 'Categoría padre',
  `orden` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `categorias_publicaciones`
--

INSERT INTO `categorias_publicaciones` (`id`, `text_titulo`, `select_categorias_publicaciones`, `orden`) VALUES
(1, 'Articulos', 0, 0),
(2, 'Congresos', 0, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categorias_publicaciones_content`
--

CREATE TABLE `categorias_publicaciones_content` (
  `id` int(11) NOT NULL,
  `idioma` varchar(250) NOT NULL,
  `text_menu` varchar(250) NOT NULL COMMENT 'Menú',
  `text_nombre` varchar(250) NOT NULL COMMENT 'Nombre',
  `textarea_description` text NOT NULL COMMENT 'Descripción',
  `text_ht_url` varchar(250) NOT NULL COMMENT 'HT Url',
  `text_ht_title` varchar(250) NOT NULL COMMENT 'HT Title',
  `notiny_ht_description` varchar(450) NOT NULL COMMENT 'HT Description'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `categorias_publicaciones_content`
--

INSERT INTO `categorias_publicaciones_content` (`id`, `idioma`, `text_menu`, `text_nombre`, `textarea_description`, `text_ht_url`, `text_ht_title`, `notiny_ht_description`) VALUES
(1, 'es', '', 'Artículo', '', '', '', ''),
(1, 'ca', '', '', '', '', '', ''),
(1, 'en', '', '', '', '', '', ''),
(2, 'es', '', 'Congreso', '', '', '', ''),
(2, 'ca', '', '', '', '', '', ''),
(2, 'en', '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `config_general`
--

CREATE TABLE `config_general` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `value` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `config_general`
--

INSERT INTO `config_general` (`id`, `name`, `value`) VALUES
(3, 'DEBUG_MODE_MOBILE', '0'),
(4, 'DEBUG_MODE_TABLET', '0'),
(7, 'MENU_TABLET_DESKTOP', '0'),
(8, 'CONFIG_LATERAL', '3'),
(9, 'CONFIG_MAPS_COORDENADAS', '41.39492,2.157072'),
(10, 'CONFIG_NIF', 'G-60070053'),
(11, 'CONFIG_PORPAGINA', '12'),
(12, 'CONFIG_TINY_IMG', '/images/pics'),
(13, 'CONFIG_BRAND', 'Fundació IMIM'),
(14, 'CONFIG_CLIENTE', 'Fundació Institut Mar d\'Investigacions Mèdiques'),
(15, 'CONFIG_MAILTO', 'codirisc@imim.es'),
(16, 'CONFIG_PHONE', '933 160 747 (ext. 1747)'),
(18, 'TAG_MANAGER_CODE', 'xxx'),
(19, 'GOOGLE_MAPS_APIKEY', 'GOOGLE_MAPS_APIKEY'),
(20, 'CONFIG_MAIL_REPLAY', ''),
(21, 'CONFIG_MAIL_NAME', ''),
(22, 'CONFIG_MAIL_SUBJECT', ''),
(23, 'APP_URL', 'http://codirisc.vl22392.dinaserver.com'),
(24, 'APP_COOKIE_DOMAIN', '.codirisc.vl22392.dinaserver.com');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `config_idiomes`
--

CREATE TABLE `config_idiomes` (
  `id_idioma` int(11) NOT NULL,
  `nom_idioma` varchar(2) NOT NULL,
  `setlocale` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `config_idiomes`
--

INSERT INTO `config_idiomes` (`id_idioma`, `nom_idioma`, `setlocale`) VALUES
(1, 'es', 'es_ES'),
(2, 'ca', 'ca_ES'),
(3, 'en', 'en_US');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `config_idiomesbackoffice`
--

CREATE TABLE `config_idiomesbackoffice` (
  `id_idioma` int(11) NOT NULL,
  `nom_idioma` varchar(2) NOT NULL,
  `setlocale` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `config_idiomesbackoffice`
--

INSERT INTO `config_idiomesbackoffice` (`id_idioma`, `nom_idioma`, `setlocale`) VALUES
(1, 'es', 'es_ES'),
(2, 'ca', 'ca_ES'),
(3, 'en', 'en_US');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `config_menus`
--

CREATE TABLE `config_menus` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `value` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `config_menus`
--

INSERT INTO `config_menus` (`id`, `name`, `value`) VALUES
(1, 'TIPO_MENU', 'menu1'),
(2, 'TIPO_AUX', 'aux3'),
(3, 'TIPO_BTN', 'btn1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `config_thumbs`
--

CREATE TABLE `config_thumbs` (
  `id` int(11) NOT NULL,
  `text_prefix` varchar(12) NOT NULL COMMENT 'Prefix: th_, med_, ...',
  `select_tipothumb` int(11) NOT NULL COMMENT 'Tipo de thumb',
  `text_mida` varchar(12) NOT NULL COMMENT 'Mida/es'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `config_thumbs`
--

INSERT INTO `config_thumbs` (`id`, `text_prefix`, `select_tipothumb`, `text_mida`) VALUES
(6, 'th_crop_', 5, '600,600'),
(7, 'th_fit_', 3, '600,600'),
(8, 'med_width_', 1, '1000'),
(10, 'med_fit_', 3, '1000,800'),
(11, 'big_full_', 1, '2000'),
(12, 'med_full_', 1, '1200'),
(13, 'small_full_', 1, '450'),
(14, 'big_crop_', 5, '1200,750'),
(15, 'flat_crop_', 5, '450, 300');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `config_tipothumb`
--

CREATE TABLE `config_tipothumb` (
  `id` int(11) NOT NULL,
  `text_titulo` varchar(250) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `config_tipothumb`
--

INSERT INTO `config_tipothumb` (`id`, `text_titulo`) VALUES
(1, 'width'),
(2, 'height'),
(3, 'fit'),
(4, 'square'),
(5, 'resize');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `equipo`
--

CREATE TABLE `equipo` (
  `id` int(11) NOT NULL,
  `orden` int(11) NOT NULL,
  `checkbox_visible` int(11) NOT NULL COMMENT 'Visible',
  `file_foto` varchar(250) NOT NULL COMMENT 'Imagen',
  `checkbox_destacado` int(11) NOT NULL COMMENT 'Destacado'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `equipo`
--

INSERT INTO `equipo` (`id`, `orden`, `checkbox_visible`, `file_foto`, `checkbox_destacado`) VALUES
(1, 0, 1, '1576503249hospitaldelmar.svg', 0),
(2, 0, 1, '1576498875parctauli.png', 0),
(3, 0, 1, '1576498900agencia-qualitat.png', 0),
(4, 0, 0, '1576498939grupo-7056.png', 0),
(5, 0, 0, '1576498985grupo-7082.png', 0),
(6, 0, 1, '1576503203santapau.svg', 0),
(7, 0, 1, '1576499157idiap.svg', 0),
(8, 0, 1, '1576499179ias.svg', 0),
(9, 0, 1, '1576499199genejusticia.svg', 0),
(10, 0, 1, '1579693715departcatalunyasalut.png', 0),
(11, 0, 1, '1579693856pere-mata.gif', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `equipo_content`
--

CREATE TABLE `equipo_content` (
  `id` int(11) NOT NULL,
  `idioma` varchar(2) NOT NULL,
  `text_titulo` varchar(250) NOT NULL COMMENT 'Título',
  `textarea_intro` text NOT NULL COMMENT 'Texto intro',
  `textarea_investigadores` text NOT NULL COMMENT 'Investigadores',
  `textarea_investigadores_extra` text NOT NULL COMMENT 'Investigadores Extra'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `equipo_content`
--

INSERT INTO `equipo_content` (`id`, `idioma`, `text_titulo`, `textarea_intro`, `textarea_investigadores`, `textarea_investigadores_extra`) VALUES
(1, 'ca', '', '', '', ''),
(1, 'en', '', '', '', ''),
(1, 'es', 'Consorcio MAR Parque de Salud de Barcelona', 'Dos grupos dentro del Parque de Salud Mar participan del proyecto. El primero centra su investigaci&oacute;n en la detecci&oacute;n de nuevas necesidades en el sistema de atenci&oacute;n sanitaria i en la utilizaci&oacute;n de los servicios sanitarios p&uacute;blicos, concretamente en referencia a trastornos de salud mental. El segundo, forma parte del Hospital del Mar, que realiza la atenci&oacute;n urgente psiqui&aacute;trica de una buena parte de Barcelona.', '<a href=\"https://www.imim.es/programesrecerca/epidemiologia/grss.html\" target=\"_blank\" rel=\"noopener\">Alonso J, Alayo I, Ballester L, Blasco MJ, De In&eacute;s A, Gomez J, Mortier P, Pu&eacute;rtolas B, Vilagut G.</a>', '<a href=\"https://www.imim.es/programesrecerca/neurociencies/salut_mental.html\" target=\"_blank\" rel=\"noopener\">P&eacute;rez V, Campillo MT, Dom&iacute;nguez B, Elices M, Su&aacute;rez R.</a>'),
(2, 'ca', '', '', '', ''),
(2, 'en', '', '', '', ''),
(2, 'es', 'Consorcio Hospitalario del Parc Taulí', 'El grupo tiene como objetivo la identificaci&oacute;n y el estudio de factores de riesgo asociados a enfermedades mentales en la comunidad, integrando neurociencias b&aacute;sicas y conocimientos cl&iacute;nicos, con perspectiva poblacional. La inclusi&oacute;n y evaluaci&oacute;n cl&iacute;nica de pacientes en Parc Taul&iacute;-Hospital Universitario-UAB, la realizar&aacute;n los investigadores del proyecto en el Servicio de Salud Mental.', '', 'Palao D, Cardoner N, Aguilar E, Cebri&agrave; A, Escayola A, Gomez N, Guinovart M, Merodio I, Montoro M, Parra-Uribe I, Via E, Vicent M.'),
(3, 'ca', '', '', '', ''),
(3, 'en', '', '', '', ''),
(3, 'es', 'Agència de Qualitat i Avaluació Sanitàries de Catalunya', 'Agencia de Calidad Sanitaria de Catalu&ntilde;a es una entidad p&uacute;blica del Dept. de Salud de Catalu&ntilde;a cuya misi&oacute;n es generar conocimiento cient&iacute;fico y relevante para contribuir a la mejora de la calidad, seguridad y sostenibilidad del sistema sanitario, con el objetivo principal de apoyar el proceso de toma de decisiones de los ciudadanos, profesionales, gestores y responsables pol&iacute;ticos', '<a href=\"http://aquas.gencat.cat/ca/inici\" target=\"_blank\" rel=\"noopener\">Garc&iacute;a-Alt&eacute;s A, Colls C.</a>', ''),
(4, 'ca', '', '', '', ''),
(4, 'en', '', '', '', ''),
(4, 'es', 'Departament de Salut de la Generalitat de Cataluña', 'Es el responsable de la gesti&oacute;n del registro del CRS, con una amplia experiencia de las bases de datos del CMBD de salud mental.', '<a href=\"http://salutweb.gencat.cat/ca/inici/\" target=\"_blank\" rel=\"noopener\">Molina C, Prat B.</a>', ''),
(5, 'ca', '', '', '', ''),
(5, 'en', '', '', '', ''),
(5, 'es', 'Institut Pere Mata Reus', 'Es el Hospital Universitario de referencia para las urgencias psiqui&aacute;tricas de Tarragona y gestiona todos los servicios de salud mental de la provincia. Es uno de los hospitales que ya ha implementado el CRS y que dispone de todos los medios para la selecci&oacute;n y evaluaci&oacute;n de los casos. Cuenta con el Institut de Recerca Pere Virgili., en el que trabaja el grupo CIBERSAM 190 que lidera la genetista Elisabet Vilella.', 'Boronat S, Bravo S, Pascual A, Vilella E.', ''),
(6, 'ca', '', '', '', ''),
(6, 'en', '', '', '', ''),
(6, 'es', 'Hospital de la Santa Creu i Sant Pau', 'Es el Hospital Universitario - UAB de referencia para urgencias psiqui&aacute;tricas de una amplia zona de Barcelona y, su grupo de suicidio ha sido el pionero en la implementaci&oacute;n de CRS. Cuenta tambi&eacute;n con los investigadores experimentados en estudios de neuroimagen en depresi&oacute;n, integrados en el Institut de Investigaci&oacute; Biom&egrave;dica Hospital de Sant Pau (IIB-Sant Pau).', '', '<a href=\"http://www.recercasantpau.cat/grup/psiquiatria-clinica/\" target=\"_blank\" rel=\"noopener\" title=\"sant pau\"><strong>Portella M</strong>, Fern&aacute;ndez A, Marin E.</a>'),
(7, 'ca', '', '', '', ''),
(7, 'en', '', '', '', ''),
(7, 'es', 'IDIAP Jordi Gol', 'Instituto de Investigaci&oacute;n en Atenci&oacute;n Primaria &ldquo;Jordi Gol&rdquo; dispone de personal con capacidad suficiente para manejar bases de datos que vinculen datos provenientes de diferentes fuentes (historia cl&iacute;nica, CMBD, Mortalidad, etc.), desde la selecci&oacute;n de las variables, a su depuraci&oacute;n y validaci&oacute;n.', '<a href=\"https://www.idiapjgol.org/index.php/ca/recerca/grups-recerca.html\" target=\"_blank\" rel=\"noopener\">Morros R.</a>', ''),
(8, 'ca', '', '', '', ''),
(8, 'en', '', '', '', ''),
(8, 'es', 'Institut d\'Assistència Sanitària de Girona (IAS-G)', '<p>Los investigadores del estudio est&aacute;n adheridos al Instituto de Recerca Biom&egrave;dica de Girona - IDIBGI -&nbsp; realizando el trabajo de campo en las urgencias de psiquiatr&iacute;a del hospital de Santa Caterina del Instituto de&nbsp;Assist&egrave;ncia&nbsp;Sanit&agrave;ria - IAS- de Girona; siendo &eacute;ste el centro de referencia que provee de servicios p&uacute;blicos de Salud Mental en la provincia de Girona.</p>', '', '<a href=\"https://idibgi.org/\" target=\"_blank\" rel=\"noopener\"><strong>Cid J</strong>, Broncano M, Frigola E, N&uacute;&ntilde;ez J.</a>'),
(9, 'ca', '', '', '', ''),
(9, 'en', '', '', '', ''),
(9, 'es', 'Institut de Medicina Legal i Ciències Forenses de Catalunya (IMLCFC)', 'Instituto de Medicina Legal y Ciencias Forenses de Catalu&ntilde;a es responsable de la generaci&oacute;n y mantenimiento del registro propio de mortalidad per causas externas, entre ellas el suicidio, siendo el IMLCFC la &uacute;nica instituci&oacute;n catalana que genera y dispone de dichos datos.', '<a href=\"http://administraciojusticia.gencat.cat/ca/seccions_tematiques/medicina_forense?destacat=1\" target=\"_blank\" rel=\"noopener\">P&eacute;rez RM, Gen&eacute; M.</a>', ''),
(10, 'ca', '', '', '', ''),
(10, 'en', '', '', '', ''),
(10, 'es', 'Departament de Salut de la Generalitat de Catalunya', '<p>Es el responsable de la creaci&oacute;n, implementaci&oacute;n y gesti&oacute;n del registro del CRS, con una amplia experiencia en la gesti&oacute;n y explotaci&oacute;n de bases de datos de salud mental.</p>', '<p><a href=\"http://salutweb.gencat.cat/ca/inici/\" target=\"_blank\" rel=\"noopener\">Blanch J, Prat Pubill B.;</a></p>', ''),
(11, 'ca', '', '', '', ''),
(11, 'en', '', '', '', ''),
(11, 'es', 'Hospital Universitari Institut Pere Mata', '<p>Es el Hospital Universitario de referencia de urgencias y de todos los servicios de salud mental&nbsp;de la regi&oacute;n sanitaria Camp de Tarragona. Es uno de los hospitales que ya ha implementado el CRS y que dispone de todos los medios para la selecci&oacute;n y evaluaci&oacute;n de los casos.&nbsp;Junto con los hospitales generales de la provincia forma parte del Institut d\'Investigaci&oacute;&nbsp;Sanit&agrave;ria Pere Virgili.&nbsp;El grupo de investigaci&oacute;n liderado por Elisabet Vilella tambi&eacute;n forma parte de la red de investigaci&oacute;n estatal CIBERSAM.</p>', '', '<p><a href=\"http://www.peremata.cat/cat/tree/recerca.html\" target=\"_blank\" rel=\"noopener\">Vilella E., Bernardo S., Pascual A., Bravo S., Guti&eacute;rrez A., Arrufat F. X.;</a></p>');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ficherosnoticias`
--

CREATE TABLE `ficherosnoticias` (
  `id_elem` int(11) NOT NULL,
  `foto` varchar(250) NOT NULL,
  `orden` int(11) NOT NULL,
  `id` int(11) NOT NULL,
  `idioma` varchar(2) NOT NULL,
  `nombre` varchar(250) NOT NULL,
  `fecha` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ficherospublicaciones`
--

CREATE TABLE `ficherospublicaciones` (
  `id_elem` int(11) NOT NULL,
  `foto` varchar(250) NOT NULL,
  `orden` int(11) NOT NULL,
  `id` int(11) NOT NULL,
  `idioma` varchar(2) NOT NULL,
  `nombre` varchar(250) NOT NULL,
  `fecha` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `ficherospublicaciones`
--

INSERT INTO `ficherospublicaciones` (`id_elem`, `foto`, `orden`, `id`, `idioma`, `nombre`, `fecha`) VALUES
(1, 'bkslider01.jpg', 0, 1, 'es', '', '2017-09-04 17:12:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fotosnoticias`
--

CREATE TABLE `fotosnoticias` (
  `id_elem` int(11) NOT NULL,
  `foto` varchar(250) NOT NULL,
  `orden` int(11) NOT NULL,
  `id` int(11) NOT NULL,
  `idioma` varchar(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `fotosnoticias`
--

INSERT INTO `fotosnoticias` (`id_elem`, `foto`, `orden`, `id`, `idioma`) VALUES
(5, '12634.jpg', 0, 55, ''),
(9, '1580202176diego-ph-szyrezsj-fe-unsplash.jpg', 0, 56, ''),
(6, '1580202176diego-ph-szyrezsj-fe-unsplash.jpg', 0, 57, '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fotospublicaciones`
--

CREATE TABLE `fotospublicaciones` (
  `id_elem` int(11) NOT NULL,
  `foto` varchar(250) NOT NULL,
  `orden` int(11) NOT NULL,
  `id` int(11) NOT NULL,
  `idioma` varchar(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fotosslide`
--

CREATE TABLE `fotosslide` (
  `id_elem` int(11) NOT NULL,
  `foto` varchar(250) NOT NULL,
  `orden` int(11) NOT NULL,
  `id` int(11) NOT NULL,
  `idioma` varchar(2) NOT NULL,
  `nombre` varchar(250) NOT NULL COMMENT 'Text',
  `descripcion` text NOT NULL COMMENT 'Descripción',
  `altimg` varchar(250) NOT NULL COMMENT 'Alt',
  `link` varchar(250) NOT NULL COMMENT 'Link'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `fotosslide`
--

INSERT INTO `fotosslide` (`id_elem`, `foto`, `orden`, `id`, `idioma`, `nombre`, `descripcion`, `altimg`, `link`) VALUES
(5, 'pascal-debrunner-634122-unsplash.jpg', 0, 6, 'es', '', '', '', ''),
(5, 'johannes-plenio-629984-unsplash.jpg', 1, 7, 'es', '', '', '', ''),
(5, 'john-westrock-638048-unsplash.jpg', 2, 8, 'es', '', '', '', ''),
(1, 'esslider.jpg', 0, 21, 'es', 'Incrementando el conocimiento sobre la prevención del suicidio en Cataluña', 'Codi Risc es un estudio coordinado basado en la intervención Codi Risc Suicidi (CRS) del Deparatment de Salut de la Generalitat. Estudia el comportamiento epidemiológico e identifica factores de riesgo para dibujar unas líneas de mejora en la gestión de la prevención de la conducta suicida.', 'Proyecto Codi Risc', ''),
(1, 'esdepsalut.jpg', 1, 22, 'es', 'Incrementando el conocimiento sobre la prevención del suicidio en Cataluña', 'Codi Risc es un estudio coordinado basado en la intervención Codi Risc Suicidi (CRS) del Deparatment de Salut de la Generalitat. Estudia el comportamiento epidemiológico e identifica factores de riesgo para dibujar unas líneas de mejora en la gestión de la prevención de la conducta suicida.', 'Proyecto Codi Risc', ''),
(1, 'esinstitut-pere-matacpu.jpg', 2, 23, 'es', 'Incrementando el conocimiento sobre la prevención del suicidio en Cataluña', 'Codi Risc es un estudio coordinado basado en la intervención Codi Risc Suicidi (CRS) del Deparatment de Salut de la Generalitat. Estudia el comportamiento epidemiológico e identifica factores de riesgo para dibujar unas líneas de mejora en la gestión de la prevención de la conducta suicida.', 'Proyecto Codi Risc', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mails`
--

CREATE TABLE `mails` (
  `id` int(11) NOT NULL,
  `fecha` datetime NOT NULL,
  `text_email` varchar(250) NOT NULL,
  `text_body` text NOT NULL,
  `error` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `mails`
--

INSERT INTO `mails` (`id`, `fecha`, `text_email`, `text_body`, `error`) VALUES
(1, '2020-02-27 16:22:06', 'edu@latevaweb.com', '<strong>Nombre:</strong> LTW nombre<br /><strong>Teléfono:</strong> 666777888<br /><strong>Email:</strong> edu@latevaweb.com<br /><strong>Comentarios:</strong> TEST<br />', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `noticias`
--

CREATE TABLE `noticias` (
  `id` int(11) NOT NULL,
  `checkbox_visible` int(11) NOT NULL COMMENT 'Visible',
  `text_fecha` datetime NOT NULL COMMENT 'Fecha|Data',
  `fotouploader_fotosnoticias` varchar(250) DEFAULT NULL COMMENT 'Fotografías'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `noticias`
--

INSERT INTO `noticias` (`id`, `checkbox_visible`, `text_fecha`, `fotouploader_fotosnoticias`) VALUES
(5, 0, '2015-11-30 00:00:00', ''),
(6, 1, '2017-03-21 00:00:00', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `noticias_content`
--

CREATE TABLE `noticias_content` (
  `id` int(11) NOT NULL,
  `idioma` varchar(2) NOT NULL,
  `text_menu` varchar(250) NOT NULL COMMENT 'Menú',
  `text_titulo` varchar(250) NOT NULL COMMENT 'Título',
  `textarea_intro` text NOT NULL COMMENT 'Texto intro',
  `textarea_texto` text NOT NULL COMMENT 'Texto',
  `notiny_video` text NOT NULL COMMENT 'Vídeo',
  `fileuploader_ficherosnoticias` varchar(250) NOT NULL COMMENT 'Ficheros',
  `text_ht_url` varchar(250) NOT NULL COMMENT 'HT Url',
  `text_ht_title` varchar(250) NOT NULL COMMENT 'HT Título',
  `notiny_ht_description` text NOT NULL COMMENT 'HT Descripción'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `noticias_content`
--

INSERT INTO `noticias_content` (`id`, `idioma`, `text_menu`, `text_titulo`, `textarea_intro`, `textarea_texto`, `notiny_video`, `fileuploader_ficherosnoticias`, `text_ht_url`, `text_ht_title`, `notiny_ht_description`) VALUES
(5, 'ca', '', '', '', '', '', '', '', '', ''),
(5, 'en', '', '', '', '', '', '', '', '', ''),
(5, 'es', '', 'Lorem ipsum dolor sit amet', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit</p>', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut vulputate vestibulum lorem eu imperdiet. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nunc vel sapien quis eros pulvinar interdum. Sed mattis augue tortor, quis malesuada ipsum volutpat ac. Nullam a pulvinar nunc. Sed tincidunt id augue eget placerat. Vivamus hendrerit nisi sed ligula malesuada scelerisque.</p>\r\n<p>In scelerisque lorem sed velit suscipit, ut malesuada enim dapibus. Sed sed mi eget elit tristique iaculis. Suspendisse nisi dolor, ultrices in porttitor et, mattis nec lacus. Ut tincidunt, magna vel tempor egestas, est leo ultricies massa, vitae aliquet sem enim suscipit libero. Sed ornare consequat eleifend. Vivamus vitae augue tincidunt, tempor nunc vitae, sollicitudin sem. Aenean ac enim eros. Mauris efficitur felis ac neque accumsan luctus. Ut ut nibh porta, faucibus lorem vitae, mollis turpis.</p>\r\n<p>Sed ultricies, ligula id iaculis laoreet, ante magna cursus lectus, quis aliquam justo felis ac leo. Proin iaculis et nunc id porta. Maecenas magna ligula, interdum nec commodo ut, scelerisque et mi. Pellentesque aliquet, libero nec tempus egestas, risus elit lobortis odio, porttitor imperdiet eros dolor sit amet massa. Morbi nec diam tincidunt, molestie enim lobortis, molestie mi. Pellentesque luctus at augue nec ultrices. Maecenas nec suscipit est, eget convallis ex. Ut ultricies lobortis ipsum, sit amet mollis arcu blandit quis. In auctor ante nec imperdiet tristique. Nulla ut augue diam.</p>', '', '', '', '', ''),
(6, 'ca', '', '', '', '', '', '', '', '', ''),
(6, 'en', '', '', '', '', '', '', '', '', ''),
(6, 'es', '', '¡CodiRisc estrena nueva página web! ', '<p>Bienvenidos a la nueva p&aacute;gina web del proyecto CodiRisc, d&oacute;nde podr&eacute;is encontrar toda la informaci&oacute;n sobre el dise&ntilde;o, desarrollo y novedades sobre el proyecto, as&iacute; como la informaci&oacute;n m&aacute;s relevante sobre la conducta suicida.</p>', '<p><span>En <strong>CodiRisc</strong> estrenamos nueva p&aacute;gina web como uno de los medios principales para llevar la comunicaci&oacute;n del proyecto. </span></p>\r\n<p><span>En una sociedad cada vez m&aacute;s informatizada, cre&iacute;mos imprescindible tener presencia en la red. De esta forma la informaci&oacute;n queda al servicio de quien la necesite; tanto cient&iacute;ficos experimentados, c&oacute;mo estudiantes o, simplemente, gente interesada en el tema de estudio puede acceder de forma sencilla y abierta, desde cualquier parte del mundo. &Eacute;ste fue un punto muy importante al considerar la nueva p&aacute;gina. Los investigadores que formamos parte del proyecto creemos firmemente en la necesidad de trasladar la ciencia a un p&uacute;blico m&aacute;s amplio, adem&aacute;s de dar a conocer nuestra investigaci&oacute;n a otros grupos con los que poder establecer relaciones profesionales y posibles colaboraciones. </span></p>\r\n<p><span>La web se estructura de forma que las primeras p&aacute;ginas son de car&aacute;cter m&aacute;s divulgativo y contienen informaci&oacute;n relacionada con la intenci&oacute;n y motivaci&oacute;n del proyecto, informaci&oacute;n general sobre el suicidio y los distintos planes de prevenci&oacute;n llevados a cabo por el gobierno y otros apartados que incluyen noticias relevantes relacionadas con el proyecto o el tema en cuesti&oacute;n y datos generales de contacto y organizaci&oacute;n. Conforme el usuario se adentra en la estructura de la p&aacute;gina, hay informaci&oacute;n m&aacute;s espec&iacute;fica sobre el proyecto: objetivos, metodolog&iacute;a, &eacute;tica, etc. Adem&aacute;s, contiene un apartado espec&iacute;fico para consultar todas las publicaciones relacionadas.</span></p>\r\n<p><span>El dise&ntilde;o ha sido espec&iacute;ficamente pensado para reducir el estigma social existente alrededor del suicidio y las personas que lo padecen: colores vibrantes, un car&aacute;cter fresco y un apartado para todas aquellas personas que accedan a la p&aacute;gina web en un momento complicado, hayan tenido pensamientos suicidas y necesiten contactos o recursos como ayuda.&nbsp; </span></p>\r\n<p><span>Iremos actualizando habitualmente la p&aacute;gina con novedades en investigaci&oacute;n y los art&iacute;culos que se vayan publicando en revistas cient&iacute;ficas. Se publicar&aacute; tambi&eacute;n en las redes sociales de los centros participantes. &iexcl;As&iacute; que estad atentos!</span></p>\r\n<p></p>', '', '', 'codirisc-estrena-web', 'CodiRisc estrena nueva página web', 'Bienvenidos a la nueva página web del proyecto CodiRisc, dónde podréis encontrar toda la información sobre el diseño, desarrollo y novedades sobre el proyecto, así como la información más relevante sobre la conducta suicida. ');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `publicaciones`
--

CREATE TABLE `publicaciones` (
  `id` int(11) NOT NULL,
  `orden` int(11) NOT NULL,
  `checkbox_visible` int(11) NOT NULL COMMENT 'Visible',
  `text_fecha` datetime NOT NULL COMMENT 'Fecha|Data',
  `select_categorias_publicaciones` int(11) NOT NULL COMMENT 'Select categorías',
  `select_tags_publicaciones` int(11) NOT NULL COMMENT 'Select Tags',
  `textarea_investigadores` text NOT NULL COMMENT 'Investigadores',
  `text_link` varchar(250) NOT NULL COMMENT 'Link'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `publicaciones`
--

INSERT INTO `publicaciones` (`id`, `orden`, `checkbox_visible`, `text_fecha`, `select_categorias_publicaciones`, `select_tags_publicaciones`, `textarea_investigadores`, `text_link`) VALUES
(3, 0, 1, '2019-12-16 00:00:00', 2, 0, '<strong>Alonso J</strong>, Alayo I, Ballester L, Blasco MJ, De In&eacute;s A, Gomez J, Mortier P, Pu&eacute;rtolas B, Vilagut G.<br />P&eacute;rez V, Campillo MT, Dom&iacute;nguez B, Elices M, Su&aacute;rez R.', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `publicaciones_content`
--

CREATE TABLE `publicaciones_content` (
  `id` int(11) NOT NULL,
  `idioma` varchar(2) NOT NULL,
  `text_menu` varchar(250) NOT NULL COMMENT 'Menú',
  `text_titulo` varchar(250) NOT NULL COMMENT 'Título',
  `textarea_intro` text NOT NULL COMMENT 'Texto intro',
  `textarea_texto` text NOT NULL COMMENT 'Texto',
  `notiny_video` text NOT NULL COMMENT 'Vídeo',
  `fileuploader_ficherospublicaciones` varchar(250) NOT NULL COMMENT 'Subida de ficheros',
  `text_ht_url` varchar(250) NOT NULL COMMENT 'HT Url',
  `text_ht_title` varchar(250) NOT NULL COMMENT 'HT Título',
  `notiny_ht_description` text NOT NULL COMMENT 'HT Descripción'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `publicaciones_content`
--

INSERT INTO `publicaciones_content` (`id`, `idioma`, `text_menu`, `text_titulo`, `textarea_intro`, `textarea_texto`, `notiny_video`, `fileuploader_ficherospublicaciones`, `text_ht_url`, `text_ht_title`, `notiny_ht_description`) VALUES
(3, 'ca', '', '', '', '', '', '', '', '', ''),
(3, 'en', '', '', '', '', '', '', '', '', ''),
(3, 'es', '', 'Clinical Predictors and Biomarkers of Suicide Attempts in Patients Registered Under the Suicide Risk Protocol: The CODIRISC Project', '', '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `slide`
--

CREATE TABLE `slide` (
  `id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `slide_content`
--

CREATE TABLE `slide_content` (
  `id` int(11) NOT NULL,
  `idioma` varchar(2) NOT NULL,
  `bannuploader_fotosslide` int(11) NOT NULL COMMENT 'Fotos'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `slide_content`
--

INSERT INTO `slide_content` (`id`, `idioma`, `bannuploader_fotosslide`) VALUES
(1, 'es', 0),
(1, 'ca', 0),
(1, 'en', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tags_publicaciones`
--

CREATE TABLE `tags_publicaciones` (
  `id` int(11) NOT NULL,
  `text_titulo` varchar(250) NOT NULL COMMENT 'Nombre (interno)',
  `select_tags_publicaciones` int(11) NOT NULL COMMENT 'Categoría padre',
  `orden` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tags_publicaciones_content`
--

CREATE TABLE `tags_publicaciones_content` (
  `id` int(11) NOT NULL,
  `idioma` varchar(250) NOT NULL,
  `text_menu` varchar(250) NOT NULL COMMENT 'Menú',
  `text_nombre` varchar(250) NOT NULL COMMENT 'Nombre',
  `textarea_description` text NOT NULL COMMENT 'Descripción',
  `text_ht_url` varchar(250) NOT NULL COMMENT 'HT Url',
  `text_ht_title` varchar(250) NOT NULL COMMENT 'HT Title',
  `notiny_ht_description` varchar(450) NOT NULL COMMENT 'HT Description'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `textos`
--

CREATE TABLE `textos` (
  `id` int(11) NOT NULL,
  `text_nodisabled` varchar(250) NOT NULL,
  `last_modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `textos`
--

INSERT INTO `textos` (`id`, `text_nodisabled`, `last_modified`) VALUES
(3, 'COMMON_ANTERIOR', '0000-00-00 00:00:00'),
(4, 'COMMON_LEERMAS', '0000-00-00 00:00:00'),
(5, 'COMMON_MASINFORMACION', '0000-00-00 00:00:00'),
(6, 'COMMON_PAGINAS', '0000-00-00 00:00:00'),
(7, 'COMMON_SIGUIENTE', '0000-00-00 00:00:00'),
(9, 'COMMON_VERMAS', '0000-00-00 00:00:00'),
(10, 'COMMON_VERTODAS', '0000-00-00 00:00:00'),
(11, 'COMMON_VERTODOS', '0000-00-00 00:00:00'),
(13, 'contacto.php', '0000-00-00 00:00:00'),
(14, 'empresa.php', '0000-00-00 00:00:00'),
(29, 'MENU_CONTACTO', '0000-00-00 00:00:00'),
(32, 'MENU_EMPRESA', '0000-00-00 00:00:00'),
(34, 'MENU_INICIO', '0000-00-00 00:00:00'),
(35, 'MENU_NOTICIAS', '0000-00-00 00:00:00'),
(36, 'MENU_PRODUCTOS', '0000-00-00 00:00:00'),
(38, 'MENU_SERVICIOS', '0000-00-00 00:00:00'),
(41, 'COMMON_VOLVER', '0000-00-00 00:00:00'),
(42, 'COMMON_NOTALEGAL', '0000-00-00 00:00:00'),
(43, 'COMMON_MARKETINGONLINE', '0000-00-00 00:00:00'),
(44, 'COMMON_MAPAMASGRANDE', '0000-00-00 00:00:00'),
(49, 'COMMON_ENVIAR', '0000-00-00 00:00:00'),
(50, 'COMMON_ENVIOERROR', '0000-00-00 00:00:00'),
(51, 'COMMON_ENVIOOK', '0000-00-00 00:00:00'),
(52, 'COMMON_LABELCOMENTARIOS', '0000-00-00 00:00:00'),
(53, 'COMMON_LABELMAIL', '0000-00-00 00:00:00'),
(54, 'COMMON_LABELNOMBRE', '0000-00-00 00:00:00'),
(55, 'COMMON_LABELTELEFONO', '0000-00-00 00:00:00'),
(62, 'CONTACTO_H1', '0000-00-00 00:00:00'),
(63, 'CONTACTO_TEXTO', '0000-00-00 00:00:00'),
(64, 'EMPRESA_H1', '0000-00-00 00:00:00'),
(67, 'COMMON_ACEPTAR', '0000-00-00 00:00:00'),
(76, 'politica-cookies.php', '0000-00-00 00:00:00'),
(78, 'COMMON_MAILAUTORESPTEXTO', '0000-00-00 00:00:00'),
(79, 'COMMON_MAILAUTORESPSUBJET', '0000-00-00 00:00:00'),
(80, 'COMMON_POLITICA_COOKIES_TEXTE', '0000-00-00 00:00:00'),
(81, 'COMMON_POLITICA_COOKIES_INFO', '0000-00-00 00:00:00'),
(82, 'COMMON_POLITICA_COOKIES_ACEPTAR', '0000-00-00 00:00:00'),
(83, 'COMMON_VALIDA_CAMPOS', '0000-00-00 00:00:00'),
(84, 'COMMON_VALIDA_EMAIL', '0000-00-00 00:00:00'),
(90, 'index.php', '0000-00-00 00:00:00'),
(92, '', '0000-00-00 00:00:00'),
(100, 'listado_noticias.php', '0000-00-00 00:00:00'),
(105, 'COMMON_LABELADJUNTAR', '0000-00-00 00:00:00'),
(106, 'PRODUCTOS_H1', '0000-00-00 00:00:00'),
(107, 'NOTICIAS_H1', '0000-00-00 00:00:00'),
(115, 'HOME_H1', '0000-00-00 00:00:00'),
(137, 'COMMON_VALIDA_TERMS', '0000-00-00 00:00:00'),
(139, 'COMMON_NOHAYRESULTADOS', '0000-00-00 00:00:00'),
(157, 'COMMON_MAILTO', '0000-00-00 00:00:00'),
(158, 'politica-legal.php', '0000-00-00 00:00:00'),
(164, 'CONFIG_DIRECCION', '0000-00-00 00:00:00'),
(171, 'COMMON_ACEPTOTERMINOS', '0000-00-00 00:00:00'),
(172, 'COMMON_CERRAR', '0000-00-00 00:00:00'),
(173, 'COMMON_AMPLIAR', '0000-00-00 00:00:00'),
(174, 'COMMON_BUSCAR', '0000-00-00 00:00:00'),
(177, 'CONFIG_METAS_KW', '0000-00-00 00:00:00'),
(182, 'HOME_METAS_TIT', '0000-00-00 00:00:00'),
(183, 'HOME_METAS_DESC', '0000-00-00 00:00:00'),
(186, 'CONTACTO_METAS_TIT', '0000-00-00 00:00:00'),
(187, 'CONTACTO_METAS_DESC', '0000-00-00 00:00:00'),
(189, 'EMPRESA_METAS_TIT', '0000-00-00 00:00:00'),
(190, 'EMPRESA_METAS_DESC', '0000-00-00 00:00:00'),
(191, 'EMPRESA_METAS_ALT', '0000-00-00 00:00:00'),
(192, 'MENU_METAS_INICIO_TIT', '0000-00-00 00:00:00'),
(193, 'MENU_METAS_EMPRESA_TIT', '0000-00-00 00:00:00'),
(194, 'MENU_METAS_INICIO_TITTLE', '0000-00-00 00:00:00'),
(195, 'MENU_METAS_EMPRESA_TITTLE', '0000-00-00 00:00:00'),
(196, 'MENU_METAS_NOTICIAS_TITTLE', '0000-00-00 00:00:00'),
(198, 'MENU_METAS_CONTACTO_TITTLE', '0000-00-00 00:00:00'),
(199, 'COOKIES_METAS_TIT', '0000-00-00 00:00:00'),
(200, 'COOKIES_METAS_DESC', '0000-00-00 00:00:00'),
(201, 'LEGAL_METAS_TIT', '0000-00-00 00:00:00'),
(202, 'LEGAL_METAS_DESC', '0000-00-00 00:00:00'),
(205, 'SOCIAL_FACEBOOK_LINK', '0000-00-00 00:00:00'),
(206, 'SOCIAL_FACEBOOK_TITLE', '0000-00-00 00:00:00'),
(207, 'SOCIAL_TWITTER_LINK', '0000-00-00 00:00:00'),
(208, 'SOCIAL_TWITTER_TITLE', '0000-00-00 00:00:00'),
(209, 'SOCIAL_GOOGLE_LINK', '0000-00-00 00:00:00'),
(210, 'SOCIAL_GOOGLE_TITLE', '0000-00-00 00:00:00'),
(211, 'SOCIAL_LINKEDIN_LINK', '0000-00-00 00:00:00'),
(212, 'SOCIAL_LINKEDIN_TITLE', '0000-00-00 00:00:00'),
(213, 'SOCIAL_INSTAGRAM_LINK', '0000-00-00 00:00:00'),
(214, 'SOCIAL_INSTAGRAM_TITLE', '0000-00-00 00:00:00'),
(215, 'SOCIAL_YOUTUBE_LINK', '0000-00-00 00:00:00'),
(216, 'SOCIAL_YOUTUBE_TITLE', '0000-00-00 00:00:00'),
(217, 'SOCIAL_PINTEREST_LINK', '0000-00-00 00:00:00'),
(218, 'SOCIAL_PINTEREST_TITLE', '0000-00-00 00:00:00'),
(219, 'SOCIAL_VIMEO_LINK', '0000-00-00 00:00:00'),
(220, 'SOCIAL_VIMEO_TITLE', '0000-00-00 00:00:00'),
(221, 'listado_productos.php', '0000-00-00 00:00:00'),
(224, 'PRODUCTOS_METAS_TIT', '0000-00-00 00:00:00'),
(225, 'PRODUCTOS_METAS_DESC', '0000-00-00 00:00:00'),
(227, 'NOTICIAS_METAS_TIT', '0000-00-00 00:00:00'),
(228, 'NOTICIAS_METAS_DESC', '0000-00-00 00:00:00'),
(229, 'PAGE404_METAS_TIT', '0000-00-00 00:00:00'),
(230, 'PAGE404_METAS_DESC', '0000-00-00 00:00:00'),
(231, 'PAGE404_H1', '0000-00-00 00:00:00'),
(232, 'PAGE404_TEXTO', '0000-00-00 00:00:00'),
(233, 'COOKIES_TEXT', '0000-00-00 00:00:00'),
(234, 'LEGAL_TEXT', '0000-00-00 00:00:00'),
(235, 'SOCIAL_TWITTER_USER', '0000-00-00 00:00:00'),
(236, 'LEGAL_H1', '0000-00-00 00:00:00'),
(237, 'COOKIES_H1', '0000-00-00 00:00:00'),
(238, 'EMPRESA_TEXT', '0000-00-00 00:00:00'),
(240, 'HOME_PRODUCTS_H2', '0000-00-00 00:00:00'),
(241, 'HOME_NEWS_H2', '0000-00-00 00:00:00'),
(242, 'COMMON_LABELCOMERCIALINFO', '0000-00-00 00:00:00'),
(243, 'COMMON_COMERCIALINFO_TEXT', '0000-00-00 00:00:00'),
(244, 'COMMON_LABELCV', '0000-00-00 00:00:00'),
(245, 'PRIVACIDAD_METAS_TIT', '0000-00-00 00:00:00'),
(246, 'PRIVACIDAD_METAS_DESC', '0000-00-00 00:00:00'),
(247, 'PRIVACIDAD_H1', '0000-00-00 00:00:00'),
(248, 'PRIVACIDAD_TEXT', '0000-00-00 00:00:00'),
(249, 'politica-privacidad.php', '0000-00-00 00:00:00'),
(250, 'COMMON_AMPLIARPRIVACIDAD_TEXT', '0000-00-00 00:00:00'),
(251, 'COMMON_PRIVACIDAD', '0000-00-00 00:00:00'),
(252, 'COMMON_COOKIES', '0000-00-00 00:00:00'),
(253, 'NOTICIAS_RELACIONADAS_H2', '0000-00-00 00:00:00'),
(254, 'COMMON_VENDEDEOR', '0000-00-00 00:00:00'),
(255, 'GOOGLE_RECAPTCHA_ERROR', '0000-00-00 00:00:00'),
(256, 'contacto-ok.php', '0000-00-00 00:00:00'),
(257, 'MENU_PUBLICACIONES', '0000-00-00 00:00:00'),
(258, 'MENU_TEAM', '0000-00-00 00:00:00'),
(259, 'MENU_ABOUT', '0000-00-00 00:00:00'),
(260, 'team.php', '0000-00-00 00:00:00'),
(261, 'aboutSuicide.php', '0000-00-00 00:00:00'),
(262, 'HOME_EMPRESA_TITLE', '0000-00-00 00:00:00'),
(263, 'HOME_EMPRESA_TEXT', '0000-00-00 00:00:00'),
(264, 'HOME_EMPRESA_TITLE_2', '0000-00-00 00:00:00'),
(265, 'HOME_EMPRESA_TEXT_2', '0000-00-00 00:00:00'),
(266, 'COMMON_SABERMAS', '0000-00-00 00:00:00'),
(267, 'HOME_SECCIONES_OBJETIVOS', '0000-00-00 00:00:00'),
(268, 'HOME_SECCIONES_ETICA', '0000-00-00 00:00:00'),
(269, 'HOME_SECCIONES_EQUIPO', '0000-00-00 00:00:00'),
(270, 'HOME_SECCIONES_METODOLOGIA', '0000-00-00 00:00:00'),
(271, 'HOME_SECCIONES_INTERVENCION', '0000-00-00 00:00:00'),
(272, 'HOME_SECCIONES_PUBLICACIONES', '0000-00-00 00:00:00'),
(273, 'HOME_NEEDHELP_TITLE', '0000-00-00 00:00:00'),
(274, 'HOME_NEEDHELP_TEXT', '0000-00-00 00:00:00'),
(275, 'HOME_NEEDHELP_TITLE_2', '0000-00-00 00:00:00'),
(276, 'HOME_NEEDHELP_TEXT_2', '0000-00-00 00:00:00'),
(277, 'COMMON_RECURSOS', '0000-00-00 00:00:00'),
(278, 'HOME_NEEDHELP_TITLE_3', '0000-00-00 00:00:00'),
(279, 'HOME_NEEDHELP_112', '0000-00-00 00:00:00'),
(280, 'HOME_NEEDHELP_HOPE', '0000-00-00 00:00:00'),
(281, 'FOOTER_COORDINACION', '0000-00-00 00:00:00'),
(282, 'FOOTER_COORDINACION_TEXT', '0000-00-00 00:00:00'),
(283, 'FOOTER_AREA_INVESTIGADORES', '0000-00-00 00:00:00'),
(284, 'FOOTER_UBICACION', '0000-00-00 00:00:00'),
(285, 'FOOTER_CONTACTO', '0000-00-00 00:00:00'),
(286, 'FOOTER_BOTTOM_END', '0000-00-00 00:00:00'),
(287, 'PUBLICACIONES_TITULO', '0000-00-00 00:00:00'),
(288, 'SEARCH_PLACEHOLDER', '0000-00-00 00:00:00'),
(289, 'FILTER_1_TITLE', '0000-00-00 00:00:00'),
(290, 'FILTER_2_TITLE', '0000-00-00 00:00:00'),
(291, 'FILTER_TODO', '0000-00-00 00:00:00'),
(292, 'FILTER_ARTICULOS', '0000-00-00 00:00:00'),
(293, 'FILTER_CONGRESOS', '0000-00-00 00:00:00'),
(294, 'FILTER_2019', '0000-00-00 00:00:00'),
(295, 'FILTER_2018', '0000-00-00 00:00:00'),
(296, 'FILTER_2017', '0000-00-00 00:00:00'),
(297, 'FILTER_2016', '0000-00-00 00:00:00'),
(298, 'COMMON_ACCEDERWEB', '0000-00-00 00:00:00'),
(299, 'TEAM_TITULO', '0000-00-00 00:00:00'),
(300, 'prevSuicide.php', '0000-00-00 00:00:00'),
(301, 'ABOUT_H1', '0000-00-00 00:00:00'),
(302, 'ABOUT_TEXT', '0000-00-00 00:00:00'),
(303, 'PREVENIR_TEXT', '0000-00-00 00:00:00'),
(304, 'PREVENIR_H1', '0000-00-00 00:00:00'),
(305, 'OBJETIVOS_H1', '0000-00-00 00:00:00'),
(306, 'OBJETIVOS_TEXT', '0000-00-00 00:00:00'),
(307, 'objetivos.php', '0000-00-00 00:00:00'),
(308, 'METODOLOGIA_H1', '0000-00-00 00:00:00'),
(309, 'METODOLOGIA_EPIDEMOLOGICO_TITLE', '0000-00-00 00:00:00'),
(310, 'METODOLOGIA_EPIDEMOLOGICO_TEXT', '0000-00-00 00:00:00'),
(311, 'METODOLOGIA_CLINICO_TITLE', '0000-00-00 00:00:00'),
(312, 'METODOLOGIA_CLINICO_TEXT', '0000-00-00 00:00:00'),
(313, 'metodologia.php', '0000-00-00 00:00:00'),
(314, 'METODOLOGIA_EPIDEMIOLOGICO_TEXT', '0000-00-00 00:00:00'),
(315, 'metodologia-epidemiologico.php', '0000-00-00 00:00:00'),
(316, 'metodologia-clinico.php', '0000-00-00 00:00:00'),
(317, 'METODOLOGIA_EPIDEMOLOGICO_TEXT_INTRO', '0000-00-00 00:00:00'),
(318, 'METODOLOGIA_CLINICO_TEXT_INTRO', '0000-00-00 00:00:00'),
(319, 'intervencion-crs.php', '0000-00-00 00:00:00'),
(320, 'INTERVENCIONCRS_H1', '0000-00-00 00:00:00'),
(321, 'INTERVENCIONCRS_TEXT', '0000-00-00 00:00:00'),
(322, 'ETICA_H1', '0000-00-00 00:00:00'),
(323, 'ETICA_TEXT', '0000-00-00 00:00:00'),
(324, 'etica.php', '0000-00-00 00:00:00'),
(325, 'recursos.php', '0000-00-00 00:00:00'),
(326, 'RECURSOS_H1', '0000-00-00 00:00:00'),
(327, 'RECURSOS_CENTROS', '0000-00-00 00:00:00'),
(328, 'RECURSOS_ACCEDE', '0000-00-00 00:00:00'),
(329, 'RECURSOS_ASOS', '0000-00-00 00:00:00'),
(330, 'RECURSOS_ASOS_TITLE_1', '0000-00-00 00:00:00'),
(331, 'RECURSOS_ASOS_TEXTO_1', '0000-00-00 00:00:00'),
(332, 'RECURSOS_ASOS_TITLE_2', '0000-00-00 00:00:00'),
(333, 'RECURSOS_ASOS_TEXTO_2', '0000-00-00 00:00:00'),
(334, 'RECURSOS_ASOS_TITLE_3', '0000-00-00 00:00:00'),
(335, 'RECURSOS_ASOS_TEXTO_3', '0000-00-00 00:00:00'),
(336, 'COMMON_ACCEDER', '0000-00-00 00:00:00'),
(337, 'COMMON_NO_RESULTS', '0000-00-00 00:00:00'),
(338, 'PREVENIR_METAS_TIT', '0000-00-00 00:00:00'),
(339, 'PREVENIR_METAS_DESC', '0000-00-00 00:00:00'),
(340, 'MENU_METAS_TEAM_TIT', '0000-00-00 00:00:00'),
(341, 'MENU_METAS_ABOUT_TIT', '0000-00-00 00:00:00'),
(342, 'MENU_METAS_NOTICIAS_TIT', '0000-00-00 00:00:00'),
(343, 'MENU_METAS_PUBLICACIONES_TIT', '0000-00-00 00:00:00'),
(344, 'MENU_METAS_CONTACTO_TIT', '0000-00-00 00:00:00'),
(345, 'ABOUT_METAS_TIT', '0000-00-00 00:00:00'),
(346, 'ABOUT_METAS_DESC', '0000-00-00 00:00:00'),
(347, 'ETICA_METAS_TIT', '0000-00-00 00:00:00'),
(348, 'ETICA_METAS_DESC', '0000-00-00 00:00:00'),
(349, 'INTERVENCIONCRS_METAS_TIT', '0000-00-00 00:00:00'),
(350, 'INTERVENCIONCRS_METAS_DESC', '0000-00-00 00:00:00'),
(351, 'METODOLOGIA_METAS_TIT', '0000-00-00 00:00:00'),
(352, 'METODOLOGIA_METAS_DESC', '0000-00-00 00:00:00'),
(353, 'METODOLOGIA_CLINICO_H1', '0000-00-00 00:00:00'),
(354, 'METODOLOGIA_EPIDEMIOLOGICO_H1', '0000-00-00 00:00:00'),
(355, 'METODOLOGIA_CLINICO_METAS_TIT', '0000-00-00 00:00:00'),
(356, 'METODOLOGIA_CLINICO_METAS_DESC', '0000-00-00 00:00:00'),
(357, 'METODOLOGIA_EPIDEMIOLOGICO_METAS_TIT', '0000-00-00 00:00:00'),
(358, 'METODOLOGIA_EPIDEMIOLOGICO_METAS_DESC', '0000-00-00 00:00:00'),
(359, 'RECURSOS_METAS_TIT', '0000-00-00 00:00:00'),
(360, 'RECURSOS_METAS_DESC', '0000-00-00 00:00:00'),
(361, 'FOOTER_AREA_INVESTIGADORES_LINK', '0000-00-00 00:00:00'),
(362, 'HOME_SLIDER_H1', '0000-00-00 00:00:00'),
(363, 'HOME_SLIDER_TEXT', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `textosbackoffice`
--

CREATE TABLE `textosbackoffice` (
  `id` int(11) NOT NULL,
  `text_titulo` varchar(250) NOT NULL COMMENT 'Nombre'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `textosbackoffice`
--

INSERT INTO `textosbackoffice` (`id`, `text_titulo`) VALUES
(30, 'actual'),
(17, 'alerta_borrar'),
(38, 'anterior'),
(14, 'borrar'),
(13, 'btn_buscar'),
(21, 'btn_crear'),
(15, 'btn_editar'),
(41, 'btn_guardar'),
(24, 'btn_modificar'),
(46, 'control'),
(43, 'crear_clave'),
(42, 'crear_registro'),
(4, 'desconectar'),
(52, 'diseno_web'),
(31, 'eliminar'),
(50, 'exportar'),
(3, 'fecha'),
(45, 'filtrar_clave'),
(49, 'importar'),
(6, 'l_crear'),
(5, 'l_listado'),
(7, 'l_no_results'),
(65, 'menu_asociaciones'),
(11, 'menu_categorias_noticias'),
(18, 'menu_categorias_productos'),
(62, 'menu_categorias_publicaciones'),
(22, 'menu_configuracion_general'),
(59, 'menu_configuracion_menu'),
(25, 'menu_configuracion_thumbs'),
(64, 'menu_equipo'),
(60, 'menu_mails'),
(16, 'menu_noticias'),
(19, 'menu_productos'),
(61, 'menu_publicaciones'),
(63, 'menu_tags_publicaciones'),
(20, 'menu_textos'),
(28, 'menu_textos_backoffice'),
(27, 'menu_usuarios'),
(32, 'navegador_no_compatible'),
(36, 'noficheros_actualmente'),
(34, 'nofotos_actualmente'),
(40, 'noitems_actualmente'),
(35, 'ordenar_ficheros'),
(33, 'ordenar_fotos'),
(51, 'orden_guardado_ok'),
(26, 'registro_creado_ok'),
(29, 'registro_modificado_ok'),
(48, 'restaurar'),
(58, 'selecciona'),
(44, 'selectm_noelementosseleccionados'),
(37, 'siguiente'),
(23, 'siguiente_paso'),
(39, 'texto_enriquecido'),
(1, 'titulo'),
(9, 'tit_crear'),
(8, 'tit_gestion_de'),
(10, 'tit_modificar'),
(12, 'tit_ordenar'),
(47, 'ubicar_enelmapa'),
(2, 'usuario');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `textosbackoffice_content`
--

CREATE TABLE `textosbackoffice_content` (
  `id` int(11) DEFAULT NULL,
  `idioma` varchar(2) NOT NULL,
  `textarea_texto` text COMMENT 'Texto'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `textosbackoffice_content`
--

INSERT INTO `textosbackoffice_content` (`id`, `idioma`, `textarea_texto`) VALUES
(1, 'es', 'Zona privada de gesti&oacute;n'),
(1, 'ca', 'Zona privada de gesti&oacute;'),
(1, 'en', 'Backoffice'),
(2, 'es', 'Usuario'),
(2, 'ca', 'Usuari'),
(2, 'en', 'User'),
(3, 'es', 'Fecha'),
(3, 'ca', 'Data'),
(3, 'en', 'Date'),
(4, 'es', 'Desconectar'),
(4, 'ca', 'Desconnectar'),
(4, 'en', 'D<span id=\"result_box\" class=\"short_text\" lang=\"en\"><span class=\"hps\">isconnect</span></span>'),
(5, 'es', 'Listado'),
(5, 'ca', 'Llistat'),
(5, 'en', 'List'),
(6, 'es', 'Crear'),
(6, 'ca', 'Crear'),
(6, 'en', 'Create'),
(7, 'es', 'No se han encontrado coincidencias en la base de datos'),
(7, 'ca', 'No s\'han trobat coincid&egrave;ncies a la base de dades'),
(7, 'en', '<span id=\"result_box\" class=\"short_text\" lang=\"en\" tabindex=\"-1\"><span class=\"hps\">No</span> <span class=\"hps\">matches found</span> <span class=\"hps\">in</span> <span class=\"hps\">the database</span></span>'),
(8, 'es', 'Gesti&oacute;n de'),
(8, 'ca', 'Gesti&oacute; de'),
(8, 'en', 'Management of'),
(9, 'es', 'Crear'),
(9, 'ca', 'Crear'),
(9, 'en', 'Create'),
(10, 'es', 'Modificar'),
(10, 'ca', 'Modificar'),
(10, 'en', 'Change'),
(11, 'es', 'Categor&iacute;as noticias'),
(11, 'ca', 'Categories not&iacute;cies'),
(11, 'en', 'News categories'),
(12, 'es', 'Ordenar'),
(12, 'ca', 'Ordenar'),
(12, 'en', 'Order'),
(13, 'es', 'Buscar'),
(13, 'ca', 'Cercar'),
(13, 'en', 'Search'),
(14, 'es', 'Borrar'),
(14, 'ca', 'Esborrar'),
(14, 'en', 'Delete'),
(15, 'es', 'Editar'),
(15, 'ca', 'Editar'),
(15, 'en', 'Edit'),
(16, 'es', 'Noticias'),
(16, 'ca', 'Not&iacute;cies'),
(16, 'en', 'News'),
(17, 'es', '&iquest;Seguro que quieres borrar los elementos seleccionados?'),
(17, 'ca', 'Segur que vols borrar els elements seleccionats?'),
(17, 'en', '<span id=\"result_box\" class=\"\" lang=\"en\" tabindex=\"-1\"><span class=\"hps\">Are you sure you</span> <span class=\"hps\">want to delete</span> <span class=\"hps\">selected items?</span></span>'),
(18, 'es', 'Categor&iacute;as productos'),
(18, 'ca', 'Categories productes'),
(18, 'en', 'Products categories'),
(19, 'es', 'Productos'),
(19, 'ca', 'Productes'),
(19, 'en', 'Products'),
(20, 'es', 'Textos'),
(20, 'ca', 'Textos'),
(20, 'en', 'Texts'),
(21, 'es', 'Crear'),
(21, 'ca', 'Crear'),
(21, 'en', 'Create'),
(22, 'es', 'Configuraci&oacute;n general'),
(22, 'ca', 'Configuraci&oacute; general'),
(22, 'en', 'General configuration'),
(23, 'es', 'En el siguiente paso podr&aacute;s a&ntilde;adir fotograf&iacute;as'),
(23, 'ca', 'En el seg&uuml;ent pas podr&agrave;s afegir fotografies'),
(23, 'en', '<span id=\"result_box\" class=\"short_text\" lang=\"en\" tabindex=\"-1\"><span class=\"hps\">In the next step you can add photos</span></span>'),
(24, 'es', 'Modificar'),
(24, 'ca', 'Modificar'),
(24, 'en', 'Modify'),
(25, 'es', 'Configuraci&oacute;n thumbs'),
(25, 'ca', 'Configuraci&oacute; thumbs'),
(25, 'en', 'Thumbs configuration'),
(26, 'es', 'Registro creado correctamente'),
(26, 'ca', 'Registre creat correctament'),
(26, 'en', '<span id=\"result_box\" class=\"short_text\" lang=\"en\" tabindex=\"-1\"><span class=\"hps\">Register</span> <span class=\"hps\">created successfully</span></span>'),
(27, 'es', 'Usuarios'),
(27, 'ca', 'Usuaris'),
(27, 'en', 'Users'),
(28, 'es', 'Textos backoffice'),
(28, 'ca', 'Textos backoffice'),
(28, 'en', 'Backoffice texts'),
(29, 'es', 'Registro modificado correctamente'),
(29, 'ca', 'Registre modificat correctament'),
(29, 'en', '<span id=\"result_box\" class=\"short_text\" lang=\"en\" tabindex=\"-1\"><span class=\"hps\">Register</span> <span class=\"hps\">successfully modified</span></span>'),
(30, 'es', 'Actual'),
(30, 'ca', 'Actual'),
(30, 'en', 'Current'),
(31, 'es', 'Eliminar'),
(31, 'ca', 'Eliminar'),
(31, 'en', '<span id=\"result_box\" class=\"short_text\" lang=\"en\" tabindex=\"-1\"><span class=\"hps\">Remove</span></span>'),
(32, 'es', 'Tu navegador no soporta HTML5, Gears o BrowserPlus'),
(32, 'ca', 'El teu navegador no suporta HTML5, Gears o BrowserPlus'),
(32, 'en', '<span id=\"result_box\" class=\"\" lang=\"en\" tabindex=\"-1\"><span class=\"hps\">Your</span> <span class=\"hps\">browser does not support</span> <span class=\"hps\">HTML5</span>, Gears <span class=\"hps\">or</span> <span class=\"hps\">BrowserPlus</span></span>'),
(33, 'es', '<strong>Fotos subidas actualmente:</strong> Puedes ordenarlas movi&eacute;ndolas a la posici&oacute;n deseada.'),
(33, 'ca', '<strong>Fotos pujades actualment:</strong> Pots ordenar-les movent-les a la posici&oacute; desitjada.'),
(33, 'en', '<span id=\"result_box\" class=\"\" lang=\"en\" tabindex=\"-1\"><strong><span class=\"hps\">Currently</span> <span class=\"hps\">uploaded photos</span></strong>: You can <span class=\"hps\">sort them</span> <span class=\"hps\">by moving </span><span class=\"hps\">to</span> <span class=\"hps\">the desired position.</span></span>'),
(34, 'es', 'No hay fotos subidas'),
(34, 'ca', 'No hi ha fotos pujades'),
(34, 'en', '<span id=\"result_box\" class=\"short_text\" lang=\"en\" tabindex=\"-1\"><span class=\"hps\">No photos uploaded</span></span>'),
(35, 'es', '<strong>Ficheros subidos actualmente:</strong> Puedes ordenarlos movi&eacute;ndolos a la posici&oacute;n deseada.'),
(35, 'ca', '<strong>Fitxers pujats actualment:</strong> Pots ordenar-los movent-los a la posici&oacute; desitjada.'),
(35, 'en', '<span id=\"result_box\" class=\"\" lang=\"en\" tabindex=\"-1\"><strong><span class=\"hps\">Currently</span> <span class=\"hps\">uploaded</span> <span class=\"hps\">files</span></strong>: You can <span class=\"hps\">sort them</span> <span class=\"hps\">by moving to</span> <span class=\"hps\">the desired position.</span></span>'),
(36, 'es', 'No hay ficheros subidos'),
(36, 'ca', 'No hi ha fitxers pujats'),
(36, 'en', '<span id=\"result_box\" class=\"short_text\" lang=\"en\" tabindex=\"-1\"><span class=\"hps\">No</span> <span class=\"hps\">uploaded files</span></span>'),
(37, 'es', 'Siguiente'),
(37, 'ca', 'Seg&uuml;ent'),
(37, 'en', 'Next'),
(38, 'es', 'Anterior'),
(38, 'ca', 'Anterior'),
(38, 'en', 'Previous'),
(39, 'es', 'Texto enriquecido'),
(39, 'ca', 'Text enriquit'),
(39, 'en', 'Rich text'),
(40, 'es', 'No hay elementos subidos'),
(40, 'ca', 'No hi ha elements pujats'),
(40, 'en', '<span id=\"result_box\" class=\"short_text\" lang=\"en\" tabindex=\"-1\"><span class=\"hps\">No</span> <span class=\"hps\">uploaded</span> <span class=\"hps\">items</span></span>'),
(41, 'es', 'Guardar'),
(41, 'ca', 'Guardar'),
(41, 'en', 'Save'),
(42, 'es', 'Crear registro'),
(42, 'ca', 'Crear registre'),
(42, 'en', 'Create record'),
(43, 'es', 'Crear clave'),
(43, 'ca', 'Crear clau'),
(43, 'en', 'Create key'),
(44, 'es', 'No hay elementos seleccionados'),
(44, 'ca', 'No hi ha elements seleccionats'),
(44, 'en', '<span id=\"result_box\" class=\"short_text\" lang=\"en\" tabindex=\"-1\"><span class=\"hps\">No items selected</span></span>'),
(45, 'es', 'Filtrar por clave:'),
(45, 'ca', 'Filtrar per clau:'),
(45, 'en', 'Filter by key:'),
(46, 'es', 'Control'),
(46, 'ca', 'Control'),
(46, 'en', 'Control'),
(47, 'es', 'Ubicar en el mapa'),
(47, 'ca', 'Ubicar al mapa'),
(47, 'en', '<span id=\"result_box\" class=\"short_text\" lang=\"en\" tabindex=\"-1\"><span class=\"hps\">Locate on map</span></span>'),
(48, 'es', 'Restaurar'),
(48, 'ca', 'Restaurar'),
(48, 'en', 'Restore'),
(49, 'es', 'Importar'),
(49, 'ca', 'Importar'),
(49, 'en', 'Import'),
(50, 'es', 'Exportar'),
(50, 'ca', 'Exportar'),
(50, 'en', 'Export'),
(51, 'es', 'Orden guardado correctamente'),
(51, 'ca', 'Ordre guardat correctament'),
(51, 'en', '<span id=\"result_box\" class=\"short_text\" lang=\"en\" tabindex=\"-1\"><span class=\"hps\">Order</span> <span class=\"hps\">saved successfully</span></span>'),
(52, 'es', 'Dise&ntilde;o web'),
(52, 'ca', 'Disseny web'),
(52, 'en', 'Web design'),
(58, 'es', 'Selecciona'),
(58, 'ca', 'Selecciona'),
(58, 'en', 'Choose'),
(59, 'es', 'Configuraci&oacute;n men&uacute;s mobile'),
(59, 'ca', 'Configuraci&oacute; men&uacute;s mobile'),
(59, 'en', 'Mobile configuration menus'),
(60, 'es', 'Mails'),
(60, 'ca', ''),
(60, 'en', ''),
(61, 'es', 'Publicaciones'),
(61, 'ca', 'Publicacions'),
(61, 'en', 'Publications'),
(62, 'es', 'Categorias Publicaciones'),
(62, 'ca', 'Categories Publicacions'),
(62, 'en', 'Categories Publications'),
(63, 'es', 'Tags Publicaciones'),
(63, 'ca', 'Tags Publicacions'),
(63, 'en', 'Tags Publications'),
(64, 'es', 'Equipo'),
(64, 'ca', 'Equip'),
(64, 'en', 'Team'),
(65, 'es', 'Asociaciones'),
(65, 'ca', 'Associacions'),
(65, 'en', 'Associations');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `textos_bk`
--

CREATE TABLE `textos_bk` (
  `id` int(11) NOT NULL,
  `text_nodisabled` varchar(250) NOT NULL,
  `last_modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `textos_content`
--

CREATE TABLE `textos_content` (
  `id` int(11) DEFAULT NULL,
  `idioma` varchar(2) NOT NULL,
  `textarea_titulo` text,
  `last_modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `textos_content`
--

INSERT INTO `textos_content` (`id`, `idioma`, `textarea_titulo`, `last_modified`) VALUES
(67, 'ca', 'Acceptar', '2015-06-18 11:46:57'),
(3, 'es', 'Anterior', '2015-06-18 11:46:57'),
(3, 'ca', 'Anterior', '2015-06-18 11:46:57'),
(4, 'es', 'Leer más', '2015-06-18 11:46:57'),
(4, 'ca', 'Llegir més', '2015-06-18 11:46:57'),
(5, 'es', 'Más info', '2015-06-18 11:46:57'),
(5, 'ca', 'Més info', '2015-06-18 11:46:57'),
(6, 'es', 'Páginas', '2015-06-18 11:46:57'),
(6, 'ca', 'Pàgines', '2015-06-18 11:46:57'),
(7, 'es', 'Siquiente', '2015-06-18 11:46:57'),
(7, 'ca', 'Següent', '2015-06-18 11:46:57'),
(9, 'es', 'Ver más', '2015-06-18 11:46:57'),
(9, 'ca', 'Veure més', '2015-06-18 11:46:57'),
(10, 'es', 'Ver todas', '2015-06-18 11:46:57'),
(10, 'ca', 'Veure totes', '2015-06-18 11:46:57'),
(11, 'es', 'Ver todas', '2020-01-30 11:14:45'),
(11, 'ca', 'Veure totes', '2020-01-30 11:14:45'),
(13, 'es', 'contacto', '2015-06-18 11:46:57'),
(13, 'ca', 'contacte', '2015-06-18 11:46:57'),
(14, 'es', 'quienes-somos', '2015-12-14 10:24:31'),
(14, 'ca', 'qui-som', '2015-12-14 10:24:31'),
(63, 'ca', 'Tens algun dubte sobre el projecte?', '2020-01-02 09:59:35'),
(63, 'es', '¿Tienes alguna duda o inquietud sobre el proyecto?', '2020-01-02 09:59:35'),
(62, 'ca', 'Contacte', '2015-06-18 11:46:57'),
(62, 'es', 'Contacta con nosotros', '2019-12-17 11:00:50'),
(28, 'ca', '<\0h\01\0>\0P\0o\0l\0í\0t\0i\0c\0a\0 \0d\0e\0 \0C\0o\0o\0k\0i\0e\0s\0<\0/\0h\01\0>\0\n\0<\0h\02\0>\0Q\0u\0è\0 \0s\0ó\0n\0 \0l\0e\0s\0 \0c\0o\0o\0k\0i\0e\0s\0?\0<\0/\0h\02\0>\0\n\0<\0p\0>\0C\0o\0o\0k\0i\0e\0 \0é\0s\0 \0u\0n\0 \0f\0i\0t\0x\0e\0r\0 \0q\0u\0e\0 \0e\0s\0 \0d\0e\0s\0c\0a\0r\0r\0e\0g\0a\0 \0a\0l\0 \0s\0e\0u\0 \0o\0r\0d\0i\0n\0a\0d\0o\0r\0 \0e\0n\0 \0a\0c\0c\0e\0d\0i\0r\0 \0a\0 \0d\0e\0t\0e\0r\0m\0i\0n\0a\0d\0e\0s\0 \0p\0à\0g\0i\0n\0e\0s\0 \0w\0e\0b\0.\0 \0L\0e\0s\0 \0c\0o\0o\0k\0i\0e\0s\0 \0p\0e\0r\0m\0e\0t\0e\0n\0 \0a\0 \0u\0n\0a\0 \0p\0à\0g\0i\0n\0a\0 \0w\0e\0b\0,\0 \0e\0n\0t\0r\0e\0 \0a\0l\0t\0r\0e\0s\0 \0c\0o\0s\0e\0s\0,\0 \0e\0m\0m\0a\0g\0a\0t\0z\0e\0m\0a\0r\0 \0i\0 \0r\0e\0c\0u\0p\0e\0r\0a\0r\0 \0i\0n\0f\0o\0r\0m\0a\0c\0i\0ó\0 \0s\0o\0b\0r\0e\0 \0e\0l\0s\0 \0h\0à\0b\0i\0t\0s\0 \0d\0e\0 \0n\0a\0v\0e\0g\0a\0c\0i\0ó\0 \0d\0 u\0n\0 \0u\0s\0u\0a\0r\0i\0 \0o\0 \0d\0e\0l\0 \0s\0e\0u\0 \0e\0q\0u\0i\0p\0 \0i\0,\0 \0d\0e\0p\0e\0n\0e\0n\0t\0 \0d\0e\0 \0l\0a\0 \0i\0n\0f\0o\0r\0m\0a\0c\0i\0ó\0 \0q\0u\0e\0 \0c\0o\0n\0t\0i\0n\0g\0u\0i\0n\0 \0i\0 \0d\0e\0 \0l\0a\0 \0f\0o\0r\0m\0a\0 \0e\0n\0 \0q\0u\0è\0 \0u\0t\0i\0l\0i\0t\0z\0i\0 \0e\0l\0 \0s\0e\0u\0 \0e\0q\0u\0i\0p\0,\0 \0e\0s\0 \0p\0o\0d\0e\0n\0 \0u\0t\0i\0l\0i\0t\0z\0a\0r\0 \0p\0e\0r\0 \0r\0e\0c\0o\0n\0è\0i\0x\0e\0r\0 \0l\0 u\0s\0u\0a\0r\0i\0.\0 \0E\0l\0 \0n\0a\0v\0e\0g\0a\0d\0o\0r\0 \0d\0e\0 \0l\0 u\0s\0u\0a\0r\0i\0 \0m\0e\0m\0o\0r\0i\0t\0z\0a\0 \0c\0o\0o\0k\0i\0e\0s\0 \0a\0l\0 \0d\0i\0s\0c\0 \0d\0u\0r\0 \0n\0o\0m\0é\0s\0 \0d\0u\0r\0a\0n\0t\0 \0l\0a\0 \0s\0e\0s\0s\0i\0ó\0 \0a\0c\0t\0u\0a\0l\0 \0o\0c\0u\0p\0a\0n\0t\0 \0u\0n\0 \0e\0s\0p\0a\0i\0 \0d\0e\0 \0m\0e\0m\0ò\0r\0i\0a\0 \0m\0í\0n\0i\0m\0 \0i\0 \0n\0o\0 \0p\0e\0r\0j\0u\0d\0i\0c\0a\0n\0t\0 \0a\0 \0l\0 o\0r\0d\0i\0n\0a\0d\0o\0r\0.\0 \0L\0e\0s\0 \0c\0o\0o\0k\0i\0e\0s\0 \0n\0o\0 \0c\0o\0n\0t\0e\0n\0e\0n\0 \0c\0a\0p\0 \0m\0e\0n\0a\0 \0d\0 i\0n\0f\0o\0r\0m\0a\0c\0i\0ó\0 \0p\0e\0r\0s\0o\0n\0a\0l\0 \0e\0s\0p\0e\0c\0í\0f\0i\0c\0a\0,\0 \0i\0 \0l\0a\0 \0m\0a\0j\0o\0r\0i\0a\0 \0d\0e\0 \0l\0e\0s\0 \0m\0a\0t\0e\0i\0x\0e\0s\0 \0s\0 e\0s\0b\0o\0r\0r\0e\0n\0 \0d\0e\0l\0 \0d\0i\0s\0c\0 \0d\0u\0r\0 \0e\0n\0 \0f\0i\0n\0a\0l\0i\0t\0z\0a\0r\0 \0l\0a\0 \0s\0e\0s\0s\0i\0ó\0 \0d\0e\0 \0n\0a\0v\0e\0g\0a\0d\0o\0r\0,\0 \0l\0e\0s\0 \0a\0n\0o\0m\0e\0n\0a\0d\0e\0s\0 \0c\0o\0o\0k\0i\0e\0s\0 \0d\0e\0 \0s\0e\0s\0s\0i\0ó\0.\0<\0/\0p\0>\0\n\0<\0p\0>\0L\0a\0 \0m\0a\0j\0o\0r\0i\0a\0 \0d\0e\0l\0s\0 \0n\0a\0v\0e\0g\0a\0d\0o\0r\0s\0 \0a\0c\0c\0e\0p\0t\0e\0n\0 \0c\0o\0m\0 \0a\0 \0e\0s\0t\0à\0n\0d\0a\0r\0d\0 \0a\0 \0l\0e\0s\0 \0c\0o\0o\0k\0i\0e\0s\0 \0i\0,\0 \0a\0m\0b\0 \0i\0n\0d\0e\0p\0e\0n\0d\0è\0n\0c\0i\0a\0 \0d\0e\0 \0l\0e\0s\0 \0m\0a\0t\0e\0i\0x\0e\0s\0,\0 \0p\0e\0r\0m\0e\0t\0e\0n\0 \0o\0 \0i\0m\0p\0e\0d\0e\0i\0x\0e\0n\0 \0a\0 \0l\0a\0 \0c\0o\0n\0f\0i\0g\0u\0r\0a\0c\0i\0ó\0 \0d\0e\0 \0s\0e\0g\0u\0r\0e\0t\0a\0t\0 \0l\0e\0s\0 \0c\0o\0o\0k\0i\0e\0s\0 \0t\0e\0m\0p\0o\0r\0a\0l\0s\0 \0o\0 \0m\0e\0m\0o\0r\0i\0t\0z\0a\0d\0e\0s\0.\0<\0/\0p\0>\0\n\0<\0p\0>\0S\0e\0n\0s\0e\0 \0l\0a\0 \0s\0e\0v\0a\0 \0e\0x\0p\0r\0é\0s\0 \0c\0o\0n\0s\0e\0n\0t\0i\0m\0e\0n\0t\0 \0 m\0i\0t\0j\0a\0n\0ç\0a\0n\0t\0 \0l\0 a\0c\0t\0i\0v\0a\0c\0i\0ó\0 \0d\0e\0 \0l\0e\0s\0 \0c\0o\0o\0k\0i\0e\0s\0 \0a\0l\0 \0s\0e\0u\0 \0n\0a\0v\0e\0g\0a\0d\0o\0r\0  \0a\0q\0u\0e\0s\0t\0a\0 \0p\0à\0g\0i\0n\0a\0 \0n\0o\0 \0e\0n\0l\0l\0a\0ç\0a\0r\0à\0 \0a\0 \0l\0e\0s\0 \0c\0o\0o\0k\0i\0e\0s\0 \0l\0e\0s\0 \0d\0a\0d\0e\0s\0 \0m\0e\0m\0o\0r\0i\0t\0z\0a\0d\0e\0s\0 \0a\0m\0b\0 \0l\0e\0s\0 \0s\0e\0v\0e\0s\0 \0d\0a\0d\0e\0s\0 \0p\0e\0r\0s\0o\0n\0a\0l\0s\0 \0p\0r\0o\0p\0o\0r\0c\0i\0o\0n\0a\0d\0e\0s\0 \0e\0n\0 \0e\0l\0 \0m\0o\0m\0e\0n\0t\0 \0d\0e\0l\0 \0r\0e\0g\0i\0s\0t\0r\0e\0 \0o\0 \0l\0a\0 \0c\0o\0m\0p\0r\0a\0.\0<\0/\0p\0>\0\n\0<\0p\0>\0Q\0u\0i\0n\0s\0 \0t\0i\0p\0u\0s\0 \0d\0e\0 \0c\0o\0o\0k\0i\0e\0s\0 \0u\0t\0i\0l\0i\0t\0z\0a\0 \0a\0q\0u\0e\0s\0t\0a\0 \0p\0à\0g\0i\0n\0a\0 \0w\0e\0b\0?\0<\0/\0p\0>\0\n\0<\0u\0l\0>\0\n\0<\0l\0i\0>\0C\0o\0o\0k\0i\0e\0s\0 \0t\0è\0c\0n\0i\0q\0u\0e\0s\0:\0 \0S\0ó\0n\0 \0a\0q\0u\0e\0l\0l\0e\0s\0 \0q\0u\0e\0 \0p\0e\0r\0m\0e\0t\0e\0n\0 \0a\0 \0l\0 u\0s\0u\0a\0r\0i\0 \0l\0a\0 \0n\0a\0v\0e\0g\0a\0c\0i\0ó\0 \0a\0 \0t\0r\0a\0v\0é\0s\0 \0d\0 u\0n\0a\0 \0p\0à\0g\0i\0n\0a\0 \0w\0e\0b\0,\0 \0p\0l\0a\0t\0a\0f\0o\0r\0m\0a\0 \0o\0 \0a\0p\0l\0i\0c\0a\0c\0i\0ó\0 \0i\0 \0l\0a\0 \0u\0t\0i\0l\0i\0t\0z\0a\0c\0i\0ó\0 \0d\0e\0 \0l\0e\0s\0 \0d\0i\0f\0e\0r\0e\0n\0t\0s\0 \0o\0p\0c\0i\0o\0n\0s\0 \0o\0 \0s\0e\0r\0v\0e\0i\0s\0 \0q\0u\0e\0 \0e\0n\0 \0e\0l\0l\0a\0 \0h\0i\0 \0h\0a\0g\0i\0 \0c\0o\0m\0,\0 \0p\0e\0r\0 \0e\0x\0e\0m\0p\0l\0e\0,\0 \0c\0o\0n\0t\0r\0o\0l\0a\0r\0 \0e\0l\0 \0t\0r\0à\0n\0s\0i\0t\0 \0i\0 \0l\0a\0 \0c\0o\0m\0u\0n\0i\0c\0a\0c\0i\0ó\0 \0d\0e\0 \0d\0a\0d\0e\0s\0,\0 \0i\0d\0e\0n\0t\0i\0f\0i\0c\0a\0r\0 \0l\0a\0 \0s\0e\0s\0s\0i\0ó\0,\0 \0a\0c\0c\0e\0d\0i\0r\0 \0a\0 \0p\0a\0r\0t\0s\0 \0d\0 a\0c\0c\0é\0s\0 \0r\0e\0s\0t\0r\0i\0n\0g\0i\0t\0,\0 \0r\0e\0c\0o\0r\0d\0a\0r\0 \0e\0l\0s\0 \0e\0l\0e\0m\0e\0n\0t\0s\0 \0q\0u\0e\0 \0i\0n\0t\0e\0g\0r\0e\0n\0 \0u\0n\0a\0 \0c\0o\0m\0a\0n\0d\0a\0,\0 \0r\0e\0a\0l\0i\0t\0z\0a\0r\0 \0e\0l\0 \0p\0r\0o\0c\0é\0s\0 \0d\0e\0 \0c\0o\0m\0p\0r\0a\0 \0d\0 u\0n\0a\0 \0c\0o\0m\0a\0n\0d\0a\0,\0 \0r\0e\0a\0l\0i\0t\0z\0a\0r\0 \0l\0a\0 \0s\0o\0l\0·\0l\0i\0c\0i\0t\0u\0d\0 \0d\0 i\0n\0s\0c\0r\0i\0p\0c\0i\0ó\0 \0o\0 \0p\0a\0r\0t\0i\0c\0i\0p\0a\0c\0i\0ó\0 \0e\0n\0 \0u\0n\0 \0e\0s\0d\0e\0v\0e\0n\0i\0m\0e\0n\0t\0,\0 \0u\0t\0i\0l\0i\0t\0z\0a\0r\0 \0e\0l\0e\0m\0e\0n\0t\0s\0 \0d\0e\0 \0s\0e\0g\0u\0r\0e\0t\0a\0t\0 \0d\0u\0r\0a\0n\0t\0 \0l\0a\0 \0n\0a\0v\0e\0g\0a\0c\0i\0ó\0,\0 \0e\0m\0m\0a\0g\0a\0t\0z\0e\0m\0a\0r\0 \0c\0o\0n\0t\0i\0n\0g\0u\0t\0s\0 \0p\0e\0r\0 \0a\0 \0l\0a\0 \0d\0i\0f\0u\0s\0i\0ó\0 \0d\0e\0 \0v\0í\0d\0e\0o\0s\0 \0o\0 \0s\0o\0 \0o\0 \0c\0o\0m\0p\0a\0r\0t\0i\0r\0 \0c\0o\0n\0t\0i\0n\0g\0u\0t\0s\0 \0a\0 \0t\0r\0a\0v\0é\0s\0 \0d\0e\0 \0x\0a\0r\0x\0e\0s\0 \0s\0o\0c\0i\0a\0l\0s\0.\0<\0/\0l\0i\0>\0\n\0<\0l\0i\0>\0C\0o\0o\0k\0i\0e\0s\0 \0d\0e\0 \0p\0e\0r\0s\0o\0n\0a\0l\0i\0t\0z\0a\0c\0i\0ó\0:\0 \0S\0ó\0n\0 \0a\0q\0u\0e\0l\0l\0e\0s\0 \0q\0u\0e\0 \0p\0e\0r\0m\0e\0t\0e\0n\0 \0a\0 \0l\0 u\0s\0u\0a\0r\0i\0 \0a\0c\0c\0e\0d\0i\0r\0 \0a\0l\0 \0s\0e\0r\0v\0e\0i\0 \0a\0m\0b\0 \0a\0l\0g\0u\0n\0e\0s\0 \0c\0a\0r\0a\0c\0t\0e\0r\0í\0s\0t\0i\0q\0u\0e\0s\0 \0d\0e\0 \0c\0a\0r\0à\0c\0t\0e\0r\0 \0g\0e\0n\0e\0r\0a\0l\0 \0p\0r\0e\0d\0e\0f\0i\0n\0i\0d\0e\0s\0 \0e\0n\0 \0f\0u\0n\0c\0i\0ó\0 \0d\0 u\0n\0a\0 \0s\0è\0r\0i\0e\0 \0d\0e\0 \0c\0r\0i\0t\0e\0r\0i\0s\0 \0e\0n\0 \0e\0l\0 \0t\0e\0r\0m\0i\0n\0a\0l\0 \0d\0e\0 \0l\0 u\0s\0u\0a\0r\0i\0 \0c\0o\0m\0 \0a\0r\0a\0 \0s\0e\0r\0i\0e\0n\0 \0l\0 i\0d\0i\0o\0m\0a\0,\0 \0e\0l\0 \0t\0i\0p\0u\0s\0 \0d\0e\0 \0n\0a\0v\0e\0g\0a\0d\0o\0r\0 \0a\0 \0t\0r\0a\0v\0é\0s\0 \0d\0e\0l\0 \0q\0u\0a\0l\0 \0s\0 a\0c\0c\0e\0d\0e\0i\0x\0 \0a\0l\0 \0s\0e\0r\0v\0e\0i\0,\0 \0l\0a\0 \0c\0o\0n\0f\0i\0g\0u\0r\0a\0c\0i\0ó\0 \0r\0e\0g\0i\0o\0n\0a\0l\0 \0d\0e\0s\0 \0d\0 o\0n\0 \0a\0c\0c\0e\0d\0e\0i\0x\0 \0a\0l\0 \0s\0e\0r\0v\0e\0,\0 \0e\0t\0c\0.\0<\0/\0l\0i\0>\0\n\0<\0l\0i\0>\0C\0o\0o\0k\0i\0e\0s\0 \0d\0 a\0n\0à\0l\0i\0s\0i\0:\0 \0S\0ó\0n\0 \0a\0q\0u\0e\0l\0l\0e\0s\0 \0q\0u\0e\0 \0b\0e\0n\0 \0t\0r\0a\0c\0t\0a\0d\0e\0s\0 \0p\0e\0r\0 \0n\0o\0s\0a\0l\0t\0r\0e\0s\0 \0o\0 \0p\0e\0r\0 \0t\0e\0r\0c\0e\0r\0s\0,\0 \0e\0n\0s\0 \0p\0e\0r\0m\0e\0t\0e\0n\0 \0q\0u\0a\0n\0t\0i\0f\0i\0c\0a\0r\0 \0e\0l\0 \0n\0o\0m\0b\0r\0e\0 \0d\0 u\0s\0u\0a\0r\0i\0s\0 \0i\0 \0a\0i\0x\0í\0 \0r\0e\0a\0l\0i\0t\0z\0a\0r\0 \0e\0l\0 \0m\0e\0s\0u\0r\0a\0m\0e\0n\0t\0 \0i\0 \0a\0n\0à\0l\0i\0s\0i\0 \0e\0s\0t\0a\0d\0í\0s\0t\0i\0c\0a\0 \0d\0e\0 \0l\0a\0 \0u\0t\0i\0l\0i\0t\0z\0a\0c\0i\0ó\0 \0q\0u\0e\0 \0f\0a\0n\0 \0e\0l\0s\0 \0u\0s\0u\0a\0r\0i\0s\0 \0d\0e\0l\0 \0s\0e\0r\0v\0e\0i\0 \0o\0f\0e\0r\0t\0.\0 \0P\0e\0r\0 \0a\0 \0a\0i\0x\0ò\0 \0s\0 a\0n\0a\0l\0i\0t\0z\0a\0 \0l\0a\0 \0s\0e\0v\0a\0 \0n\0a\0v\0e\0g\0a\0c\0i\0ó\0 \0a\0 \0l\0a\0 \0n\0o\0s\0t\0r\0a\0 \0p\0à\0g\0i\0n\0a\0 \0w\0e\0b\0 \0a\0m\0b\0 \0l\0a\0 \0f\0i\0n\0a\0l\0i\0t\0a\0t\0 \0d\0e\0 \0m\0i\0l\0l\0o\0r\0a\0r\0 \0l\0 o\0f\0e\0r\0t\0a\0 \0d\0e\0 \0p\0r\0o\0d\0u\0c\0t\0e\0s\0 \0o\0 \0s\0e\0r\0v\0e\0i\0s\0 \0q\0u\0e\0 \0l\0i\0 \0o\0f\0e\0r\0i\0m\0.\0<\0/\0l\0i\0>\0\n\0<\0l\0i\0>\0C\0o\0o\0k\0i\0e\0s\0 \0p\0u\0b\0l\0i\0c\0i\0t\0à\0r\0i\0e\0s\0:\0 \0S\0ó\0n\0 \0a\0q\0u\0e\0l\0l\0e\0s\0 \0q\0u\0e\0,\0 \0b\0e\0n\0 \0t\0r\0a\0c\0t\0a\0d\0e\0s\0 \0p\0e\0r\0 \0n\0o\0s\0a\0l\0t\0r\0e\0s\0 \0o\0 \0p\0e\0r\0 \0t\0e\0r\0c\0e\0r\0s\0,\0 \0e\0n\0s\0 \0p\0e\0r\0m\0e\0t\0e\0n\0 \0g\0e\0s\0t\0i\0o\0n\0a\0r\0 \0d\0e\0 \0l\0a\0 \0f\0o\0r\0m\0a\0 \0m\0é\0s\0 \0e\0f\0i\0c\0a\0ç\0 \0p\0o\0s\0s\0i\0b\0l\0e\0 \0l\0 o\0f\0e\0r\0t\0a\0 \0d\0e\0l\0s\0 \0e\0s\0p\0a\0i\0s\0 \0p\0u\0b\0l\0i\0c\0i\0t\0a\0r\0i\0s\0 \0q\0u\0e\0 \0h\0i\0 \0h\0a\0 \0a\0 \0l\0a\0 \0p\0à\0g\0i\0n\0a\0 \0w\0e\0b\0,\0 \0a\0d\0e\0q\0u\0a\0n\0t\0 \0e\0l\0 \0c\0o\0n\0t\0i\0n\0g\0u\0t\0 \0d\0e\0 \0l\0 a\0n\0u\0n\0c\0i\0 \0a\0l\0 \0c\0o\0n\0t\0i\0n\0g\0u\0t\0 \0d\0e\0l\0 \0s\0e\0r\0v\0e\0i\0 \0s\0o\0l\0·\0l\0i\0c\0i\0t\0a\0t\0 \0o\0a\0 \0l\0 ú\0s\0 \0q\0u\0e\0 \0r\0e\0a\0l\0i\0t\0z\0i\0 \0d\0e\0 \0l\0a\0 \0n\0o\0s\0t\0r\0a\0 \0p\0à\0g\0i\0n\0a\0 \0w\0e\0b\0.\0 \0P\0e\0r\0 \0a\0 \0a\0i\0x\0ò\0 \0p\0o\0d\0e\0m\0 \0a\0n\0a\0l\0i\0t\0z\0a\0r\0 \0e\0l\0s\0 \0s\0e\0u\0s\0 \0h\0à\0b\0i\0t\0s\0 \0d\0e\0 \0n\0a\0v\0e\0g\0a\0c\0i\0ó\0 \0a\0 \0I\0n\0t\0e\0r\0n\0e\0t\0 \0i\0 \0p\0o\0d\0e\0m\0 \0m\0o\0s\0t\0r\0a\0r\0 \0p\0u\0b\0l\0i\0c\0i\0t\0a\0t\0 \0r\0e\0l\0a\0c\0i\0o\0n\0a\0d\0a\0 \0a\0m\0b\0 \0e\0l\0 \0s\0e\0u\0 \0p\0e\0r\0f\0i\0l\0 \0d\0e\0 \0n\0a\0v\0e\0g\0a\0c\0i\0ó\0.\0<\0/\0l\0i\0>\0\n\0<\0l\0i\0>\0C\0o\0o\0k\0i\0e\0s\0 \0d\0e\0 \0p\0u\0b\0l\0i\0c\0i\0t\0a\0t\0 \0c\0o\0m\0p\0o\0r\0t\0a\0m\0e\0n\0t\0a\0l\0:\0 \0S\0ó\0n\0 \0a\0q\0u\0e\0l\0l\0e\0s\0 \0q\0u\0e\0 \0p\0e\0r\0m\0e\0t\0e\0n\0 \0l\0a\0 \0g\0e\0s\0t\0i\0ó\0,\0 \0d\0e\0 \0l\0a\0 \0f\0o\0r\0m\0a\0 \0m\0é\0s\0 \0e\0f\0i\0c\0a\0ç\0 \0p\0o\0s\0s\0i\0b\0l\0e\0,\0 \0d\0e\0l\0s\0 \0e\0s\0p\0a\0i\0s\0 \0p\0u\0b\0l\0i\0c\0i\0t\0a\0r\0i\0s\0 \0q\0u\0e\0,\0 \0s\0i\0 \0e\0s\0c\0a\0u\0,\0 \0l\0 e\0d\0i\0t\0o\0r\0 \0h\0a\0g\0i\0 \0i\0n\0c\0l\0ò\0s\0 \0e\0n\0 \0u\0n\0a\0 \0p\0à\0g\0i\0n\0a\0 \0w\0e\0b\0,\0 \0a\0p\0l\0i\0c\0a\0c\0i\0ó\0 \0o\0 \0p\0l\0a\0t\0a\0f\0o\0r\0m\0a\0 \0d\0e\0s\0 \0d\0e\0 \0l\0a\0 \0q\0u\0e\0 \0p\0r\0e\0s\0t\0a\0 \0e\0l\0 \0s\0e\0r\0v\0e\0i\0 \0s\0o\0l\0·\0l\0i\0c\0i\0t\0a\0t\0.\0 \0A\0q\0u\0e\0s\0t\0e\0s\0 \0c\0o\0o\0k\0i\0e\0s\0 \0e\0m\0m\0a\0g\0a\0t\0z\0e\0m\0e\0n\0 \0i\0n\0f\0o\0r\0m\0a\0c\0i\0ó\0 \0d\0e\0l\0 \0c\0o\0m\0p\0o\0r\0t\0a\0m\0e\0n\0t\0 \0d\0e\0l\0s\0 \0u\0s\0u\0a\0r\0i\0s\0 \0o\0b\0t\0i\0n\0g\0u\0d\0a\0 \0a\0 \0t\0r\0a\0v\0é\0s\0 \0d\0e\0 \0l\0 o\0b\0s\0e\0r\0v\0a\0c\0i\0ó\0 \0c\0o\0n\0t\0i\0n\0u\0a\0d\0a\0 \0d\0e\0l\0s\0 \0s\0e\0u\0s\0 \0h\0à\0b\0i\0t\0s\0 \0d\0e\0 \0n\0a\0v\0e\0g\0a\0c\0i\0ó\0,\0 \0e\0l\0 \0q\0u\0e\0 \0p\0e\0r\0m\0e\0t\0 \0d\0e\0s\0e\0n\0v\0o\0l\0u\0p\0a\0r\0 \0u\0n\0 \0p\0e\0r\0f\0i\0l\0 \0e\0s\0p\0e\0c\0í\0f\0i\0c\0 \0p\0e\0r\0 \0m\0o\0s\0t\0r\0a\0r\0 \0p\0u\0b\0l\0i\0c\0i\0t\0a\0t\0 \0e\0n\0 \0f\0u\0n\0c\0i\0ó\0 \0d\0 a\0q\0u\0e\0s\0t\0.\0<\0/\0l\0i\0>\0\n\0<\0/\0u\0l\0>\0\n\0<\0h\02\0>\0C\0o\0o\0k\0i\0e\0s\0 \0d\0e\0 \0t\0e\0r\0c\0e\0r\0s\0<\0/\0h\02\0>\0\n\0<\0p\0>\0A\0q\0u\0e\0s\0t\0 \0l\0l\0o\0c\0 \0w\0e\0b\0 \0p\0o\0t\0 \0u\0t\0i\0l\0i\0t\0z\0a\0r\0 \0s\0e\0r\0v\0e\0i\0s\0 \0d\0e\0 \0t\0e\0r\0c\0e\0r\0s\0 \0q\0u\0e\0 \0r\0e\0c\0o\0p\0i\0l\0a\0r\0a\0n\0 \0i\0n\0f\0o\0r\0m\0a\0c\0i\0ó\0 \0a\0m\0b\0 \0f\0i\0n\0a\0l\0i\0t\0a\0t\0s\0 \0e\0s\0t\0a\0d\0í\0s\0t\0i\0q\0u\0e\0s\0,\0 \0d\0 ú\0s\0 \0d\0e\0l\0 \0S\0i\0t\0e\0 \0p\0e\0r\0 \0p\0a\0r\0t\0 \0d\0e\0 \0l\0 u\0s\0u\0a\0r\0i\0 \0i\0 \0p\0e\0r\0 \0a\0 \0l\0a\0 \0p\0r\0e\0s\0t\0a\0c\0i\0o\0n\0 \0d\0 a\0l\0t\0r\0e\0s\0 \0s\0e\0r\0v\0e\0i\0s\0 \0r\0e\0l\0a\0c\0i\0o\0n\0a\0t\0s\0 \0a\0m\0b\0 \0l\0 a\0c\0t\0i\0v\0i\0t\0a\0t\0 \0d\0e\0l\0 \0l\0l\0o\0c\0 \0w\0e\0b\0 \0i\0 \0a\0l\0t\0r\0e\0s\0 \0s\0e\0r\0v\0e\0i\0s\0 \0d\0 I\0n\0t\0e\0r\0n\0e\0t\0.\0<\0/\0p\0>\0\n\0<\0p\0>\0E\0n\0 \0p\0a\0r\0t\0i\0c\0u\0l\0a\0r\0,\0 \0a\0q\0u\0e\0s\0t\0 \0l\0l\0o\0c\0 \0w\0e\0b\0 \0u\0t\0i\0l\0i\0t\0z\0a\0:\0<\0/\0p\0>\0\n\0<\0u\0l\0>\0\n\0<\0l\0i\0>\0L\0e\0s\0 \0c\0o\0o\0k\0i\0e\0s\0 \0d\0e\0 \0G\0o\0o\0g\0l\0e\0 \0A\0n\0a\0l\0y\0t\0i\0c\0s\0:\0 \0s\0 u\0t\0i\0l\0i\0t\0z\0e\0n\0 \0p\0e\0r\0 \0t\0a\0l\0 \0d\0 a\0n\0a\0l\0i\0t\0z\0a\0r\0 \0i\0 \0m\0e\0s\0u\0r\0a\0r\0 \0c\0o\0m\0 \0e\0l\0s\0 \0v\0i\0s\0i\0t\0a\0n\0t\0s\0 \0u\0s\0e\0n\0 \0a\0q\0u\0e\0s\0t\0 \0l\0l\0o\0c\0 \0w\0e\0b\0.\0 \0L\0a\0 \0i\0n\0f\0o\0r\0m\0a\0c\0i\0ó\0 \0s\0e\0r\0v\0e\0i\0x\0 \0p\0e\0r\0 \0e\0l\0a\0b\0o\0r\0a\0r\0 \0i\0n\0f\0o\0r\0m\0e\0s\0 \0q\0u\0e\0 \0p\0e\0r\0m\0e\0t\0e\0n\0 \0m\0i\0l\0l\0o\0r\0a\0r\0 \0a\0q\0u\0e\0s\0t\0 \0l\0l\0o\0c\0.\0 \0A\0q\0u\0e\0s\0t\0e\0s\0 \0c\0o\0o\0k\0i\0e\0s\0 \0r\0e\0c\0o\0p\0i\0l\0e\0n\0 \0i\0n\0f\0o\0r\0m\0a\0c\0i\0ó\0 \0e\0n\0 \0f\0o\0r\0m\0a\0 \0a\0n\0ò\0n\0i\0m\0a\0,\0 \0i\0n\0c\0l\0o\0e\0n\0t\0 \0e\0l\0 \0n\0o\0m\0b\0r\0e\0 \0d\0e\0 \0v\0i\0s\0i\0t\0a\0n\0t\0s\0 \0a\0l\0 \0l\0l\0o\0c\0,\0 \0c\0o\0m\0 \0h\0a\0n\0 \0a\0r\0r\0i\0b\0a\0t\0 \0a\0 \0a\0q\0u\0e\0s\0t\0 \0i\0 \0l\0e\0s\0 \0p\0à\0g\0i\0n\0e\0s\0 \0q\0u\0e\0 \0v\0a\0 \0v\0i\0s\0i\0t\0a\0r\0 \0m\0e\0n\0t\0r\0e\0 \0n\0a\0v\0e\0g\0a\0v\0a\0 \0a\0l\0 \0n\0o\0s\0t\0r\0e\0 \0l\0l\0o\0c\0 \0w\0e\0b\0 \0<\0a\0 \0h\0r\0e\0f\0=\0\"\0h\0t\0t\0p\0:\0/\0/\0w\0w\0w\0.\0g\0o\0o\0g\0l\0e\0.\0c\0o\0m\0/\0i\0n\0t\0l\0/\0e\0s\0/\0a\0n\0a\0l\0y\0t\0i\0c\0s\0/\0p\0r\0i\0v\0a\0c\0y\0o\0v\0e\0r\0v\0i\0e\0w\0.\0h\0t\0m\0l\0\"\0>\0h\0t\0t\0p\0:\0/\0/\0w\0w\0w\0.\0g\0o\0o\0g\0l\0e\0.\0c\0o\0m\0/\0i\0n\0t\0l\0/\0e\0s\0/\0a\0n\0a\0l\0y\0t\0i\0c\0s\0/\0p\0r\0i\0v\0a\0c\0y\0o\0v\0e\0r\0v\0i\0e\0w\0.\0h\0t\0m\0l\0<\0/\0a\0>\0<\0/\0l\0i\0>\0\n\0<\0l\0i\0>\0L\0e\0s\0 \0c\0o\0o\0k\0i\0e\0s\0 \0d\0e\0 \0Y\0o\0u\0t\0u\0b\0e\0:\0 \0s\0 u\0t\0i\0l\0i\0t\0z\0e\0n\0 \0p\0e\0r\0 \0c\0o\0m\0p\0t\0a\0b\0i\0l\0i\0t\0z\0a\0r\0 \0e\0l\0 \0n\0o\0m\0b\0r\0e\0 \0d\0e\0 \0v\0i\0s\0u\0a\0l\0i\0t\0z\0a\0c\0i\0o\0n\0s\0 \0d\0e\0l\0s\0 \0v\0í\0d\0e\0o\0s\0 \0i\0n\0c\0r\0u\0s\0t\0a\0t\0s\0 \0e\0n\0 \0a\0l\0t\0r\0e\0s\0 \0w\0e\0b\0.\0 \0<\0a\0 \0h\0r\0e\0f\0=\0\"\0h\0t\0t\0p\0s\0:\0/\0/\0s\0u\0p\0p\0o\0r\0t\0.\0g\0o\0o\0g\0l\0e\0.\0c\0o\0m\0/\0y\0o\0u\0t\0u\0b\0e\0/\0a\0n\0s\0w\0e\0r\0/\02\04\00\07\07\08\05\0?\0h\0l\0=\0e\0s\0-\04\01\09\0\"\0>\0h\0t\0t\0p\0s\0:\0/\0/\0s\0u\0p\0p\0o\0r\0t\0.\0g\0o\0o\0g\0l\0e\0.\0c\0o\0m\0/\0y\0o\0u\0t\0u\0b\0e\0/\0a\0n\0s\0w\0e\0r\0/\02\04\00\07\07\08\05\0?\0h\0l\0=\0e\0s\0-\04\01\09\0<\0/\0a\0>\0<\0/\0l\0i\0>\0\n\0<\0l\0i\0>\0L\0e\0s\0 \0c\0o\0o\0k\0i\0e\0s\0 \0d\0e\0 \0A\0d\0d\0T\0h\0i\0s\0:\0 \0é\0s\0 \0u\0n\0a\0 \0e\0m\0p\0r\0e\0s\0a\0 \0t\0e\0c\0n\0o\0l\0ò\0g\0i\0c\0a\0 \0q\0u\0e\0 \0p\0e\0r\0m\0e\0t\0 \0a\0l\0s\0 \0l\0l\0o\0c\0s\0 \0w\0e\0b\0 \0i\0 \0a\0l\0s\0 \0s\0e\0u\0s\0 \0u\0s\0u\0a\0r\0i\0s\0 \0c\0o\0m\0p\0a\0r\0t\0i\0r\0 \0f\0à\0c\0i\0l\0m\0e\0n\0t\0 \0e\0l\0 \0c\0o\0n\0t\0i\0n\0g\0u\0t\0 \0a\0m\0b\0 \0e\0l\0s\0 \0a\0l\0t\0r\0e\0s\0,\0 \0a\0 \0t\0r\0a\0v\0é\0s\0 \0d\0 i\0c\0o\0n\0e\0s\0 \0d\0 i\0n\0t\0e\0r\0c\0a\0n\0v\0i\0 \0i\0 \0d\0e\0 \0l\0e\0s\0 \0d\0e\0s\0t\0i\0n\0a\0c\0i\0o\0n\0s\0 \0d\0e\0 \0b\0o\0o\0k\0m\0a\0r\0k\0i\0n\0g\0 \0s\0o\0c\0i\0a\0l\0.\0 \0L\0e\0s\0 \0c\0o\0o\0k\0i\0e\0s\0 \0A\0d\0d\0T\0h\0i\0s\0 \0s\0 u\0t\0i\0l\0i\0t\0z\0e\0n\0 \0p\0e\0r\0 \0t\0a\0l\0 \0d\0 h\0a\0b\0i\0l\0i\0t\0a\0r\0 \0e\0l\0 \0c\0o\0n\0t\0i\0n\0g\0u\0t\0 \0p\0e\0r\0 \0s\0e\0r\0 \0c\0o\0m\0p\0a\0r\0t\0i\0t\0.\0 \0A\0d\0d\0T\0h\0i\0s\0 \0t\0a\0m\0b\0é\0 \0s\0 u\0t\0i\0l\0i\0t\0z\0a\0 \0p\0e\0r\0 \0r\0e\0c\0o\0p\0i\0l\0a\0r\0 \0i\0n\0f\0o\0r\0m\0a\0c\0i\0ó\0 \0s\0o\0b\0r\0e\0 \0c\0o\0m\0 \0e\0s\0 \0c\0o\0m\0p\0a\0r\0t\0e\0i\0x\0 \0c\0o\0n\0t\0i\0n\0g\0u\0t\0 \0d\0e\0l\0 \0l\0l\0o\0c\0 \0w\0e\0b\0.\0 \0L\0e\0s\0 \0c\0o\0o\0k\0i\0e\0s\0 \0a\0j\0u\0d\0e\0n\0 \0a\0 \0i\0d\0e\0n\0t\0i\0f\0i\0c\0a\0r\0 \0d\0e\0 \0f\0o\0r\0m\0a\0 \0ú\0n\0i\0c\0a\0 \0a\0 \0u\0n\0 \0u\0s\0u\0a\0r\0i\0 \0 e\0n\0c\0a\0r\0a\0 \0q\0u\0e\0 \0n\0o\0 \0d\0e\0 \0f\0o\0r\0m\0a\0 \0p\0e\0r\0s\0o\0n\0a\0l\0,\0 \0s\0i\0n\0ó\0 \0p\0e\0l\0 \0q\0u\0e\0 \0f\0a\0 \0a\0 \0d\0i\0r\0e\0c\0c\0i\0ó\0  \0p\0e\0r\0 \0n\0o\0 \0r\0e\0p\0e\0t\0i\0r\0 \0t\0a\0s\0q\0u\0e\0s\0 \0d\0i\0n\0s\0 \0d\0 u\0n\0 \0p\0e\0r\0í\0o\0d\0e\0 \0d\0e\0 \0t\0e\0m\0p\0s\0 \0e\0s\0p\0e\0c\0i\0f\0i\0c\0a\0t\0.\0<\0b\0r\0 \0/\0>\0<\0a\0 \0h\0r\0e\0f\0=\0\"\0h\0t\0t\0p\0:\0/\0/\0w\0w\0w\0.\0a\0d\0d\0t\0h\0i\0s\0.\0c\0o\0m\0/\0p\0r\0i\0v\0a\0c\0y\0\"\0>\0h\0t\0t\0p\0:\0/\0/\0w\0w\0w\0.\0a\0d\0d\0t\0h\0i\0s\0.\0c\0o\0m\0/\0p\0r\0i\0v\0a\0c\0y\0<\0/\0a\0>\0<\0/\0l\0i\0>\0\n\0<\0l\0i\0>\0A\0l\0t\0r\0e\0s\0 \0c\0o\0o\0k\0i\0e\0s\0 \0d\0e\0 \0p\0e\0r\0s\0o\0n\0a\0l\0i\0t\0z\0a\0c\0i\0ó\0:\0 \0A\0q\0u\0e\0s\0t\0e\0s\0 \0c\0o\0o\0k\0i\0e\0s\0 \0p\0e\0r\0m\0e\0t\0e\0n\0 \0a\0 \0l\0 u\0s\0u\0a\0r\0i\0 \0e\0s\0p\0e\0c\0i\0f\0i\0c\0a\0r\0 \0o\0 \0p\0e\0r\0s\0o\0n\0a\0l\0i\0t\0z\0a\0r\0 \0a\0l\0g\0u\0n\0e\0s\0 \0c\0a\0r\0a\0c\0t\0e\0r\0í\0s\0t\0i\0q\0u\0e\0s\0 \0d\0e\0 \0l\0e\0s\0 \0o\0p\0c\0i\0o\0n\0s\0 \0g\0e\0n\0e\0r\0a\0l\0s\0 \0d\0e\0 \0l\0a\0 \0p\0à\0g\0i\0n\0a\0 \0w\0e\0b\0.\0 \0P\0e\0r\0 \0e\0x\0e\0m\0p\0l\0e\0,\0 \0d\0e\0f\0i\0n\0i\0r\0 \0l\0 i\0d\0i\0o\0m\0a\0,\0 \0c\0o\0n\0f\0i\0g\0u\0r\0a\0c\0i\0ó\0 \0r\0e\0g\0i\0o\0n\0a\0l\0 \0o\0 \0t\0i\0p\0u\0s\0 \0d\0e\0 \0n\0a\0v\0e\0g\0a\0d\0o\0r\0.\0<\0b\0r\0 \0/\0>\0A\0d\0d\0t\0h\0i\0s\0:\0 \0<\0a\0 \0h\0r\0e\0f\0=\0\"\0h\0t\0t\0p\0:\0/\0/\0w\0w\0w\0.\0a\0d\0d\0t\0h\0i\0s\0.\0c\0o\0m\0/\0p\0r\0i\0v\0a\0c\0y\0\"\0>\0h\0t\0t\0p\0:\0/\0/\0w\0w\0w\0.\0a\0d\0d\0t\0h\0i\0s\0.\0c\0o\0m\0/\0p\0r\0i\0v\0a\0c\0y\0<\0/\0a\0>\0<\0b\0r\0 \0/\0>\0F\0a\0c\0e\0b\0o\0o\0k\0:\0 \0<\0a\0 \0h\0r\0e\0f\0=\0\"\0h\0t\0t\0p\0s\0:\0/\0/\0e\0s\0-\0e\0s\0.\0f\0a\0c\0e\0b\0o\0o\0k\0.\0c\0o\0m\0/\0h\0e\0l\0p\0/\0c\0o\0o\0k\0i\0e\0s\0\"\0>\0h\0t\0t\0p\0s\0:\0/\0/\0e\0s\0-\0e\0s\0.\0f\0a\0c\0e\0b\0o\0o\0k\0.\0c\0o\0m\0/\0h\0e\0l\0p\0/\0c\0o\0o\0k\0i\0e\0s\0<\0/\0a\0>\0<\0b\0r\0 \0/\0>\0T\0w\0i\0t\0t\0e\0r\0:\0 \0<\0a\0 \0h\0r\0e\0f\0=\0\"\0h\0t\0t\0p\0s\0:\0/\0/\0t\0w\0i\0t\0t\0e\0r\0.\0c\0o\0m\0/\0p\0r\0i\0v\0a\0c\0y\0\"\0>\0h\0t\0t\0p\0s\0:\0/\0/\0t\0w\0i\0t\0t\0e\0r\0.\0c\0o\0m\0/\0p\0r\0i\0v\0a\0c\0y\0<\0/\0a\0>\0<\0b\0r\0 \0/\0>\0G\0o\0o\0g\0l\0e\0:\0 \0<\0a\0 \0h\0r\0e\0f\0=\0\"\0h\0t\0t\0p\0:\0/\0/\0w\0w\0w\0.\0g\0o\0o\0g\0l\0e\0.\0e\0s\0/\0i\0n\0t\0l\0/\0e\0s\0/\0p\0o\0l\0i\0c\0i\0e\0s\0/\0t\0e\0c\0h\0n\0o\0l\0o\0g\0i\0e\0s\0/\0t\0y\0p\0e\0s\0/\0\"\0>\0h\0t\0t\0p\0:\0/\0/\0w\0w\0w\0.\0g\0o\0o\0g\0l\0e\0.\0e\0s\0/\0i\0n\0t\0l\0/\0e\0s\0/\0p\0o\0l\0i\0c\0i\0e\0s\0/\0t\0e\0c\0h\0n\0o\0l\0o\0g\0i\0e\0s\0/\0t\0y\0p\0e\0s\0/\0<\0/\0a\0>\0<\0b\0r\0 \0/\0>\0Y\0o\0u\0T\0u\0b\0e\0:\0 \0<\0a\0 \0h\0r\0e\0f\0=\0\"\0h\0t\0t\0p\0s\0:\0/\0/\0s\0u\0p\0p\0o\0r\0t\0.\0g\0o\0o\0g\0l\0e\0.\0c\0o\0m\0/\0y\0o\0u\0t\0u\0b\0e\0/\0a\0n\0s\0w\0e\0r\0/\02\04\00\07\07\08\05\0?\0h\0l\0=\0e\0s\0-\04\01\09\0\"\0>\0h\0t\0t\0p\0s\0:\0/\0/\0s\0u\0p\0p\0o\0r\0t\0.\0g\0o\0o\0g\0l\0e\0.\0c\0o\0m\0/\0y\0o\0u\0t\0u\0b\0e\0/\0a\0n\0s\0w\0e\0r\0/\02\04\00\07\07\08\05\0?\0h\0l\0=\0e\0s\0-\04\01\09\0<\0/\0a\0>\0<\0/\0l\0i\0>\0\n\0<\0/\0u\0l\0>\0\n\0<\0p\0>\0<\0s\0t\0r\0o\0n\0g\0>\0L\0 u\0s\0u\0a\0r\0i\0 \0a\0c\0c\0e\0p\0t\0a\0 \0e\0x\0p\0r\0e\0s\0s\0a\0m\0e\0n\0t\0,\0 \0p\0e\0r\0 \0l\0a\0 \0u\0t\0i\0l\0i\0t\0z\0a\0c\0i\0ó\0 \0d\0 a\0q\0u\0e\0s\0t\0 \0S\0i\0t\0e\0,\0 \0e\0l\0 \0t\0r\0a\0c\0t\0a\0m\0e\0n\0t\0 \0d\0e\0 \0l\0a\0 \0i\0n\0f\0o\0r\0m\0a\0c\0i\0ó\0 \0r\0e\0c\0o\0l\0l\0i\0d\0a\0 \0e\0n\0 \0l\0a\0 \0f\0o\0r\0m\0a\0 \0i\0 \0a\0m\0b\0 \0l\0e\0s\0 \0f\0i\0n\0a\0l\0i\0t\0a\0t\0s\0 \0a\0n\0t\0e\0r\0i\0o\0r\0m\0e\0n\0t\0 \0e\0s\0m\0e\0n\0t\0a\0t\0s\0.\0<\0/\0s\0t\0r\0o\0n\0g\0>\0 \0I\0 \0a\0i\0x\0í\0 \0m\0a\0t\0e\0i\0x\0 \0r\0e\0c\0o\0n\0e\0i\0x\0 \0c\0o\0n\0è\0i\0x\0e\0r\0 \0l\0a\0 \0p\0o\0s\0s\0i\0b\0i\0l\0i\0t\0a\0t\0 \0d\0e\0 \0r\0e\0b\0u\0t\0j\0a\0r\0 \0e\0l\0 \0t\0r\0a\0c\0t\0a\0m\0e\0n\0t\0 \0d\0 a\0q\0u\0e\0s\0t\0e\0s\0 \0d\0a\0d\0e\0s\0 \0o\0 \0i\0n\0f\0o\0r\0m\0a\0c\0i\0ó\0 \0r\0e\0b\0u\0t\0j\0a\0n\0t\0 \0l\0 ú\0s\0 \0d\0e\0 \0c\0o\0o\0k\0i\0e\0s\0 \0m\0i\0t\0j\0a\0n\0ç\0a\0n\0t\0 \0l\0a\0 \0s\0e\0l\0e\0c\0c\0i\0ó\0 \0d\0e\0 \0l\0a\0 \0c\0o\0n\0f\0i\0g\0u\0r\0a\0c\0i\0ó\0 \0a\0p\0r\0o\0p\0i\0a\0d\0a\0 \0a\0 \0t\0a\0l\0 \0f\0i\0 \0e\0n\0 \0e\0l\0 \0s\0e\0u\0 \0n\0a\0v\0e\0g\0a\0d\0o\0r\0.\0 \0S\0i\0 \0b\0é\0 \0a\0q\0u\0e\0s\0t\0a\0 \0o\0p\0c\0i\0ó\0 \0d\0e\0 \0b\0l\0o\0q\0u\0e\0i\0g\0 \0d\0e\0 \0c\0o\0o\0k\0i\0e\0s\0 \0a\0l\0 \0s\0e\0u\0 \0n\0a\0v\0e\0g\0a\0d\0o\0r\0 \0p\0o\0t\0 \0n\0o\0 \0p\0e\0r\0m\0e\0t\0r\0e\0 \0l\0 ú\0s\0 \0p\0l\0e\0 \0d\0e\0 \0t\0o\0t\0e\0s\0 \0l\0e\0s\0 \0f\0u\0n\0c\0i\0o\0n\0a\0l\0i\0t\0a\0t\0s\0 \0d\0e\0l\0 \0l\0l\0o\0c\0 \0w\0e\0b\0<\0/\0p\0>\0\n\0<\0p\0>\0P\0o\0t\0 \0v\0o\0s\0t\0è\0 \0p\0e\0r\0m\0e\0t\0r\0e\0,\0 \0b\0l\0o\0q\0u\0e\0j\0a\0r\0 \0o\0 \0e\0l\0i\0m\0i\0n\0a\0r\0 \0l\0e\0s\0 \0c\0o\0o\0k\0i\0e\0s\0 \0i\0n\0s\0t\0a\0l\0·\0l\0a\0d\0e\0s\0 \0e\0n\0 \0e\0l\0 \0s\0e\0u\0 \0e\0q\0u\0i\0p\0 \0m\0i\0t\0j\0a\0n\0ç\0a\0n\0t\0 \0l\0a\0 \0c\0o\0n\0f\0i\0g\0u\0r\0a\0c\0i\0ó\0 \0d\0e\0 \0l\0e\0s\0 \0o\0p\0c\0i\0o\0n\0s\0 \0d\0e\0l\0 \0n\0a\0v\0e\0g\0a\0d\0o\0r\0 \0i\0n\0s\0t\0a\0l\0·\0l\0a\0t\0 \0a\0l\0 \0s\0e\0u\0 \0o\0r\0d\0i\0n\0a\0d\0o\0r\0:\0<\0/\0p\0>\0\n\0<\0u\0l\0>\0\n\0<\0l\0i\0>\0<\0a\0 \0h\0r\0e\0f\0=\0\"\0h\0t\0t\0p\0:\0/\0/\0s\0u\0p\0p\0o\0r\0t\0.\0g\0o\0o\0g\0l\0e\0.\0c\0o\0m\0/\0c\0h\0r\0o\0m\0e\0/\0b\0i\0n\0/\0a\0n\0s\0w\0e\0r\0.\0p\0y\0?\0h\0l\0=\0e\0s\0&\0a\0n\0s\0w\0e\0r\0=\09\05\06\04\07\0\"\0 \0t\0a\0r\0g\0e\0t\0=\0\"\0_\0b\0l\0a\0n\0k\0\"\0>\0C\0h\0r\0o\0m\0e\0<\0/\0a\0>\0<\0/\0l\0i\0>\0\n\0<\0l\0i\0>\0<\0a\0 \0h\0r\0e\0f\0=\0\"\0h\0t\0t\0p\0:\0/\0/\0w\0i\0n\0d\0o\0w\0s\0.\0m\0i\0c\0r\0o\0s\0o\0f\0t\0.\0c\0o\0m\0/\0e\0s\0-\0e\0s\0/\0w\0i\0n\0d\0o\0w\0s\07\0/\0h\0o\0w\0-\0t\0o\0-\0m\0a\0n\0a\0g\0e\0-\0c\0o\0o\0k\0i\0e\0s\0-\0i\0n\0-\0i\0n\0t\0e\0r\0n\0e\0t\0-\0e\0x\0p\0l\0o\0r\0e\0r\0-\09\0\"\0 \0t\0a\0r\0g\0e\0t\0=\0\"\0_\0b\0l\0a\0n\0k\0\"\0>\0E\0x\0p\0l\0o\0r\0e\0r\0<\0/\0a\0>\0<\0/\0l\0i\0>\0\n\0<\0l\0i\0>\0<\0a\0 \0h\0r\0e\0f\0=\0\"\0h\0t\0t\0p\0:\0/\0/\0s\0u\0p\0p\0o\0r\0t\0.\0m\0o\0z\0i\0l\0l\0a\0.\0o\0r\0g\0/\0e\0s\0/\0k\0b\0/\0h\0a\0b\0i\0l\0i\0t\0a\0r\0-\0y\0-\0d\0e\0s\0h\0a\0b\0i\0l\0i\0t\0a\0r\0-\0c\0o\0o\0k\0i\0e\0s\0-\0q\0u\0e\0-\0l\0o\0s\0-\0s\0i\0t\0i\0o\0s\0-\0w\0e\0\"\0 \0t\0a\0r\0g\0e\0t\0=\0\"\0_\0b\0l\0a\0n\0k\0\"\0>\0F\0i\0r\0e\0f\0o\0x\0<\0/\0a\0>\0<\0/\0l\0i\0>\0\n\0<\0l\0i\0>\0<\0a\0 \0h\0r\0e\0f\0=\0\"\0h\0t\0t\0p\0:\0/\0/\0s\0u\0p\0p\0o\0r\0t\0.\0a\0p\0p\0l\0e\0.\0c\0o\0m\0/\0k\0b\0/\0p\0h\05\00\04\02\0\"\0 \0t\0a\0r\0g\0e\0t\0=\0\"\0_\0b\0l\0a\0n\0k\0\"\0>\0S\0a\0f\0a\0r\0i\0<\0/\0a\0>\0<\0/\0l\0i\0>\0\n\0<\0/\0u\0l\0>\0\n\0<\0p\0>\0S\0i\0 \0t\0é\0 \0d\0u\0b\0t\0e\0s\0 \0s\0o\0b\0r\0e\0 \0a\0q\0u\0e\0s\0t\0a\0 \0p\0o\0l\0í\0t\0i\0c\0a\0 \0d\0e\0 \0c\0o\0o\0k\0i\0e\0s\0,\0 \0p\0o\0t\0 \0c\0o\0n\0t\0a\0c\0t\0a\0r\0 \0a\0m\0b\0 \0n\0o\0s\0a\0l\0t\0r\0e\0s\0 \0a\0 \0<\0a\0 \0t\0i\0t\0l\0e\0=\0\"\0m\0a\0i\0l\0 \0c\0o\0o\0k\0i\0e\0s\0\"\0 \0h\0r\0e\0f\0=\0\"\0m\0a\0i\0l\0t\0o\0:\0{\0M\0A\0I\0L\0T\0O\0}\0\"\0>\0{\0M\0A\0I\0L\0T\0O\0}\0<\0/\0a\0>\0<\0/\0p\0>\0', '0000-00-00 00:00:00'),
(29, 'es', 'Contacto', '2015-06-18 11:46:57'),
(29, 'ca', 'Contacte', '2015-06-18 11:46:57'),
(31, 'es', 'Ver más', '0000-00-00 00:00:00'),
(32, 'es', 'El proyecto', '2019-12-11 16:48:06'),
(32, 'ca', 'El projecte', '2019-12-11 16:48:06'),
(34, 'es', 'Inicio', '2015-06-18 11:46:57'),
(34, 'ca', 'Inici', '2015-06-18 11:46:57'),
(35, 'es', 'Noticias', '2015-06-18 11:46:57'),
(35, 'ca', 'Notícies', '2015-06-18 11:46:57'),
(36, 'es', 'Productos', '2015-06-18 11:46:57'),
(36, 'ca', 'Productes', '2015-06-18 11:46:57'),
(38, 'es', 'Servicios', '2015-06-18 11:46:58'),
(38, 'ca', 'Serveis', '2015-06-18 11:46:58'),
(67, 'es', 'Aceptar', '2019-05-03 11:42:52'),
(41, 'es', 'Volver', '2015-06-18 11:46:57'),
(41, 'ca', 'Tornar', '2015-06-18 11:46:57'),
(42, 'es', 'Nota legal', '2015-06-18 11:46:57'),
(42, 'ca', 'Nota legal', '2015-06-18 11:46:57'),
(43, 'es', 'Diseño web', '2020-01-28 09:03:46'),
(43, 'ca', 'Disseny web', '2020-01-28 09:03:46'),
(44, 'es', 'Ver mapa más grande', '2015-06-18 11:46:57'),
(44, 'ca', 'Veure mapa més gran', '2015-06-18 11:46:57'),
(83, 'en', 'Mandatory fields (*)', '2015-06-18 11:46:57'),
(84, 'ca', 'L\'email introduït no és vàlid', '2015-06-18 11:46:57'),
(49, 'es', 'Enviar', '2015-06-18 11:46:57'),
(49, 'ca', 'Enviar', '2015-06-18 11:46:57'),
(50, 'es', 'Se ha producido un error al enviar el formulario. Por favor, inténtelo de nuevo en unos minutos.', '2018-09-06 13:29:39'),
(50, 'ca', 'S’ha produït un error en enviar el formulari. Si us plau-ho a provar en uns minuts.', '2018-09-06 13:29:39'),
(83, 'ca', 'Camps obligatoris (*)', '2015-06-18 11:46:57'),
(84, 'es', 'El email introducido no es válido', '2015-06-18 11:46:57'),
(51, 'es', 'Su solicitud ha sido enviada correctamente. En breve nos podremos en contacto con usted.', '2018-09-06 13:30:51'),
(51, 'ca', 'La seva sol·licitud ha estat enviada correctament. En breu ens posarem en contacte amb vostè.', '2018-09-06 13:30:51'),
(52, 'es', 'Mensaje', '2019-12-17 11:05:26'),
(52, 'ca', 'Missatge', '2019-12-17 11:05:26'),
(53, 'es', 'E-mail', '2015-06-18 11:46:57'),
(53, 'ca', 'E-mail', '2015-06-18 11:46:57'),
(54, 'es', 'Nombre y apellidos', '2019-12-17 11:04:51'),
(54, 'ca', 'Nom y cognoms', '2019-12-17 11:04:51'),
(55, 'es', 'Teléfono', '2015-06-18 11:46:57'),
(55, 'ca', 'Telèfon', '2015-06-18 11:46:57'),
(64, 'es', 'Empresa', '2017-04-27 11:48:37'),
(64, 'ca', 'Empresa', '2015-06-18 11:46:57'),
(83, 'es', 'Campos obligatorios (*)', '2015-06-18 11:46:57'),
(3, 'en', 'Previous', '2015-06-18 11:46:57'),
(49, 'en', 'Send', '2015-06-18 11:46:57'),
(50, 'en', 'There was an error submitting the form. Please try again in a few minutes.', '2018-09-06 13:29:39'),
(51, 'en', 'Your request has been sent successfully. We can contact you soon.', '2018-09-06 13:30:51'),
(52, 'en', 'Comments', '2015-06-18 11:46:57'),
(53, 'en', 'E-mail', '2015-06-18 11:46:57'),
(54, 'en', 'Name and surname', '2019-12-17 11:04:51'),
(55, 'en', 'Phone', '2015-06-18 11:46:57'),
(4, 'en', 'Read more', '2015-06-18 11:46:57'),
(44, 'en', 'See map bigger', '2015-06-18 11:46:57'),
(43, 'en', 'Web design', '2020-01-28 09:03:47'),
(42, 'en', 'Legal notice', '2015-06-18 11:46:57'),
(6, 'en', 'Pages', '2015-06-18 11:46:57'),
(7, 'en', 'Next', '2015-06-18 11:46:57'),
(5, 'en', 'More info', '2015-06-18 11:46:57'),
(9, 'en', 'See more', '2015-06-18 11:46:57'),
(10, 'en', 'See all', '2015-06-18 11:46:57'),
(11, 'en', 'See all', '2015-06-18 11:46:57'),
(41, 'en', 'Back', '2015-06-18 11:46:57'),
(67, 'en', 'Accept', '2015-06-18 11:46:57'),
(63, 'en', 'Send us your details and we will contact you.', '2020-01-02 09:59:35'),
(62, 'en', 'Contact', '2015-06-18 11:46:57'),
(29, 'en', 'Contact', '2015-06-18 11:46:57'),
(32, 'en', 'The project', '2019-12-11 16:48:06'),
(34, 'en', 'Home', '2015-06-18 11:46:57'),
(35, 'en', 'News', '2015-06-18 11:46:57'),
(36, 'en', 'Products', '2015-06-18 11:46:57'),
(38, 'en', 'Services', '2015-06-18 11:46:58'),
(13, 'en', 'contact', '2015-06-18 11:46:57'),
(14, 'en', 'about', '2015-12-14 10:24:31'),
(64, 'en', 'About Us', '2015-06-18 11:46:57'),
(76, 'es', 'cookies', '2015-06-18 11:46:58'),
(76, 'ca', 'cookies-ca', '2016-07-05 10:57:59'),
(76, 'en', 'cookies-en', '2016-07-05 10:58:01'),
(78, 'es', 'Hemos recibido su solicitud y en breve nos pondremos en contacto con usted. Gracias.<br /><br />Atentamente,<br />{CONFIG_CLIENTE}<br />{CONFIG_MAILTO}<br /><br />Este mensaje puede contener información confidencial protegida legalmente. Si usted no es el destinatario, o no está autorizado a recibirlo por parte del destinatario, no debe usar, copiar, informar, distribuir, difundir o imprimir este mensaje, ni la información adjunta, bajo ningún medio. Si por error ha recibido este mensaje y usted no es el destinatario del mismo, por favor notifique este hecho al remitente y borre este mensaje. Gracias por su colaboración.', '2016-06-23 09:22:33'),
(78, 'ca', 'Hem rebut la seva sol·licitud i en breu ens posarem en contacte amb vostè. Gràcies. <br /> <br /> Atentament, <br />{CONFIG_CLIENTE}<br />{CONFIG_MAILTO}<br /><br />Aquest missatge pot contenir informació confidencial protegida legalment. Si no és el destinatari, o no està autoritzat a rebre per part del destinatari, no ha d’usar, copiar, informar, distribuir, difondre o imprimir aquest missatge, ni la informació adjunta, sota cap mitjà. Si per error ha rebut aquest missatge i no és el destinatari del mateix, si us plau notifiqui aquest fet al remitent i esborri aquest missatge. Gràcies per la seva col·laboració.', '2016-06-23 09:22:33'),
(78, 'en', 'We have received your request and shortly we will contact you. Thank you.<br /><br />Sincerely,<br />{CONFIG_CLIENTE}<br />{CONFIG_MAILTO}<br /><br />This message may contain confidential and legally privileged information. If you are not the addressee, or are not authorized to receive it for the addressee, you must not use, copy, disclose, distribute, print or disseminate this message or information attached, by any means. If by mistake you have received this message and you are not the intended recipient, please notify this fact to the sender and delete this message. Thanks for your cooperation.', '2016-06-23 09:22:33'),
(79, 'es', 'Envío de formulario de contacto', '2015-06-18 11:46:57'),
(79, 'ca', 'Enviament de formulari de contacte', '2015-06-18 11:46:57'),
(79, 'en', 'Sending contact form', '2015-06-18 11:46:57'),
(80, 'es', 'Utilizamos cookies propias y de terceros para ofrecer nuestros servicios y recoger datos estadísticos. Continuar navegando implica su aceptación.', '2015-06-18 11:46:57'),
(80, 'ca', 'Utilitzem galetes pròpies i de tercers per oferir els nostres serveis i recollir dades estadístiques. Continuar navegant implica la seva acceptació.', '2015-06-18 11:46:57'),
(80, 'en', 'We use our own cookies and third parties ones to offer our services and collect statistical data. If you continue browsing the internet you accept them.', '2015-06-18 11:46:57'),
(81, 'es', 'Más información', '2015-06-18 11:46:57'),
(81, 'ca', 'Més informació', '2015-06-18 11:46:57'),
(81, 'en', 'More information', '2015-06-18 11:46:57'),
(82, 'es', 'Aceptar', '2015-06-18 11:46:57'),
(82, 'ca', 'Acceptar', '2015-06-18 11:46:57'),
(82, 'en', 'Accept', '2015-06-18 11:46:57'),
(84, 'en', 'Wrong email address', '2015-06-18 11:46:57'),
(90, 'es', 'inicio', '2017-01-24 11:02:05'),
(90, 'ca', 'inici', '2015-06-18 11:46:57'),
(90, 'en', 'home', '2015-06-18 11:46:57'),
(92, 'es', 'El email introducido no es válido', '2015-06-18 14:03:30'),
(92, 'ca', 'Camps obligatoris (*)', '2015-06-18 14:03:30'),
(92, 'en', 'Mandatory fields (*)', '2015-06-18 14:03:30'),
(100, 'es', 'noticias', '0000-00-00 00:00:00'),
(100, 'ca', 'noticies', '0000-00-00 00:00:00'),
(100, 'en', 'news', '0000-00-00 00:00:00'),
(101, 'es', '', '0000-00-00 00:00:00'),
(101, 'ca', '', '0000-00-00 00:00:00'),
(101, 'en', '', '0000-00-00 00:00:00'),
(102, 'es', '', '0000-00-00 00:00:00'),
(102, 'ca', '', '0000-00-00 00:00:00'),
(102, 'en', '', '0000-00-00 00:00:00'),
(103, 'es', '', '0000-00-00 00:00:00'),
(103, 'ca', '', '0000-00-00 00:00:00'),
(103, 'en', '', '0000-00-00 00:00:00'),
(104, 'es', '', '0000-00-00 00:00:00'),
(104, 'ca', '', '0000-00-00 00:00:00'),
(104, 'en', '', '0000-00-00 00:00:00'),
(105, 'es', 'Adjuntar', '0000-00-00 00:00:00'),
(105, 'ca', 'Adjuntar', '0000-00-00 00:00:00'),
(105, 'en', 'Attach', '0000-00-00 00:00:00'),
(106, 'es', 'Productos', '0000-00-00 00:00:00'),
(106, 'ca', 'Productes', '0000-00-00 00:00:00'),
(106, 'en', 'Products', '0000-00-00 00:00:00'),
(107, 'es', 'Noticias', '0000-00-00 00:00:00'),
(107, 'ca', 'Notícies', '0000-00-00 00:00:00'),
(107, 'en', 'News', '0000-00-00 00:00:00'),
(115, 'es', 'Home', '2017-04-27 11:48:48'),
(115, 'ca', 'Home', '2017-04-27 11:48:48'),
(115, 'en', 'Home', '2017-04-27 11:48:48'),
(137, 'es', 'Debes aceptar los términos y condiciones', '0000-00-00 00:00:00'),
(137, 'ca', 'Cal aceptar els termes i condicions', '0000-00-00 00:00:00'),
(137, 'en', 'Accept terms and conditions', '0000-00-00 00:00:00'),
(139, 'es', 'No hay resultados para la búsqueda', '0000-00-00 00:00:00'),
(139, 'ca', 'No hi ha resultats per a la cerca', '0000-00-00 00:00:00'),
(139, 'en', '', '0000-00-00 00:00:00'),
(157, 'es', 'info@latevaweb.com', '0000-00-00 00:00:00'),
(157, 'ca', 'info@latevaweb.com', '0000-00-00 00:00:00'),
(157, 'en', 'info@latevaweb.com', '0000-00-00 00:00:00'),
(158, 'es', 'legal', '0000-00-00 00:00:00'),
(158, 'ca', 'legal-ca', '0000-00-00 00:00:00'),
(158, 'en', 'legal-en', '0000-00-00 00:00:00'),
(160, 'es', 'es', '0000-00-00 00:00:00'),
(160, 'ca', 'es', '0000-00-00 00:00:00'),
(160, 'en', 'es', '0000-00-00 00:00:00'),
(164, 'es', '<span>Unidad de investigación en Servicios Sanitarios, </span><br /><span>Instituto Hospital del Mar de Investigaciones Médicas (IMIM)</span><br /><span>Parc de Recerca Biomèdica de Barcelona (PRBB),</span><br /><span>Calle Dr Aiguader, 88</span><br /><span>08003, Barcelona</span>', '2020-01-22 14:48:27'),
(164, 'ca', '<span>Unidad de investigación en Servicios Sanitarios, </span><br /><span>Instituto Hospital del Mar de Investigaciones Médicas (IMIM)</span><br /><span>Parc de Recerca Biomèdica de Barcelona (PRBB),</span><br /><span>Calle Dr Aiguader, 88</span><br /><span>08003, Barcelona</span>', '2020-01-22 14:48:27'),
(164, 'en', '<span>Unidad de investigación en Servicios Sanitarios, </span><br /><span>Instituto Hospital del Mar de Investigaciones Médicas (IMIM)</span><br /><span>Parc de Recerca Biomèdica de Barcelona (PRBB),</span><br /><span>Calle Dr Aiguader, 88</span><br /><span>08003, Barcelona</span>', '2020-01-22 14:48:27'),
(171, 'es', 'Acepto la', '2018-06-05 08:51:34'),
(171, 'ca', 'Accepto la', '2018-06-05 08:51:34'),
(171, 'en', 'I accept', '2018-06-05 08:51:34'),
(172, 'es', 'Cerrar', '0000-00-00 00:00:00'),
(172, 'ca', 'Tancar', '0000-00-00 00:00:00'),
(172, 'en', 'Close', '0000-00-00 00:00:00'),
(173, 'es', 'Ampliar', '0000-00-00 00:00:00'),
(173, 'ca', 'Ampliar', '0000-00-00 00:00:00'),
(173, 'en', 'Enlarge', '0000-00-00 00:00:00'),
(174, 'es', 'Buscar', '0000-00-00 00:00:00'),
(174, 'ca', 'Buscar', '0000-00-00 00:00:00'),
(174, 'en', 'Search', '0000-00-00 00:00:00'),
(177, 'es', '', '0000-00-00 00:00:00'),
(177, 'ca', '', '0000-00-00 00:00:00'),
(177, 'en', '', '0000-00-00 00:00:00'),
(182, 'es', 'Home', '2017-04-27 11:48:59'),
(182, 'ca', 'Home', '2017-04-27 11:48:59'),
(182, 'en', 'Home', '2017-04-27 11:48:59'),
(183, 'es', '', '0000-00-00 00:00:00'),
(183, 'ca', '', '0000-00-00 00:00:00'),
(183, 'en', '', '0000-00-00 00:00:00'),
(186, 'es', 'Contacto', '2017-04-27 11:47:56'),
(186, 'ca', 'Contacte', '2017-04-27 11:47:57'),
(186, 'en', 'Contact', '2017-04-27 11:47:57'),
(187, 'es', '', '0000-00-00 00:00:00'),
(187, 'ca', '', '0000-00-00 00:00:00'),
(187, 'en', '', '0000-00-00 00:00:00'),
(189, 'es', '', '0000-00-00 00:00:00'),
(189, 'ca', '', '0000-00-00 00:00:00'),
(189, 'en', '', '0000-00-00 00:00:00'),
(190, 'es', '', '0000-00-00 00:00:00'),
(190, 'ca', '', '0000-00-00 00:00:00'),
(190, 'en', '', '0000-00-00 00:00:00'),
(191, 'es', 'Empresa', '2017-04-27 11:48:36'),
(191, 'ca', 'Empresa', '2017-04-27 11:48:36'),
(191, 'en', 'About Us', '2017-04-27 11:48:36'),
(192, 'es', 'Codi Risc', '2020-01-02 08:31:12'),
(192, 'ca', 'Codi Risc', '2020-01-02 08:31:12'),
(192, 'en', 'Codi Risc', '2020-01-02 08:31:12'),
(193, 'es', 'El proyecto Codi Risc', '2020-01-02 08:34:49'),
(193, 'ca', 'El projecte Codi Risc', '2020-01-02 08:34:49'),
(193, 'en', 'Codi Risc Project', '2020-01-02 08:34:49'),
(194, 'es', 'Codi Risc', '2020-01-02 08:31:14'),
(194, 'ca', 'Codi Risc', '2020-01-02 08:31:14'),
(194, 'en', 'Codi Risc', '2020-01-02 08:31:15'),
(195, 'es', 'Codi Risc', '2020-01-02 08:28:50'),
(195, 'ca', 'Codi Risc', '2020-01-02 08:28:50'),
(195, 'en', 'Codi Risc', '2020-01-02 08:28:50'),
(196, 'es', 'Noticias', '2020-01-02 08:31:42'),
(196, 'ca', 'Noticíes', '2020-01-02 08:31:42'),
(196, 'en', 'News', '2020-01-02 08:31:42'),
(198, 'es', 'Contacto', '2020-01-02 08:31:05'),
(198, 'ca', 'Contacte', '2020-01-02 08:31:05'),
(198, 'en', 'Contact', '2020-01-02 08:31:05'),
(199, 'es', 'Política de Cookies', '2017-04-27 11:48:18'),
(199, 'ca', 'Política de Cookies', '2017-04-27 11:48:19'),
(199, 'en', 'Cookies Policy', '2017-04-27 11:48:19'),
(200, 'es', '', '0000-00-00 00:00:00'),
(200, 'ca', '', '0000-00-00 00:00:00'),
(200, 'en', '', '0000-00-00 00:00:00'),
(201, 'es', 'Nota Legal', '2017-04-27 11:49:19'),
(201, 'ca', 'Nota Legal', '2017-04-27 11:49:19'),
(201, 'en', 'Legal Notice', '2017-04-27 11:49:19'),
(202, 'es', '', '0000-00-00 00:00:00'),
(202, 'ca', '', '0000-00-00 00:00:00'),
(202, 'en', '', '0000-00-00 00:00:00'),
(205, 'es', 'https://facebook.com/', '2017-02-02 14:40:06'),
(205, 'ca', 'https://facebook.com/', '2017-02-02 14:40:06'),
(205, 'en', 'https://facebook.com/', '2017-02-02 14:40:06'),
(206, 'es', '', '0000-00-00 00:00:00'),
(206, 'ca', '', '0000-00-00 00:00:00'),
(206, 'en', '', '0000-00-00 00:00:00'),
(207, 'es', '', '2017-04-27 11:46:54'),
(207, 'ca', '', '2017-04-27 11:46:54'),
(207, 'en', '', '2017-04-27 11:46:54'),
(208, 'es', '', '0000-00-00 00:00:00'),
(208, 'ca', '', '0000-00-00 00:00:00'),
(208, 'en', '', '0000-00-00 00:00:00'),
(209, 'es', '', '2017-04-27 11:46:30'),
(209, 'ca', '', '2017-04-27 11:46:30'),
(209, 'en', '', '2017-04-27 11:46:30'),
(210, 'es', '', '0000-00-00 00:00:00'),
(210, 'ca', '', '0000-00-00 00:00:00'),
(210, 'en', '', '0000-00-00 00:00:00'),
(211, 'es', '', '2017-04-27 11:46:40'),
(211, 'ca', '', '2017-04-27 11:46:40'),
(211, 'en', '', '2017-04-27 11:46:40'),
(212, 'es', '', '0000-00-00 00:00:00'),
(212, 'ca', '', '0000-00-00 00:00:00'),
(212, 'en', '', '0000-00-00 00:00:00'),
(213, 'es', '', '2017-04-27 11:46:35'),
(213, 'ca', '', '2017-04-27 11:46:35'),
(213, 'en', '', '2017-04-27 11:46:35'),
(214, 'es', '', '0000-00-00 00:00:00'),
(214, 'ca', '', '0000-00-00 00:00:00'),
(214, 'en', '', '0000-00-00 00:00:00'),
(215, 'es', '', '2017-04-27 11:47:04'),
(215, 'ca', '', '2017-04-27 11:47:04'),
(215, 'en', '', '2017-04-27 11:47:04'),
(216, 'es', '', '0000-00-00 00:00:00'),
(216, 'ca', '', '0000-00-00 00:00:00'),
(216, 'en', '', '0000-00-00 00:00:00'),
(217, 'es', '', '2017-04-27 11:46:49'),
(217, 'ca', '', '2017-04-27 11:46:49'),
(217, 'en', '', '2017-04-27 11:46:49'),
(218, 'es', '', '0000-00-00 00:00:00'),
(218, 'ca', '', '0000-00-00 00:00:00'),
(218, 'en', '', '0000-00-00 00:00:00'),
(219, 'es', '', '2017-04-27 11:46:59'),
(219, 'ca', '', '2017-04-27 11:46:59'),
(219, 'en', '', '2017-04-27 11:46:59'),
(220, 'es', '', '0000-00-00 00:00:00'),
(220, 'ca', '', '0000-00-00 00:00:00'),
(220, 'en', '', '0000-00-00 00:00:00'),
(221, 'es', 'publicaciones', '2020-02-27 16:48:20'),
(221, 'ca', 'publicacions', '2020-02-27 16:48:20'),
(221, 'en', 'publications', '2020-02-27 16:48:20'),
(224, 'es', 'Publicaciones', '2020-01-02 09:15:52'),
(224, 'ca', 'Publicacions', '2020-01-02 09:15:52'),
(224, 'en', 'Publications', '2020-01-02 09:15:52'),
(225, 'es', '', '0000-00-00 00:00:00'),
(225, 'ca', '', '0000-00-00 00:00:00'),
(225, 'en', '', '0000-00-00 00:00:00'),
(227, 'es', 'Noticias', '2020-01-02 08:49:55'),
(227, 'ca', 'Notícies', '2020-01-02 08:49:55'),
(227, 'en', 'News', '2020-01-02 08:49:55'),
(228, 'es', 'Noticias', '2020-01-02 08:49:45'),
(228, 'ca', 'Notícies', '2020-01-02 08:49:45'),
(228, 'en', 'News', '2020-01-02 08:49:45'),
(229, 'es', 'Página no encontrada \"asd\" ok', '2019-06-21 10:53:43'),
(229, 'ca', 'No s\'ha trobat la pàgina', '2017-04-27 11:38:51'),
(229, 'en', 'Page not found', '2017-04-27 11:38:41'),
(230, 'es', 'La página solicitada puede no estar disponible, haber cambiado de dirección (URL) o no existir.', '2017-04-27 11:39:44'),
(230, 'ca', 'La pàgina sol·licitada pot no estar disponible, haver canviat de direcció (URL) o no existir.', '2017-04-27 11:39:44'),
(230, 'en', 'The page you requested may not be available, must have changed (URL) or does not exist.', '2017-04-27 11:39:44'),
(231, 'es', 'PÁGINA NO ENCONTRADA', '2017-03-29 09:40:22'),
(231, 'ca', 'NO S\'HA TROBAT LA PÀGINA', '2017-03-29 09:40:22'),
(231, 'en', 'PAGE NOT FOUND', '2017-03-29 09:40:22'),
(232, 'es', '<p>La página solicitada puede no estar disponible, haber cambiado de dirección (URL) o no existir. Disculpe las molestias.</p>\n<p>Con frecuencia es debido a algún error al escribir la dirección de la página (URL). Compruébela de nuevo para ver si es correcta.</p>', '2020-01-02 10:52:30'),
(232, 'ca', '<p>La pàgina sol·licitada pot no estar disponible, haver canviat de direcció (URL) o no existir. Disculpi les molèsties.</p>\n<p>Sovint és a causa d\'algun error en escriure l\'adreça de la pàgina (URL). Comprovi-la nou per veure si és correcta.</p>', '2020-01-02 10:52:30'),
(232, 'en', '<p>The page you requested may not be available, must have changed (URL) or does not exist. Sorry for the inconvenience.</p>\n<p>It is often due to an error when writing the page address (URL). Check it again to see if it is correct.</p>', '2020-01-02 10:52:30'),
(233, 'es', '<h2>¿Qué son las cookies?</h2>\n<p>Cookie es un fichero que se descarga en su ordenador al acceder a determinadas páginas web. Las cookies permiten a una página web, entre otras cosas, almacenar y recuperar información sobre los hábitos de navegación de un usuario o de su equipo y, dependiendo de la información que contengan y de la forma en que utilice su equipo, pueden utilizarse para reconocer al usuario. El navegador del usuario memoriza cookies en el disco duro solamente durante la sesión actual ocupando un espacio de memoria mínimo y no perjudicando al ordenador. Las cookies no contienen ninguna clase de información personal específica, y la mayoría de las mismas se borran del disco duro al finalizar la sesión de navegador (las denominadas cookies de sesión).</p>\n<p>La mayoría de los navegadores aceptan como estándar a las cookies y, con independencia de las mismas, permiten o impiden en los ajustes de seguridad las cookies temporales o memorizadas.</p>\n<p>Sin su expreso consentimiento –mediante la activación de las cookies en su navegador– esta página no enlazará en las cookies los datos memorizados con sus datos personales proporcionados en el momento del registro o la compra.</p>\n<p><strong>¿Qué tipos de cookies utiliza esta página web?</strong></p>\n<ul>\n<li>Cookies técnicas: Son aquéllas que permiten al usuario la navegación a través de una página web, plataforma o aplicación y la utilización de las diferentes opciones o servicios que en ella existan como, por ejemplo, controlar el tráfico y la comunicación de datos, identificar la sesión, acceder a partes de acceso restringido, recordar los elementos que integran un pedido, realizar el proceso de compra de un pedido, realizar la solicitud de inscripción o participación en un evento, utilizar elementos de seguridad durante la navegación, almacenar contenidos para la difusión de videos o sonido o compartir contenidos a través de redes sociales.</li>\n<li>Cookies de personalización: Son aquéllas que permiten al usuario acceder al servicio con algunas características de carácter general predefinidas en función de una serie de criterios en el terminal del usuario como por ejemplo serian el idioma, el tipo de navegador a través del cual accede al servicio, la configuración regional desde donde accede al servicio, etc.</li>\n<li>Cookies de análisis: Son aquéllas que bien tratadas por nosotros o por terceros, nos permiten cuantificar el número de usuarios y así realizar la medición y análisis estadístico de la utilización que hacen los usuarios del servicio ofertado. Para ello se analiza su navegación en nuestra página web con el fin de mejorar la oferta de productos o servicios que le ofrecemos.</li>\n<li>Cookies publicitarias: Son aquéllas que, bien tratadas por nosotros o por terceros, nos permiten gestionar de la forma más eficaz posible la oferta de los espacios publicitarios que hay en la página web, adecuando el contenido del anuncio al contenido del servicio solicitado o al uso que realice de nuestra página web. Para ello podemos analizar sus hábitos de navegación en Internet y podemos mostrarle publicidad relacionada con su perfil de navegación.</li>\n<li>Cookies de publicidad comportamental: Son aquéllas que permiten la gestión, de la forma más eficaz posible, de los espacios publicitarios que, en su caso, el editor haya incluido en una página web, aplicación o plataforma desde la que presta el servicio solicitado. Estas cookies almacenan información del comportamiento de los usuarios obtenida a través de la observación continuada de sus hábitos de navegación, lo que permite desarrollar un perfil específico para mostrar publicidad en función del mismo.</li>\n</ul>\n<h2>Cookies de terceros</h2>\n<p>Este sitio web puede utilizar servicios de terceros que recopilarán información con fines estadísticos, de uso del Site por parte del usuario y para la prestacion de otros servicios relacionados con la actividad del Website y otros servicios de Internet.</p>\n<p>En particular, este sitio Web utiliza:</p>\n<ul>\n<li>Las cookies de <a title=\"cookies de Google Analytics\" href=\"http://www.google.com/intl/es/analytics/privacyoverview.html\" target=\"_blank\">Google Analytics</a>: se utilizan con el fin de analizar y medir cómo los visitantes usan este sitio web. La información sirve para elaborar informes que permiten mejorar este sitio. Estas <em>cookies </em>recopilan información en forma anónima, incluyendo el número de visitantes al sitio, cómo han llegado al mismo y las páginas que visitó mientras navegaba en nuestro sitio web.</li>\n<li>Las cookies de <a title=\" cookies de Youtube\" href=\"https://support.google.com/youtube/answer/2407785?hl=es-419\" target=\"_blank\">Youtube</a>: se utilizan para contabilizar el número de visualizaciones de los videos incrustados en otras web.</li>\n<li>Las cookies de <a title=\"cookies de AddThis\" href=\"http://www.addthis.com/privacy\" target=\"_blank\">AddThis</a>: es una empresa tecnológica que permite a los sitios web y a sus usuarios compartir fácilmente el contenido con los demás, a través de iconos de intercambio y de los destinos de bookmarking social. Las cookies AddThis se utilizan con el fin de habilitar el contenido para ser compartido. AddThis también se utiliza para recopilar información sobre cómo se comparte contenido del sitio web. Las cookies ayudan a identificar de forma única a un usuario (aunque no de forma personal, sino en cuanto a dirección) para no repetir tareas dentro de un periodo de tiempo especificado.</li>\n<li>Otras cookies de personalización: Estas cookies permiten al usuario especificar o personalizar algunas características de las opciones generales de la página web. Por ejemplo, definir el idioma, configuración regional o tipo de navegador.<br /><br /><a title=\"Addthis\" href=\"http://www.addthis.com/privacy\" target=\"_blank\">Addthis</a><br /><a title=\"Facebook\" href=\"https://es-es.facebook.com/help/cookies\" target=\"_blank\">Facebook</a>  <br /><a title=\"Twitter\" href=\"https://twitter.com/privacy\" target=\"_blank\">Twitter</a><br /><a title=\"Google\" href=\"http://www.google.es/intl/es/policies/technologies/types/\" target=\"_blank\">Google</a><br /><a title=\"YouTube\" href=\"https://support.google.com/youtube/answer/2407785?hl=es-419\" target=\"_blank\">YouTube</a><strong><br /><br />El Usuario acepta expresamente, por la utilización de este Site, el tratamiento de la informació  recabada en la forma y con los fines anteriormente mencionados.</strong> Y asimismo reconoce conocer la posibilidad de rechazar el tratamiento de tales datos o informació  rechazando el uso de Cookies mediante la selección de la configuración apropiada a tal fin en su navegador. Si bien esta opción de bloqueo de Cookies en su navegador puede no permitirle el uso pleno de todas las funcionalidades del Website</li>\n</ul>\n<p>Puede permitir, bloquear o eliminar las cookies instaladas en su equipo mediante la configuración de las opciones del navegador instalado en su ordenador:</p>\n<ul>\n<li><a href=\"http://support.google.com/chrome/bin/answer.py?hl=es&answer=95647\" target=\"_blank\">Chrome</a></li>\n<li><a href=\"http://windows.microsoft.com/es-es/windows7/how-to-manage-cookies-in-internet-explorer-9\" target=\"_blank\">Explorer</a></li>\n<li><a href=\"http://support.mozilla.org/es/kb/habilitar-y-deshabilitar-cookies-que-los-sitios-we\" target=\"_blank\">Firefox</a></li>\n<li><a href=\"http://support.apple.com/kb/ph5042\" target=\"_blank\">Safari</a></li>\n</ul>\n<p>Si tiene dudas sobre esta política de cookies, puede contactar con nosotros en <a title=\"mail cookies\" href=\"mailto:{CONFIG_MAILTO}\">{CONFIG_MAILTO}</a></p>', '2017-04-27 11:50:32');
INSERT INTO `textos_content` (`id`, `idioma`, `textarea_titulo`, `last_modified`) VALUES
(233, 'ca', '<h2>¿Qué són les cookies?</h2>\n<p>Cookie és un fitxer que es descarrega al seu ordinador en accedir a determinades pàgines web. Les cookies permeten a una pàgina web, entre altres coses, emmagatzemar i recuperar informació sobre els hàbits de navegació d’un usuari o del seu equip i, depenent de la informació que continguin i de la forma en què utilitzi el seu equip, es poden utilitzar per reconèixer l’usuari. El navegador de l’usuari memoritza cookies al disc dur només durant la sessió actual ocupant un espai de memòria mínim i no perjudicant a l’ordinador. Les galetes no contenen cap mena d’informació personal específica, i la majoria de les mateixes s’esborren del disc dur en finalitzar la sessió de navegador (les anomenades galetes de sessió).</p>\n<p>La mayoría de los navegadores aceptan como estándar a las cookies y, con independencia de las mismas, permiten o impiden en los ajustes de seguridad las cookies temporales o memorizadas.</p>\n<p>Sin su expreso consentimiento –mediante la activación de las cookies en su navegador– esta página no enlazará en las cookies los datos memorizados con sus datos personales proporcionados en el momento del registro o la compra.</p>\n<p><strong>Quins tipus de cookies utilitza aquesta pàgina web?</strong></p>\n<ul>\n<li>Cookies tècniques: Són aquelles que permeten a l’usuari la navegació a través d’una pàgina web, plataforma o aplicació i la utilització de les diferents opcions o serveis que en ella hi hagi com, per exemple, controlar el trànsit i la comunicació de dades, identificar la sessió, accedir a parts d’accés restringit, recordar els elements que integren una comanda, realitzar el procés de compra d’una comanda , realitzar la sol · licitud d’inscripció o participació en un esdeveniment , utilitzar elements de seguretat durant la navegació , emmagatzemar continguts per a la difusió de vídeos o so o compartir continguts a través de xarxes socials.</li>\n<li>Cookies de personalització: Són aquelles que permeten a l’usuari accedir al servei amb algunes característiques de caràcter general predefinides en funció d’una sèrie de criteris en el terminal de l’usuari com ara serien l’idioma, el tipus de navegador a través del qual s’accedeix al servei , la configuració regional des d’on accedeix al servei , etc .</li>\n<li>Cookies d’anàlisi: Són aquelles que ben tractades per nosaltres o per tercers , ens permeten quantificar el nombre d’usuaris i així realitzar el mesurament i anàlisi estadística de la utilització que fan els usuaris del servei ofert . Per a això s’analitza la seva navegació a la nostra pàgina web amb la finalitat de millorar l’oferta de productes o serveis que li oferim .</li>\n<li>Cookies publicitàries: Són aquelles que, ben tractades per nosaltres o per tercers, ens permeten gestionar de la forma més eficaç possible l’oferta dels espais publicitaris que hi ha a la pàgina web, adequant el contingut de l’ anunci al contingut del servei sol · licitat oa l’ús que realitzi de la nostra pàgina web . Per a això podem analitzar els seus hàbits de navegació a Internet i podem mostrar publicitat relacionada amb el seu perfil de navegació .</li>\n<li>Cookies de publicitat comportamental: Són aquelles que permeten la gestió, de la forma més eficaç possible, dels espais publicitaris que, si escau, l’editor hagi inclòs en una pàgina web, aplicació o plataforma des de la que presta el servei sol · licitat . Aquestes cookies emmagatzemen informació del comportament dels usuaris obtinguda a través de l’observació continuada dels seus hàbits de navegació, el que permet desenvolupar un perfil específic per mostrar publicitat en funció d’aquest.</li>\n</ul>\n<h2>Cookies de tercers</h2>\n<p>Aquest lloc web pot utilitzar serveis de tercers que recopilaran informació amb finalitats estadístiques, d’ús del Site per part de l’usuari i per a la prestacion d’altres serveis relacionats amb l’activitat del lloc web i altres serveis d’Internet .</p>\n<p>En particular, aquest lloc web utilitza:</p>\n<ul>\n<li>Les cookies de <a title=\"cookies de Google Analytics\" href=\"http://www.google.com/intl/es/analytics/privacyoverview.html\" target=\"_blank\">Google Analytics</a>: s’utilitzen per tal d’analitzar i mesurar com els visitants usen aquest lloc web . La informació serveix per elaborar informes que permeten millorar aquest lloc . Aquestes cookies recopilen informació en forma anònima, incloent el nombre de visitants al lloc, com han arribat a aquest i les pàgines que va visitar mentre navegava al nostre lloc web.</li>\n<li>Les cookies de <a title=\" cookies de Youtube\" href=\"https://support.google.com/youtube/answer/2407785?hl=es-419\" target=\"_blank\">Youtube</a>: s’utilitzen per comptabilitzar el nombre de visualitzacions dels vídeos incrustats en altres web.</li>\n<li>Les cookies de <a title=\"cookies de AddThis\" href=\"http://www.addthis.com/privacy\" target=\"_blank\">AddThis</a>: és una empresa tecnològica que permet als llocs web i als seus usuaris compartir fàcilment el contingut amb els altres, a través d’icones d’intercanvi i de les destinacions de bookmarking social . Les cookies AddThis s’utilitzen per tal d’habilitar el contingut per ser compartit . AddThis també s’utilitza per recopilar informació sobre com es comparteix contingut del lloc web . Les cookies ajuden a identificar de forma única a un usuari ( encara que no de forma personal, sinó pel que fa a direcció ) per no repetir tasques dins d’un període de temps especificat.</li>\n<li>Altres cookies de personalització: Aquestes cookies permeten a l’usuari especificar o personalitzar algunes característiques de les opcions generals de la pàgina web . Per exemple, definir l’idioma, configuració regional o tipus de navegador .<br /><br /><a title=\"Addthis\" href=\"http://www.addthis.com/privacy\" target=\"_blank\">Addthis</a><br /><a title=\"Facebook\" href=\"https://es-es.facebook.com/help/cookies\" target=\"_blank\">Facebook</a>  <br /><a title=\"Twitter\" href=\"https://twitter.com/privacy\" target=\"_blank\">Twitter</a><br /><a title=\"Google\" href=\"http://www.google.es/intl/es/policies/technologies/types/\" target=\"_blank\">Google</a><br /><a title=\"YouTube\" href=\"https://support.google.com/youtube/answer/2407785?hl=es-419\" target=\"_blank\">YouTube</a><strong><br /><br />El Usuario acepta expresamente, por la utilización de este Site, el tratamiento de la informació  recabada en la forma y con los fines anteriormente mencionados.</strong> Y asimismo reconoce conocer la posibilidad de rechazar el tratamiento de tales datos o informació  rechazando el uso de Cookies mediante la selección de la configuración apropiada a tal fin en su navegador. Si bien esta opción de bloqueo de Cookies en su navegador puede no permitirle el uso pleno de todas las funcionalidades del Website</li>\n</ul>\n<p>Pot vostè permetre, bloquejar o eliminar les cookies instal · lades en el seu equip mitjançant la configuració de les opcions del navegador instal·lat al seu ordinador:</p>\n<ul>\n<li><a href=\"http://support.google.com/chrome/bin/answer.py?hl=es&answer=95647\" target=\"_blank\">Chrome</a></li>\n<li><a href=\"http://windows.microsoft.com/es-es/windows7/how-to-manage-cookies-in-internet-explorer-9\" target=\"_blank\">Explorer</a></li>\n<li><a href=\"http://support.mozilla.org/es/kb/habilitar-y-deshabilitar-cookies-que-los-sitios-we\" target=\"_blank\">Firefox</a></li>\n<li><a href=\"http://support.apple.com/kb/ph5042\" target=\"_blank\">Safari</a></li>\n</ul>\n<p>Si té dubtes sobre aquesta política de cookies, pot contactar amb nosaltres a <a title=\"mail cookies\" href=\"mailto:{CONFIG_MAILTO}\">{CONFIG_MAILTO}</a></p>', '2017-04-27 11:50:32'),
(233, 'en', '<h2>What are cookies</h2>\n<p>Cookies Policy Cookie is a file that is downloaded to your computer to access certain web pages. Cookies allow a website, among other things, store and retrieve information about browsing habits of a user or your computer, depending on the information they contain and the way you use your computer, can be used to recognize user . The user’s browser stores cookies on the hard drive only during the current session occupies minimal memory space and not harming the computer. The cookies do not contain any specific personal information, and most of them are deleted from the hard drive at the end of the browser session (so-called session cookies) .</p>\n<p>Most browsers accept cookies as standard and, independently thereof, in granting or denying security settings stored cookies or temporary .</p>\n<p>Without your express consent, by enabling cookies in your browser, this page will not link in the cookies stored data with your personal data provided at the time of registration or purchase ..</p>\n<p><strong>What types of cookies used this site ?</strong></p>\n<ul>\n<li>Technical Cookies: These are those that allow the user to navigate through a web page or application platform and the use of different options or services it exist as, for example, control traffic and data communication, identify session, access restricted parts, remember the elements of an order, make the buying process an order, make the request for registration or participation in an event, use the security features while browsing store content for broadcast or sound or video content sharing through social networks.</li>\n<li>Customization Cookies: These are those that allow the user to access the service with some features of a general nature based on a predefined set of criteria in the user terminal such as would be the language, the type of browser through which you access the service, the locale from which you access the service, etc. .</li>\n<li>Cookies analysis: those that are well treated by us or by third parties, allow us to quantify the number of users and perform the measurement and statistical analysis of the use made by users of the service provided. This is discussed in browsing our website in order to improve the supply of products or services that we offer.</li>\n<li>Advertising Cookies: These are those who, well treated by us or by third parties, allow us to manage the most effective way possible to offer advertising spaces in the website, tailoring ad content to the content of the requested service or use they make of our website. So we can analyze your browsing habits on the Internet and we can show you regarding your listing Advertise with navigation</li>\n<li>Cookies behavioral advertising: are those that enable the management, the most efficient way of advertising spaces, where appropriate, the publisher has included a website, application or platform from providing the requested service. These cookies store information on user behavior obtained through continued observing their browsing habits, allowing develop a specific profile to display ads based on the same .</li>\n</ul>\n<h2>Third Party Cookies</h2>\n<p>This website may use third party services that collect information for statistical purposes, for use of the Site by the user and to the provision of other services relating to website activity and other Internet services.<br />In particular, this Web site uses:</p>\n<ul>\n<li><a title=\"cookies de Google Analytics\" href=\"http://www.google.com/intl/es/analytics/privacyoverview.html\" target=\"_blank\">Google Analytics</a> cookies: are used to analyze and measure how visitors use this website. The information used to prepare reports that improve this site. These cookies collect information in an anonymous form, including the number of visitors to the site, how they got to it and which pages you visited while sailing on our website.</li>\n<li>Cookies <a title=\" cookies de Youtube\" href=\"https://support.google.com/youtube/answer/2407785?hl=es-419\" target=\"_blank\">Youtube</a>: used to count the number of views of objects embedded in other web videos.</li>\n<li><a title=\"cookies de AddThis\" href=\"http://www.addthis.com/privacy\" target=\"_blank\">AddThis</a> cookies: is a technology company that enables web sites and users to easily share content with others through sharing icons and destinations of social bookmarking. The AddThis cookies are used in order to enable the content to be shared. AddThis also used to collect information about website content is shared. Cookies help uniquely identify a user (but not personally, but in terms of direction ) to avoid repeating tasks within a specified time period.</li>\n<li>Other customization cookies: These cookies allow the user to specify or customize some features of the general options of the website. For example, set the language, locale or browser type.<br /><br /><a title=\"Addthis\" href=\"http://www.addthis.com/privacy\" target=\"_blank\">Addthis</a><br /><a title=\"Facebook\" href=\"https://es-es.facebook.com/help/cookies\" target=\"_blank\">Facebook</a>  <br /><a title=\"Twitter\" href=\"https://twitter.com/privacy\" target=\"_blank\">Twitter</a><br /><a title=\"Google\" href=\"http://www.google.es/intl/es/policies/technologies/types/\" target=\"_blank\">Google</a><br /><a title=\"YouTube\" href=\"https://support.google.com/youtube/answer/2407785?hl=es-419\" target=\"_blank\">YouTube<br /><br /></a><strong>The user expressly agrees by using this Site, treatment of information collected in the manner and with the above purposes.</strong> And know also recognizes the possibility of refusing the processing of such data or information by rejecting the use of cookies by selecting the appropriate settings for such purpose in your browser . While this option of blocking cookies on your browser may not allow the full use of all the functionality of the Website.</li>\n</ul>\n<p>You can allow, block or delete cookies installed on your computer by setting your browser options installed on your computer:</p>\n<ul>\n<li><a href=\"http://support.google.com/chrome/bin/answer.py?hl=es&answer=95647\" target=\"_blank\">Chrome</a></li>\n<li><a href=\"http://windows.microsoft.com/es-es/windows7/how-to-manage-cookies-in-internet-explorer-9\" target=\"_blank\">Explore</a></li>\n<li><a href=\"http://support.mozilla.org/es/kb/habilitar-y-deshabilitar-cookies-que-los-sitios-we\" target=\"_blank\">Firefox</a></li>\n<li><a href=\"http://support.apple.com/kb/ph5042\" target=\"_blank\">Safari</a></li>\n</ul>\n<p>If you have questions about this policy of cookies, please contact us at <a title=\"mail cookies\" href=\"mailto:{CONFIG_MAILTO}\">{CONFIG_MAILTO}</a></p>', '2017-04-27 11:50:32'),
(234, 'es', '<p><strong>Datos fiscales</strong></p>\n<ul>\n<li>Titular: Fundació Institut Mar d\'Investigacions Mèdiques</li>\n<li>NIF: G-60070053</li>\n<li>Dirección:</li>\n<li>Unidad de investigación en Servicios Sanitarios,<br />Instituto Hospital del Mar de Investigaciones Médicas (IMIM)<br />Parc de Recerca Biomèdica de Barcelona (PRBB),<br />Calle Dr Aiguader, 88<br />08003, Barcelona</li>\n<li>Teléfono: 933 160 747 (ext. 1747)</li>\n</ul>\n<p><strong>Protección de datos personales</strong></p>\n<p>Los datos de carácter personal que se recogen a través de la página web, son únicamente utilizados a efectos de prestar los servicios solicitados. Sus datos serán ubicados en ficheros titularidad de Fundació Institut Mar d\'Investigacions Mèdiques y serán archivados por el periodo de tiempo legalmente necesario. Usted podrá ejercer los derechos de acceso, rectificación, cancelación y oposición por escrito a Fundació Institut Mar d\'Investigacions Mèdiques en Unidad de investigación en Servicios Sanitarios,<br />Instituto Hospital del Mar de Investigaciones Médicas (IMIM)<br />Parc de Recerca Biomèdica de Barcelona (PRBB),<br />Calle Dr Aiguader, 88<br />08003, Barcelona.</p>\n<p></p>\n<p><strong>Propiedad intelectual y responsabilidad sobre los contenidos</strong></p>\n<p>Queda prohibido cualquier uso de todos los contenidos de la página web, concretamente sobre los textos, diseño y código fuente, sin la autorización expresa de sus titulares. Cualquier uso no permitido será debidamente perseguido por los legítimos propietarios.</p>\n<p>El titular de la página web no se hace responsable de los contenidos a los que se dirigen los enlaces ubicados en ésta, según lo que establece el artículo 17 de la LSSICE.</p>', '2020-02-20 12:54:26'),
(234, 'ca', '<p><strong>Dades fiscals</strong></p>\n<ul>\n<li>Titular: Fundació Institut Mar d\'Investigacions Mèdiques</li>\n<li>NIF: G-60070053</li>\n<li>Adreça:</li>\n<li>Unidad de investigación en Servicios Sanitarios,<br />Instituto Hospital del Mar de Investigaciones Médicas (IMIM)<br />Parc de Recerca Biomèdica de Barcelona (PRBB),<br />Calle Dr Aiguader, 88<br />08003, Barcelona</li>\n<li>Telèfon: 933 160 747 (ext. 1747)</li>\n</ul>\n<p><strong>Protecció de dades personals</strong></p>\n<p>Les dades de caràcter personal que es recullen a través de la pàgina web, únicament són utilitzades a efectes de prestar els serveis sol·licitats així com procedir a la facturació dels mateixos. Les seves dades seran ubicades en fitxers titularitat de {CONFIG_CLIENTE} a efectes de facturació i seran arxivades pel període de temps legalment necessari. Vostè pot exercir els drets d’accés, rectificació, cancel·lació i oposició mitjançant escrit a {CONFIG_CLIENTE} en {CONFIG_DIRECCION}.</p>\n<p><strong>Propietat intel·lectual i responsabilitat sobre els continguts</strong></p>\n<p>Queda prohibit qualsevol ús de tots els continguts de la pàgina web, concretament sobre els textos, disseny i codi font, sense l’autorització expressa dels seus titulars. Qualsevol ús no permès serà degudament perseguit pels legítims propietaris.</p>\n<p>El titular de la pàgina web no es fa responsable dels continguts als que es dirigeixen els enllaços ubicats en aquesta, segons el que estableix l’article 17 de la LSSICE.</p>', '2020-02-20 12:49:17'),
(234, 'en', '<p><strong>Owner</strong></p>\n<ul>\n<li>Owner: Fundació Institut Mar d\'Investigacions Mèdiques</li>\n<li>NIF: G-60070053</li>\n<li>Address:</li>\n<li>Unidad de investigación en Servicios Sanitarios,<br />Instituto Hospital del Mar de Investigaciones Médicas (IMIM)<br />Parc de Recerca Biomèdica de Barcelona (PRBB),<br />Calle Dr Aiguader, 88<br />08003, Barcelona</li>\n<li>Phone: 933 160 747 (ext. 1747)</li>\n</ul>\n<p><strong>Personal data protection</strong></p>\n<p>The data of a personal nature collected from the website is utilised solely in order to provide services requested as well as invoicing for same. Your data will be stored in files owned by {CONFIG_CLIENTE} for invoicing and will be stored for the legally required period of time. You will be able to exercise your rights of access, rectification, cancellation and opposition in writing to {CONFIG_CLIENTE} at {CONFIG_DIRECCION}.</p>\n<p><strong>Intellectual property and responsibility for content</strong></p>\n<p>Any use of the website content is prohibited, specifically the text, design and source code, without the express authorisation of its owners. Any unauthorised use will be pursued by the legitimate owners.</p>\n<p>The owner of the website will not be held responsible for the content of the sites to which links on this website may lead, in accordance with article 17 of the LSSICE.</p>', '2020-02-20 12:49:17'),
(235, 'es', '', '0000-00-00 00:00:00'),
(235, 'ca', '', '0000-00-00 00:00:00'),
(235, 'en', '', '0000-00-00 00:00:00'),
(236, 'es', 'Nota Legal', '2017-04-27 11:50:53'),
(236, 'ca', 'Nota Legal', '2017-04-27 11:50:53'),
(236, 'en', 'Legal Notice', '2017-04-27 11:50:54'),
(237, 'es', 'Política de Cookies', '2017-04-27 11:50:37'),
(237, 'ca', 'Política de Cookies', '2017-04-27 11:50:37'),
(237, 'en', 'Cookies Policy', '2017-04-27 11:50:37'),
(238, 'es', '<p>Aliquam non massa velit. Praesent ut posuere ante. Integer nec bibendum massa, nec sagittis enim. Donec ut libero sit amet mauris pretium laoreet. Ut maximus mi eu lobortis fringilla. Donec ultrices urna vitae mauris sodales sodales. Ut neque nulla, facilisis non tristique dapibus, aliquam sed libero.</p>\n<p>Maecenas suscipit ligula a vehicula consectetur. Sed magna nibh, elementum a arcu non, facilisis tincidunt nibh. Ut ullamcorper, elit ut ultrices tincidunt, mauris orci vehicula augue, non tincidunt diam felis ac augue. Nam tristique ornare erat, ut tincidunt erat efficitur quis. Suspendisse auctor ex enim, in interdum mi malesuada non. Nulla pulvinar elementum scelerisque. Morbi mattis dapibus aliquet. Curabitur at tristique nisl. Mauris non vestibulum dolor. Quisque orci metus, maximus vitae lectus id, viverra sodales purus. Proin aliquam condimentum leo. Morbi condimentum venenatis ex at ullamcorper. Etiam eu sodales ante. Mauris laoreet, ante vel iaculis facilisis, sem augue tempor odio, id aliquet dolor dolor vitae leo. Nullam id urna erat.</p>\n<p>Aenean vehicula quis lectus id volutpat. Aenean eu porttitor mi, vel finibus odio. Fusce pellentesque sem id malesuada vulputate. Morbi quis fringilla risus. Aenean placerat faucibus placerat. Aliquam condimentum elit ligula, vel aliquet tellus maximus vitae. Aliquam erat volutpat.</p>', '2019-06-04 09:26:02'),
(238, 'ca', '', '0000-00-00 00:00:00'),
(238, 'en', '', '0000-00-00 00:00:00'),
(240, 'es', 'Productos', '0000-00-00 00:00:00'),
(240, 'ca', 'Productes', '0000-00-00 00:00:00'),
(240, 'en', 'Products', '0000-00-00 00:00:00'),
(241, 'es', 'Últimas noticias', '2019-12-12 14:23:28'),
(241, 'ca', 'Ùltimes notícies', '2019-12-12 14:23:28'),
(241, 'en', 'News', '0000-00-00 00:00:00'),
(242, 'es', 'Acepto recibir información comercial, incluso por correo electrónico.', '0000-00-00 00:00:00'),
(242, 'ca', 'Accepto rebre informació comercial, fins i tot per correu electrònic.', '0000-00-00 00:00:00'),
(242, 'en', 'I agree to receive commercial information, including by email.', '0000-00-00 00:00:00'),
(243, 'es', 'INFORMACIÓN PROTECCIÓN DE DATOS. Finalidades: Responder a sus solicitudes y remitirle información comercial de nuestros productos y servicios, incluso por correo electrónico. Legitimación: Consentimiento del interesado. Destinatarios: No están previstas cesiones de datos. Derechos: Puede retirar su consentimiento en cualquier momento, así como acceder, rectificar, suprimir sus datos y demás derechos en <a href=\"mailto:{CONFIG_MAILTO}\" target=\"_blank\">{CONFIG_MAILTO}</a>. ', '2018-05-25 10:42:05'),
(243, 'ca', '', '0000-00-00 00:00:00'),
(243, 'en', '', '0000-00-00 00:00:00'),
(244, 'es', 'Adjunta CV', '0000-00-00 00:00:00'),
(244, 'ca', 'Adjunta CV', '0000-00-00 00:00:00'),
(244, 'en', 'Attach CV', '0000-00-00 00:00:00'),
(245, 'es', 'Política de Privacidad', '2018-05-25 10:57:51'),
(245, 'ca', '', '0000-00-00 00:00:00'),
(245, 'en', '', '0000-00-00 00:00:00'),
(246, 'es', 'Política de Privacidad', '2018-05-25 10:57:49'),
(246, 'ca', '', '0000-00-00 00:00:00'),
(246, 'en', '', '0000-00-00 00:00:00'),
(247, 'es', 'Política de Privacidad', '2018-05-25 10:57:49'),
(247, 'ca', '', '0000-00-00 00:00:00'),
(247, 'en', '', '0000-00-00 00:00:00'),
(248, 'es', '<p>La confidencialidad y la seguridad son valores primordiales de {CONFIG_CLIENTE} y, en consecuencia, asumimos el compromiso de garantizar la privacidad del Usuario en todo momento y de no recabar información innecesaria. A continuación, le proporcionamos toda la información necesaria sobre nuestra Política de Privacidad en relación con los datos personales que recabamos, explicándole:</p>\n<ul>\n<li>Quién es el responsable del tratamiento de sus datos.</li>\n<li>Para qué finalidades recabamos los datos que le solicitamos.</li>\n<li>Cuál es la legitimación para su tratamiento.</li>\n<li>Durante cuánto tiempo los conservamos.</li>\n<li>A qué destinatarios se comunican sus datos.</li>\n<li>Cuáles son sus derechos.</li>\n</ul>\n<strong>1. Responsable</strong><strong>:</strong><br />\n<p>{CONFIG_CLIENTE} ({CONFIG_NIF})</p>\n<ul>\n<li>{CONFIG_DIRECCION}</li>\n<li>e-mail: <a title=\"{CONFIG_MAILTO}\" href=\"mailto:{CONFIG_MAILTO}\">{CONFIG_MAILTO}</a></li>\n</ul>\n<strong>2. Finalidades, legitimación y conservación de los tratamientos de los datos enviados a través de</strong><strong>:</strong>\n<ul>\n<li>FORMULARIO DE CONTACTO.<br />Finalidad: Facilitarle un medio para que pueda ponerse en contacto con nosotros y contestar a sus solicitudes de información. <br />Legitimación: El consentimiento del usuario al solicitarnos información a través de nuestro formulario de contactos y al marcar la casilla de aceptación de envío de información.<br />Conservación: Una vez resuelta su solicitud por medio de nuestro formulario o contestada por correo electrónico, si no ha generado un nuevo tratamiento., y mientras puedan ser necesarios conforme a la naturaleza de su comunicación y en caso de obligación legal de conservación.</li>\n</ul>\n<ul>\n<li>ENVÍO DE CORREOS ELECTRÓNICOS.<br />Finalidad: Contestar a sus solicitudes de información, atender sus peticiones y responder sus consultas o dudas.<br />Legitimación: El consentimiento del usuario al solicitarnos información a través de la dirección de correo electrónico.<br />Conservación: Una vez resulta contestada su petición por correo electrónico, si no ha generado un nuevo tratamiento.</li>\n</ul>\n<ul>\n<li>ENVÍO DEL CURRÍCULUM VITAE POR CORREO ELECTRÓNICO<br />Finalidad: Disponer de su currículum para participar en nuestros procesos de selección de personal.<br />Legitimación: El consentimiento del usuario al remitirnos su información personal y currículum para nuestros procesos de selección de personal.<br />Conservación: Durante el desarrollo de los procesos de selección de personal abiertos y durante 1 año para futuros procesos.</li>\n</ul>\n<strong>3. Destinatarios de sus datos</strong><br />\n<p>Sus datos son confidenciales y no se cederán a terceros, salvo que exista obligación legal.</p>\n<strong>4. Derechos en relación con sus datos personales</strong><br />\n<p>Cualquier persona puede retirar su consentimiento en cualquier momento, cuando el mismo se haya otorgado para el tratamiento de sus datos. En ningún caso, la retirada de este consentimiento condiciona la ejecución del contrato de suscripción o las relaciones generadas con anterioridad.</p>\n<p>Igualmente, puede ejercer los siguientes derechos:</p>\n<ul>\n<li>Solicitar el acceso a sus datos personales o su rectificación cuando sean inexactos.</li>\n<li>Solicitar su supresión cuando, entre otros motivos, los datos ya no sean necesarios para los fines para los que fueron recogidos.</li>\n<li>Solicitar la limitación de su tratamiento en determinadas circunstancias.</li>\n<li>Solicitar la oposición al tratamiento de sus datos por motivos relacionados con su situación particular.</li>\n<li>Solicitar la portabilidad de los datos en los casos previstos en la normativa.</li>\n<li>Otros derechos reconocidos en las normativas aplicables.</li>\n</ul>\n<p>Dónde y cómo solicitar sus Derechos: Mediante un escrito dirigido al responsable a su dirección postal o electrónica (indicadas en el apartado A), indicando la referencia “Datos Personales”, especificando el derecho que se quiere ejercer y respecto a qué datos personales.</p>\n<p>En caso de divergencias con la empresa en relación con el tratamiento de sus datos, puede presentar una reclamación ante la Autoridad Catalana de Protección de Datos (<span><a href=\"http://www.apd.cat\">www.apd.cat</a></span>).</p>\n<strong>5. Cookies</strong><br />\n<p><strong>Cookies</strong>Esta página web, únicamente utiliza cookies técnicas, de personalización y análisis, propias y de terceros (Google Analytics), que en ningún caso tratan datos de carácter personal ni captan hábitos de navegación para fines publicitarios.</p>\n<p>Por ello, al acceder a nuestra web, en cumplimiento del artículo 22 de la Ley de Servicios de la Sociedad de la Información, al tratar cookies de análisis, le hemos solicitado su consentimiento para su uso y facilitado información de las mismas.</p>\n<p>Las cookies de tipo analítico, así como aquellas que requieran el consentimiento del interesado, sólo se descargarán cuando el interesado acepte las cookies o en tanto que realice actos de navegación, entendiendo el mismo como un acto afirmativo respecto de su consentimiento. </p>\n<p><strong>6. Seguridad de sus datos personales</strong></p>\n<p>Con el objetivo de salvaguardar la seguridad de sus datos personales, le informamos que hemos adoptado todas las medidas de índole técnica y organizativa necesarias para garantizar la seguridad de los datos personales suministrados de su alteración, pérdida, y tratamientos o accesos no autorizados.</p>\n<strong>7. Actualización de sus datos</strong><br />\n<p>Es importante que para que podamos mantener sus datos personales actualizados, nos informe siempre que haya habido alguna modificación en ellos, en caso contrario, no respondemos de la veracidad de los mismos.</p>\n<p>No nos hacemos responsables de la política de privacidad respecto a los datos personales que pueda facilitar a terceros por medio de los enlaces disponibles en nuestra página web.</p>\n<p>La presente Política de Privacidad puede ser modificada para adaptarlas a los cambios que se produzca en nuestra web, así como modificaciones legislativas o jurisprudenciales sobre datos personales que vayan apareciendo, por lo que exige su lectura, cada vez que nos facilite sus datos a través de esta Web.</p>', '2020-02-20 13:02:17'),
(248, 'ca', '<p>La confidencialitat i la seguretat són valors primordials de {CONFIG_CLIENTE} i, en conseqüència, assumim el compromís de garantir la privacitat de l\'Usuari en tot moment i de no recaptar informació innecessària. A continuació, li proporcionem tota la informació necessària sobre la nostra Política de Privacitat en relació amb les dades personals que recaptem, li expliquem:</p>\n<ul>\n<li>Qui és el responsable del tractament de les seves dades.</li>\n<li>Per quines finalitats demanem les dades que li demanem.</li>\n<li>Quina és la legitimació per al seu tractament.</li>\n<li>Durant quant de temps els conservem.</li>\n<li>A què destinataris es comuniquen les seves dades.</li>\n<li>Quins són els seus drets.</li>\n</ul>\n<p><strong>1. Responsable</strong><strong>:</strong></p>\n<p>{CONFIG_CLIENTE} ({CONFIG_NIF})</p>\n<ul>\n<li>{CONFIG_DIRECCION}</li>\n<li>e-mail: <a title=\"{CONFIG_MAILTO}\" href=\"mailto:{CONFIG_MAILTO}\">{CONFIG_MAILTO}</a></li>\n</ul>\n<p><strong>2. Finalitats, legitimació i conservació dels tractaments de les dades enviades a través de:<br /></strong></p>\n<ul>\n<li>FORMULARI DE CONTACTE.<br />Finalitat: Facilitar-un mitjà perquè pugui posar-se en contacte amb nosaltres i contestar les seves sol·licituds d\'informació, així com enviar-li comunicacions dels nostres productes, serveis i activitats, inclusivament per mitjans electrònics (Correu electrònic, SMS, WhatsApp), si marca la casella d\'acceptació.<br />Legitimació: El consentiment de l\'usuari al sol·licitar-nos informació a través del nostre formulari de contactes i en marcar la casella d\'acceptació d\'enviament d\'informació.<br />Conservació: Un cop resulta la seva sol·licitud per mitjà del nostre formulari o contestada per correu electrònic, si no ha generat un nou tractament, i en cas d\'haver acceptat rebre enviaments comercials, fins que sol·liciti la baixa dels mateixos.</li>\n<li>ENVIAMENT DE CORREUS ELECTRÒNICS.<br />Finalitat: Contestar les seves sol·licituds d\'informació, atendre les seves peticions i respondre les seves consultes o dubtes.<br />Legitimació: El consentiment de l\'usuari al sol·licitar-nos informació a través de l\'adreça de correu electrònic.<br />Conservació: Un cop resulta contestada la seva petició per correu electrònic, si no ha generat un nou tractament.</li>\n<li>ENVIAMENT DE L\'CURRÍCULUM VITAE PER CORREU ELECTRÒNIC<br />Finalitat: Disposar del seu currículum per participar en els nostres processos de selecció de personal.<br />Legitimació: El consentiment de l\'usuari al remetre\'ns la vostra informació personal i currículum per als nostres processos de selecció de personal.<br />Conservació: Durant el desenvolupament dels processos de selecció de personal oberts i durant 1 any per a futurs processos.</li>\n</ul>\n<p><strong>3. Destinataris de les seves dades</strong></p>\n<p>Les seves dades són confidencials i no se cediran a tercers, llevat que hi hagi obligació legal.</p>\n<p><strong>4. Drets en relació amb les seves dades personals</strong></p>\nQualsevol persona pot retirar el seu consentiment en qualsevol moment, quan el mateix s\'hagi atorgat pel tractament de les seves dades. En cap cas, la retirada d\'aquest consentiment condiciona l\'execució del contracte de subscripció o les relacions generades amb anterioritat.\n<p>Igualment, pot exercir els següents drets:</p>\n<ul>\n<li>Sol·licitar l\'accés a les seves dades personals o la seva rectificació quan siguin inexactes.</li>\n<li>Sol·licitar la seva supressió quan, entre d\'altres motius, les dades ja no siguin necessaris per als fins per als quals van ser recollides.</li>\n<li>Sol·licitar la limitació del seu tractament en determinades circumstàncies.</li>\n<li>Sol·licitar l\'oposició al tractament de les seves dades per motius relacionats amb la seva situació particular.</li>\n<li>Sol·licitar la portabilitat de les dades en els casos que preveu la normativa.</li>\n<li>Altres drets reconeguts en les normatives aplicables.<strong></strong></li>\n</ul>\n<p>On i com sol·licitar els seus Drets: Mitjançant un escrit dirigit al responsable a la seva adreça postal o electrònica (indicades en l\'apartat A), indicant la referència \"Dades Personals\", especificant el dret que es vol exercir i respecte a quines dades personals.</p>\n<p>En cas de divergències amb l\'empresa en relació amb el tractament de les seves dades, pot presentar una reclamació davant l\'Autoritat de Protecció de Dades <span>(</span><span><a href=\"http://www.agpd.es\">www.agpd.es</a>)<a href=\"http://www.agpd.es/\"></a></span></p>\n<p><strong>5. Cookies</strong></p>\n<p>Aquesta pàgina web, únicament utilitza cookies tècniques, de personalització i anàlisi, pròpies i de tercers (Google Analytics), que en cap cas tracten dades de caràcter personal ni capten hàbits de navegació per a fins publicitaris.</p>\n<p>Per això, en accedir a la nostra web, en compliment de l\'article 22 de la Llei de Serveis de la Societat de la Informació, en tractar galetes d\'anàlisi, li hem sol·licitat el seu consentiment per al seu ús i facilitat informació de les mateixes, que en tot cas s\'instal·laran passat un termini de temps prudencial perquè l\'usuari tingui temps de decidir prestar el seu consentiment o no.</p>\n<p><strong>6. Seguretat de les seves dades personals</strong></p>\n<p>Amb l\'objectiu de salvaguardar la seguretat de les seves dades personals, l\'informem que hem adoptat totes les mesures d\'índole tècnica i organitzativa necessàries per garantir la seguretat de les dades personals subministrades de la seva alteració, pèrdua, i tractaments o accessos no autoritzats.</p>\n<p><strong>7. Actualització de les seves dades</strong></p>\n<p>És important que perquè puguem mantenir les seves dades personals actualitzades, ens informi sempre que hagi hagut alguna modificació en ells, en cas contrari, no responem de la veracitat dels mateixos.</p>\n<p>No ens fem responsables de la política de privacitat respecte a les dades personals que pugui facilitar a tercers per mitjà dels enllaços disponibles a la nostra pàgina web.</p>\n<p>La present Política de Privacitat pot ser modificada per adaptar-les als canvis que es produeixin en la nostra web, així com modificacions legislatives o jurisprudencials sobre dades personals que vagin apareixent, pel que exigeix ​​la seva lectura, cada vegada que ens faciliti les seves dades a través de aquest web.</p>', '2018-07-02 10:36:43'),
(248, 'en', '<p>Confidentiality and security are core values ​​of {CONFIG_CLIENTE} and, consequently, we assume the commitment to guarantee the privacy of the User at all times and not to collect unnecessary information. Next, we provide you with all the necessary information about our Privacy Policy in relation to the personal information we collect, explaining:</p>\n<ul>\n<li>Who is responsible for the processing of your data.</li>\n<li>For what purposes we collect the data that we request.</li>\n<li>What is the legitimacy for their treatment.</li>\n<li>How long we keep them.</li>\n<li>To which recipients their data is communicated.</li>\n<li>What are your rights?</li>\n</ul>\n<p><strong>1. Responsible:</strong></p>\n<ul>\n<ul>\n<ul>\n<li>{CONFIG_DIRECCION}</li>\n<li>e-mail: <a title=\"{CONFIG_MAILTO}\" href=\"mailto:{CONFIG_MAILTO}\">{CONFIG_MAILTO}</a></li>\n</ul>\n</ul>\n</ul>\n<strong>2. Purposes, legitimization and conservation of the data processing sent through:</strong>\n<ul>\n<ul>\n<ul>\n<li><strong>CONTACT FORM.</strong><br />Purpose: To provide a means for you to contact us and answer your requests for information, as well as send you communications about our products, services and activities, including by electronic means (Email, SMS, WhatsApp), if you check the box acceptance.<br />Legitimation: The user\'s consent when requesting information through our contact form and by checking the acceptance box for sending information.<br />Conservation: Once your application is submitted through our form or answered by email, if you have not generated a new treatment, and if you have agreed to receive commercial shipments, until you request the withdrawal of them.</li>\n<li><strong>SUBMISSION OF EMAILS.</strong><br />Purpose: Answer your requests for information, respond to your requests and answer your questions or doubts.<br />Legitimation: The user\'s consent to request information through the email address. Conservation: Once your request is answered by email, if you have not generated a new treatment.</li>\n<li><strong>SELLING THE CURRÍCULUM VITAE BY E-MAIL</strong><br />Purpose: To have your CV to participate in our staff selection processes.<br />Legitimation: The consent of the user by sending us your personal information and resume for our staff selection processes.<br />Conservation: During the development of open personnel selection processes and for 1 year for future processes.</li>\n</ul>\n</ul>\n</ul>\n<strong>3. Recipients of your data</strong>\n<p>Your data is confidential and will not be ceded to third parties, unless there is a legal obligation.</p>\n<p><strong>4. Rights in relation to your personal data</strong></p>\n<p>Anyone can withdraw their consent at any time, when the same has been granted for the treatment of their data. In no case, the withdrawal of this consent conditions the execution of the subscription agreement or the relations generated previously.</p>\n<p>Likewise, you can exercise the following rights:</p>\n<ul>\n<ul>\n<ul>\n<li>Request access to your personal data or its rectification when they are inaccurate.</li>\n<li>Request their deletion when, among other reasons, the data are no longer necessary for the purposes for which they were collected.</li>\n<li>Request limitation of treatment in certain circumstances.</li>\n<li>Request opposition to the processing of your data for reasons related to your particular situation.</li>\n<li>Request the portability of the data in the cases provided by the regulations.</li>\n<li>Other rights recognized in the applicable regulations.</li>\n</ul>\n</ul>\n</ul>\n<p>Where and how to request your rights: By means of a letter addressed to the person in charge at your postal or electronic address (indicated in section A), indicating the reference \"Personal Data\", specifying the right to be exercised and respect what personal information</p>\n<p>In case of divergences with the company in relation to the processing of your data, you can file a complaint with the Data Protection Authority (<a href=\"http://www.agpd.es\" target=\"_blank\" rel=\"noopener\">www.agpd.es</a>).</p>\n<p><strong>5. Cookies</strong></p>\n<p>This website only uses technical, customization and analysis cookies, own and third parties (Google Analytics), which in no case treat personal data or capture browsing habits for advertising purposes.</p>\n<p>Therefore, by accessing our website, in compliance with article 22 of the Law on Information Society Services, when dealing with analytical cookies, we have requested your consent for its use and ease Information of the same, which in any case will be installed after a reasonable period of time so that the user has time to decide whether or not to consent.</p>\n<p><strong>6. Security of your personal data</strong></p>\n<p>With the objective of safeguarding the security of your personal data, we inform you that we have adopted all the necessary technical and organizational measures to guarantee the security of the personal data provided of its alteration, loss, and treatments or access unauthorized</p>\n<p><strong>7. Update your data</strong></p>\n<p>It is important for us to keep your personal information up to date, always inform us that there has been a change in them, otherwise we will not be responsible for the veracity of the same.</p>\n<p>We are not responsible for the privacy policy regarding personal data that may be provided to third parties through the links available on our website.</p>\n<p>The present Privacy Policy can be modified to adapt them to the changes that occur on our website, as well as legislative or jurisprudential changes on personal data that appear, so that it requires reading, every time you provide us with your data through this website.</p>', '2018-07-02 10:36:51'),
(249, 'es', 'politica-privacidad', '0000-00-00 00:00:00'),
(249, 'ca', 'politica-privacitat', '0000-00-00 00:00:00'),
(249, 'en', 'privacy-policy', '0000-00-00 00:00:00'),
(250, 'es', ' Información Adicional: puede ampliar la información en', '2018-05-25 10:42:39'),
(250, 'ca', 'pot ampliar la informació a', '2018-05-25 12:11:39'),
(250, 'en', '', '0000-00-00 00:00:00'),
(251, 'es', 'Política de Privacidad', '0000-00-00 00:00:00'),
(251, 'ca', '', '0000-00-00 00:00:00'),
(251, 'en', '', '0000-00-00 00:00:00'),
(252, 'es', 'Política de Cookies', '0000-00-00 00:00:00'),
(252, 'ca', 'Política de Cookies', '0000-00-00 00:00:00'),
(252, 'en', 'Cookies Policy', '0000-00-00 00:00:00'),
(253, 'es', 'Noticias relacionadas', '0000-00-00 00:00:00'),
(253, 'ca', 'Noticies relacionades', '0000-00-00 00:00:00'),
(253, 'en', 'Related news', '0000-00-00 00:00:00'),
(254, 'es', 'Vendedor', '0000-00-00 00:00:00'),
(254, 'ca', 'Venedor', '0000-00-00 00:00:00'),
(254, 'en', 'Seller', '0000-00-00 00:00:00'),
(255, 'es', 'La verificación de robots (captcha) ha fallado. Por favor, inténtelo de nuevo.', '0000-00-00 00:00:00'),
(255, 'ca', 'La verificació de robots (captcha) ha fallat. Torneu-ho a provar de nou.', '0000-00-00 00:00:00'),
(255, 'en', 'The verification of robots (captcha) has failed. Please, try again.', '0000-00-00 00:00:00'),
(256, 'es', 'contacto-ok', '0000-00-00 00:00:00'),
(256, 'ca', 'contacte-ok', '0000-00-00 00:00:00'),
(256, 'en', 'contact-ok', '0000-00-00 00:00:00'),
(257, 'es', 'Publicaciones', '0000-00-00 00:00:00'),
(257, 'ca', 'Publicacions', '0000-00-00 00:00:00'),
(257, 'en', 'Publications', '0000-00-00 00:00:00'),
(258, 'es', 'Equipo investigador', '0000-00-00 00:00:00'),
(258, 'ca', 'Equip investigador', '0000-00-00 00:00:00'),
(258, 'en', 'Research team', '0000-00-00 00:00:00'),
(259, 'es', 'Sobre el suicidio', '0000-00-00 00:00:00'),
(259, 'ca', 'Sobre el suicidi', '0000-00-00 00:00:00'),
(259, 'en', 'About suicide', '0000-00-00 00:00:00'),
(260, 'es', 'equipo-investigador', '0000-00-00 00:00:00'),
(260, 'ca', 'equip-investigador', '0000-00-00 00:00:00'),
(260, 'en', 'research-team', '0000-00-00 00:00:00'),
(261, 'es', 'sobre-el-suicidio', '0000-00-00 00:00:00'),
(261, 'ca', 'sobre-el-suicidi', '0000-00-00 00:00:00'),
(261, 'en', 'about-suicide', '0000-00-00 00:00:00'),
(262, 'es', 'Un proyecto con impacto social', '2020-01-23 14:54:27'),
(262, 'ca', 'Un projecte amb impacte social', '2020-01-23 14:54:27'),
(262, 'en', 'A project that makes an impact', '2020-01-29 13:51:39'),
(263, 'es', '', '2020-01-23 14:54:54'),
(263, 'ca', '', '0000-00-00 00:00:00'),
(263, 'en', '', '0000-00-00 00:00:00'),
(264, 'es', '', '2020-01-23 14:56:20'),
(264, 'ca', '', '0000-00-00 00:00:00'),
(264, 'en', '', '0000-00-00 00:00:00'),
(265, 'es', '<p><span>El suicidio es la segunda causa de muerte entre los jóvenes de 15-29 años en el mundo y la primera causa de muerte externa en España, por delante de los fallecimientos debidos a accidentes de tráfico</span><span><sup>1</sup></span><span>. El comportamiento o conducta suicida es un <strong>fenómeno complejo</strong> que puede verse influenciado por la combinación de múltiples factores (por ejemplo; problemas de salud mental, socioeconómicos, o por cierta vulnerabilidad biológica al estrés, etc.), por lo que su abordaje supone un <strong>reto para los sistemas de salud pública</strong>.</span></p>\n<p><span>El actual proyecto generará información sobre las tasas de suicidio actuales en Cataluña e identificará los factores de riesgo más relevantes en la conducta suicida. También plantea generar modelos de predicción individuales del riesgo de tentativa y repetición de la tentativa de suicidio y evaluar el programa de prevención CRS.</span></p>\n<p><span>Los resultados del estudio ayudaran a la generación de recomendaciones para mejorar los servicios de atención y los programas preventivos en salud mental. </span></p>', '2020-01-22 15:46:42'),
(265, 'ca', '', '0000-00-00 00:00:00'),
(265, 'en', '', '0000-00-00 00:00:00'),
(266, 'es', 'Saber más', '0000-00-00 00:00:00'),
(266, 'ca', 'Saber més ', '0000-00-00 00:00:00'),
(266, 'en', 'See more', '0000-00-00 00:00:00'),
(267, 'es', 'Objetivos', '0000-00-00 00:00:00'),
(267, 'ca', 'Objectius', '0000-00-00 00:00:00'),
(267, 'en', 'Goals', '0000-00-00 00:00:00'),
(268, 'es', 'Ética', '0000-00-00 00:00:00'),
(268, 'ca', 'Ètica', '0000-00-00 00:00:00'),
(268, 'en', '', '0000-00-00 00:00:00'),
(269, 'es', 'Equipo investigador', '0000-00-00 00:00:00'),
(269, 'ca', 'Equip investigador', '0000-00-00 00:00:00'),
(269, 'en', '', '0000-00-00 00:00:00'),
(270, 'es', 'Metodología', '0000-00-00 00:00:00'),
(270, 'ca', 'Metodologia', '0000-00-00 00:00:00'),
(270, 'en', '', '0000-00-00 00:00:00'),
(271, 'es', 'Intervención CRS', '0000-00-00 00:00:00'),
(271, 'ca', 'Intervenció CRS', '0000-00-00 00:00:00'),
(271, 'en', '', '0000-00-00 00:00:00'),
(272, 'es', 'Publicaciones', '0000-00-00 00:00:00'),
(272, 'ca', 'Publicacions', '0000-00-00 00:00:00'),
(272, 'en', 'Publications', '2020-01-02 09:14:52'),
(273, 'es', '¿Qué sabemos sobre la conducta suicida?', '0000-00-00 00:00:00'),
(273, 'ca', '', '0000-00-00 00:00:00'),
(273, 'en', '', '0000-00-00 00:00:00'),
(274, 'es', 'Múltiples investigaciones demuestran que la conducta suicida es una de las principales causas de muerte y de años potenciales de vida perdidos por discapacidad a nivel mundial. Pero… ¿a qué nos referimos exactamente con conducta suicida? ¿Cuáles son las cifras en España y Cataluña? ¿Qué retos concretos supone esta problemática para el sistema político?', '2020-01-22 14:45:52'),
(274, 'ca', '', '0000-00-00 00:00:00'),
(274, 'en', '', '0000-00-00 00:00:00'),
(275, 'es', '¿Se puede prevenir el suicidio?', '0000-00-00 00:00:00'),
(275, 'ca', '', '0000-00-00 00:00:00'),
(275, 'en', '', '0000-00-00 00:00:00'),
(276, 'es', 'Sí, el suicidio y los intentos de suicidio se pueden prevenir. Actualmente muchos países han desarrollado estrategias a nivel nacional de detección precoz y prevención que tratan de abordar algunos de los diferentes factores de riesgo relacionados con esta problemática. Aun así, sigue siendo difícil determinar la efectividad y coste de dichas estrategias. ', '2020-01-22 14:45:54'),
(276, 'ca', '', '0000-00-00 00:00:00'),
(276, 'en', '', '0000-00-00 00:00:00'),
(277, 'es', 'Accede a recursos en línea', '0000-00-00 00:00:00'),
(277, 'ca', '', '0000-00-00 00:00:00'),
(277, 'en', '', '0000-00-00 00:00:00'),
(278, 'es', '¿Necesitas ayuda para salir de una situación difícil?', '0000-00-00 00:00:00'),
(278, 'ca', '', '0000-00-00 00:00:00'),
(278, 'en', '', '0000-00-00 00:00:00'),
(279, 'es', 'Emergencias médicas  <a href=\"tel:%20112\"><strong>112</strong></a>', '2019-12-17 10:04:01'),
(279, 'ca', '', '0000-00-00 00:00:00'),
(279, 'en', '', '0000-00-00 00:00:00'),
(280, 'es', 'Teléfono de la esperanza  <a href=\"tel: +34934144848\"><strong>934 144 848</strong></a>', '2019-12-17 10:04:20'),
(280, 'ca', '', '0000-00-00 00:00:00'),
(280, 'en', '', '0000-00-00 00:00:00'),
(281, 'es', 'Coordinación', '0000-00-00 00:00:00'),
(281, 'ca', 'Coordinació', '0000-00-00 00:00:00'),
(281, 'en', '', '0000-00-00 00:00:00'),
(282, 'es', '<strong>Jordi Alonso</strong><br /><span><b>Investigador Principal</b><br /></span><span>Instituto Hospital del Mar de Investigaciones Médicas</span><br /><br /><br /><strong>Diego Palao</strong><br /><span><b>Investigador Principal</b><br /></span><span>Consorci Corporació Sanitària Parc Taulí de Sabadell</span>', '2020-02-19 14:49:41'),
(282, 'ca', '<strong>Jordi Alonso</strong><br />Coordinador del estudio epidemiológico<br /><br /><strong>Diego Palao</strong><br />Coordinador del estudio clínico', '2020-01-22 14:49:58'),
(282, 'en', 'Jordi Alonso<br />Coordinador del estudio epidemiológico<br /><br />Diego Palao<br />Coordinador del estudio clínico', '0000-00-00 00:00:00'),
(283, 'es', 'ÁREA INVESTIGADORES', '0000-00-00 00:00:00'),
(283, 'ca', '', '0000-00-00 00:00:00'),
(283, 'en', '', '0000-00-00 00:00:00'),
(284, 'es', 'Ubicación', '0000-00-00 00:00:00'),
(284, 'ca', 'Ubicació', '0000-00-00 00:00:00'),
(284, 'en', '', '0000-00-00 00:00:00'),
(285, 'es', 'Contacto', '0000-00-00 00:00:00'),
(285, 'ca', 'Contacte', '0000-00-00 00:00:00'),
(285, 'en', 'Contact', '0000-00-00 00:00:00'),
(286, 'es', 'Este estudio ha sido financiado por el Instituto de Salud Carlos III (PI17/00521 i PI17/01205) con fondos FEDER.', '2020-01-22 14:47:26'),
(286, 'ca', '', '0000-00-00 00:00:00'),
(286, 'en', '', '0000-00-00 00:00:00'),
(287, 'es', 'Publicaciones', '0000-00-00 00:00:00'),
(287, 'ca', 'Publicacions', '0000-00-00 00:00:00'),
(287, 'en', 'Publications', '2020-01-02 09:14:57');
INSERT INTO `textos_content` (`id`, `idioma`, `textarea_titulo`, `last_modified`) VALUES
(288, 'es', 'Buscar...', '0000-00-00 00:00:00'),
(288, 'ca', 'Buscar...', '0000-00-00 00:00:00'),
(288, 'en', 'Search...', '0000-00-00 00:00:00'),
(289, 'es', 'Ver', '0000-00-00 00:00:00'),
(289, 'ca', 'Veure', '0000-00-00 00:00:00'),
(289, 'en', 'Watch', '0000-00-00 00:00:00'),
(290, 'es', 'Archivo', '0000-00-00 00:00:00'),
(290, 'ca', 'Archiu', '0000-00-00 00:00:00'),
(290, 'en', '', '0000-00-00 00:00:00'),
(291, 'es', 'Todo', '0000-00-00 00:00:00'),
(291, 'ca', 'Tot', '0000-00-00 00:00:00'),
(291, 'en', 'All', '0000-00-00 00:00:00'),
(292, 'es', 'Artículos', '2019-12-16 10:15:20'),
(292, 'ca', 'Articles', '0000-00-00 00:00:00'),
(292, 'en', 'Articles', '0000-00-00 00:00:00'),
(293, 'es', 'Congresos', '0000-00-00 00:00:00'),
(293, 'ca', 'Congresos', '0000-00-00 00:00:00'),
(293, 'en', 'Congresses', '0000-00-00 00:00:00'),
(294, 'es', '2019', '0000-00-00 00:00:00'),
(294, 'ca', '2019', '0000-00-00 00:00:00'),
(294, 'en', '2019', '0000-00-00 00:00:00'),
(295, 'es', '2018', '0000-00-00 00:00:00'),
(295, 'ca', '2018', '0000-00-00 00:00:00'),
(295, 'en', '2018', '0000-00-00 00:00:00'),
(296, 'es', '2017', '0000-00-00 00:00:00'),
(296, 'ca', '2017', '0000-00-00 00:00:00'),
(296, 'en', '2017', '0000-00-00 00:00:00'),
(297, 'es', '2016', '0000-00-00 00:00:00'),
(297, 'ca', '2016', '0000-00-00 00:00:00'),
(297, 'en', '2016', '0000-00-00 00:00:00'),
(298, 'es', 'Acceder a la web', '0000-00-00 00:00:00'),
(298, 'ca', 'Accedir a la web', '0000-00-00 00:00:00'),
(298, 'en', 'Go to website', '0000-00-00 00:00:00'),
(299, 'es', 'Equipo investigador', '0000-00-00 00:00:00'),
(299, 'ca', 'Equip investigador', '0000-00-00 00:00:00'),
(299, 'en', 'Research team', '0000-00-00 00:00:00'),
(300, 'es', 'prevenir-el-suicidio', '0000-00-00 00:00:00'),
(300, 'ca', 'prevenir-el-suicidi', '0000-00-00 00:00:00'),
(300, 'en', 'prevent-suicide', '0000-00-00 00:00:00'),
(301, 'es', '¿Qué sabemos de la conducta suicida?', '0000-00-00 00:00:00'),
(301, 'ca', 'Què sabem de la conducta suïcida?', '0000-00-00 00:00:00'),
(301, 'en', 'What we know about the suicidal behaviour?\n', '0000-00-00 00:00:00'),
(302, 'es', '<p>La Organización Mundial de la Salud (OMS) ha definido el <strong>suicidio</strong> como un acto intencionado de provocarse la muerte con resultado fatal o letal<span><sup>3</sup></span>. Existe cierto consenso en considerar el suicidio a lo largo de un proceso o continuo de menor a mayor severidad que incluiría los pensamientos de muerte o ideación suicida, la planificación suicida, la tentativa de suicidio y el suicidio consumado. La <strong>conducta o comportamiento suicida</strong> sería el término utilizado para englobar este conjunto de comportamientos. Podemos denominar <strong>ideación suicida</strong> a los pensamientos o cogniciones de muerte por suicidio; <strong>planificación suicida</strong> a la programación del método y/o del procedimiento para llevar a cabo la conducta suicida; la <strong>tentativa o intento  de suicidio</strong> haría referencia al aquel acto con resultado no fatal (aunque potencialmente lesivo) en el que concurre evidencia implícita/explícita de la intención de autoprovocarse la muerte<span><sup>4</sup></span>.</p>\n<p><span>La conducta suicida es un problema de salud pública a nivel mundial. Se ha estimado </span><span>que aproximadamente 800.000 personas al año mueren en el mundo por suicidio, lo que representa una tasa de mortalidad estandarizada por edad de 10,5 por 100.000 habitantes (1,4% de la mortalidad a nivel mundial)</span><span><sup>3,5</sup></span><span>. </span></p>\n<p><span>La conducta suicida puede aparecer a lo largo del ciclo vital, siendo la segunda causa de muerte entre los jóvenes de 15 a 29 años a nivel mundial. </span></p>\n<p><span>Se ha estimado que por cada muerte por suicidio habría 20 tentativas de suicidio. El suicidio y el intento de suicidio ocasionan terribles consecuencias no solo para el propio individuo sino para sus familiares, amigos y la sociedad en general. Por ejemplo, en Estados Unidos, se ha estimado en 70 billones de dólares anuales los costes médicos y de pérdidas de productividad laboral asociados a la conducta suicida</span><span><sup>6</sup></span><span>. Según el Global Burden of Disease, el suicidio se encuentra entre las 10 principales causas de años potenciales de vida perdidos (APVP) estandarizados por edad en diversas áreas de Europa (Occidental, Central y Europa del Este), Asia Central, Australasia, Sur de América Latina, el Caribe y América del Norte, representando el 2,2% del total de años de vida perdidos a nivel mundial</span><span><sup>7</sup></span><span>. </span></p>\n<p><span>En <strong>España</strong>, en 2017 murieron 3679 personas por suicidio, representando una tasa de suicidio de 7,7 por 100.000 habitantes</span><span><sup>8</sup></span><span> [1,1% del total de Años de Vida Ajustados por Discapacidad (AVAD) a nivel mundial]</span><span><sup>9</sup></span><span>. <strong>Cataluña</strong> registró una tasa de suicidio inferior de 6,6 por 100.000 habitantes; sin embargo, el suicidio y <u>las autolesiones</u> </span><span>representan, tanto en los hombres como en mujeres, la tercera principal causa de muerte prematura (con una media de 24 y 21,8 APVP</span><span>, respectivamente)</span><span><sup>10,11</sup></span></p>\n<p><span>El suicidio y el intento de suicidio constituyen un fenómeno de comportamiento complejo que puede ser determinado por la combinación de múltiples factores de orígenes muy diversos - biológicos, psicológicos, sociales, culturales o ambientales (p. ej., problemas de salud mental, abuso de sustancias, privación socioeconómica, dificultad para recibir tratamiento médico adecuado, etc.). En las últimas décadas de investigación se ha intentado discernir la posible combinación de factores que conducen al suicidio dando lugar a múltiples teorías sobre el suicidio. Sin embargo, estas teorías a menudo carecen de evidencia empírica y no logran converger en un paradigma final. </span></p>\n<p><span>Dadas</span><span> la actual disponibilidad de gran cantidad de datos a nivel individual y poblacional y los avances tecnológicos, varios grupos de investigación </span><span>ya están usando nuevas técnicas de análisis dentro del campo de la ciencia de datos, por ejemplo, algoritmos de <em>machine learning.</em> Estas técnicas permiten analizar mayores volúmenes de datos, relaciones más complejas entre las variables o crear modelos más completos, con más interacciones, entre otros. Juntamente con los registros públicos de datos permitirán avanzar en la investigación científica sobre el suicidio y, de esta forma, fundamentar la futura práctica clínica y de prevención de la conducta suicida basada en la evidencia.</span></p>\n<p></p>\n<p><span> </span></p>\n<ol>\n<li><a href=\"https://www.ine.es/prensa/edcm_2017.pdf\" target=\"_blank\" rel=\"noopener\">INE</a>. Defunciones según las causas de muerte por grupos de enfermedades. 2018;2017:1–8. </li>\n<li>Departament de Salut. Generalitat de Catalunya. Instrucció sobre l’atenció a les persones en risc de suïcidi. Codi Risc Suïcidi [Internet]. 10/2105 2015. Available from: <a href=\"https://scientiasalut.gencat.cat/bitstream/handle/11351/1654/catsalut_instruccio_10_2015.pdf?sequence=1&isAllowed=y\" target=\"_blank\" rel=\"noopener\">scientiasalut.gencat.cat</a> </li>\n<li>World Health Organization. Preventing suicide: A global imperative. 2014.</li>\n<li>Ministerio de Sanidad. Guía de práctica clínica de prevención y tratamiento de la conducta suicida guía de práctica clínica de prevención y tratamiento de la conducta suicida. Agencia Evaluación Tecnol Galicia. 2012;398.</li>\n<li>Global Health Observatory (GHO) data. WHO | Suicide rates [Internet]. WHO. <a href=\"https://www.who.int/gho/mental_health/suicide_rates/en/\" target=\"_blank\" rel=\"noopener\">World Health Organization</a>; 2018.</li>\n<li><a href=\"https://www.cdc.gov/violenceprevention/suicide/fastfact.html?CDC_AA_refVal=https%3A%2F%2Fwww.cdc.gov%2Fviolenceprevention%2Fsuicide%2Fconsequences.html\" target=\"_blank\" rel=\"noopener\">Centers for Disease Control and Prevention</a>. Violence Prevention: Suicide prevention [Internet]. </li>\n<li>Naghavi M. Global, regional, and national burden of suicide mortality 1990 to 2016: systematic analysis for the Global Burden of Disease Study 2016. <a href=\"http://www.bmj.com/lookup/doi/10.1136/bmj.l94\" target=\"_blank\" rel=\"noopener\">BMJ</a> [Internet]. 2019 Feb 6;364:l94.  </li>\n<li><a href=\"http://www.ine.es/jaxi/Datos.htm?path=/t15/p417/a2017/l0/&file=05008.px\" target=\"_blank\" rel=\"noopener\">Instituto Nacional de Estadística</a>. Tasas de suicidios por edad y sexo por 100.000 habitantes [Internet]. Defunciones según causa de muerte. 2017. </li>\n<li>The Institute for Health Metrics and Evaluation (<a href=\"https://vizhub.healthdata.org/gbd-compare/\" target=\"_blank\" rel=\"noopener\">IHME</a>). Global Burden of Disease (GBD) compare [Internet]. 2017.  </li>\n<li>Departament de Salut. Generalitat de Catalunya. Pla de salut de Catalunya 2016-2020. 2016.</li>\n<li><a href=\"http://salutweb.gencat.cat/web/.content/_departament/estadistiques-sanitaries/dades-de-salut-serveis-sanitaris/mortalitat/documents/mortalitat_2016.pdf\" target=\"_blank\" rel=\"noopener\">Generalitat de Catalunya</a>. Anàlisi de la mortalitat a Catalunya 2016. Avanç de resultats [Internet]. 2018. </li>\n<li><a href=\"http://apps.who.int/iris/bitstream/10665/89966/1/9789241506021_eng.pdf\" target=\"_blank\" rel=\"noopener\">World Health Organization</a>. Mental Health Action Plan 2013-2020. World Heal Organ [Internet]. 2013;1–44. </li>\n<li>United Nations Development Programme. The 2030 Agenda for <a href=\"https://sustainabledevelopment.un.org/content/documents/21252030%20Agenda for Sustainable Development web.pdf\" target=\"_blank\" rel=\"noopener\">Sustainable Development</a> [Internet]. Undp. 2015.</li>\n</ol>\n<p></p>\n<p></p>\n<p></p>\n<p></p>', '2020-01-29 14:32:42'),
(302, 'ca', '', '0000-00-00 00:00:00'),
(302, 'en', '', '0000-00-00 00:00:00'),
(303, 'es', '<p><span>La conducta suicida es un fenómeno complejo que se ha estigmatizado y silenciado históricamente lo cual ha dificultado su detección y abordaje . Sin embargo, en los últimos años, la preocupación por esta problemática y por el reto que representa su abordaje para los servicios de salud, ha </span><span>centrado la agenda política a nivel mundial. En este sentido, la Organización Mundial de la Salud (OMS), en su <strong>Plan de Acción sobre Salud Mental 2013-2020</strong>, fijó como prioridad la prevención del suicidio, con objeto de reducir la tasa de suicidio al 10% para el año 2020</span><span><sup>12</sup></span><span>; enfatizando la necesidad de estrategias de prevención integral o multisectorial del suicidio y la continua </span><span>evaluación de su efectividad para asegurar intervenciones basadas en la evidencia</span><span><sup>3</sup></span><span>. Asimismo, la prevención del suicidio forma parte del objetivo de salud y bienestar global previsto por las Naciones Unidas en su <strong>Agenda de Objetivos de Desarrollo Sostenible</strong> (ODS) para el 2030</span><span><sup>13</sup></span><span>.</span></p>\n<p><span>En esta línea, muchos países han venido desarrollando estrategias nacionales de prevención del suicidio que tratan de abordar desde diversos sectores (salud, educación, trabajo, medios de comunicación, etc.) diferentes factores que pueden influir en la conducta suicida. Tales estrategias pueden incluir componentes como: sistemas de vigilancia para la detección temprana de casos, abordaje sanitario en el momento de crisis y seguimiento especializado posterior, así como formación de profesionales (de salud, educación, fuerzas de seguridad...), entre otros</span><span><sup>3</sup></span><span>. </span></p>\n<p><span>Cataluña ha recogido la priorización de esta problemática establecida por la OMS en el <strong>Pla de Salut 2011-2015</strong>, estableciendo como objetivo de salud para el año 2020, la disminución de la mortalidad por suicidio. El <strong>Pla de Salut 2016-2020</strong> propone la salud mental como área prioritaria seleccionando dentro de la misma, la prevención del suicidio como actuación principal. El programa <strong>Código Riesgo Suicidio</strong> constituye el <em>core</em> (centro) de estas actuaciones, como estrategia de prevención indicada </span><span>de la conducta suicida</span><span><sup>10</sup></span><span>. La evaluación de la implementación del programa, de su efectividad en la reducción de la conducta suicida en Cataluña y de los costes de su aplicación, contemplada en nuestro proyecto, permitirá formular recomendaciones para la mejora del propio programa y de su implementación, así como de otros programas preventivos en salud mental.</span></p>\n<ol>\n<li>INE. Defunciones según las causas de muerte por grupos de enfermedades. 2018;2017:1–8. Available from: https://www.ine.es/prensa/edcm_2017.pdf</li>\n<li>Departament de Salut. Generalitat de Catalunya. Instrucció sobre l’atenció a les persones en risc de suïcidi. Codi Risc Suïcidi [Internet]. 10/2105 2015. Available from: https://scientiasalut.gencat.cat/bitstream/handle/11351/1654/catsalut_instruccio_10_2015.pdf?sequence=1&isAllowed=y</li>\n<li>World Health Organization. Preventing suicide: A global imperative. 2014.</li>\n<li>Ministerio de Sanidad. Guía de práctica clínica de prevención y tratamiento de la conducta suicida guía de práctica clínica de prevención y tratamiento de la conducta suicida. Agencia Evaluación Tecnol Galicia. 2012;398.</li>\n<li>Global Health Observatory (GHO) data. WHO | Suicide rates [Internet]. WHO. World Health Organization; 2018. Available from: https://www.who.int/gho/mental_health/suicide_rates/en/</li>\n<li>Centers for Disease Control and Prevention. Violence Prevention: Suicide prevention [Internet]. Available from: https://www.cdc.gov/violenceprevention/suicide/fastfact.html?CDC_AA_refVal=https%3A%2F%2Fwww.cdc.gov%2Fviolenceprevention%2Fsuicide%2Fconsequences.html</li>\n<li>Naghavi M. Global, regional, and national burden of suicide mortality 1990 to 2016: systematic analysis for the Global Burden of Disease Study 2016. BMJ [Internet]. 2019 Feb 6;364:l94. Available from: http://www.bmj.com/lookup/doi/10.1136/bmj.l94</li>\n<li>Instituto Nacional de Estadística. Tasas de suicidios por edad y sexo por 100.000 habitantes [Internet]. Defunciones según causa de muerte. 2017. Available from: http://www.ine.es/jaxi/Datos.htm?path=/t15/p417/a2017/l0/&file=05008.px</li>\n<li>The Institute for Health Metrics and Evaluation (IHME). Global Burden of Disease (GBD) compare [Internet]. 2017. Available from: https://vizhub.healthdata.org/gbd-compare/</li>\n<li>Departament de Salut. Generalitat de Catalunya. Pla de salut de Catalunya 2016-2020. 2016.</li>\n<li>Generalitat de Catalunya. Anàlisi de la mortalitat a Catalunya 2016. Avanç de resultats [Internet]. 2018. Available from: http://salutweb.gencat.cat/web/.content/_departament/estadistiques-sanitaries/dades-de-salut-serveis-sanitaris/mortalitat/documents/mortalitat_2016.pdf</li>\n<li>World Health Organization. Mental Health Action Plan 2013-2020. World Heal Organ [Internet]. 2013;1–44. Available from: http://apps.who.int/iris/bitstream/10665/89966/1/9789241506021_eng.pdf</li>\n<li>United Nations Development Programme. The 2030 Agenda for Sustainable Development [Internet]. Undp. 2015. Available from: https://sustainabledevelopment.un.org/content/documents/21252030 Agenda for Sustainable Development web.pdf</li>\n</ol>\n<p></p>\n<p></p>\n<p></p>', '2020-01-22 14:37:00'),
(303, 'ca', '', '0000-00-00 00:00:00'),
(303, 'en', '', '0000-00-00 00:00:00'),
(304, 'es', '¿Se puede prevenir el suicidio?', '0000-00-00 00:00:00'),
(304, 'ca', 'Es pot prevenir el suïcidi?', '0000-00-00 00:00:00'),
(304, 'en', 'Can we prevent the suicide?\n', '0000-00-00 00:00:00'),
(305, 'es', 'Objetivos', '0000-00-00 00:00:00'),
(305, 'ca', 'Objectius', '0000-00-00 00:00:00'),
(305, 'en', 'Objectives', '0000-00-00 00:00:00'),
(306, 'es', '<h2><span>Objetivos generales</span></h2>\n<ol>\n<li><span>Estimar la <strong>frecuencia </strong>del comportamiento o conducta suicida.</span><span></span><span></span></li>\n<li><span>Identificar <strong>factores de riesgo </strong>relacionados con la conducta suicida.</span><span></span><span></span></li>\n<li><span>Desarrollar <strong>algoritmos de predicción</strong> individualizados del riesgo de tentativa y de la repetición de la tentativa de suicidio.</span><span></span><span></span></li>\n<li><span>Identificar factores de riesgo clínicos y biológicos relacionados con la <strong>repetición</strong> de la conducta suicida.</span><span></span><span></span></li>\n<li><strong><span>Evaluar el programa</span></strong><span> Código Riesgo Suicidio (CRS) de Cataluña.</span></li>\n</ol>\n<h2><span>Objetivos específicos</span></h2>\n<h3><span>Estudio epidemiológico</span></h3>\n<ol>\n<li><span>Estimar la <strong>incidencia</strong> <strong>poblacional</strong> de la tentativa de suicidio, la repetición de la tentativa de suicidio y del suicidio en Cataluña.</span><span></span><span></span></li>\n<li><span>Identificar <strong>factores de riesgo, tanto a nivel individual como a nivel poblacional, </strong>relacionados con la conducta suicida.</span><span></span><span></span></li>\n<li><span>Desarrollar un <strong>sistema de puntuación de riesgo personalizado</strong> para la conducta suicida.</span><span></span><span></span></li>\n<li><strong><span>Evaluar la implementación</span></strong><span> del programa CRS; su efectividad y coste-efectividad. </span></li>\n</ol>\n<h3><span>Estudio clínico</span></h3>\n<ol>\n<li><span>Identificar los <strong>factores de riesgo biológicos, neuropsicológicos y clínicos</strong> de la repetición de la tentativa de suicidio.</span><span></span><span></span></li>\n<li><span>Analizar las diferencias biológicas relacionadas con la respuesta al estrés (psiconeuroendocrinas, inflamatorias, genéticas y marcadores de neuroimagen) <strong>entre personas con tentativa de suicidio y personas que repitieron la tentativa de suicidio.</strong></span><span><strong></strong></span><span><strong></strong></span></li>\n<li><span>Analizar las diferencias biológicas relacionadas con la respuesta al estrés (psiconeuroendocrinas, inflamatorias, genéticas y marcadores de neuroimagen) <strong>entre personas con tentativa de suicidio, personas que repitieron la tentativa, y un grupo control formado por personas sin conducta suicida previa. </strong></span></li>\n</ol>', '2020-02-12 11:18:09'),
(306, 'ca', '', '0000-00-00 00:00:00'),
(306, 'en', '', '0000-00-00 00:00:00'),
(307, 'es', 'objetivos', '0000-00-00 00:00:00'),
(307, 'ca', 'objectius', '0000-00-00 00:00:00'),
(307, 'en', 'objectives', '0000-00-00 00:00:00'),
(308, 'es', 'Metodología', '0000-00-00 00:00:00'),
(308, 'ca', 'Metodología', '0000-00-00 00:00:00'),
(308, 'en', 'Metodology', '0000-00-00 00:00:00'),
(309, 'es', 'Estudio epidemiológico', '0000-00-00 00:00:00'),
(309, 'ca', 'Estudi epidemiològic\n', '0000-00-00 00:00:00'),
(309, 'en', 'Epidemiological survey\n', '0000-00-00 00:00:00'),
(310, 'es', 'Estudio caso control anidado de retrospectivo (periodo 2014-2019) en Cataluña. Cuenta con información sociodemográfica, clínica y de uso de servicios asistenciales procedente de diversos registros de datos administrativos y clínicos del sistema de salud público catalán con una población de unos 7,5 millones de asegurados. ', '2020-01-22 15:03:14'),
(310, 'ca', '', '0000-00-00 00:00:00'),
(310, 'en', '', '0000-00-00 00:00:00'),
(311, 'es', 'Estudio clínico', '0000-00-00 00:00:00'),
(311, 'ca', 'Estudi clínic', '0000-00-00 00:00:00'),
(311, 'en', 'Clinical survey\n', '0000-00-00 00:00:00'),
(312, 'es', '<p><span>El <strong>principal objetivo</strong> de este estudio es analizar las posibles diferencias a nivel clínico, neuropsicológico y biológico entre personas con conducta suicida, personas con repetición de la conducta suicida y personas sin antecedentes personales ni familiares de conducta suicida.</span></p>\n<p><span>Es un estudio de <strong>cohortes prospectivo</strong>, con un sub-estudio <strong>caso-control anidado</strong>, formado por una muestra representativa de unos 900 pacientes mayores de 18 años, que fueron atendidos y registrados por tentativa de suicidio en alguno de los <strong>5 centros hospitalarios </strong>que participan en el estudio: Consorci Hospitalari del Parc Taulí de Sabadell (Barcelona), Consorci MAR Parc de Salut de Barcelona,  Hospital de Sant Pau i la Santa Creu de Barcelona, Institut d\'Assistència Sanitària de Girona e Institut Pere Mata de Reus (Tarragona). Estos hospitales cubren una población aproximada de 2.5 millones de habitantes de Cataluña (33%).</span></p>\n<p><span>Tomando como consideraciones previas que se considera reintento de suicidio aquel nuevo intento que se produzca transcurridos como mínimo 30 días desde el episodio previo, que el 15-20% de los casos de tentativa reintentan durante el siguiente año y que existen unas pérdidas de seguimiento de en el seguimiento del 25% []. </span></p>\n<p><span>En el <strong>sub-estudio caso control-anidado</strong> en la cohorte, hemos considerado como <strong>casos </strong>(n~100), </span><span>a aquellas personas que durante el periodo de seguimiento presentan un nuevo intento de suicidio (repetición de la conducta suicida o reintento de suicidio). Se considerará reintento aquel nuevo intento de suicidio que se produzca transcurridos como mínimo 30 días desde el episodio índice de intento de suicidio. Esta cohorte se comparará a un grupo de controles sin ningún tipo de tendencia suicida.</span></p>\n<p><span>Los pacientes con tentativa de suicidio son evaluados en el plazo de 10 días desde el episodio de tentativa, mediante un conjunto de <strong>evaluaciones clínicas y pruebas neuropsicológicas</strong>, así como mediante la obtención de muestras <strong>biológicas</strong>; </span><span>. En el caso de repetición de la conducta suicida durante el periodo de seguimiento, se repetirá la evaluación (clínica, neuropsicológica y de muestras biológicas) en el plazo de 10 días desde el episodio de reintento, con objeto analizar el impacto de estos factores en la repetición de la conducta suicida. Asimismo, se obtendrán las mismas medidas de evaluación en la muestra de personas sin antecedentes personales ni familiares de conducta suicida.</span></p>\n<p><span> </span></p>\n<h2><strong>Evaluaciones clínicas:</strong></h2>\n<ul>\n<li><span><strong>Trastornos mentales</strong>: MINI International Neuropsychiatric Interview. </span></li>\n<li><span><strong>Características sociodemográficas y antecedentes personales y familiares</strong>: Protocolo breve de evaluación del suicidio</span></li>\n<li><strong>Depresión</strong>: Hamilton Depression Rating Scale-17 items (HDRS-17)</li>\n<li><span><strong>Gravedad médica</strong>: Medical Damage Scale (MDS)</span></li>\n<li><span><strong>Características y gravedad de la conducta suicida</strong>: Suicide Intent Scale (SIS)</span></li>\n<li><span><strong>Experiencias traumáticas en edad temprana</strong>: Childhood Trauma Questionnaire - Short Form (CTQ-SF) </span></li>\n<li><strong>Impulsividad</strong>: Barratt Impulsiveness Scale - 11 (BIS-11)</li>\n<li><strong>Agresividad</strong>: Brown-Goodwin Lifetime History of Aggression (BGLHA)</li>\n<li><span><strong>Calidad de vida relacionada con la salud</strong>: EUROQoL-5D </span></li>\n<li><span><strong>Trastorno de la personalidad e historial de autolesiones</strong>: Personality and Life Event Scale -Short version (S-PLE) </span></li>\n</ul>\n<h2><span><br />Pruebas neuropsicológicas:</span></h2>\n<ul>\n<li><strong>Memoria declarativa</strong>: Rey Auditory Verbal Learning Test (RAVLT)</li>\n<li><strong>Memoria de trabajo: </strong>Digits subtest (forward and backward) of the WAIS-IV</li>\n<li><span><strong>Atención, velocidad de procesamiento y flexibilidad cognitiva</strong>: Trail Making Test (Part A and B)</span></li>\n<li><span><strong>Atención y velocidad de procesamiento:</strong> Stroop test</span></li>\n<li><strong>Fluencia verbal:</strong> Semantic and phonemic fluency</li>\n<li><strong>Comprensión verbal</strong>: Vocabulary subtest of the WAIS-IV</li>\n</ul>\n<p> </p>\n<h2><span>Muestras biológicas:</span></h2>\n<ul>\n<li><span><strong>Muestras de sangre</strong>: con objeto de evaluar (1) parámetros de inflamación: Interleucina-6, Interleucina-1-Beta, Factor de necrosis tumoral (TNF)-alpha, interferón gamma (INF) y proteína C reactiva, utilizando kit ELISA luminex; (2) parámetros genéticos relacionados con la regulación del estrés por parte del eje hipotalámico hipofisario adrenal (HHA): NRC3, FKBP5, BDNF, CRHR1, SK2A, 5-HT2A, 5-HTT and TPH2.</span></li>\n<li><span><strong>Muestras de cabello</strong>: de 3 cm de longitud procedentes del vértice posterior de la región parietal de la cabeza, para medir con el kit ELISA los niveles de concentración de cortisol, como medida de exposición al estrés. </span></li>\n</ul>\n<p><span> </span></p>\n<p><span>A una <strong>sub-muestra de personas </strong>atendidas por conducta suicida en la provincia de Barcelona (n~60) </span><span>y de personas sin conducta suicida previa (n~60) se les realizará un <strong>estudio de neuroimagen</strong> (resonancia magnética Philips 3 Tesla), en el centro de investigación Fundació Pasqual Maragall (Barcelona), con el objetivo de establecer los correlatos neuroanatómicos estructurales y funcionales de las medidas clínicas y biológicas.</span></p>\n<p><span> </span></p>\n<h2><span>Pruebas de neuroimagen:</span></h2>\n<p><span>Se realiza un estudio de <strong>resonancia magnética</strong> que incluye diversas secuencias: (1) una secuencia estructural tridimensional (3D); (2) secuencia T2 y secuencia FLAIR para valorar posibles hallazgos estructurales; (3) una secuencia en estado de reposo y (4) una secuencia de tarea (funcional) de estrés de Montreal para neuroimagen (MIST), que es una adaptación de la prueba “Trier Mental Challenge Test” que consiste en una serie computerizada de tareas aritméticas con un componente de fallo inducido (límite de tiempo de ejecución) y un componente adicional de estrés social, con objeto de medir el patrón de respuesta del cerebro al estrés en condición de laboratorio. </span></p>\n<p></p>', '2020-01-22 14:58:43'),
(312, 'ca', '', '0000-00-00 00:00:00'),
(312, 'en', '', '0000-00-00 00:00:00'),
(313, 'es', 'metodologia', '0000-00-00 00:00:00'),
(313, 'ca', 'metodologia-ca', '0000-00-00 00:00:00'),
(313, 'en', 'metodology', '0000-00-00 00:00:00'),
(314, 'es', '<p><span>El desarrollo del estudio se basa principalmente en la combinación y análisis de datos de distintos registros públicos que son representativos de la población catalana y que incluyen variables de naturaleza muy diversa. </span></p>\n<p><span>Se llevará a cabo un<strong> estudio caso-control</strong> anidado <strong>retrospectivo</strong> definido a partir de la cohorte dinámica de población con acceso a los servicios públicos de salud de Cataluña en el período 2014-2019. La <strong>población de estudio o casos</strong> son aquellas personas atendidas por tentativa de suicidio en los servicios de salud públicos catalanes y que son registradas en CRS durante el periodo de estudio. </span></p>\n<p><span>Abordamos un problema importante de infraregistro de intento de suicidio; no todos los casos de intento de suicidio se detectan o se reportan como tal, por lo que no son codificados correctamente y registrados en CRS. Por ello, se revisarán tambien las bases de datos CMBD hospitalarias, de urgencias y de atención primaria y se incluirán como casos los pacientes que presenten algún diagnóstico que codifique por autolesión, ya que los códigos ICD (ICD-9-CM y ICD-10-CM), actualmente utilizados para codificar intentos de suicidio, no distinguen entre autolesiones con intención o sin intención suicida. En total se calcula una muestra total de unos 15000 casos.</span></p>\n<p><span>Todos los datos disponibles son de origen público y se gestionan a través de la Agència de Qualitat i Avaluació Sanitària de Catalunya (AQuAS). </span></p>\n<p><span>Como resultados, se estimarán medidas de incidencia de intento y reintento de suicidio y se calculará qué relación existe con los principales factores de riesgo a nivel individual y poblacional.  El gran reto recae en el gran número de factores de riesgo que pueden ser explicativos de la conducta suicida y sus orígenes tan diversos: factores sociodemográficos (p. ej., edad, sexo o área de residencia), socioeconómicos (p. ej., nivel de renta), clínicos (p. ej., diagnósticos médicos o psiquiátricos) y de uso de servicios asistenciales (p. ej., visitas de tratamiento y medicación). </span></p>\n<p><span>Para abordar este reto, desarrollaremos algoritmos de clasificación avanzados (por ejemplo, regresión logística regularizada, random forests o gradient boosting) para tener en cuenta los diversos efectos aditivos y de interacción que se dan entre la gran cantidad de factores de riesgo en estudio. Esto dará como resultado la identificación de varios conjuntos de factores de riesgo relevantes y proporcionarán nueva información a los médicos sobre cómo evaluar el riesgo de suicidio en la práctica diaria. Además, nuestro estudio dará como resultado herramientas útiles para ayudar a los médicos a evaluar el riesgo real de suicidio, es decir, estimar la probabilidad de intento de suicidio posterior a las visitas de atención médica en diferentes entornos de atención médica.</span></p>\n<p><span>Finalmente se estimará la efectividad del programa teniendo en cuenta la mortalidad y las hospitalizaciones por suicidio, así como los costes derivados de la implementación y uso del programa, comparándose tales resultados con el tratamiento habitual de estos pacientes. Asimismo, se evaluará la implementación del programa en Cataluña utilizando indicadores de cobertura y calidad (es decir, indicadores que tienen en cuenta la proporción de la población diana que recibe la intervención y el grado en el que el programa cumple con el protocolo de actuaciones previsto).</span></p>\n<p></p>', '2020-01-22 15:02:25'),
(314, 'ca', '', '0000-00-00 00:00:00'),
(314, 'en', '', '0000-00-00 00:00:00'),
(315, 'es', 'epidemiologico', '0000-00-00 00:00:00'),
(315, 'ca', 'epidemiologic', '0000-00-00 00:00:00'),
(315, 'en', 'epidemiologico-en', '0000-00-00 00:00:00'),
(316, 'es', 'clinico', '0000-00-00 00:00:00'),
(316, 'ca', 'clinic', '0000-00-00 00:00:00'),
(316, 'en', 'clinico-en', '0000-00-00 00:00:00'),
(317, 'es', 'Estudio caso control anidado de retrospectivo (periodo 2014-2019) en Cataluña. Cuenta con información sociodemográfica, clínica y de uso de servicios asistenciales procedente de diversos registros de datos administrativos y clínicos del sistema de salud público catalán con una población de unos 7,5 millones de asegurados. ', '2020-01-22 15:03:54'),
(317, 'ca', '', '0000-00-00 00:00:00'),
(317, 'en', '', '0000-00-00 00:00:00'),
(318, 'es', 'Un estudio de seguimiento a 12 meses de una cohorte de personas con intento de suicidio-reciente, en el que se realiza el estudio clínico, neuropsicológico y medición de marcadores biológicos en sangre y pelo. Incluye un estudio caso-control anidado para comparar los resultados de aquellos pacientes que repitan el intento de suicidio durante el tiempo de seguimiento con los pacientes que no y con controles sanos. ', '2020-01-22 14:59:37'),
(318, 'ca', '', '0000-00-00 00:00:00'),
(318, 'en', '', '0000-00-00 00:00:00'),
(319, 'es', 'intervencion-crs', '0000-00-00 00:00:00'),
(319, 'ca', 'intervencio-crs', '0000-00-00 00:00:00'),
(319, 'en', 'crs-intervention', '0000-00-00 00:00:00'),
(320, 'es', 'Intervención CRS', '0000-00-00 00:00:00'),
(320, 'ca', 'Intervenció CRS', '0000-00-00 00:00:00'),
(320, 'en', 'CRS intervention', '0000-00-00 00:00:00'),
(321, 'es', '<p><span>El <strong>Código Riesgo Suicidio (CRS)</strong> es un programa protocolizado de actuaciones asistenciales y preventivas que el Departament de Salut de la Generalitat de Cataluña puso en marcha en 2014 para la detección y atención precoz de las conductas suicidas en Cataluña. La población diana de la intervención son aquellas personas que en el momento que entran en contacto con el sistema sanitario integral de utilización pública de Cataluña (SISCAT) presentan un riesgo alto de suicidio. </span></p>\n<p><span>El programa comprende: (1) un sistema de vigilancia y registro de conductas suicidas; y (2) una intervención de prevención indicada orientada a sujetos con riesgo de suicidio o tentativa previa de suicidio. El objetivo principal es proporcionar una atención continuada a personas con un alto riesgo de suicidio o tentativa de suicidio. CRS cubre tanto la activación de los recursos necesarios para garantizar atención urgente como el seguimiento preventivo posterior.</span></p>\n<p><span>La intervención se divide en tres fases: </span></p>\n<ol>\n<li><strong><span>Detección precoz y cribado inicial</span></strong><span> para tomar una decisión asistencial adecuada. El protocolo ofrece unas prácticas y guías clínicas para el primer cribado y remarca la necesidad de validar el nivel de riesgo de suicidio del paciente con una evaluación clínica a cargo de un especialista en salud mental. Según el resultado de la valoración resultaran distintos niveles de riesgo: bajo, medio y alto. Todos aquellos casos con una puntuación de 10 puntos o más en el módulo de suicidio en la escala Mini International Neuropsychiatric Interview (MINI) pueden haber sufrido un intento previo o manifestar una planificación de la conducta suicida, por lo que se los considera de alto riesgo. <strong>Éste constituye el grupo de personas que se incluyen en el registro oficial de CRS. </strong>Ante la sospecha de un caso de alto riesgo se articula un sistema de notificación y derivación a los sistemas de atención de emergencia, donde <strong>se atiende al paciente de forma urgente y se activa el protocolo.</strong></span><span><strong></strong></span><strong><span></span></strong></li>\n<li><strong><span>Seguimiento proactivo posthospitalario</span></strong><span> que se inicia después del alta durante 30 días como mínimo. Esta fase incluye:</span><span></span><span></span>\n<ol>\n<li><span>Una <strong>visita de atención especializada</strong> en un Centro de Salud Mental (CSMA), Centro de Salud Mental Infantil y Juvenil (CSMIJ), Centro de Atención y Seguimiento a las drogodependencias (CAS), Hospital de Día, etc. antes de 10 días (72 horas en caso de personas menores de 18 años) para reevaluar el riesgo de suicidio y diseñar un plan terapéutico de seguimiento individualizado acorde al nivel de riesgo de suicidio y a los factores psicopatológicos concomitantes detectados; </span><strong><span></span></strong><span></span></li>\n<li><strong><span>Un seguimiento telefónico del </span></strong><span>061 CatSalut Respon/equips EMSE antes de los 30 días, para reevaluar el riesgo de suicidio y el estado de salud de la persona, proporcionar apoyo y consejo sanitario, así como verificar la recepción de la visita de atención especializada y detectar qué personas se desvinculan de los servicios.</span><strong><span></span></strong><span></span></li>\n<li><strong><span>Seguimiento preventivo longitudinal</span></strong><span> durante 12 meses </span>por diferentes dispositivos de atención especializada [<span>CSMA/CSMIJ, CAS, Hospital de Día o Atención Primaria (AP)] </span>según el nivel de riesgo de suicidio presente. Finalmente, si al año de seguimiento el riesgo de suicidio se encuentra estabilizado en nivel bajo, se procederá al alta del protocolo CRS.</li>\n</ol>\n</li>\n</ol>\n<p><span>Además de activar un sistema de alerta para pacientes con un alto riesgo de suicidio, e</span>l protocolo CRS también incluye guías de actuación frente a personas con riesgo bajo o moderado.</p>\n<p> </p>\n<p>Toda la información está sacada de la instrucción <strong>10/2015</strong> de la Generalitat de Catalunya (<em>Atenció a les persones en risc de suïcidi. Codi risc de suïcidi (CRS)</em>), en la que se describe en detalle el protocolo de actuaciones en distintos casos, la coordinación e interacción entre servicios asistenciales y la ética del protocolo CRS. Accede a este <span><a href=\"https://scientiasalut.gencat.cat/bitstream/handle/11351/1654/catsalut_instruccio_10_2015.pdf?sequence=1&isAllowed=y\"><em>link</em></a></span> para su consulta.</p>\n<p><strong>Figura 1. Protocolo de actuaciones y recorrido asistencial del programa Código Riesgo Suicidio.</strong><span></span></p>', '2020-01-22 14:52:00'),
(321, 'ca', '', '0000-00-00 00:00:00'),
(321, 'en', '', '0000-00-00 00:00:00'),
(322, 'es', 'Ética', '0000-00-00 00:00:00'),
(322, 'ca', 'Ètica', '0000-00-00 00:00:00'),
(322, 'en', 'Ethic', '0000-00-00 00:00:00'),
(323, 'es', '<p><span>El <strong>proyecto Codi Risc</strong> fue aprobado por el Comité Ético de Investigación Médica del Parc de Salut Mar (CEIC) el 6 de febrero de 2018 (protocolo CEIC 2017/7431/I). El estudio se ajusta a los principios establecidos en la Declaración de Helsinki, en la Carta de los Derechos Fundamentales de la Unión Europea (2000/C 364/01) y en el Convenio Europeo de Derechos Humanos. Como se explica en el apartado 4.2, todos los datos brutos de este estudio proceden del programa de Análisis de Datos Públicos para la Investigación y la Innovación en Salud (PADRIS), gestionado por la Agencia Catalana de Calidad y Evaluación de la Salud (AQuAS), dependiente del Departamento de Salud de Cataluña. </span></p>\n<p><span>El estudio implicará el procesamiento de datos de registros sanitarios completamente anonimizados (sin datos personales). AQuAS/PADRIS cuenta con todos los órganos de gobierno y control necesarios y con los recursos tecnológicos y humanos para cumplir las funciones de gestión estratégica, seguimiento, evaluación e implementación operativa, anonimización e indización del conjunto de datos a efectos de investigación, innovación y evaluación. Los datos anonimizados y desidentificados sólo serán accesibles para los investigadores de los centros de investigación certificados por los Centros de Investigación de Cataluña (CERCA), los representantes del Sistema Sanitario Catalán (SISCAT) y los centros de investigación universitarios públicos, así como la propia administración sanitaria (es decir, los grupos de investigación del Plan Estratégico de Salud de Cataluña). Las partes interesadas sólo podrán acceder a los datos de acuerdo con los criterios establecidos para garantizar la salvaguarda de los principios éticos, de seguridad y de análisis de riesgos de PADRIS y tras la aprobación por un comité de ética de investigación médica. Los principios éticos establecidos por el programa PADRIS incluyen el respeto a las personas, la solidaridad de paciente a paciente, la equidad, la eficiencia, la transparencia, la investigación responsable y la protección de datos personales. Para las actividades de vinculación de registros, se sigue la Orden Española SAS/3470/2009 para los datos obtenidos en estudios observacionales, que sólo requiere consentimiento informado en los casos en que no se adopten medidas de disociación de datos. Esto también está en línea con la Directiva de Protección de Datos de la Unión Europea (Directiva 95/46/CE). </span></p>\n<p><span>Únicamente se procesarán datos relevantes y limitados a los fines del proyecto (principio de minimización de datos). En este sentido, el proyecto Codi Risc sólo evalúa las variables sociodemográficas, clínicas y de uso de servicios sobre las que concurre presunción razonable de su potencial predictivo sobre la conducta suicida.</span></p>', '2020-01-22 14:38:55'),
(323, 'ca', '', '0000-00-00 00:00:00'),
(323, 'en', '', '0000-00-00 00:00:00'),
(324, 'es', 'etica', '0000-00-00 00:00:00'),
(324, 'ca', 'etica-ca', '0000-00-00 00:00:00'),
(324, 'en', 'ethic', '0000-00-00 00:00:00'),
(325, 'es', 'recursos', '0000-00-00 00:00:00'),
(325, 'ca', 'recursos-ca', '0000-00-00 00:00:00'),
(325, 'en', 'resources', '0000-00-00 00:00:00'),
(326, 'es', 'Recursos en línea', '0000-00-00 00:00:00'),
(326, 'ca', 'Recursos en linea', '0000-00-00 00:00:00'),
(326, 'en', 'Resources', '0000-00-00 00:00:00'),
(327, 'es', 'Centros de Salud en Cataluña', '0000-00-00 00:00:00'),
(327, 'ca', '', '0000-00-00 00:00:00'),
(327, 'en', '', '0000-00-00 00:00:00'),
(328, 'es', 'Accede al mapa interactivo, para encontrar el centro de salud más cercano y adiente para lo que buscas, a través de este link al recurso de Catsalut', '0000-00-00 00:00:00'),
(328, 'ca', '', '0000-00-00 00:00:00'),
(328, 'en', '', '0000-00-00 00:00:00'),
(329, 'es', 'Asociaciones y otras organizaciones', '0000-00-00 00:00:00'),
(329, 'ca', '', '0000-00-00 00:00:00'),
(329, 'en', '', '0000-00-00 00:00:00'),
(330, 'es', 'American Foundation for Suicide Prevention', '0000-00-00 00:00:00'),
(330, 'ca', '', '0000-00-00 00:00:00'),
(330, 'en', '', '0000-00-00 00:00:00'),
(331, 'es', 'Asociacion de supervivientes y entidades colaboradoras\n\n', '0000-00-00 00:00:00'),
(331, 'ca', '', '0000-00-00 00:00:00'),
(331, 'en', '', '0000-00-00 00:00:00'),
(332, 'es', 'Associació Catalana per la Prevenció del Suïcidi\n\n', '0000-00-00 00:00:00'),
(332, 'ca', '', '0000-00-00 00:00:00'),
(332, 'en', '', '0000-00-00 00:00:00'),
(333, 'es', '<div id=\"u645\" class=\"ax_default\" data-left=\"202\" data-top=\"715\" data-width=\"566\" data-height=\"108\">\n<div id=\"u647\" class=\"ax_default paragraph\">\n<div id=\"u647_text\" class=\"text \">\n<p><span>Descripció</span><span>. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar sic tempor. Sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus pronin sapien nunc accuan eget.</span></p>\n</div>\n</div>\n</div>', '0000-00-00 00:00:00'),
(333, 'ca', '', '0000-00-00 00:00:00'),
(333, 'en', '', '0000-00-00 00:00:00'),
(334, 'es', 'Nom', '0000-00-00 00:00:00'),
(334, 'ca', '', '0000-00-00 00:00:00'),
(334, 'en', '', '0000-00-00 00:00:00'),
(335, 'es', '<div id=\"u648\" class=\"ax_default\" data-left=\"202\" data-top=\"837\" data-width=\"566\" data-height=\"108\">\n<div id=\"u650\" class=\"ax_default paragraph\">\n<div id=\"u650_text\" class=\"text \">\n<p><span>Descripció</span><span>. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar sic tempor. Sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus pronin sapien nunc accuan eget.</span></p>\n</div>\n</div>\n</div>', '0000-00-00 00:00:00'),
(335, 'ca', '', '0000-00-00 00:00:00'),
(335, 'en', '', '0000-00-00 00:00:00'),
(336, 'es', 'Accede', '0000-00-00 00:00:00'),
(336, 'ca', 'Accedeix', '0000-00-00 00:00:00'),
(336, 'en', 'Access', '0000-00-00 00:00:00'),
(337, 'es', 'No hay resultados', '0000-00-00 00:00:00'),
(337, 'ca', 'No hi ha resultats', '0000-00-00 00:00:00'),
(337, 'en', 'No results', '0000-00-00 00:00:00'),
(338, 'es', 'Prevenir el suicidio', '0000-00-00 00:00:00'),
(338, 'ca', '', '0000-00-00 00:00:00'),
(338, 'en', '', '0000-00-00 00:00:00'),
(339, 'es', 'Prevenir el suicidio', '0000-00-00 00:00:00'),
(339, 'ca', '', '0000-00-00 00:00:00'),
(339, 'en', '', '0000-00-00 00:00:00'),
(340, 'es', 'Equipo investigador', '2020-01-02 08:32:37'),
(340, 'ca', 'Equip investigador', '2020-01-02 08:32:37'),
(340, 'en', 'Research team', '2020-01-02 08:32:37'),
(341, 'es', 'Sobre el suicidio', '0000-00-00 00:00:00'),
(341, 'ca', 'Sobre el suicidi', '2020-01-02 08:30:48'),
(341, 'en', 'About suicide', '2020-01-02 08:30:48'),
(342, 'es', 'Noticias', '0000-00-00 00:00:00'),
(342, 'ca', 'Noticíes', '2020-01-02 08:31:30'),
(342, 'en', 'News', '2020-01-02 08:31:30'),
(343, 'es', 'Publicaciones', '0000-00-00 00:00:00'),
(343, 'ca', 'Publicacions', '0000-00-00 00:00:00'),
(343, 'en', '\nPublications', '0000-00-00 00:00:00'),
(344, 'es', 'Contacto', '0000-00-00 00:00:00'),
(344, 'ca', 'Contacte', '0000-00-00 00:00:00'),
(344, 'en', 'Contact', '0000-00-00 00:00:00'),
(345, 'es', 'Sobre el suicidio', '0000-00-00 00:00:00'),
(345, 'ca', 'Sobre el suicidi', '0000-00-00 00:00:00'),
(345, 'en', 'About suicide', '0000-00-00 00:00:00'),
(346, 'es', 'Sobre el suicidio', '0000-00-00 00:00:00'),
(346, 'ca', 'Sobre el suicidi', '0000-00-00 00:00:00'),
(346, 'en', 'About suicide', '0000-00-00 00:00:00'),
(347, 'es', 'Ética', '0000-00-00 00:00:00'),
(347, 'ca', 'Ètica', '0000-00-00 00:00:00'),
(347, 'en', 'Ethics', '0000-00-00 00:00:00'),
(348, 'es', 'Ética', '2020-01-02 08:52:34'),
(348, 'ca', 'Ètica', '2020-01-02 08:52:34'),
(348, 'en', 'Ethic', '2020-01-02 08:52:34'),
(349, 'es', 'Intervención CRS', '0000-00-00 00:00:00'),
(349, 'ca', 'Intervenció CRS', '0000-00-00 00:00:00'),
(349, 'en', 'CRS Intervention', '0000-00-00 00:00:00'),
(350, 'es', 'Intervención CRS', '0000-00-00 00:00:00'),
(350, 'ca', 'Intervenció CRS', '0000-00-00 00:00:00'),
(350, 'en', 'CRS Intervention', '0000-00-00 00:00:00'),
(351, 'es', 'Metodología', '0000-00-00 00:00:00'),
(351, 'ca', 'Metodologia', '0000-00-00 00:00:00'),
(351, 'en', 'Methodology', '0000-00-00 00:00:00'),
(352, 'es', 'Metodología', '0000-00-00 00:00:00'),
(352, 'ca', 'Metodologia', '0000-00-00 00:00:00'),
(352, 'en', 'Methodology', '0000-00-00 00:00:00'),
(353, 'es', 'Estudio clínico', '0000-00-00 00:00:00'),
(353, 'ca', 'Estudi clínic', '0000-00-00 00:00:00'),
(353, 'en', 'Clinical research', '2020-01-02 09:09:11'),
(354, 'es', 'Estudio epidemiológico', '0000-00-00 00:00:00'),
(354, 'ca', 'Estudi epidemiològic', '0000-00-00 00:00:00'),
(354, 'en', 'Epidemiological study', '0000-00-00 00:00:00'),
(355, 'es', 'Estudio clínico', '2020-01-02 09:07:05'),
(355, 'ca', 'Estudi clínic', '2020-01-02 09:07:05'),
(355, 'en', 'Clinical research', '2020-01-02 09:09:15'),
(356, 'es', 'Estudio clínico', '2020-01-02 09:06:55'),
(356, 'ca', 'Estudi clínic', '2020-01-02 09:06:55'),
(356, 'en', 'Clinical research', '2020-01-02 09:09:13'),
(357, 'es', 'Estudio epidemiológico', '2020-01-02 09:06:40'),
(357, 'ca', 'Estudi epidemiològic', '2020-01-02 09:06:40'),
(357, 'en', 'Epidemiological study', '2020-01-02 09:06:40'),
(358, 'es', 'Estudio clínico', '2020-01-02 09:07:56'),
(358, 'ca', 'Estudi clínic', '2020-01-02 09:07:56'),
(358, 'en', 'Clinical research', '2020-01-02 09:09:17'),
(359, 'es', 'Recursos en linea', '0000-00-00 00:00:00'),
(359, 'ca', 'Recursos en línea', '0000-00-00 00:00:00'),
(359, 'en', 'Online resources', '0000-00-00 00:00:00'),
(360, 'es', 'Recursos en linea', '0000-00-00 00:00:00'),
(360, 'ca', 'Recursos en línea', '0000-00-00 00:00:00'),
(360, 'en', 'Online resources', '0000-00-00 00:00:00'),
(361, 'es', 'https://folduka.imim.science/login?path=/CodiRisc', '2020-02-19 16:08:37'),
(361, 'ca', 'https://folduka.imim.science/login?path=/CodiRisc', '2020-02-19 16:08:37'),
(361, 'en', 'https://folduka.imim.science/login?path=/CodiRisc', '2020-02-19 16:08:37'),
(362, 'es', 'Incrementando el conocimiento sobre la prevención del suicidio en Cataluña', '0000-00-00 00:00:00'),
(362, 'ca', '', '0000-00-00 00:00:00'),
(362, 'en', '', '0000-00-00 00:00:00'),
(363, 'es', 'Codi Risc es un estudio coordinado basado en la intervención Codi Risc Suicidi (CRS) del Deparatment de Salut de la Generalitat. Estudia el comportamiento epidemiológico e identifica factores de riesgo para dibujar unas líneas de mejora en la gestión de la prevención de la conducta suicida.', '0000-00-00 00:00:00'),
(363, 'ca', '', '0000-00-00 00:00:00'),
(363, 'en', '', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `textos_content_bk`
--

CREATE TABLE `textos_content_bk` (
  `id` int(11) DEFAULT NULL,
  `idioma` varchar(2) NOT NULL,
  `textarea_titulo` text,
  `last_modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `txtcontrol`
--

CREATE TABLE `txtcontrol` (
  `id` int(11) NOT NULL,
  `fecha` datetime NOT NULL,
  `operacion` varchar(250) NOT NULL,
  `fichero` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuaris`
--

CREATE TABLE `usuaris` (
  `id` int(11) NOT NULL,
  `text_titulo` varchar(50) NOT NULL COMMENT 'Nom',
  `text_codiuser` varchar(20) NOT NULL COMMENT 'Nom d''usuari',
  `text_password` varchar(250) NOT NULL COMMENT 'Password',
  `dataalta` date NOT NULL DEFAULT '0000-00-00',
  `checkbox_nivell` int(11) NOT NULL DEFAULT '1' COMMENT 'Usuario Cliente - MARCAR SIEMPRE EXCEPTO PARA NUESTRO USUARIO',
  `loginon` int(11) NOT NULL DEFAULT '0',
  `timelogin` int(11) DEFAULT '0',
  `timelastclick` int(11) DEFAULT '0',
  `idioma` varchar(2) NOT NULL,
  `checkbox_textos` int(11) NOT NULL COMMENT 'Accedir a textos'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `usuaris`
--

INSERT INTO `usuaris` (`id`, `text_titulo`, `text_codiuser`, `text_password`, `dataalta`, `checkbox_nivell`, `loginon`, `timelogin`, `timelastclick`, `idioma`, `checkbox_textos`) VALUES
(1, 'La Teva Web', 'ManagerLTW-isc', 'c288200c623f19e51c9183e2ef745561c3010b00feb0bdea8568bdbe91b9cb7a', '0000-00-00', 0, 0, 0, 0, 'es', 1),
(2, 'javier', 'javier', 'c9e1e6a61fc07c41ef68cdca92b8130d3efbb2718ee0a4b9eeaf1a57c9af48b0', '0000-00-00', 1, 0, 0, 0, 'es', 1);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `asociaciones`
--
ALTER TABLE `asociaciones`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `asociaciones_content`
--
ALTER TABLE `asociaciones_content`
  ADD PRIMARY KEY (`id`,`idioma`);

--
-- Indices de la tabla `categorias_publicaciones`
--
ALTER TABLE `categorias_publicaciones`
  ADD PRIMARY KEY (`id`),
  ADD KEY `select_categorias_productos` (`select_categorias_publicaciones`),
  ADD KEY `orden` (`orden`);

--
-- Indices de la tabla `categorias_publicaciones_content`
--
ALTER TABLE `categorias_publicaciones_content`
  ADD KEY `id` (`id`),
  ADD KEY `idioma` (`idioma`);

--
-- Indices de la tabla `config_general`
--
ALTER TABLE `config_general`
  ADD PRIMARY KEY (`id`),
  ADD KEY `key` (`name`);

--
-- Indices de la tabla `config_idiomes`
--
ALTER TABLE `config_idiomes`
  ADD PRIMARY KEY (`id_idioma`);

--
-- Indices de la tabla `config_idiomesbackoffice`
--
ALTER TABLE `config_idiomesbackoffice`
  ADD PRIMARY KEY (`id_idioma`);

--
-- Indices de la tabla `config_menus`
--
ALTER TABLE `config_menus`
  ADD PRIMARY KEY (`id`),
  ADD KEY `key` (`name`);

--
-- Indices de la tabla `config_thumbs`
--
ALTER TABLE `config_thumbs`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `config_tipothumb`
--
ALTER TABLE `config_tipothumb`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `equipo`
--
ALTER TABLE `equipo`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `equipo_content`
--
ALTER TABLE `equipo_content`
  ADD PRIMARY KEY (`id`,`idioma`);

--
-- Indices de la tabla `ficherosnoticias`
--
ALTER TABLE `ficherosnoticias`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_elem` (`id_elem`),
  ADD KEY `orden` (`orden`),
  ADD KEY `idioma` (`idioma`);

--
-- Indices de la tabla `ficherospublicaciones`
--
ALTER TABLE `ficherospublicaciones`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_elem` (`id_elem`),
  ADD KEY `orden` (`orden`),
  ADD KEY `idioma` (`idioma`);

--
-- Indices de la tabla `fotosnoticias`
--
ALTER TABLE `fotosnoticias`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_elem` (`id_elem`),
  ADD KEY `orden` (`orden`),
  ADD KEY `idioma` (`idioma`);

--
-- Indices de la tabla `fotospublicaciones`
--
ALTER TABLE `fotospublicaciones`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_elem` (`id_elem`),
  ADD KEY `orden` (`orden`),
  ADD KEY `idioma` (`idioma`);

--
-- Indices de la tabla `fotosslide`
--
ALTER TABLE `fotosslide`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_elem` (`id_elem`),
  ADD KEY `orden` (`orden`),
  ADD KEY `idioma` (`idioma`);

--
-- Indices de la tabla `mails`
--
ALTER TABLE `mails`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `noticias`
--
ALTER TABLE `noticias`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `noticias_content`
--
ALTER TABLE `noticias_content`
  ADD PRIMARY KEY (`id`,`idioma`);

--
-- Indices de la tabla `publicaciones`
--
ALTER TABLE `publicaciones`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `publicaciones_content`
--
ALTER TABLE `publicaciones_content`
  ADD PRIMARY KEY (`id`,`idioma`);

--
-- Indices de la tabla `tags_publicaciones`
--
ALTER TABLE `tags_publicaciones`
  ADD PRIMARY KEY (`id`),
  ADD KEY `select_categorias_productos` (`select_tags_publicaciones`),
  ADD KEY `orden` (`orden`);

--
-- Indices de la tabla `tags_publicaciones_content`
--
ALTER TABLE `tags_publicaciones_content`
  ADD KEY `id` (`id`),
  ADD KEY `idioma` (`idioma`);

--
-- Indices de la tabla `textos`
--
ALTER TABLE `textos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `textosbackoffice`
--
ALTER TABLE `textosbackoffice`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `text_titulo` (`text_titulo`);

--
-- Indices de la tabla `textosbackoffice_content`
--
ALTER TABLE `textosbackoffice_content`
  ADD KEY `id` (`id`),
  ADD KEY `idioma` (`idioma`);

--
-- Indices de la tabla `textos_bk`
--
ALTER TABLE `textos_bk`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `textos_content`
--
ALTER TABLE `textos_content`
  ADD KEY `id` (`id`),
  ADD KEY `idioma` (`idioma`);

--
-- Indices de la tabla `textos_content_bk`
--
ALTER TABLE `textos_content_bk`
  ADD KEY `id` (`id`),
  ADD KEY `idioma` (`idioma`);

--
-- Indices de la tabla `txtcontrol`
--
ALTER TABLE `txtcontrol`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `usuaris`
--
ALTER TABLE `usuaris`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `asociaciones`
--
ALTER TABLE `asociaciones`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `categorias_publicaciones`
--
ALTER TABLE `categorias_publicaciones`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `config_general`
--
ALTER TABLE `config_general`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT de la tabla `config_idiomes`
--
ALTER TABLE `config_idiomes`
  MODIFY `id_idioma` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `config_idiomesbackoffice`
--
ALTER TABLE `config_idiomesbackoffice`
  MODIFY `id_idioma` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `config_menus`
--
ALTER TABLE `config_menus`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `config_thumbs`
--
ALTER TABLE `config_thumbs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT de la tabla `config_tipothumb`
--
ALTER TABLE `config_tipothumb`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `equipo`
--
ALTER TABLE `equipo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT de la tabla `ficherosnoticias`
--
ALTER TABLE `ficherosnoticias`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `ficherospublicaciones`
--
ALTER TABLE `ficherospublicaciones`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `fotosnoticias`
--
ALTER TABLE `fotosnoticias`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;

--
-- AUTO_INCREMENT de la tabla `fotospublicaciones`
--
ALTER TABLE `fotospublicaciones`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `fotosslide`
--
ALTER TABLE `fotosslide`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT de la tabla `mails`
--
ALTER TABLE `mails`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `noticias`
--
ALTER TABLE `noticias`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de la tabla `publicaciones`
--
ALTER TABLE `publicaciones`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `tags_publicaciones`
--
ALTER TABLE `tags_publicaciones`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `textos`
--
ALTER TABLE `textos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=364;

--
-- AUTO_INCREMENT de la tabla `textosbackoffice`
--
ALTER TABLE `textosbackoffice`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=66;

--
-- AUTO_INCREMENT de la tabla `textos_bk`
--
ALTER TABLE `textos_bk`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `txtcontrol`
--
ALTER TABLE `txtcontrol`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `usuaris`
--
ALTER TABLE `usuaris`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
