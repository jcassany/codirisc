<?php
include "head.php";
$print_ltw_link = true;
$ht_title = HOME_METAS_TIT;
$ht_description = htmlspecialchars(HOME_METAS_DESC, ENT_QUOTES, 'UTF-8');
?>
<!DOCTYPE html>
<html lang="<?php echo IDIOMA; ?>">
<head><?php include "header.php";?></head>
<body class="home">
	<?php include "body.php";?>

	<div class="pr">
        <div class="slickSlider slickSlider--home js-slider-home">
            <?php
            $s = mysqli_query($link, "SELECT * FROM fotosslide WHERE id_elem=1 AND idioma='" . IDIOMA . "' ORDER BY orden ASC");
            while ($slide = mysqli_fetch_array($s)) { ?>
                <div class="slickSlider__item">
                    <div class="img-frame">
                        <img src="pics_fotosslide/1/<?php echo $slide['foto']; ?>" alt="<?php echo $slide['altimg']; ?>">
                    </div>
                    <a href="<?php echo $slide['link']; ?>">
                        
                    </a>
                </div>
            <?php }
            ?>
        </div>
        <div class="claim-container container-fluid px-md-0 pa-tl">
            <div class="claim">
                <h1 class="title h-display mt-0 mb-15"><?php echo HOME_SLIDER_H1; ?></h1>
                <p><?php echo HOME_SLIDER_TEXT; ?></p>
            </div>
        </div>
    </div>

    <section class="empresa-home" id="elproyecto">
        <div class="container">   

            <div class="row justify-content-center">
                <div class="col-lg-11 col-md-12 pt-125 pr px-lg-0 blueTop">
                    <div class="row mx-0">
                        <div class="col-lg-6 col-md-6 whiteB z-2 whiteBg px-0">
                            <h2 class="pb-25 h-display text-center">
                                <?php echo HOME_EMPRESA_TITLE; ?>
                            </h2>
                            <div class="text-center blueBg p-md-50 p-25">
                                <?php echo HOME_EMPRESA_TEXT_2; ?>
                            </div>

                        </div>
                        <div class="col-lg-6 px-md-0">
                            <div class="img-frame h-100 bottomBR">
                                <img src="/images/empresa-home.jpg" alt="<?php echo HOME_EMPRESA_ALT ;?>" class="img-fluid d-block mx-auto">
                            </div>
                        </div>
                        <!-- <div class="mxb-negative-1 col-lg-7 col-md-6 z-1 pr-md-0 pr-lg-20">
                            <div class="img-frame h-100 bottomBR">
                                <img src="/images/empresa-home.jpg" alt="<?php echo HOME_EMPRESA_ALT ;?>" class="img-fluid d-block mx-auto">
                            </div>
                        </div> -->
                    </div>
                </div>
                <!-- <div class="col-lg-11 col-md-12 blueBg p-lg-100 p-50">
                    <div class="row pt-xl-60 pt-lg-20 pt-30">
                        <div class="col-lg-4 offset-lg-2">
                            <h2 class="mt-0 text-lg-right text-center">
                                <?php echo HOME_EMPRESA_TITLE_2; ?>
                            </h2>
                        </div>
                    </div>
                    <div class="row justify-content-center pt-lg-25 pb-lg-25 pb-30" id="elproyecto">
                        <div class="col-lg-6 text-center">
                            <?php echo HOME_EMPRESA_TEXT_2; ?>
                        </div>
                    </div>

                </div> -->
            </div>

            <div class="sectionsList row justify-content-center pt-50 pt-md-100 px-lg-200">

                <div class="sectionsList__item col-lg-4 col-sm-6 col-6 py-15">
                    <div class="inner-item">
                        <div class="thumb-wrapper">
                            <div class="thumb d-flex justify-content-center">
                                <img src="/images/objetivos.svg" alt="<?php echo HOME_SECCIONES_OBJETIVOS_ALT; ?>">
                            </div>
                        </div>
                        <div class="content">
                            <div class="text-wrapper">
                                <h3 class="title text-center"><?php echo HOME_SECCIONES_OBJETIVOS; ?></h3>
                            </div>
                            <div class="button-wrapper text-center">
                                <a href="<?php echo $links['objetivos.php'] ?>" title="" class="btn btn-link"><?php echo COMMON_SABERMAS; ?></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="sectionsList__item col-lg-4 col-sm-6 col-6 py-15">
                    <div class="inner-item">
                        <div class="thumb-wrapper">
                            <div class="thumb d-flex justify-content-center">
                                <img src="/images/publicaciones.svg" alt="<?php echo HOME_SECCIONES_PUBLICACIONES_ALT; ?>">
                            </div>
                        </div>
                        <div class="content">
                            <div class="text-wrapper">
                                <h3 class="title text-center"><?php echo HOME_SECCIONES_PUBLICACIONES; ?></h3>
                            </div>
                            <div class="button-wrapper text-center">
                                <a href="<?php echo $links['listado_productos.php'] ?>" title="" class="btn btn-link"><?php echo COMMON_SABERMAS; ?></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="sectionsList__item col-lg-4 col-sm-6 col-6 py-15">
                    <div class="inner-item">
                        <div class="thumb-wrapper">
                            <div class="thumb d-flex justify-content-center">
                                <img src="/images/intervencion.svg" alt="<?php echo HOME_SECCIONES_INTERVENCION_ALT; ?>">
                            </div>
                        </div>
                        <div class="content">
                            <div class="text-wrapper">
                                <h3 class="title text-center"><?php echo HOME_SECCIONES_INTERVENCION; ?></h3>
                            </div>
                            <div class="button-wrapper text-center">
                                <a href="<?php echo $links['intervencion-crs.php'] ?>" title="" class="btn btn-link"><?php echo COMMON_SABERMAS; ?></a>
                            </div>
                        </div>
                    </div>
                </div>
              
                <div class="sectionsList__item col-lg-4 col-sm-6 col-6 py-15">
                    <div class="inner-item">
                        <div class="thumb-wrapper">
                            <div class="thumb d-flex justify-content-center">
                                <img src="/images/metodologia.svg" alt="<?php echo HOME_SECCIONES_METODOLOGIA_ALT; ?>">
                            </div>
                        </div>
                        <div class="content">
                            <div class="text-wrapper">
                                <h3 class="title text-center"><?php echo HOME_SECCIONES_METODOLOGIA; ?></h3>
                            </div>
                            <div class="button-wrapper text-center">
                                <a href="<?php echo $links['metodologia.php'] ?>" title="" class="btn btn-link"><?php echo COMMON_SABERMAS; ?></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="sectionsList__item col-lg-4 col-sm-6 col-6 py-15">
                    <div class="inner-item">
                        <div class="thumb-wrapper">
                            <div class="thumb d-flex justify-content-center">
                                <img src="/images/equipo.svg" alt="<?php echo HOME_SECCIONES_EQUIPO_ALT; ?>">
                            </div>
                        </div>
                        <div class="content">
                            <div class="text-wrapper">
                                <h3 class="title text-center"><?php echo HOME_SECCIONES_EQUIPO; ?></h3>
                            </div>
                            <div class="button-wrapper text-center">
                                <a href="<?php echo $links['team.php'] ?>" title="" class="btn btn-link"><?php echo COMMON_SABERMAS; ?></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="sectionsList__item col-lg-4 col-sm-6 col-6 py-15">
                    <div class="inner-item">
                        <div class="thumb-wrapper">
                            <div class="thumb d-flex justify-content-center">
                                <img src="/images/etica.svg" alt="<?php echo HOME_SECCIONES_ETICA_ALT; ?>">
                            </div>
                        </div>
                        <div class="content">
                            <div class="text-wrapper">
                                <h3 class="title text-center"><?php echo HOME_SECCIONES_ETICA; ?></h3>
                            </div>
                            <div class="button-wrapper text-center">
                                <a href="<?php echo $links['etica.php'] ?>" title="" class="btn btn-link"><?php echo COMMON_SABERMAS; ?></a>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </section>

    <section class="needHelp pb-0" id="aboutSuicide">
        <div class="container pr z-2">

            <div class="helpList row">
                <div class="helpList__item col-lg-6 block1  my-negative-bot">
                    <div class="inner-item p-lg-100 p-50">
                        <div class="thumb-wrapper">
                            <div class="thumb d-flex justify-content-center">
                                <img src="/images/conducta.svg" alt="<?php echo HOME_CONDUCTA_ALT; ?>">
                            </div>
                        </div>
                        <div class="content">
                            <div class="text-wrapper">
                                <h3 class="title text-center"><?php echo HOME_NEEDHELP_TITLE; ?></h3>
                                <div class="text text-center"><p><?php echo HOME_NEEDHELP_TEXT; ?></p></div>
                            </div>
                            <div class="button-wrapper text-center">
                                <a href="aboutSuicide.php" title="" class="btn btn-link"><?php echo COMMON_SABERMAS; ?></a>
                            </div>
                        </div>
                    </div>
                </div>
            
                <div class="helpList__item col-lg-6 block2 bottomBL">
                    <div class="inner-item p-lg-100 p-50">
                        <div class="thumb-wrapper">
                            <div class="thumb d-flex justify-content-center">
                                <img src="/images/prevencion.svg" alt="<?php echo HOME_NEEDHELP_ALT; ?>">
                            </div>  
                        </div>
                        <div class="content">
                            <div class="text-wrapper">
                                <h3 class="title text-center"><?php echo HOME_NEEDHELP_TITLE_2; ?></h3>
                                <div class="text text-center"><p><?php echo HOME_NEEDHELP_TEXT_2; ?></p></div>
                            </div>
                            <div class="button-wrapper text-center">
                                <a href="prevSuicide.php" title="" class="btn btn-link"><?php echo COMMON_SABERMAS; ?></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid hopeBg px-lg-100 px-50">
            <div class="row justify-content-center py-200">
                <div class="col-lg-6 text-center">
                    <h2 class="mt-0 mb-25"><?php echo HOME_NEEDHELP_TITLE_3; ?></h2>
                    <a href="<?php echo $links['recursos.php'] ?>" title="<?php echo HOME_NEEDHELP_TITLE ;?>" class="btn btn-primary"><?php echo COMMON_RECURSOS ;?></a>
                    <div class="contactos mt-50">
                        <p><?php echo HOME_NEEDHELP_112; ?></p>
                        <p><?php echo HOME_NEEDHELP_HOPE; ?></p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    

	<section class="home-news block3">
		<div class="container">

			<h2 class="mt-0 text-center"><?php echo HOME_NEWS_H2; ?></h2>

			<div class="newsList newsList--column row mt-50 justify-content-center">
				<?php
                //Configurar
                $table           = "noticias";
                $table_content   = $table . "_content";
                $hay_locationmap = 1; // 1 si hay mapa de localizacion
                $sufijo_plural   = "Noticias";
                $sufijo_singular = "Noticia";
                $campo_listar    = "t.text_fecha"; //t.campo si es de la tabla 'normal' y tc.campo si es de la tabla 'content'
                $orden_listar    = "DESC";
                $pinta_listar    = "text_titulo";
                $pint2_listar    = "textarea_intro";
                $cols            = "SHOW FULL COLUMNS FROM " . $DATABASE . "." . $table;
                $cols_content    = "SHOW FULL COLUMNS FROM " . $DATABASE . "." . $table_content;
                $limite          = " LIMIT 3";

                if (isset($_GET['id'])) {
                    $listar = "SELECT * FROM " . $table . " t LEFT JOIN " . $table_content . " tc ON (t.id=tc.id AND tc.idioma='" . IDIOMA . "') WHERE tc." . $pinta_listar . " != '' AND t.text_fecha <= CURDATE() AND t.checkbox_visible='1' AND t.select_categorias_productos = '" . (int) $_GET['id'] . "' ORDER BY " . $campo_listar . " " . $orden_listar . " " . $limite;
                } else {
                    $listar = "SELECT * FROM " . $table . " t LEFT JOIN " . $table_content . " tc ON (t.id=tc.id AND tc.idioma='" . IDIOMA . "') WHERE tc." . $pinta_listar . " != '' AND t.text_fecha <= CURDATE() AND t.checkbox_visible='1' ORDER BY " . $campo_listar . " " . $orden_listar . " " . $limite;
                }

                $l = db_query($link, $listar);
                while ($list = mysqli_fetch_array($l)) {
                    $title = $list[$pinta_listar];
                    $url   = urls_amigables($title);
                    $url   = $url . "-n-" . $list['id'] . "-" . IDIOMA; ?>

                    <article class="newsList__item col-md-4">
                        <div class="inner-item">

                            <div class="thumb-wrapper">
                                <div class="thumb img-frame">
                                    <a href="<?php echo $url; ?>" title="<?php echo $title; ?>">
                                    <?php
                                        //Foto
                                        $r        = db_query($link, "SELECT * FROM " . $table . " WHERE id=" . $list['id']);
                                        $registro = mysqli_fetch_array($r);
                                        if ($registro['file_foto'] != "") {
                                            echo '<img src="images/' . $table . '/th_crop_' . $registro['file_foto'] . '" alt="' . $title . '" />';
                                        } else {
                                            $list_cols = db_query($link, $cols);
                                            while ($lista = mysqli_fetch_array($list_cols)) {
                                                $field = $lista[0];
                                                if (substr($field, 0, 12) == "fotouploader") {
                                                    $tablafotos = substr($field, 13, strlen($field) - 13);
                                                    $ff         = db_query($link, "SELECT * FROM " . $tablafotos . " WHERE id_elem=" . $list['id'] . " AND orden=0 LIMIT 1");
                                                    $ffoto      = mysqli_fetch_array($ff);
                                                    if ($ffoto['foto'] != "") {
                                                        echo '<img src="pics_fotos' . $table . '/' . $list['id'] . '/flat_crop_' . $ffoto['foto'] . '" alt="' . $title . '" />';
                                                    }
                                                }
                                            }
                                        } ?>
                                    </a>
                                </div>
                            </div>

                            <div class="content">
                                <div class="text-wrapper">
                                    <h3 class="title"><a href="<?php echo $url; ?>" title="<?php echo $title; ?>"><?php echo $title; ?></a></h3>
                                    <div class="text"><?php echo $list[$pint2_listar]; ?></div>
                                </div>
                                <div class="date"><?php echo fechaEspExtAno($list['text_fecha']); ?></div>
                            </div>
                        </div>
                    </article>
                <?php }
                ?>
            </div>
            <div class="btn-wrapper text-center mt-50">
                <a href="<?php echo $links['listado_noticias.php'] ?>" title="<?php echo _TITLE ;?>" class="btn btn-primary"><?php echo COMMON_VERTODOS ;?></a>
            </div>
		</div>
    </section>

    <section class="home-logos py-25">
        <div class="container">

            <div class="row justify-content-center align-items-center carousel">
                <div class="item">
                    <img src="/images/parc_tauli.png" alt="<?php echo _ALT ;?>" class="img-fluid d-block mx-auto">
                </div>
                <div class="item">
                    <img src="/images/hospital-del-mar.png" alt="<?php echo _ALT ;?>" class="img-fluid d-block mx-auto">
                </div>
                <div class="item">
                    <img src="/images/agencia-qualitat.png" alt="<?php echo _ALT ;?>" class="img-fluid d-block mx-auto">
                </div>
                <div class="item">
                    <img src="/images/sant-pau.png" alt="<?php echo _ALT ;?>" class="img-fluid d-block mx-auto">
                </div>
                <div class="item">
                    <img src="/images/idiap.svg" alt="<?php echo _ALT ;?>" class="img-fluid d-block mx-auto">
                </div>
                <div class="item">
                    <img src="/images/IAS.svg" alt="<?php echo _ALT ;?>" class="img-fluid d-block mx-auto">
                </div>
                <div class="item">
                    <img src="/images/IMLCFC.svg" alt="<?php echo _ALT ;?>" class="img-fluid d-block mx-auto">
                </div>
                <div class="item">
                    <img src="/images/departament-salut.png" alt="<?php echo _ALT ;?>" class="img-fluid d-block mx-auto">
                </div>
                <div class="item">
                    <img src="/images/pere-mata.png" alt="<?php echo _ALT ;?>" class="img-fluid d-block mx-auto">
                </div>
            </div>
        </div>
    </section>

    


	<?php include "footer.php";?>

	<script src="js/slick.min.js"></script>
	<script>
		$(document).ready(function(){
			$('.js-slider-home').slick({
				slidesToShow: 1,
				slidesToScroll: 1,
				dots: false,
				arrows: true,
				nextArrow: '<button class="slick-next slick-arrow" aria-label="Next" type="button" style=""></button>',
				prevArrow: '<button class="slick-prev slick-arrow" aria-label="Prev" type="button" style=""></button>',
				infinite: true,
				speed: 300,
                autoplay: true,
                autoplaySpeed: 3500
            });
            $('.carousel').slick({
                infinite: false,
                speed: 300,
                arrows: false,
                slidesToShow: 6,
                slidesToScroll: 1,
                speed: 300,
                dots: false,
                infinite: true,
                autoplay: true,
                autoplaySpeed: 2500,
                responsive: [
                    {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 6,
                        slidesToScroll: 1,
                        infinite: true,
                    }
                    },
                    {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                    },
                    {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                    }
                    // You can unslick at a given breakpoint now by adding:
                    // settings: "unslick"
                    // instead of a settings object
                ]
                });
                        
                        });
	</script>
</body>
</html>
<?php include "bottom.php";?>
