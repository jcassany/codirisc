<?php
include("head.php");

//Configurar
$table = "publicaciones";
$table_content = $table."_content";
$hay_locationmap = 1; // 1 si hay mapa de localizacion
$sufijo_plural = "Publicaciones";
$sufijo_singular = "Publicacion";
$campo_listar = "t.orden"; //t.campo si es de la tabla 'normal' y tc.campo si es de la tabla 'content'
$orden_listar = "ASC";
$pinta_listar = "text_titulo";
$pint2_listar = "textarea_intro";
$seo_url 		 = "text_ht_url";
$seo_title 		 = "text_ht_title";
$seo_description = "notiny_ht_description";
$cols = "SHOW FULL COLUMNS FROM ".$DATABASE.".".$table;
$cols_content = "SHOW FULL COLUMNS FROM ".$DATABASE.".".$table_content;

// Paginacion
$este_fichero = $_SERVER['PHP_SELF'];
$porpagina = CONFIG_PORPAGINA;
if ((isset($_GET["pag"])) && (!isset($_POST["pagina"]))) $pagina= $_GET["pag"];
if ($pagina<=0) $pagina=1;
$limite = " LIMIT ".(($pagina-1)*$porpagina).",".$porpagina;

// Consulta per saber el total de projectes que compleixen
$sql = "SELECT COUNT(*) FROM ".$table." t LEFT JOIN ".$table_content." tc ON (t.id=tc.id AND tc.idioma='".IDIOMA."') WHERE tc.".$pinta_listar." != '' AND t.checkbox_visible='1'";
$resultpagina = db_query($link,$sql);
$linepagina = mysqli_fetch_array($resultpagina);
$total_buscats_paginas = $linepagina["COUNT(*)"];

//Busco nom categoria
if(isset($_GET['id'])){
	$current_category = (int)$_GET['id'];
	$cate = db_query($link,"SELECT * FROM categorias_publicaciones_content WHERE idioma='".IDIOMA."' AND id=".$current_category);
	$category = mysqli_fetch_array($cate);
	$catNombre = ": ".$category['text_nombre'];

	$ht_title = ($category[$seo_title]) ? $category[$seo_title]: html_entity_decode(strip_tags($category['text_nombre']), ENT_COMPAT, 'UTF-8');
	$ht_description = ($category[$seo_description]) ? $category[$seo_description]: html_entity_decode(strip_tags($category['textarea_description']), ENT_COMPAT, 'UTF-8');
} else{
	$catNombre = "";
	$ht_title = PRODUCTOS_METAS_TIT;
	$ht_description = PRODUCTOS_METAS_DESC;
}

/* === Redireccion en caso de que no exista en la base de datos === */
if (isset($_GET['id']) && $category == null) {
	header("Location: 404.php", true, 404);
	require_once('./404.php');
} else {

	include("auto_functions.php");
	?>
	<!DOCTYPE html>
	<html lang="<?php echo IDIOMA;?>">
	<head><?php include("header.php");?></head>
	<body>
		<?php include("body.php");?>


		<?php
			$filter_ver = "true";
				if ($_GET['ver_articulos'] == "on") {
					$filter_ver .= " && t.select_categorias_publicaciones = '1'";
				}
				if ($_GET['ver_congresos'] == "on") {
					$filter_ver .= " && t.select_categorias_publicaciones = '2'";
				}
				if ($_GET['ver_articulos'] == "on" && $_GET['ver_congresos'] == "on") {
					$filter_ver = "true";
				}

			$filter_archivo = "true";
				if (!$_GET['archivo_2019'] == "on" && !$_GET['archivo_2018'] == "on" && !$_GET['archivo_2017'] == "on" && !$_GET['archivo_2016'] == "on") {
					$filter_archivo = "true";
				} else {
					$filter_archivo = "false";
					if ($_GET['archivo_2019'] == "on") {
						$filter_archivo .= " || t.text_fecha LIKE '2019-%'";
					}
					if ($_GET['archivo_2018'] == "on") {
						$filter_archivo .= " || t.text_fecha LIKE '2018-%'";
					}
					if ($_GET['archivo_2017'] == "on") {
						$filter_archivo .= " || t.text_fecha LIKE '2017-%'";
					}
					if ($_GET['archivo_2016'] == "on") {
						$filter_archivo .= " || t.text_fecha LIKE '2016-%'";
					}
				}

			$filter_key = "tc.text_titulo LIKE '%".$_GET['key']."%'";
			$filter_basic = " tc.".$pinta_listar." != '' AND t.checkbox_visible='1'";
			$filter_extra = "ORDER BY ".$campo_listar." ".$orden_listar." ".$limite;
			$l = db_query_default($link, $table, $where = "(".$filter_ver.") AND (".$filter_archivo.") AND (".$filter_key.") AND (".$filter_basic.") " . $filter_extra);
			?>

				<section class="marmol-header pt-0">
					<div class="container-fluid px-md-0">						
						<div class="row violetBg">
							<div class="col-lg-8 col-md-6 whiteB z-2 align-self-center">
								<h1 class="text-lg-right text-center lightblueBg pr-lg-30 py-25"><?php echo PUBLICACIONES_TITULO ;?></h1>
							</div>
							<div class="col-lg-6 col-md-8 z-1 pr-md-0 ml-negative align-self-xs-center align-self-md-start">
								<div class="bottomBLL violetBg"></div>
							</div>
						</div>

					</div>
				</section>

				<section class="pt-0 pt-md-30">
					<div class="container">
						
						<div class="row">

							<div class="col-lg-2 col-10 offset-1 offset-lg-0 filters-publicaciones">

								<div class="row justify-content-center">
									<ul class="list-unstyled first-level">
										<li class="first-li">
											<span class="filter-title toggle pl-10 mb-15 pb-10 borderB <?php if ($filter_ver != 'true') { echo 'open'; } ?>">
												<?php echo FILTER_1_TITLE;?>
											</span>

											<ul class="second-level pl-10 <?php if ($filter_ver != 'true') { echo 'open'; } ?>">
												<li>
													<div class="form-group ml-5">
														<label for="Form_ver_articulos" class="custom-control custom-checkbox">
															<input type="checkbox" name="Form_ver_articulos" onclick="form_check(this.id)" id="Form_ver_articulos" class="custom-control-input" required required <?php if($_GET['ver_articulos']){ echo "checked"; }?>>
															<div class="custom-control-indicator"></div>
															<div class="custom-control-description">
																<?php echo FILTER_ARTICULOS;?>
															</div>
														</label>
													</div>
												</li>
												
												<li>
													<div class="form-group ml-5">
														<label for="Form_ver_congresos" class="custom-control custom-checkbox">
															<input type="checkbox" name="Form_ver_congresos" onclick="form_check(this.id)" id="Form_ver_congresos" class="custom-control-input" required required <?php if($_GET['ver_congresos']){ echo "checked"; }?>>
															<div class="custom-control-indicator"></div>
															<div class="custom-control-description">
																<?php echo FILTER_CONGRESOS;?>
															</div>
														</label>
													</div>
												</li>
											</ul>
										</li>
										<li class="first-li">
											<span class="filter-title toggle pl-10 mb-15 pb-10 borderB <?php if ($filter_archivo != 'true') { echo 'open'; } ?>">
												<?php echo FILTER_2_TITLE;?>
											</span>

											<ul class="second-level pl-10 <?php if ($filter_archivo != 'true') { echo 'open'; } ?>">
												<li>
													<div class="form-group ml-5">
														<label for="Form_archivo_2019" class="custom-control custom-checkbox">
															<input type="checkbox" name="Form_archivo_2019" onclick="form_check(this.id)" id="Form_archivo_2019" class="custom-control-input" required required <?php if($_GET['archivo_2019']){ echo "checked"; }?>>
															<div class="custom-control-indicator"></div>
															<div class="custom-control-description">
																<?php echo FILTER_2019;?>
															</div>
														</label>
													</div>
												</li>
												
												<li>
													<div class="form-group ml-5">
														<label for="Form_archivo_2018" class="custom-control custom-checkbox">
															<input type="checkbox" name="Form_archivo_2018" onclick="form_check(this.id)" id="Form_archivo_2018" class="custom-control-input" required required <?php if($_GET['archivo_2018']){ echo "checked"; }?>>
															<div class="custom-control-indicator"></div>
															<div class="custom-control-description">
																<?php echo FILTER_2018;?>
															</div>
														</label>
													</div>
												</li>

												<li>
													<div class="form-group ml-5">
														<label for="Form_archivo_2017" class="custom-control custom-checkbox">
															<input type="checkbox" name="Form_archivo_2017" onclick="form_check(this.id)" id="Form_archivo_2017" class="custom-control-input" required required <?php if($_GET['archivo_2017']){ echo "checked"; }?>>
															<div class="custom-control-indicator"></div>
															<div class="custom-control-description">
																<?php echo FILTER_2017;?>
															</div>
														</label>
													</div>
												</li>

												<li>
													<div class="form-group ml-5">
														<label for="Form_archivo_2016" class="custom-control custom-checkbox">
															<input type="checkbox" name="Form_archivo_2016" onclick="form_check(this.id)" id="Form_archivo_2016" class="custom-control-input" required required <?php if($_GET['archivo_2016']){ echo "checked"; }?>>
															<div class="custom-control-indicator"></div>
															<div class="custom-control-description">
																<?php echo FILTER_2016;?>
															</div>
														</label>
													</div>
												</li>
											</ul>
										</li>
									</ul>
								</div>

							</div>

							<div class="col-lg-9 offset-md-1 ">
								<div class="row">
									<div class="col-lg-4 offset-lg-2 mb-50">
										<div id="search" class="search">
											<form action="<?php echo $links['buscador.php']; ?>" method="GET" id="form_data">
												<input name="key" id="key" type="text" value="" placeholder="<?php echo SEARCH_PLACEHOLDER ?>" />
												<input name="submit_search" id="submit_search" type="submit" value="" placeholder="" class="btn btn-primary">
												<div id="autocomplete" class="autocomplete"></div>

												<input type="checkbox" name="ver_articulos" id="ver_articulos" class="d-none"
												<?php if($_GET['ver_articulos']){ echo "checked"; }?>>
												<input type="checkbox" name="ver_congresos" id="ver_congresos" class="d-none"
												<?php if($_GET['ver_congresos']){ echo "checked"; }?>>

												<input type="checkbox" name="archivo_2019" id="archivo_2019" class="d-none"
												<?php if($_GET['archivo_2019']){ echo "checked"; }?>>
												<input type="checkbox" name="archivo_2018" id="archivo_2018" class="d-none"
												<?php if($_GET['archivo_2018']){ echo "checked"; }?>>
												<input type="checkbox" name="archivo_2017" id="archivo_2017" class="d-none"
												<?php if($_GET['archivo_2017']){ echo "checked"; }?>>
												<input type="checkbox" name="archivo_2016" id="archivo_2016" class="d-none"
												<?php if($_GET['archivo_2016']){ echo "checked"; }?>>

												<script type="text/javascript">
													function form_check(id){
														$value = false;
														if (document.getElementById(id).checked) { $value = true; }
														document.getElementById(id.replace("Form_", "")).checked = $value;
														document.getElementById('form_data').submit();
													}
												</script>
											</form>
										</div>
									</div>
								</div>
								<div class="productsList productsList--column row justify-content-center">
								<?php
								if (mysqli_num_rows($l) > 0) {
								while($list = mysqli_fetch_array($l)){
									$title = $list[$pinta_listar];
									$url = $url = url_element($list, $title, 'p', IDIOMA);
									?>

									<article class="productsList__item col-md-12 col-11 pb-25 pb-md-0">
										<div class="inner-item">
											

											<?php $categoria = mysqli_fetch_array(db_query_default($link, 'categorias_publicaciones', $where = "t.id = '".$list['select_categorias_publicaciones']."'")); ?>
													<?php
													$r = db_query($link,"SELECT * FROM ".$table." WHERE id=".$list['id']);
													$registro = mysqli_fetch_array($r);
													if($registro['file_foto'] != ""){ ?>
														<div class="thumb-wrapper color_<?php echo $categoria['id']; ?>">
														<div class="thumb">
														<a href="<?php echo $list['text_link']; ?>" target="_blank" title="<?php echo $title; ?>">
														<img src="images/<?php echo $table; ?>/<?php echo $registro['file_foto']; ?>" alt="<?php echo $title; ?>" class="img-fluid mx-auto d-block"/>
													<?php } else{ ?>
														<div class="thumb-wrapper w-0 pl-10 color_<?php echo $categoria['id']; ?>">
														<div class="thumb">
													<?php } ?>
													
													</a>
												</div>
											</div>

											<div class="content">
												<div class="text-wrapper">
													
													<div class="date">
													<?php
														$categoria = mysqli_fetch_array(db_query_default($link, 'categorias_publicaciones', $where = "t.id = '".$list['select_categorias_publicaciones']."'")); ?>
														<span class="text-uppercase"> <?php echo $categoria['text_nombre'];?></span>
													<?php	if($categoria['text_nombre']){
															echo '<span class="mx-5">|</span>';
														}
													?>
													<?php echo fechaEspExtAno($list['text_fecha']); ?>
													</div>
													<h3 class="title"><a href="<?php echo $list['text_link']; ?>" target="_blank" title="<?php echo $title; ?>"><?php echo $title; ?></a></h3>
													<div class="text"><?php echo $list['textarea_investigadores']; ?></div>
												</div>

												<div class="button-wrapper">
													<a href="<?php echo $list['text_link']; ?>" target="_blank" title="<?php echo $title; ?>" class="btn btn-link text-uppercase color_<?php echo $categoria['id']; ?>"><?php echo COMMON_ACCEDERWEB; ?></a>
												</div>
											</div>
										</div>
									</article>
								<?php } ?>
							<?php } else {
								echo COMMON_NO_RESULTS;
							} ?>
							</div>

							<?php $total_paginas = ceil($total_buscats_paginas/$porpagina);
							if($total_paginas > 1){
								$url = parse_url($_SERVER["REQUEST_URI"], PHP_URL_PATH)."?";
								paginacion($total_paginas,$pagina,$url);
							} ?>
							</div>
						</div>


						
					</div>
				</section>

		<?php include("footer.php");?>
	</body>
	</html>
	<?php include("bottom.php");
}
?>