<?php
header("HTTP/1.0 404 Not Found");
unset($_GET['lang']);
include_once "head.php";

$ht_title = PAGE404_METAS_TIT;
$ht_description = htmlspecialchars(PAGE404_METAS_DESC, ENT_QUOTES, 'UTF-8');

?>
<!DOCTYPE html>
<html lang="<?php echo IDIOMA;?>">

<head><?php include_once("header.php");?></head>

<body>
	<?php include_once("body.php");?>

	<section class="block3">
		<div class="container">
			<div class="row">
				<div class="col-10 offset-1 col-md-6 offset-md-3 text-center">
					<h1 class="mt-0 mb-50"><?php echo PAGE404_H1  ;?></h1>
					<?php echo PAGE404_TEXTO ;?>
				</div>
			</div>
		</div>
	</section>	

	<section class="needHelp py-0" id="aboutSuicide">
        <div class="container-fluid hopeBg">
            <div class="row justify-content-center py-200">
                <div class="col-lg-6 text-center">
                    <h2 class="mt-0 mb-25"><?php echo HOME_NEEDHELP_TITLE_3; ?></h2>
                    <a href="<?php echo $links['recursos.php'] ?>" title="<?php echo HOME_NEEDHELP_TITLE ;?>" class="btn btn-primary"><?php echo COMMON_RECURSOS ;?></a>
                    <div class="contactos mt-50">
                        <p><?php echo HOME_NEEDHELP_112; ?></p>
                        <p><?php echo HOME_NEEDHELP_HOPE; ?></p>
                    </div>
                </div>
            </div>
        </div>
    </section>

	<?php include_once("footer.php");?>
</body>

</html>
<?php include_once("bottom.php");?>
