<?php
include("head.php");
$ht_title = EMPRESA_METAS_TIT;
$ht_description = htmlspecialchars(EMPRESA_METAS_DESC, ENT_QUOTES, 'UTF-8');
?>
<!DOCTYPE html>
<html lang="<?php echo IDIOMA;?>">
<head><?php include("header.php");?></head>
<body>
	<?php include("body.php");?>

	<section>
		<div class="container">
			<h1 class="mt-0"><?php echo EMPRESA_H1;?></h1>
			<?php echo EMPRESA_TEXT;?>
		</div>
	</section>

	<?php include("footer.php");?>
</body>
</html>
<?php include("bottom.php");?>
