<?php
include("head.php");
$ht_title = COOKIES_METAS_TIT;
$ht_description = htmlspecialchars(COOKIES_METAS_DESC, ENT_QUOTES, 'UTF-8');
?>
<!DOCTYPE html>
<html lang="<?php echo IDIOMA;?>">
<head><?php include("header.php");?></head>
<body>
	<?php include("body.php");?>

	<section class="container">
		<div class="row justify-content-center">
			<div class="col-lg-7">

				<h1 class="mt-0"><?php echo COOKIES_H1;?></h1>

				<?php
				$paginaCookies = COOKIES_TEXT;
				$paginaCookies = str_replace("{CONFIG_MAILTO}",CONFIG_MAILTO,$paginaCookies);
				echo $paginaCookies;
				?>
			</div>
		</div>
	</section>

	<?php include("footer.php");?>
</body>
</html>
<?php include("bottom.php");?>
