<?php
include("head.php");
$ht_title = INTERVENCIONCRS_METAS_TIT;
$ht_description = htmlspecialchars(INTERVENCIONCRS_METAS_DESC, ENT_QUOTES, 'UTF-8');
?>
<!DOCTYPE html>
<html lang="<?php echo IDIOMA;?>">
<head><?php include("header.php");?></head>
<body>
	<?php include("body.php");?>

	<section class="marmol-header pt-0">
		<div class="container-fluid px-md-0">						
			<div class="row violetBg">
				<div class="col-lg-8 col-md-6 whiteB z-2 align-self-center">
					<h1 class="text-lg-right text-center lightblueBg pr-lg-30 py-25"><?php echo INTERVENCIONCRS_H1  ;?></h1>
				</div>
				<div class="col-lg-6 col-md-8 z-1 pr-md-0 ml-negative align-self-xs-center align-self-md-start">
					<div class="bottomBLL violetBg"></div>
				</div>
			</div>

		</div>
	</section>

	
	<section class="prevSuicide-body">
		<div class="container">
	
			<div class="row justify-content-center">
				<div class="col-lg-7">
					<?php echo INTERVENCIONCRS_TEXT ;?>
				</div>
			</div>
			<div class="row justify-content-center mt-50">
				<div class="col-lg-7">
					<img src="/images/infografiaCRS.jpg" alt="<?php echo _ALT ;?>" class="img-fluid d-block mx-auto">
				</div>
			</div>
		</div>
	</section>

	<?php include("footer.php");?>
</body>
</html>
<?php include("bottom.php");?>
