<form action="send.php" class="form" method="post" enctype="multipart/form-data">

	<input type="hidden" name="form" id="form" value="form" />
	<input type="hidden" name="url" id="url" value="contacto-ok.php" />
	<input type="hidden" name="subject" id="subject" value="<?php echo COMMON_MAILAUTORESPSUBJET.' '.CONFIG_BRAND; ?>" />
	
	<div class="form-group">
		<label for="Name"><?php echo COMMON_LABELNOMBRE; ?>*</label>
		<input type="text" class="form-control" name="Nombre" type="text" id="Nombre" required>
	</div>

	<div class="form-row">
		<div class="form-group col-12 col-md-6">
			<label for="Telefono"><?php echo COMMON_LABELTELEFONO; ?>*</label>
			<input type="text" class="form-control" name="Telefono" type="tel" id="Telefono" required>
		</div>
		<div class="form-group col-12 col-md-6">
			<label for="Email"><?php echo COMMON_LABELMAIL; ?>*</label>
			<input type="email" class="form-control" name="Email" type="email" id="Email" required>
		</div>
	</div>

	<div class="form-group">
		<label for="Comentarios"><?php echo COMMON_LABELCOMENTARIOS; ?>*</label>
		<textarea class="form-control" name="Comentarios" id="Comentarios" required></textarea>
	</div>

	<small class="d-block my-25">
		<?php
		$paginaLegal = COMMON_COMERCIALINFO_TEXT;
		$paginaLegal = str_replace("{CONFIG_MAILTO}",CONFIG_MAILTO,$paginaLegal);
		$paginaLegal = str_replace("{CONFIG_CLIENTE}",CONFIG_CLIENTE,$paginaLegal);
		$paginaLegal = str_replace("{CONFIG_NIF}",CONFIG_NIF,$paginaLegal);
		$paginaLegal = str_replace("{CONFIG_DIRECCION}",CONFIG_DIRECCION,$paginaLegal);
		$paginaLegal = str_replace("{CONFIG_PHONE}",CONFIG_PHONE,$paginaLegal);
		echo $paginaLegal;
		echo COMMON_AMPLIARPRIVACIDAD_TEXT;
		?>
		<a href="<?php echo $links['politica-privacidad.php'];?>" title="<?php echo COMMON_PRIVACIDAD;?>"><?php echo COMMON_PRIVACIDAD;?></a>
	</small>

	<div class="form-group">
		<label for="termsConditions" class="custom-control custom-checkbox">
			<input type="checkbox" name="termsConditions" id="termsConditions" class="custom-control-input" required>
			<span class="custom-control-indicator"></span>
			<span class="custom-control-description">
				<?php echo COMMON_ACEPTOTERMINOS;?>
				<a href="<?php echo $links['politica-privacidad.php']; ?>"><?php echo COMMON_PRIVACIDAD; ?></a>
			</span>
		</label>
	</div>

	<div id="errores"></div>
	<div class="text-center mt-25 mt-lg-50">
		<input type="hidden" name="iso_idioma" id="iso_idioma" value="<?php echo IDIOMA;?>" />
		<button type="submit" name="button" id="button" class="btn btn-primary mb-15 w-33"><?php echo COMMON_ENVIAR; ?></button>
	</div>

</form>