<?php 
include("head.php");
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ES">
<head>
<title>Backoffice</title>
<meta name="keywords" content="" />
<meta name="description" content="" />

<?php 
// CONFIGURAR
$table = "mails";
//$max_fotos['fotosnoticias'] = 25; //Máximo de fotos que puede tener la cosa del uploader
//$max_fotos['ficherosnoticias'] = 25;
//$hay_fotouploader = 1; // 1 si hay fotouploader y/o fileuploader
//$hay_locationmap = 1; // 1 si hay mapa de localizacion
$no_borrar = 01; // 1 si no se pueden borrar items
$es_blog = ""; //Vacio si no se suben fotos dentro del texto, 'jbimages' si se puede hacer
//$ordenable = 0; //1 si se pueden ordenar los items
$sufijo_plural = "mails";
$sufijo_singular = "mail";
$campo_listar = "t.fecha"; //t.campo si es de la tabla 'normal' y tc.campo si es de la tabla 'content'
$orden_listar = "DESC";
$pinta_listar = "text_titulo";
// END CONFIGURAR

include("header.php");?>
</head>

<body>
<?php include("body.php");?>

<!-- Content -->
<div class="section-header">
	<h1>Gestión de Mails</h1>
</div>

<form method="post" action="">
	<input type="text" name="search" id="search" value="<?php echo $_SESSION['search'];?>" placeholder="Buscar email" />
	<input type="submit" name="buscar" value="<?php echo $backoffice['btn_buscar'][$_SESSION['bo_idioma']];?>" class="busca">
</form>

<style type="text/css">
	.error {
		background-color: grey;
		color: white;
	}
</style>
<table>
	<tr>
		<td>Fecha</td><td>Email</td><td>Mail</td>
	</tr>
<?php

	// ***
	// Paginacion
	// ***
	$porpagina = 10;
	if ((isset($_GET["pag"])) && (!isset($_POST["pagina"]))) $pagina= $_GET["pag"];
	if (!isset($pagina)) $pagina=1;
	$limite = " LIMIT ".(($pagina-1)*$porpagina).",".$porpagina;
	// Consulta per saber el total de projectes que compleixen
	$search = ($_POST['search']) ? "text_email LIKE '%".$_POST['search']."%'" : "true" ;
	$sql = "SELECT COUNT(*) FROM ".$table." WHERE ".$search." ORDER BY fecha " . $orden_listar;
	$resultpagina = db_query($link,$sql);
	$linepagina = mysqli_fetch_array($resultpagina);
	$total_buscats_paginas = $linepagina["COUNT(*)"];
	// ***
	// END Paginacion
	// ***


	$sql = "SELECT * FROM ".$table." WHERE ".$search." ORDER BY fecha " . $orden_listar . $limite;
	$pagos = db_query($link,$sql);
	while($f = mysqli_fetch_array($pagos)){
		?>
			<tr class="<?php echo $class = ($f['error']) ? 'error' : '' ; ?>">
				<td><?php echo $f['fecha']; ?></td>
				<td><?php echo $f['text_email']; ?></td>
				<td><?php echo $f['text_body']; ?></td>
			</tr>
		<?php
	}
?>
</table>

<?php
	$total_paginas = ceil($total_buscats_paginas/$porpagina);
    if($total_paginas > 1){
        $url = parse_url($_SERVER["REQUEST_URI"], PHP_URL_PATH)."?";
        paginacion($total_paginas,$pagina,$url);
    }
?>

<!-- End Content -->

<?php include("footer.php");?>
</body>
</html>
<?php include("bottom.php");?>