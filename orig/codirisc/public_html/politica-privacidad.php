<?php
include("head.php");
$ht_title = PRIVACIDAD_METAS_TIT;
$ht_description = htmlspecialchars(PRIVACIDAD_METAS_DESC, ENT_QUOTES, 'UTF-8');
?>
<!DOCTYPE html>
<html lang="<?php echo IDIOMA;?>">
<head><?php include("header.php");?></head>
<body>

	<?php include("body.php");?>

	<section class="container">
		<div class="row justify-content-center">
			<div class="col-lg-7">
				<h1 class="mt-0"><?php echo PRIVACIDAD_H1;?></h1>

				<?php
				$paginaLegal = PRIVACIDAD_TEXT;
				$paginaLegal = str_replace("{CONFIG_CLIENTE}",CONFIG_CLIENTE,$paginaLegal);
				$paginaLegal = str_replace("{CONFIG_NIF}",CONFIG_NIF,$paginaLegal);
				$paginaLegal = str_replace("{CONFIG_DIRECCION}",CONFIG_DIRECCION,$paginaLegal);
				$paginaLegal = str_replace("{CONFIG_PHONE}",CONFIG_PHONE,$paginaLegal);
				$paginaLegal = str_replace("{CONFIG_MAILTO}",CONFIG_MAILTO,$paginaLegal);
				echo $paginaLegal;
				?>
			</div>
		</div>
	</section>

	<?php include("footer.php");?>
</body>
</html>
<?php include("bottom.php");?>
