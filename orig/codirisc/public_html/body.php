
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=<?php echo TAG_MANAGER_CODE ;?>"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>

<?php include('cookies.php'); ?>

<header>
	<div class="container">
		<div id="logo">
			<a href="<?php echo $links['index.php'];?>" title=""><img src="images/logo.svg" alt=""></a>
		</div>
	</div>

	<div class="mainMenu">
		<div class="container">
			<div onclick="" class="langMenu langMenu--select">
				<span class="current"><?php echo IDIOMA;?></span>
				<?php $i = db_query($link,"SELECT * FROM config_idiomes ORDER BY id_idioma ASC");
				while($idiomas = mysqli_fetch_array($i)){
					$b = db_query($link,"SELECT * FROM textos_content WHERE idioma='".$idiomas['nom_idioma']."' AND id IN(SELECT id FROM textos WHERE texT_nodisabled = 'index.php' )");
					$busca = mysqli_fetch_array($b);
					if($idiomas['nom_idioma'] != IDIOMA){
						echo '<a href="'.$busca['textarea_titulo'].'">'.strtoupper($idiomas['nom_idioma']).'</a>';
					}
				} ?>
			</div>
		</div>

		<div class="container">
			<nav class="mainMenu__nav">
				<ul class="first-level">
					<?php include('nav.php'); ?>
				</ul>
			</nav>
		</div>

		<!-- <div class="container">
			<div class="socialMenu">
				<?php include('social.php') ?>
			</div>
		</div> -->
	</div>

	<div class="menuBurger"><span class="menuBurger__icon"></span></div>
</header>

<div class="generalContent">
