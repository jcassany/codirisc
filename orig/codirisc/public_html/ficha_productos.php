<?php
include("head.php");

//Configurar
$table = "publicaciones";
$table_content = $table."_content";
$sufijo_plural = "Publicaciones";
$sufijo_singular = "Publicacion";
$campo_listar = "t.orden"; //t.campo si es de la tabla 'normal' y tc.campo si es de la tabla 'content'
$orden_listar = "ASC";
$pinta_listar = "text_titulo";
$pint2_listar = "textarea_intro";
$seo_url 		 = "text_ht_url";
$seo_title 		 = "text_ht_title";
$seo_description = "notiny_ht_description";
$cols = "SHOW FULL COLUMNS FROM ".$DATABASE.".".$table;
$cols_content = "SHOW FULL COLUMNS FROM ".$DATABASE.".".$table_content;
//End configurar

$id = $_GET['id'];
$_SESSION['id'] = $id;
$e = db_query($link,"SELECT * FROM ".$table." t LEFT JOIN ".$table_content." tc ON (t.id=tc.id AND tc.idioma='".IDIOMA."') WHERE t.id='".$id."'");
$element = mysqli_fetch_array($e);

if (false) { // Show $array data
	showVPHP($element);}

/* === Redireccion en caso de que no exista en la base de datos === */
if (is_null($element)) {
	header("Location: 404.php", true, 404);
	require_once('./404.php');
} else {

	$ht_title = ($element[$seo_title] != "") ? $element[$seo_title]: html_entity_decode(strip_tags($element[$pinta_listar]), ENT_COMPAT, 'UTF-8');
	$ht_description = ($element[$seo_description] != "") ? $element[$seo_description]: html_entity_decode(strip_tags($element[$pint2_listar]), ENT_COMPAT, 'UTF-8');
	$listado_imagenes = mysqli_query($link,"SELECT * FROM fotospublicaciones WHERE id_elem = '".$id."' ORDER BY orden ASC");

	if(mysqli_num_rows($listado_imagenes)>0){
		$row_image = mysqli_fetch_array($listado_imagenes);
		$og_image = 'pics_fotos'.$table.'/'.$id.'/'.$row_image['foto'].'';
	}

	//Busco nom categoria
	if((int)$element['select_categorias_publicaciones']>0){
		$current_category = (int)$element['select_categorias_publicaciones'];
		$cate = db_query($link,"SELECT * FROM categorias_publicaciones_content WHERE idioma='".IDIOMA."' AND id=".(int)$element['select_categorias_publicaciones']);
		$category = mysqli_fetch_array($cate);
		$catNombre = ": ".$category['text_nombre'];
	} else{
		$catNombre = "";
	}

	include("auto_functions.php");
	?>
	<!DOCTYPE html>
	<html lang="<?php echo IDIOMA;?>">
	<head>
		<?php if ($element['checkbox_visible'] == 0) { ?>
		<meta name="robots" content="noindex">
		<?php }
		include("header.php");?></head>
	<body>
		<?php include("body.php");?>

		<main>
			<div class="container">
				<article class="productsheet">
					<div class="row">
						<div class="col-lg-6">
							<div class="slick-slider" id="slider">
								<?php
								$tablafotos = 'fotospublicaciones';
								$ff = db_query($link,"SELECT * FROM ".$tablafotos." WHERE id_elem=".$element['id']." ORDER BY orden ASC");
								while ($ffoto = mysqli_fetch_array($ff)) {
									if ($ffoto['foto'] != "") {
										echo '<img src="pics_fotospublicaciones/' . $element['id'] . '/big_crop_' . $ffoto['foto'] . '" alt="' . $element['text_titulo'] . '" />';
									}
								}
								?>
							</div>

							<?php if(mysqli_num_rows(db_query($link,"SELECT * FROM ".$tablafotos." WHERE id_elem=".$element['id']." ORDER BY orden ASC"))>1){ ?>
							<div class="slick-slider mb-50" id="slider-nav">
								<?php
								$tablafotos = 'fotospublicaciones';
								$ff = db_query($link,"SELECT * FROM ".$tablafotos." WHERE id_elem=".$element['id']." ORDER BY orden ASC");
								while ($ffoto = mysqli_fetch_array($ff)) {
									if ($ffoto['foto'] != "") {
										echo '<img src="pics_fotospublicaciones/' . $element['id'] . '/th_crop_' . $ffoto['foto'] . '" alt="' . $element['text_titulo'] . '" class="item-nav" />';
									}
								}
								?>
							</div>
							<?php } ?>

							<?php if(isset($element['notiny_video']) && $element['notiny_video'] != ''){ ?>
								<div class="attached_video">
									<div class="embed-responsive embed-responsive-16by9">
										<?php pinta('notiny_video'); ?>
									</div>
								</div>
							<?php } ?>
						</div>

						<div class="col-lg-6">
							<h1 class="mt-0"><?php pinta('text_titulo');?></h1>
							<div class="attached_siglephoto"><?php pinta('file_foto');?></div>
							<div class="bodysheet"><?php pinta('textarea_texto');?></div>
							<div class="fileuploader"><?php pinta('fileuploader_ficherospublicaciones', IDIOMA);?></div>
						</div>
					</div>
				</article>
			</div>
		</main>

		<?php include("footer.php");?>

		<script src="js/slick.min.js"></script>
		<script>
			$(function(){
				$('#slider').slick({
					slidesToShow: 1,
					slidesToScroll: 1,
					arrows: false,
					dots: false
				});
				$('#slider-nav').slick({
					slide: '.item-nav',
					asNavFor: '#slider',
					slidesToShow: 6,
					slidesToScroll: 1,
					arrows: false,
					dots: false,
					focusOnSelect: true
				});
			});
		</script>
	</body>
	</html>
	<?php include("bottom.php");
}
?>