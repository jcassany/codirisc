<?php
include("head.php");
$ht_title = CONTACTO_METAS_TIT;
$ht_description = htmlspecialchars(CONTACTO_METAS_DESC, ENT_QUOTES, 'UTF-8');
?>
<!DOCTYPE html>
<html lang="<?php echo IDIOMA;?>">

<head><?php include("header.php");?></head>
<body class="contacto-page">
	<?php include("body.php");?>

	<section class="marmol-header pt-0">
		<div class="container-fluid px-md-0">						
			<div class="row violetBg">
				<div class="col-lg-8 col-md-6 whiteB z-2 align-self-end">
					<h1 class="text-lg-right text-center lightblueBg pr-lg-30 py-25"><?php echo CONTACTO_H1 ;?></h1>
				</div>
				<div class="col-lg-6 col-md-8 z-1 pr-md-0 ml-negative align-self-xs-center align-self-md-start">
					<div class="bottomBLL violetBg"></div>
				</div>
			</div>
		</div>
	</section>


	<section class="pr pt-0">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-lg-5">
					<h2 class="mt-75 mb-30"><?php echo CONTACTO_TEXTO  ;?></h2>

					<?php require_once('contact-form.php'); ?>
				</div>
			</div>
		</div>
		
	</section>

	<?php include("footer.php");?>
</body>
</html>
<?php include("bottom.php");?>
