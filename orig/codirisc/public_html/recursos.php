<?php
include("head.php");
$ht_title = RECURSOS_METAS_TIT;
$ht_description = htmlspecialchars(RECURSOS_METAS_DESC, ENT_QUOTES, 'UTF-8');
?>
<!DOCTYPE html>
<html lang="<?php echo IDIOMA;?>">
<head><?php include("header.php");?></head>
<body>
	<?php include("body.php");?>

	<section class="marmol-header pt-0">
        <div class="container-fluid px-md-0">						
            <div class="row violetBg">
                <div class="col-lg-8 col-md-6 whiteB z-2 align-self-center">
                    <h1 class="text-lg-right text-center lightblueBg pr-lg-30 py-25"><?php echo RECURSOS_H1 ;?></h1>
                </div>
                <div class="col-lg-6 col-md-8 z-1 pr-md-0 ml-negative align-self-xs-center align-self-md-start">
					<div class="bottomBLL violetBg"></div>
                </div>
            </div>
        </div>
    </section>

	<section class="recursos-body pt-0 pt-sm-50 pt-md-0">
		<div class="container">
	
			<div class="row justify-content-center">
				<div class="contactos mt-50 col-xl-4 col-md-5 text-center">
                    <p><?php echo HOME_NEEDHELP_112; ?></<p>
                    <p><?php echo HOME_NEEDHELP_HOPE; ?></p>
				</div>
			</div>
			<h3 class="text-center bottomLine pt-25"><?php echo RECURSOS_CENTROS; ?></h3>
			<div class="row justify-content-center">
				<a href="http://drogues.gencat.cat/ca/ciutadania/busqueu_ajuda/on_os_podeu_tractar/" class="accede mt-50 col-lg-8 p-md-50 px-md-100 px-50 py-25" target="_blank">
                   <?php echo RECURSOS_ACCEDE;?>
				</a>
			</div>
			<h3 class="text-center bottomLine pt-25"><?php echo RECURSOS_ASOS; ?></h3>

			<div class="accesosList row justify-content-center">
				<?php
					$asociaciones = db_query_default($link, "asociaciones", "t.checkbox_visible = 1");
					while ($asociacion = mysqli_fetch_array($asociaciones)) { ?>
						<div class="accesosList__item col-lg-10 d-sm-inline-flex align-items-center">						
							<div class="text-wrapper w-sm-66 w-100">
								<h3 class="title"><?= $asociacion['text_titulo'] ?></h3>
								<div class="text"><p><?= $asociacion['textarea_texto'] ?></p></div>
							</div>
							<div class="button-wrapper w-sm-33 w-100 text-sm-right">
								<a href="<?= $asociacion['text_link'] ?>" target="_blank" title="" class="btn btn-primary"><?php echo COMMON_ACCEDER; ?></a>
							</div>						
						</div>
					<?php }
				?>
			</div>
		</div>
	</section>

	<?php include("footer.php");?>
</body>
</html>
<?php include("bottom.php");?>
