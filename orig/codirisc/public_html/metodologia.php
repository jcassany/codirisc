<?php
include("head.php");
$ht_title = METODOLOGIA_METAS_TIT;
$ht_description = htmlspecialchars(METODOLOGIA_METAS_DESC, ENT_QUOTES, 'UTF-8');
?>
<!DOCTYPE html>
<html lang="<?php echo IDIOMA;?>">
<head><?php include("header.php");?></head>
<body>
	<?php include("body.php");?>

    <section class="marmol-header pt-0">
        <div class="container-fluid px-md-0">						
            <div class="row violetBg">
                <div class="col-lg-8 col-md-6 whiteB z-2 align-self-center">
                    <h1 class="text-lg-right text-center lightblueBg pr-lg-30 py-25"><?php echo METODOLOGIA_H1 ;?></h1>
                </div>
                <div class="col-lg-6 col-md-8 z-1 pr-md-0 ml-negative align-self-xs-center align-self-md-start">
                    <div class="bottomBLL violetBg"></div>
                </div>
            </div>
        </div>
    </section>

	
	<section class="prevSuicide-body pb-lg-200">
		<div class="container">

			<div class="metodologyList row">

                <a class="metodologyList__item col-lg-6 block1  bottomBRotate "  href="<?php echo $links['metodologia-epidemiologico.php'];?>">
                    <div class="inner-item p-lg-100 p-50">
                        <div class="thumb-wrapper">
                            <div class="thumb d-flex justify-content-center">
                                <img src="/images/conducta.svg" alt="">
                            </div>
                        </div>
                        <div class="content">
                            <div class="text-wrapper">
                                <h3 class="title text-center"><?php echo METODOLOGIA_EPIDEMOLOGICO_TITLE; ?></h3>
                                <div class="text text-center"><p><?php echo METODOLOGIA_EPIDEMOLOGICO_TEXT_INTRO; ?></p></div>
                            </div>
                        </div>
                    </div>
				</a>

                <a class="metodologyList__item col-lg-6 block2 my-negative-bot" href="<?php echo $links['metodologia-clinico.php'];?>">
                    <div class="inner-item p-lg-100 p-50">
                        <div class="thumb-wrapper">
                            <div class="thumb d-flex justify-content-center">
                                <img src="/images/prevencion.svg" alt="">
                            </div>  
                        </div>
                        <div class="content">
                            <div class="text-wrapper">
                                <h3 class="title text-center"><?php echo METODOLOGIA_CLINICO_TITLE; ?></h3>
                                <div class="text text-center"><p><?php echo METODOLOGIA_CLINICO_TEXT_INTRO; ?></p></div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
	
			
		</div>
	</section>

	<?php include("footer.php");?>
</body>
</html>
<?php include("bottom.php");?>
