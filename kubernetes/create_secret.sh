kubectl create secret generic git-creds -n codirisc \
    --from-file=ssh=deploy \
    --from-file=known_hosts=deploy.hosts
